//
//  PramocodeCell.swift
//  iTowu
//
//  Created by iroid on 19/10/21.
//

import UIKit

class PramocodeCell: UITableViewCell {

    @IBOutlet weak var lessLabel: UILabel!
    @IBOutlet weak var promoCodeLabel: UILabel!
    
    var onCopyPromocode: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item: CouponListResponse?{
        didSet{
            self.promoCodeLabel.text = item?.code
            self.lessLabel.text = "Get up to \(item?.percentage ?? 0)% less"
        }
    }
    
    @IBAction func onCopy(_ sender: Any) {
        self.onCopyPromocode?()
    }
}
