//
//  ModeTableViewCell.swift
//  iTowu
//
//  Created by iroid on 11/03/22.
//

import UIKit

class ModeTableViewCell: UITableViewCell {

    @IBOutlet weak var selectImageView: dateSportImageView!
    @IBOutlet weak var modeTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
