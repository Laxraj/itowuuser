//
//  MenuOptionCell.swift
//  iTowu
//
//  Created by iroid on 16/10/21.
//

import UIKit

class MenuOptionCell: UITableViewCell {

    @IBOutlet weak var optionImageView: UIImageView!
    @IBOutlet weak var optionTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item:OnMenuOptionRequest?{
        didSet{
            self.optionImageView.image = UIImage(named: item?.image ?? "")
            self.optionTitleLabel.text = item?.title
        }
    }
}
