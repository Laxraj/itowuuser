//
//  CardTableViewCell.swift
//  iTowu
//
//  Created by iroid on 17/10/21.
//

import UIKit

class CardTableViewCell: UITableViewCell {

    @IBOutlet weak var cardView: dateSportView!
    @IBOutlet weak var addCardButtonView: dateSportView!
    
    @IBOutlet weak var cardTypeImageView: UIImageView!
    @IBOutlet weak var accountNumberLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var expireLabel: UILabel!
    
    
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    
    var onEdit:(()->Void)?
    var onDelete:(()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onEdit(_ sender: UIButton) {
        self.onEdit!()
    }
    
    @IBAction func onDelete(_ sender: UIButton) {
        self.onDelete!()
    }
    var item:CardResponse?{
        didSet{
            self.accountNumberLabel.text  = "**** **** **** " + "\(item?.card_number ?? "")"
            self.nameLabel.text = item?.cardHolderName
            self.expireLabel.text = item?.expiration_date
            self.cardView.borderColor = item?.isDefault == 1 ? Utility.getUIcolorfromHex(hex: "41EAD4") : .clear
        }
    }
}
