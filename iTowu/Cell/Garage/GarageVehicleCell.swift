//
//  GarageVehicleCell.swift
//  iTowu
//
//  Created by Nikunj on 17/10/21.
//

import UIKit

class GarageVehicleCell: UITableViewCell {

    @IBOutlet weak var createdByAdminLabel: UILabel!
    @IBOutlet weak var vehicleNameLabel: UILabel!
    @IBOutlet weak var numberPlateLabel: UILabel!
    @IBOutlet weak var mainView: dateSportView!
    @IBOutlet weak var garageView: dateSportView!
    @IBOutlet weak var vehicleImageView: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.numberPlateLabel.textColor = Utility.checkLightModeUserDefalt() ? #colorLiteral(red: 0.01960784314, green: 0.0862745098, blue: 0.1490196078, alpha: 0.4):#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item: VehicleListResponse?{
        didSet{
            self.vehicleNameLabel.text = "\(item?.make ?? "") "+"\(item?.model ?? "")" //item?.model
            self.numberPlateLabel.text = item?.plate
            Utility.setImage(item?.image, imageView: vehicleImageView)
            
            self.createdByAdminLabel.isHidden = !(item?.is_added_by_feat_admin ?? false)
        }
    }
    
}
