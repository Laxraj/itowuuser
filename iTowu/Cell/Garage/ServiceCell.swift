//
//  ServiceCell.swift
//  iTowu
//
//  Created by Nikunj on 17/10/21.
//

import UIKit

class ServiceCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var mainView: dateSportView!
    
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imageViewWidth: NSLayoutConstraint!
    
    @IBOutlet weak var newImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var item: ServiceResponse?{
        didSet{
            self.label.text = item?.name
            Utility.setImage(Utility.checkLightModeUserDefalt() ? item?.image:item?.dark_image, imageView: self.imageView)
            if item?.name == "Auto Transport"{
//                self.imageViewHeight.constant = 60
                self.imageViewWidth.constant = self.bounds.width * 0.72
            }else{
                self.imageViewWidth.constant = self.bounds.width * 0.4
            }
            self.newImageView.isHidden = item?.isNewService == false
        }
    }
}
