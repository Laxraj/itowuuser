//
//  ServiceQuestionCell.swift
//  iTowu
//
//  Created by Nikunj on 17/10/21.
//

import UIKit
import IQKeyboardManagerSwift
import DropDown

class ServiceQuestionCell: UITableViewCell {
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var noButton: dateSportButton!
    @IBOutlet weak var yesButton: dateSportButton!
    @IBOutlet weak var answerView: dateSportView!
    @IBOutlet weak var answerTextField: UITextField!
    @IBOutlet weak var otherTextView: dateSportView!
    
    @IBOutlet weak var inputTextView: IQTextView!
    
    @IBOutlet weak var optionsTableView: UITableView!
    
    @IBOutlet weak var optionTableViewHeightConstraint: NSLayoutConstraint!
    
    var onNoQuestion: (() -> Void)?
    var onYesQuestion: (() -> Void)?
    var onDropDown: (() -> Void)?
    var changeUserInputText: ((String?) -> Void)?
    var itemArray: [QuestionAnswer] = []
    var selectedAnswer: (([QuestionAnswer]) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.inputTextView.delegate = self
        self.optionsTableView.delegate = self
        self.optionsTableView.dataSource = self
        self.optionsTableView.register(UINib(nibName: "WindowOptionsCell", bundle: Bundle.main), forCellReuseIdentifier: "WindowOptionsCell")
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.otherTextView.isHidden = true
        self.optionsTableView.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onNo(_ sender: Any) {
        self.onNoQuestion?()
    }
    
    @IBAction func onYes(_ sender: Any) {
        self.onYesQuestion?()
    }
    
    @IBAction func onSelectOption(_ sender: Any) {
        self.onDropDown?()
    }
    
    var item: Questions?{
        didSet{
            self.questionLabel.text = item?.question
            self.answerTextField.text = item?.selectedAnswer
            
            if let obj = item?.answers?.first(where: {$0.answer == self.item?.selectedAnswer}){
                if obj.isUserInputData == true{
                    self.otherTextView.isHidden = false
                    self.inputTextView.text = obj.userInputText
                }else{
                    self.inputTextView.text = nil
                    self.otherTextView.isHidden = true
                }
            }else{
                self.inputTextView.text = nil
                self.otherTextView.isHidden = true
            }
            
            self.optionTableViewHeightConstraint.constant = self.getTableViewHeight(count: (item?.answers ?? []).count)

            if item?.isPercentRange == true{
                self.answerView.isHidden = true
                self.optionsTableView.isHidden = false
                
                self.optionTableViewHeightConstraint.constant = self.getTableViewHeight(count: self.itemArray.count)
                self.optionsTableView.reloadData()
            }else{
                self.answerView.isHidden = false
                self.optionsTableView.isHidden = true
            }
        }
    }
    
    func getTableViewHeight(count: Int) -> CGFloat{
        return CGFloat(71 * count)
    }
    
    //MARK:- DROP DOWN
    func displayDropDown(view: UIView,indexPath: IndexPath){
        let arr = ["None","10","20","30","40","50","60","70","80","90","100"]
        let dropDown = DropDown()
        dropDown.anchorView = view.superview
        dropDown.dataSource = arr.map({ $0 == "None" ? "\($0)" : "\($0)"+"%"})
        dropDown.direction = .bottom
        dropDown.width = view.bounds.width
        dropDown.bottomOffset = CGPoint(x: view.frame.minX, y: 65)
        dropDown.backgroundColor = .white
        dropDown.cornerRadius = 15
        dropDown.show()
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            let option = index == 0 ? nil : "\(arr[index])"
            if indexPath.row == 0{
                self?.itemArray.forEach({$0.selectedWindowAnswer = option})
            }else{
                self?.itemArray[indexPath.row].selectedWindowAnswer = option
            }
            self?.selectedAnswer?(self?.itemArray ?? [])
            self?.optionsTableView.reloadData()
            dropDown.hide()
        }
    }
}
//MARK: - TEXTVIEW DELEGATE
extension ServiceQuestionCell: UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars <= 2000    // 10 Limit Value
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.changeUserInputText?(textView.text.trimmingCharacters(in: .whitespacesAndNewlines))
    }
}

//MARK: - TABLEVIEW DELEGATE AND DATASOURCE
extension ServiceQuestionCell: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.optionsTableView.dequeueReusableCell(withIdentifier: "WindowOptionsCell", for: indexPath) as! WindowOptionsCell
        cell.item = self.itemArray[indexPath.row]
        cell.onSelectAnswer = { [weak self] in
            self?.displayDropDown(view: cell.answerView, indexPath: indexPath)
        }
        return cell
    }
}
