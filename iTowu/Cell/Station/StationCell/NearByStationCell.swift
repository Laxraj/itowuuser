//
//  NearByStationCell.swift
//  iTowu
//
//  Created by Nikunj Vaghela on 17/06/23.
//

import UIKit

class NearByStationCell: UICollectionViewCell {

    @IBOutlet weak var stationImageMainView: dateSportView!
    
    @IBOutlet weak var stationImageView: dateSportImageView!
    
    @IBOutlet weak var stationNameLabel: UILabel!
    
    @IBOutlet weak var stationImgView: UIImageView!
    
    var isGas: Bool = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var item: NearByGasStationList?{
        didSet{
            self.stationImgView.image = UIImage(named: self.isGas ? "gas_station_icon" : "electric_station_icon")
            if item?.image == nil || item?.image?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
                self.stationImageMainView.isHidden = true
            }else{
                self.stationImageMainView.isHidden = false
            }
            Utility.setImage(item?.image, imageView: self.stationImageView)
            let string = "\(item?.name ?? "") \(String(format: "%.2f Mile", item?.distance ?? 0.0))"
            let attributedString = NSMutableAttributedString(string: string)
            attributedString.addAttributes([NSAttributedString.Key.font : UIFont(name: "LexendDeca-Regular", size: 12)!], range: (string as NSString).range(of: string))
            attributedString.addAttributes([NSAttributedString.Key.foregroundColor: UIColor(named: "app_black_color")!], range: (string as NSString).range(of: "\(item?.name ?? "")"))
            attributedString.addAttributes([NSAttributedString.Key.foregroundColor: Utility.getUIcolorfromHex(hex: "00C85E")], range: (string as NSString).range(of: "\(String(format: "%.2f Mile", item?.distance ?? 0.0))"))
            self.stationNameLabel.attributedText = attributedString
        }
    }
}
