//
//  AdvertisementMainCell.swift
//  iTowu
//
//  Created by Nikunj Vaghela on 08/07/23.
//

import UIKit
import CenteredCollectionView

class AdvertisementMainCell: UICollectionViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var itemArray: [AdvertisementList] = []
    
    var centeredCollectionViewFlowLayout: CenteredCollectionViewFlowLayout!
    
    var changeIndex: ((Int) -> Void)?
        
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initializeDetails()
        // Initialization code
    }

    func initializeDetails(){
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(UINib(nibName: "AdvertisementListCell", bundle: Bundle.main), forCellWithReuseIdentifier: "AdvertisementListCell")
        
        centeredCollectionViewFlowLayout = (self.collectionView.collectionViewLayout as! CenteredCollectionViewFlowLayout)
        
        // Modify the collectionView's decelerationRate (REQURED)
        collectionView.decelerationRate = UIScrollView.DecelerationRate.fast
        
        // Make the example pretty ✨
        
        // Assign delegate and data source
        collectionView.delegate = self
        collectionView.dataSource = self
        
        // Configure the required item size (REQURED)
        let width = (screenWidth * 315) / 375

        centeredCollectionViewFlowLayout.itemSize = CGSize(width: width, height: (width * 100) / 375)
        
        // Configure the optional inter item spacing (OPTIONAL)
        centeredCollectionViewFlowLayout.minimumLineSpacing = 10
    }
}
//MARK: COLLECTIONVIEW DELEGATE AND DATASOURCE
extension AdvertisementMainCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.itemArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "AdvertisementListCell", for: indexPath) as! AdvertisementListCell
        cell.item = self.itemArray[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (screenWidth * 315) / 375
        return CGSize(width: width, height: (width * 100) / 375)
    }
}
//MARK:- SCROLLVIEW DELEGATE
extension AdvertisementMainCell{
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let index = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        self.changeIndex?(self.centeredCollectionViewFlowLayout.currentCenteredPage ?? 0)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let index = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        self.changeIndex?(self.centeredCollectionViewFlowLayout.currentCenteredPage ?? 0)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        let index = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        self.changeIndex?(self.centeredCollectionViewFlowLayout.currentCenteredPage ?? 0)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let index = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        self.changeIndex?(self.centeredCollectionViewFlowLayout.currentCenteredPage ?? 0)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        let index = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        self.changeIndex?(self.centeredCollectionViewFlowLayout.currentCenteredPage ?? 0)
    }
}
