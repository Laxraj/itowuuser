//
//  AdvertisementListCell.swift
//  iTowu
//
//  Created by Nikunj Vaghela on 08/07/23.
//

import UIKit

class AdvertisementListCell: UICollectionViewCell {

    @IBOutlet weak var mainView: dateSportView!
    
    @IBOutlet weak var shopImageView: dateSportImageView!
    
    @IBOutlet weak var shopNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var addressNameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        self.mainView.layer.masksToBounds = true
        self.setGradientBackground()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setGradientBackground()
    }

    func setGradientBackground() {
        self.mainView.layer.sublayers?.filter({$0 is CAGradientLayer}).forEach({$0.removeFromSuperlayer()})
        let colorTop =  Utility.getUIcolorfromHex(hex: "#E1FBF0")
        let colorBottom = UIColor(red: 255.0/255.0, green: 94.0/255.0, blue: 58.0/255.0, alpha: 1.0).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop,colorTop, colorBottom]
        gradientLayer.locations = [0.0,0.5, 1.0]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
        
        let width = (screenWidth * 315) / 375
        gradientLayer.frame = CGRect(x: 0, y: 0, width: width, height: (width * 100) / 375)
                
        self.mainView.layer.insertSublayer(gradientLayer, at:0)
    }
    
    var item: AdvertisementList?{
        didSet{
            Utility.setImage(item?.image, imageView: self.shopImageView)
            self.shopNameLabel.text = item?.shop_name
            self.descriptionLabel.text = item?.description
            self.addressNameLabel.text = item?.address
            self.phoneLabel.text = item?.phone
            self.backgroundImageView.image = item?.gradientImage
        }
    }
    
    @IBAction func onLocation(_ sender: Any) {
        guard let controller = UIApplication.topViewController() else {
            return
        }
        FirebaseanalyticsManager.share.logEvent(event: .shopNearbyAddressClick)

        OpenMapDirections.present(in: controller, sourceView: self, fromLat: currentLatitude, fromLong: currentLongitude, toLat: item?.latitude ?? "", toLong: item?.longitude ?? "")

    }
    
    @IBAction func onPhone(_ sender: Any) {
        guard let phone = item?.phone else {
            return
        }
        FirebaseanalyticsManager.share.logEvent(event: .shopNearbyPhonesClick)

        if let url = URL(string: "tel://\(phone)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
}
