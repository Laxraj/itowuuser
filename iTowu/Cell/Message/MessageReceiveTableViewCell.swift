//
//  MessageReceiveTableViewCell.swift
//  iTowuDriver
//
//  Created by iroid on 08/11/21.
//

import UIKit

class MessageReceiveTableViewCell: UITableViewCell {

    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.messageView.clipsToBounds = true
        self.messageView.layer.cornerRadius = 10
        self.messageView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner, .layerMaxXMaxYCorner]
        
        if Utility.checkLightModeUserDefalt(){
            self.messageView.layer.borderColor = UIColor.clear.cgColor
        }else{
            self.messageView.layer.borderColor = Utility.getUIcolorfromHex(hex: "ffffff").cgColor
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
    func setData(data:MessageData){
        self.messageLabel.text = data.message
        self.timeLabel.text = Utility.UTCToLocal(serverDate: data.createdAt ?? 0, getTimeFormate: "h:mm a")
        self.nameLabel.text = data.senderName
//        UTCToLocalForChat(timeStamp: data.time, toFormat: "hh:mma")
//        timeLabel.text = data.time
    }
    
}
