//
//  MessageSenderTableViewCell.swift
//  iTowuDriver
//
//  Created by iroid on 07/11/21.
//

import UIKit

class MessageSenderTableViewCell: UITableViewCell {

    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.messageView.clipsToBounds = true
        self.messageView.layer.cornerRadius = 10
        self.messageView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner, .layerMinXMaxYCorner]
        if Utility.checkLightModeUserDefalt(){
            self.messageView.layer.borderColor = UIColor.clear.cgColor
        }else{
            self.messageView.layer.borderColor = Utility.getUIcolorfromHex(hex: "ffffff").cgColor
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(data:MessageData){
        messageLabel.text = data.message
        timeLabel.text = Utility.UTCToLocal(serverDate: data.createdAt ?? 0, getTimeFormate: "h:mm a")
//        UTCToLocalForChat(timeStamp: data.time, toFormat: "hh:mma")
//        timeLabel.text = data.time
    }
    
}
extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
