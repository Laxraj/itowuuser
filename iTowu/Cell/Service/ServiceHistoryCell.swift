//
//  ServiceHistoryCell.swift
//  iTowu
//
//  Created by iroid on 17/10/21.
//

import UIKit
import MapKit
class ServiceHistoryCell: UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var serviceLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    
    //MARK: Variable
    var locationManager: CLLocationManager!
    var onDelete : (() -> ()) = {}

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         if let mapView = self.mapView
         {
             mapView.delegate = self
         }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item:FirebaseAcceptedServiceResponse?{
        didSet{
            self.priceLabel.text =  "$"+String(format: "%.2f", item?.price ?? 0)
//            self.priceLabel.text = "$\(item?.price ?? 0)"
            self.serviceLabel.text = "\(item?.service?.name ?? "") service"
            if let scheduleDate = item?.scheduleDate,scheduleDate != 0{
                self.timeLabel.text = Utility.UTCToLocal(serverDate: scheduleDate, getTimeFormate: "h:mm a")
            }else{
                self.timeLabel.text = Utility.UTCToLocal(serverDate: item?.createdAt ?? 0, getTimeFormate: "h:mm a")
            }
            self.dateLabel.text = Utility.UTCToLocal(serverDate: item?.createdAt ?? 0, getTimeFormate: "MMMM dd, YYYY")
            self.mapViewSetup()
        }
    }
    
    
    @IBAction func onDelete(_ sender: UIButton) {
        self.onDelete()
    }
    
    //MARK:- removeRoute
    func removeRoute(){
        let overlays = mapView.overlays
        self.mapView.removeAnnotations(self.mapView.annotations)
        mapView.removeOverlays(overlays)
    }
    
    // MARK: MapView Setup
    func mapViewSetup(){
        removeRoute()
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.showsUserLocation = false
        
        if let coor = mapView.userLocation.location?.coordinate{
            mapView.setCenter(coor, animated: true)
        }
        
        let fromLat = Double(item?.pickupLatitude ?? 0)
        let fromLong = Double(item?.pickupLongitude ?? 0)
        
        let toLat = Double(item?.driver?.latitude ?? 0)
        let toLong = Double(item?.driver?.longitude ?? 0)
    
        
        if let annotations = self.mapView?.annotations {
            for _annotation in annotations {
                if let annotation = _annotation as? MKAnnotation
                {
                      print(annotation.title)
                      self.mapView.removeAnnotation(annotation)
                }
            }
        }
        
        let newPin1 = MKPointAnnotation()
        
        let location1 = CLLocationCoordinate2D(latitude: fromLat , longitude: fromLong )
        newPin1.coordinate = location1
        
        let pin1 = CustomPin(pinTitle: item?.user?.firstName ?? "", Location: location1, storeImage: item?.user?.profile ?? "", pinId: item?.user?.userId ?? 0)
        self.mapView.addAnnotation(pin1)
        
        let newPin2 = MKPointAnnotation()
        let location2 = CLLocationCoordinate2D(latitude: toLat , longitude: toLong )
        newPin2.coordinate = location2
        let pin2 = CustomPin(pinTitle: item?.driver?.firstName ?? "", Location: location2, storeImage: item?.driver?.profile ?? "", pinId: item?.driver?.driverId ?? 0)
        self.mapView.addAnnotation(pin2)
       
        self.mapView.showAnnotations(self.mapView.annotations, animated: true)
        let myLocation = CLLocation(latitude: location1.latitude, longitude: location1.longitude)
        self.requestETA(userCLLocation: myLocation, coordinate: location2)
//        mapView.fitAllAnnotations()
    }
}
//MARK: MKMapViewDelegate and CLLocationManagerDelegate Delegate Methods
extension ServiceHistoryCell: CLLocationManagerDelegate, MKMapViewDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //        if let location = locations.last{
        //            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        //            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        //            self.mapView.setRegion(region, animated: true)
        //        }
    }
    
    // MARK: Set Pin On MapView
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        if annotation is MKUserLocation {
            return nil
        }
        let reuseID = "Location"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseID)
            annotationView?.canShowCallout = true
            let pin = MKAnnotationView(annotation: annotation,
                                       reuseIdentifier: reuseID)
            
            let pinData = annotation as? CustomPin
            
            if pinData?.pinId == -1{
                pin.image = UIImage(named: "currant_location_pin")
            }else{
                pin.isEnabled = true
                pin.canShowCallout = true
                pin.image = UIImage(named: "driver_map_pin")
                let storeImageView = UIImageView()
                storeImageView.frame = CGRect(x: 6.5, y: 7, width: 27, height: 27)
                storeImageView.layer.cornerRadius = storeImageView.frame.height/2
                storeImageView.layer.masksToBounds = true
//                storeImageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//                storeImageView.layer.borderWidth = 2
                
                let label = UILabel(frame: CGRect(x: pin.center.x, y: pin.center.y, width: 25, height: 25))
                label.textAlignment = .center
                label.textColor = .white
                label.font = label.font.withSize(14)
               
                //            label.text = cpa.pinId
                Utility.setImage(pinData?.storeImage, imageView: storeImageView)
                pin.addSubview(storeImageView)
                
            }
            annotationView = pin
            annotationView?.annotation = annotation
        } else {
            let pin = MKAnnotationView(annotation: annotation,
                                       reuseIdentifier: reuseID)
            let pinData = annotation as? CustomPin
            
            if pinData?.pinId == -1{
                pin.image = UIImage(named: "currant_location_pin")
            }else{
                pin.isEnabled = true
                pin.canShowCallout = true
                pin.image = UIImage(named: "driver_map_pin")
                let storeImageView = UIImageView()
                storeImageView.frame = CGRect(x: 6.5, y: 7, width: 27, height: 27)
                storeImageView.layer.cornerRadius = storeImageView.frame.height/2
                storeImageView.layer.masksToBounds = true
//                storeImageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//                storeImageView.layer.borderWidth = 2
                
                let label = UILabel(frame: CGRect(x: pin.center.x, y: pin.center.y, width: 25, height: 25))
                label.textAlignment = .center
                label.textColor = .white
                label.font = label.font.withSize(14)
               
                //            label.text = cpa.pinId
                Utility.setImage(pinData?.storeImage, imageView: storeImageView)
                pin.addSubview(storeImageView)
                
            }
            annotationView = pin
            annotationView?.annotation = annotation
        }
        return annotationView
    }
    
    
    func requestETA(userCLLocation: CLLocation, coordinate: CLLocationCoordinate2D) {
        let request = MKDirections.Request()
        
        let mapItem1 = MKMapItem(placemark: MKPlacemark(coordinate: userCLLocation.coordinate))
        let mapItem2 = MKMapItem(placemark: MKPlacemark(coordinate: coordinate))
        
        request.source = mapItem1
        request.destination = mapItem2
        let directions = MKDirections(request: request)
        
        var travelTime: String?
        directions.calculate { response, error in
            if let route = response?.routes.first {
                travelTime = String(route.expectedTravelTime/60)
                self.mapView.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)
            }
        }
    }
    
//    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay)
//        -> MKOverlayRenderer {
//            let renderer = MKPolylineRenderer(overlay: overlay)
//            renderer.strokeColor = UIColor.blue
//            renderer.lineWidth = 5.0
//
//            return renderer
//    }
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        renderer.lineWidth = 5.0
        return renderer
    }
}
extension MKMapView {
     func fitAllAnnotations() {
          var zoomRect = MKMapRect.null;
          for annotation in annotations {
               let annotationPoint = MKMapPoint(annotation.coordinate)
               let pointRect = MKMapRect(x: annotationPoint.x, y: annotationPoint.y, width: 0.1, height: 0.1);
               zoomRect = zoomRect.union(pointRect);
          }
          setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 50, left: 50, bottom: 50, right: 50), animated: true)
     }
}
