//
//  WebServiceURLs.swift
//  iTowu
//
//  Created by Nikunj Vaghela on 03/11/24.
//

import Foundation

//production
let server_url = "https://itowu.com/api/v1/"
let server_v2_url = "https://itowu.com/api/v2/"
let socket_url = "http://18.224.97.107:7001/"

//development
//let server_url = "https://dev.itowu.com/api/v1/"
//let server_v2_url = "https://dev.itowu.com/api/v2/"
//let socket_url = "http://18.224.97.107:7001"

//stageing and production mate
//MARK:- REGISTER
let registerURL = server_url+"register"
let loginURL = server_url+"login"
let changePasswordURL = server_url+"change/password"
let registerFotPushURL = server_url+"save-for-push"
let socialLoginURL = server_url+"social/login"
let resetPasswordSendEmailURL = server_url+"forget/password"
let verifyPhoneOrEmailURL = server_url+"forget/password/check/otp"
let registerChangePhoneURL = server_url+"changePhone"

let changeEmailURL = server_url+"change-email"
let verifyEmailURL =  server_url+"verify-email"

let changePhoneURL = server_url+"change-phone"
let verifyPhoneURL =  server_url+"verify-phone"

let verifyPhoneOrEmailLoginURL = server_url+"verify-user"

let resendOtpURL = server_url+"resend-otp"

let forgetPasswordURL = server_url+"forget/password/update"
let referralCodeURL = server_url+"user/referral/code"
let appVersiobURL = server_url+"appversion"
let logOutURL = server_url+"logout"
let deleteAccountURL = server_url+"user/delete/account"

//MARK:- SERVICE
let serviceURL     = server_url+"service"
let checkOrderURL  = server_url+"order/check"
let checkOrderStatusURL = server_url+"order/check/status"
let serviceListURL = server_url+"order?page="
let fuelPriceURL   = server_url+"rapidapi-fuel?state="

//MARK:- PAYMENT
let cardListURL = server_url+"card/list"
let addUpdateCardURL = server_url+"add/update/card"
let deleteCardURL = server_url+"card/delete/"
let servicePaymentURL = server_url+"payment"
let rattingURL = server_url+"rating"
let defaultCardURL = server_url+"card/default/"

//MARK:- REVIEW DRIVER

//MARK:- COMMON
let vehicleMakeURL = server_url+"vehicle-make"
let vehicleYearURL = server_url+"vehicle-year"
let vehicleColorURL = server_url+"vehicle-color"

//MARK:- USER
let updateProfileURL = server_url+"user/update/profile"

//MARK:- ORDER
let orderURL = server_url+"order"
let orderCheckURL = server_url+"order/check"
let cancelOrderURL = server_url+"order/cancle"
let cancelPriceURL = server_url+"order/cancle/details"
let orderDetailURL = server_url+"order/"
let driverOrderRequestURL = server_url+"order/driver/status"
let confirmOrderURL = server_url+"order/confirm"
let orderCalculatePriceURL = server_v2_url+"order/calculate-price"
let orderV2URL = server_v2_url+"order"

//MARK:- VEHICLE
let vehicleURL = server_url+"vehicle"

//MARK:- COUPON
let couponListURL = server_url+"coupon"
let checkCouponURL = server_url+"order/check-coupon"

//MARK:- CHAT
let chatDetailUrl = server_url+"messages/"
let messageCountUrl = server_url+"chatcount"

let appVersion = server_url+"appversion"

//MARK:- EXTRA CHARGES
let extraChargesURL = server_url+"order/accept-reject-extra-charge"

//MARK: - USER GARAGE
let getGarageVehicleDetailURL = server_url+"user/garage-vehicle-detail"
let addUpdateVehicleURL = server_url+"user/add-update-vehicle"
let addManualVehicleURL = server_url+"user/add-manual-vehicle"
let garageVehicleListURL = server_url+"user/garage/list"
let deleteVehicleURL = server_url+"user/garage"

//MARK: ADDITIONAL CHANGES
let nearByGasStationURL = server_url+"nearby/gas-station"
let nearByChargeStationURL = server_url+"nearby/charge-station"
let advertisementURL = server_url+"advertisements"

//MARK: NOTIFICATION
let notificationURL = server_url+"user/notifications"

//MARK: - US STATE CITY
let usStateURL = server_url+"us-states"

//MARK: - ADMIN JOIN
let adminJoinURL = server_url+"feat-admin/join"

//MARK: - SUBSCRIPTION
let subscriptionURL    = server_url+"subscribe"
let checkSubscriptionURL    = server_url+"check-subscription"

