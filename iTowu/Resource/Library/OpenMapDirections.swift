//
//  OpenMapDirections.swift
//  iTowu
//
//  Created by Nikunj Vaghela on 17/06/23.
//

import CoreLocation
import MapKit
import UIKit

class OpenMapDirections {
    // If you are calling the coordinate from a Model, don't forgot to pass it in the function parenthesis.
    static func present(in viewController: UIViewController, sourceView: UIView,fromLat: String,fromLong: String,toLat: String,toLong: String) {
        let actionSheet = UIAlertController(title: "Open Location", message: "Choose an app to open direction", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Google Maps", style: .default, handler: { _ in
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                let url = URL(string:"comgooglemaps://?saddr=\(fromLat),\(fromLong)&daddr=\(toLat),\(toLong)&directionsmode=driving")!
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            } else {
                Utility.showAlert(vc: viewController, message: "Please install google map application and start navigate!.")
            }
        }))
        actionSheet.addAction(UIAlertAction(title: "Apple Maps", style: .default, handler: { _ in
            let directionsURL = "http://maps.apple.com/?saddr=\(fromLat),\(fromLong)&daddr=\(toLat),\(toLong)"
            guard let url = URL(string: directionsURL) else {
                return
            }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }

        }))
        actionSheet.popoverPresentationController?.sourceRect = sourceView.bounds
        actionSheet.popoverPresentationController?.sourceView = sourceView
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        viewController.present(actionSheet, animated: true, completion: nil)
    }
}
