//
//  MapViewPinClass.swift
//  Bar-Surge
//
//  Created by iroid on 21/01/20.
//  Copyright © 2020 iroid. All rights reserved.
//

import Foundation
import MapKit

class CustomPin: NSObject, MKAnnotation{
    dynamic var coordinate:CLLocationCoordinate2D
    dynamic var title:String?
    dynamic var storeImage:String?
    dynamic var pinId:Int?
    
    
    init(pinTitle:String,Location:CLLocationCoordinate2D,storeImage:String,pinId:Int) {
        self.coordinate = Location
        self.title = pinTitle
        self.storeImage = storeImage
        self.pinId = pinId
        
        super.init()
    }
}




