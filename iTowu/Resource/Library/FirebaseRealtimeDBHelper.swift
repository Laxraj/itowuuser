//
//  FirebaseRealtimeDBHelper.swift
//  iTowu
//
//  Created by Nikunj on 24/10/21.
//

import Foundation
import FirebaseDatabase
import FirebaseCore
import FirebaseAuth

struct FirebaseRealtimeDB{
    //MARK:- PRODUCTION
    static let ref: DatabaseReference = Database.database().reference().child("production")
    
    //MARK:- DEVELOPMENT
//    static let ref: DatabaseReference = Database.database().reference().child("development")
    
    
//    static let temp = Database.database().reference().c
}
class FirebaseRealtimeDBHelper{
    static let shared = { FirebaseRealtimeDBHelper() }()
    
    func saveData(childName: String,value: [String: Any]){
        FirebaseRealtimeDB.ref.child(childName).setValue(value)
    }
    
    func authUser(completion: @escaping () -> Void){
        Auth.auth().signInAnonymously { response, error in
            if let res = response{
                debugPrint(res)
                completion()
            }else if let err = error{
                debugPrint(err)
            }
        }
    }
}
