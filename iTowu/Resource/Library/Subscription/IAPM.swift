//
//  IAPM.swift
//  Shybe
//
//  Created by iroid on 07/04/22.
//

import Foundation
import RevenueCat

class IAPM {
    static let shared = { IAPM() } ()
    
    //STANDER TIER
    let SIX_MONTHLY_STANDER_PLAN_ID = "isr_119.99_6m"
    let YEAR_STANDER_PLAN_ID = "isr_199.99_1y"
   
    //PREMIUM TIER
    let SIX_MONTHLY_PREMIUM_PLAN_ID = "isr_pr_119.99_6m"
    let YEAR_PREMIUM_PLAN_ID = "isr_34999_1y"
    
    var storeProduct:StoreProduct?

    var subscriptionProduct:[String] = []
    
    //MARK: - GET THE PRODUCT INFO
    func purchaseProduct(
        productId: String,
        purchaseStatusMessage: @escaping (String) -> Void,
        success: @escaping (String) -> Void,
        failure: @escaping (String) -> Void
    ) {
        // Update status message
        purchaseStatusMessage("Getting offerings...")
        
        Purchases.shared.getOfferings { [weak self] (offerings, error) in
            guard let self = self else { return }
            
            if let error = error {
                print("Error fetching offerings: \(error.localizedDescription)")
                failure(error.localizedDescription)
                return
            }
            
            guard let offering = offerings?.current else {
                print("No current offering configured.")
                failure("No current offering configured")
                return
            }
            
            print("Available packages count: \(offering.availablePackages.count)")
            
            if let package = offering.availablePackages.first(where: { $0.identifier == productId }) {
                print("Product: \(package.storeProduct.localizedDescription), price: \(package.localizedPriceString)")
                
                // Update status message
                purchaseStatusMessage("Purchasing package...")
                
                Purchases.shared.purchase(package: package) { (transaction, customerInfo, error, cancelled) in
                    
                    
                    if cancelled {
                        print("User cancelled purchase.")
                        failure("Cancelled purchase")
                        return
                    }
                    
                    if let error = error {
                        print("Error during purchase: \(error.localizedDescription)")
                        if let revenueCatError = error as? RevenueCat.ErrorCode {
                            switch revenueCatError {
                            case .purchaseNotAllowedError:
                                failure("Purchases not allowed on this device.")
                            case .purchaseInvalidError:
                                failure("Purchase invalid, check payment source.")
                            case .networkError:
                                failure("Network error occurred.")
                            default:
                                failure(error.localizedDescription)
                            }
                        } else {
                            failure(error.localizedDescription)
                        }
                        return
                    }
                    
                    if let customerInfo = customerInfo {
                        print("Customer Info: \(customerInfo.entitlements.all)")
                        if customerInfo.entitlements[productId]?.isActive == true {
                            print("Unlocked Pro Cats 🎉")
                            purchaseStatusMessage("Successful purchase!")
                            success(self.handlePurchaseInfo() ?? "")
                        } else {
                            print("Purchase successful, but entitlement is inactive.")
                            purchaseStatusMessage("Successful purchase but active status is false")
                            
                            // Try refreshing customer info
                            Purchases.shared.getCustomerInfo { updatedInfo, error in
                                if let updatedInfo = updatedInfo {
                                    print("Updated Customer Info: \(updatedInfo.entitlements)")
                                } else if let error = error {
                                    print("Error refreshing customer info: \(error.localizedDescription)")
                                }
                            }
                            success(self.handlePurchaseInfo() ?? "")
                        }
                    } else {
                        print("Customer info is nil after purchase.")
                        failure("Failed to retrieve customer info.")
                    }
                }
            } else {
                print("Product not found.")
                failure("Product is not found")
            }
        }
    }

    func handlePurchaseInfo() -> String? {
        // Get the latest receipt
        if let receiptURL = Bundle.main.appStoreReceiptURL,
           let receiptData = try? Data(contentsOf: receiptURL) {
            let latestReceipt = receiptData.base64EncodedString(options: [])
            print("Latest Receipt: \(latestReceipt)")
            return latestReceipt
        } else {
            print("Failed to retrieve receipt.")
            return nil
        }
    }

    
    func getProduct(success:@escaping ([StoreProduct]) -> Void){
        Utility.showIndicator()
        Purchases.shared.getProducts(subscriptionProduct) { [weak self] storeProduct in
            guard let self = self else { return }
//            self.storeProduct = storeProduct.first(where: { $0.productIdentifier == self.MONTHLY_PLAN_ID })
//            print(storeProduct.count)
            Utility.hideIndicator()
            success(storeProduct)
        }
    }
    
    func restorePurchase(success: @escaping(String, String)->Void,failure: @escaping(String)-> Void){
        Purchases.shared.restorePurchases { (customerInfo, error) in
            if let error {
                print("Customer Info: \(customerInfo?.entitlements.all)")
                print("Restore Failed: \(error.localizedDescription)")
                failure(error.localizedDescription)
            } else {
                print("Restore Success: \(customerInfo?.activeSubscriptions)")
                print("Customer Info: \(customerInfo?.entitlements.all)")
                success(self.handlePurchaseInfo() ?? "", customerInfo?.activeSubscriptions.first ?? "")
            }
        }
    }

}
