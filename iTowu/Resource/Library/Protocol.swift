//
//  Protocol.swift
//  Source-App
//
//  Created by Nikunj on 11/04/21.
//

import Foundation

protocol SelectVehicleDelegate: class {
    func selectVehicle(vehicle: VehicleListResponse)
}
protocol FirebaseAcceptServiceDelegate: class {
    func applyCouponCode(data: FirebaseAcceptedServiceResponse,promocode: String?,isReferralCheck: Int)
}
