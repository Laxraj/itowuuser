//
//  FirebaseanalyticsManager.swift
//  iTowu
//
//  Created by Laxmansinh Rajpurohit on 20/12/24.
//

import UIKit
import FirebaseAnalytics

class FirebaseanalyticsManager {
    
    static let share = { FirebaseanalyticsManager() }()

    func logEvent(event: AnalyticsEvent, parameters: [String: Any]? = ["email": Utility.getUserData()?.email ?? ""]) {
        Analytics.logEvent(event.rawValue, parameters: parameters)
    }
    
    func setUserId(userId: String) {
        Analytics.setUserID(userId)
    }
    
    func setUserProperties(value: String?, property: String) {
        Analytics.setUserProperty(value, forName: property)
    }
    
}


enum AnalyticsEvent: String {
    case loginSuccessfully = "login_successfully"
    case registerSuccessfully = "register_successfully"
    case shopNearbyAddressClick = "shop_nearby_address_click"
    case shopNearbyPhonesClick = "shop_nearby_phone_click"
    case gasStationNearbyClick = "gas_station_nearby_click"
    case chargingStationNearbyClick = "charging_station_nearby_click"
    case beforeAcceptPaymentCancelClick = "before_accept_payment_cancel_click"    
    case acceptPaymentClick = "accept_payment_click"
    case afterAcceptPaymentcancelClick = "after_accept_payment_cancelClick"
    case addedReview = "added_review"
    case cancelAfterDriverAc = "cancel_after_driver_ac"
    case serviceHistoryScreen = "servicehisotry_screen"
    case addCard = "add_card"
    case editCard = "edit_card"
    case deleteCard = "delete_card"
    case promoCodeScreen = "promocode_screen"
    case referralShareClick = "referral_share_click"
    case notificationScreen = "notification_screen"
    case profileScreen = "profile_screen"
    case addEditVehicle = "add_edit_vehicle"
    case deleteVehicle = "delete_vehicle_click"
    case driveAccepted = "driver_accepted"
    case subscriptionFromServiceFlow = "subscribe_from_service_flow"
    case subscriptionFromSideMenuFlow = "subscribe_from_side_menu_flow"
}
