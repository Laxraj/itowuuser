//
//  Constants.swift
//  Medics2you
//
//  Created by Techwin iMac-2 on 11/03/20.
//  Copyright © 2020 Techwin iMac-2. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import GoogleSignIn
import FirebaseDatabase
import LocalAuthentication


struct STORYBOARD {
    static let authentication = UIStoryboard(name: "Authentication", bundle: Bundle.main)
    static let sideMenu = UIStoryboard(name: "SideMenu", bundle: Bundle.main)
    static let map = UIStoryboard(name: "Map", bundle: Bundle.main)
    static let payment = UIStoryboard(name: "Payment", bundle: Bundle.main)
    static let webView = UIStoryboard(name: "WebView", bundle: Bundle.main)
    static let service = UIStoryboard(name: "Service", bundle: Bundle.main)
    static let garage = UIStoryboard(name: "Garage", bundle: Bundle.main)
    static let promocode = UIStoryboard(name: "Promocode", bundle: Bundle.main)
    static let message = UIStoryboard(name: "Message", bundle: Bundle.main)
    static let review = UIStoryboard(name: "Review", bundle: Bundle.main)
    static let refral = UIStoryboard(name: "Refral", bundle: Bundle.main)
    static let help = UIStoryboard(name: "Help", bundle: Bundle.main)
    static let notification = UIStoryboard(name: "Notification", bundle: Bundle.main)
    static let subscription = UIStoryboard(name: "Subscription", bundle: Bundle.main)


//    static let onBoarding = UIStoryboard(name: "OnBoarding", bundle: Bundle.main)
//    static let menu = UIStoryboard(name: "Menu", bundle: Bundle.main)
//    static let login = UIStoryboard(name: "Login", bundle: Bundle.main)
//    static let home = UIStoryboard(name: "Home", bundle: Bundle.main)
//    static let explore = UIStoryboard(name: "Explore", bundle: Bundle.main)
//    static let notifications = UIStoryboard(name: "Notifications", bundle: Bundle.main)
//    static let myLibrary = UIStoryboard(name: "MyLibrary", bundle: Bundle.main)
//    static let profile = UIStoryboard(name: "Profile", bundle: Bundle.main)
//    static let post = UIStoryboard(name: "Post", bundle: Bundle.main)
//    static let changePassword = UIStoryboard(name: "ChangePassword", bundle: Bundle.main)
//    static let register = UIStoryboard(name: "Register", bundle: Bundle.main)
//    static let forgot = UIStoryboard(name: "Forgot", bundle: Bundle.main)
//    static let payForParkDescription = UIStoryboard(name: "payForParkDescription", bundle: nil)
//    static let auction = UIStoryboard(name: "Auction", bundle: Bundle.main)
//    static let tabbar = UIStoryboard(name: "TabBar", bundle: Bundle.main)
//    static let otp = UIStoryboard(name: "Otp", bundle: Bundle.main)
//    static let webView = UIStoryboard(name: "WebView", bundle: Bundle.main)
//    static let search = UIStoryboard(name: "Search", bundle: Bundle.main)
//    static let setting = UIStoryboard(name: "Setting", bundle: Bundle.main)
//    static let community = UIStoryboard(name: "Community", bundle: Bundle.main)
//    static let notification = UIStoryboard(name: "Notification", bundle: Bundle.main)
//    static let message =  UIStoryboard(name: "Message", bundle: Bundle.main)
}

let GOOGLE_CLIENT_ID = "705217604046-huhc4feomjnpdoflqr3ck8ajm9e41l7n.apps.googleusercontent.com"

//MARK:- Social Key
let EMAIL = "email"
let USER_IDD = "userid"
let USERNAME = "username"
let LAST_NAME = "lastName"
let SOCIAL_provider = "socail_provider"
let USER_DATA = "USER_DATA"

let FACEBOOK = "facebook"
let GOOGLE = "google"
let APPLE = "apple"
let EMAIL_PASSWORD                       = "emailPassword"
let appDelegate = UIApplication.shared.delegate as! AppDelegate

let termConditionURL = "https://itowu.com/terms-conditions"
let privacyPolicyURL = "https://itowu.com/privacy-policy"
let aboutUsURL = "https://itowu.com/aboutus"
let faqUsURL = "https://itowu.com/user-faq"
let serviceAggrementURL = "https://itowu.com/service-agreement"
let endLicenseURL = "https://itowu.com/end-user-license-agreement"
let refferalURL = "https://itowu.com/draft-referral-terms"
let appStoreURL = "https://apps.apple.com/us/app/itowu-services-request/id1591145630"

// googleProvideAPIKey = "AIzaSyCgZpHSjRegJLHBRffAChaZwFrvlI_iRXY"
let googleProvideAPIKey = "AIzaSyATy2MJNzdt_gDg8M6-cNyhMP6Y3OKUIRo"

//Revenue cat(Subscription) Public Key
let public_sdk_key = "appl_krNhwgijLPGfbVxbIlDLFMCWfkV"
let SHARED_SECRET   = "652ca464b6a4472283d56e481d8b7f8f"

let FACEID_NOT_SET_IN_OS                = "Seems like you haven't added FaceID in your OS setting. Please goto your OS settings and add your FaceID to use this feature."
let FACEID_NOT_SET_IN_APP               = "You haven't setup FaceID in the app. Please login in the app to set it up."
let ADD_FACEID                          = "Your device supports the FaceID. Would you like to use your FaceID to make the all next login process smoother and faster?"

var isAppInTestMode: Bool  {
#if DEBUG
    return true
#else
    return false
#endif
}


enum PostType: Int{
    case podcast = 1
    case video = 2
    case book = 3
    case artical = 4
    case reShare = 5
}

enum historyType: Int{
    case upcoming = 1
    case inprogress = 2
    case completed = 3
    case cancel = 4
}

enum MessageNavigationType: Int{
    case homeScreen = 1
    case detailScreen = 2
    case shareDetailScreen = 3
    case exploreScreen = 4
    case myLibraryScreen = 5
    case profileScreen = 6
    case likeScreen = 7
    case newUserScreen = 8
}

enum PostWise: String{
    case saveForLater = "1"
    case completed = "2"
}

let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height


//let activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: (screenWidth / 2) - 25, y:(screenHeight / 2) - 25, width: 50, height: 50),type: .ballSpinFadeLoader, color: UIColor.black)

let activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: (screenWidth / 2) - 25, y:(screenHeight / 2) - 25, width: 50, height: 50),type: .ballSpinFadeLoader, color: UIColor.black)

//let linkLoadView = NVActivityIndicatorView(frame: CGRect(x: (screenWidth / 2) - 25, y:(screenHeight / 2) - 25, width: 50, height: 50),type: .ballClipRotateMultiple, color: UIColor.black)

//func createActivityView(view: UIView) -> NVActivityIndicatorView{
//    return NVActivityIndicatorView(frame: CGRect(x: (view.bounds.width / 2) - 25, y:(view.bounds.height  / 2) - 25, width: 50, height: 50),type: .ballSpinFadeLoader, color: UIColor.black)
//}

//var intrestedListArray: [InterestsListData] = []


var topSafeArea: CGFloat{
    if #available(iOS 11.0, *) {
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        return window?.safeAreaInsets.top ?? 0
    }
    return 0
}
var bottomSafeArea: CGFloat{
    if #available(iOS 11.0, *) {
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        return window?.safeAreaInsets.bottom ?? 0
    }
    return 0
}

let linkTag = 1000

let signInConfig = GIDConfiguration.init(clientID: GOOGLE_CLIENT_ID)

var homeVC: UIViewController?

class CacheArray {
    static let shared = CacheArray()
    var serviceArray: [ServiceResponse] = []
    var vehicleMakeArray: [VehicleMakeResponse] = []
    var vehicleYearArray: [VehicleYearRespose] = []
    var vehicleColorArray: [VehicleColorResponse] = []
    var vehicleModelArray: [Models] = []
    var placeOrderRequest: PlaceOrderRequest?
    var vehicleImage: Data?
    var currentService: FirebaseAcceptedServiceResponse?
    var vehicleListResponse: [VehicleListResponse] = []
    var usStateArray: [UsStateResponse] = []
}


enum OrderStatus: Int {
    case pending = 1
    case inProgress = 2
    case completed = 3
    case cancle = 4
    case userAcceptDriver = 5
    case paymentPending = 6
    case hookedVehicle = 7
    case jobCompleted = 8
}

func biometricType() -> BiometricType {
    let authContext = LAContext()
    if #available(iOS 11, *) {
        let _ = authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
        switch(authContext.biometryType) {
        case .none:
            return .none
        case .touchID:
            return .touch
        case .faceID:
            return .face
        case .opticID:
            return .none
        @unknown default:
            return .none
        }
    } else {
        return authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) ? .touch : .none
    }
}

enum BiometricType {
    case none
    case touch
    case face
}

public class LocalAuthManager: NSObject {

    public static let shared = LocalAuthManager()
    private let context = LAContext()
    private let reason = "Your Request Message"
    private var error: NSError?

    enum BiometricType: String {
        case none
        case touchID
        case faceID
    }

    private override init() {

    }

    // check type of local authentication device currently support
    var biometricType: BiometricType {
        guard self.context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
            return .none
        }

        if #available(iOS 11.0, *) {
            switch context.biometryType {
            case .none:
                return .none
            case .touchID:
                return .touchID
            case .faceID:
                return .faceID
            case .opticID:
                return .none
            @unknown default:
                return .none
            }
        } else {
            return self.context.canEvaluatePolicy(.deviceOwnerAuthentication, error: nil) ? .touchID : .none
        }
    }
}


//MARK: GOOGLE ADMOB

//testing
//let google_banner_ad_id = "ca-app-pub-3940256099942544/2934735716"

//MARK: PRODUCTION
let google_banner_ad_id = "ca-app-pub-9524014088011415/1621324656"//"ca-app-pub-9524014088011415~2659151767"

let google_admob_id = "ca-app-pub-9524014088011415~2659151767"

enum NotificationType: Int{
    case order = 1
    case admin = 2
    case referral = 3
    case document = 4
    case driver_vehicle = 5
    case bank_verify = 6
    case background_checker_verify = 7
    case profile_verify = 8
    case service_verify = 9
    case vehicle_verify = 10
    case document_verify = 11
    case review = 12
    case message = 13
    case featAdminJoin = 14
}

let isiPad: Bool = UIDevice.current.userInterfaceIdiom == .pad
