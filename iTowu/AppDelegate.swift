//
//  AppDelegate.swift
//  iTowu
//
//  Created by Nikunj on 14/10/21.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleSignIn
import Firebase
import Messages
import MapKit
import GooglePlaces
import NotificationBannerSwift
import FBSDKCoreKit
import AppTrackingTransparency
import AdSupport
import GoogleMobileAds
import AdSupport
import RevenueCat

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    var fcmToken = ""
    let locationManager = CLLocationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.window = UIWindow(frame: UIScreen.main.bounds)
        ApplicationDelegate.shared.initializeSDK()
      
        FirebaseApp.configure()
        
        GIDSignIn.sharedInstance.configuration = signInConfig
        
        
        print(
            ASIdentifierManager.shared().advertisingIdentifier
        )
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self

            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        AppEvents.shared.activateApp()
        Settings.shared.enableLoggingBehavior(.appEvents)
        Settings.shared.enableLoggingBehavior(.developerErrors)
        Settings.shared.enableLoggingBehavior(.cacheErrors)
        Settings.shared.enableLoggingBehavior(.uiControlErrors)
        Settings.shared.isAdvertiserTrackingEnabled = true
        Settings.shared.isAutoLogAppEventsEnabled = true
        Settings.shared.isAdvertiserIDCollectionEnabled = true
        
        application.registerForRemoteNotifications()
//        self.notificationClick(id: "10")
        Messaging.messaging().delegate = self
        
       // Analytics.setAnalyticsCollectionEnabled(true)
      //  FirebaseConfiguration.shared.setLoggerLevel(.debug)
        
#if DEBUG
       FirebaseConfiguration.shared.setLoggerLevel(.debug)
   #endif
        
        self.locationPermistion()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true

        GMSPlacesClient.provideAPIKey(googleProvideAPIKey)
        
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [ "8458767d9aab9d1bb0094c14e75ef2aa","846baeb8f3aa1d7b503ce7ef662faaa1" ]
                
        GADMobileAds.sharedInstance().start(completionHandler: nil)

        
        AppEvents.shared.logEvent(AppEvents.Name("battledAnOrc"))
       
        
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization { status in
                switch status {
                case .authorized:
                    // Tracking authorization dialog was shown
                    // and we are authorized
                    print("Authorized")
                    Settings.shared.isAdvertiserTrackingEnabled = true
                             Settings.shared.isAdvertiserIDCollectionEnabled = true
                    // Now that we are authorized we can get the IDFA
                    print(ASIdentifierManager.shared().advertisingIdentifier)
                case .denied:
                    // Tracking authorization dialog was
                    // shown and permission is denied
                    Settings.shared.isAdvertiserTrackingEnabled = false
                              Settings.shared.isAdvertiserIDCollectionEnabled = false
                    print("Denied")
                case .notDetermined:
                    // Tracking authorization dialog has not been shown
//                    Settings.shared.isAdvertiserTrackingEnabled = false
//                              Settings.shared.isAdvertiserIDCollectionEnabled = false
                    print("Not Determined")
                case .restricted:
                    print("Restricted")
                @unknown default:
                    print("Unknown")
                }
            }
        }
        
        Purchases.logLevel = .debug
        Purchases.configure(withAPIKey: public_sdk_key, appUserID: nil)
        
        Utility.setViewControllerRoot()

        return true
    }
    
//    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//
//        return GIDSignIn.sharedInstance.handle(url)
//    }
    
    func application(
           _ app: UIApplication,
           open url: URL,
           options: [UIApplication.OpenURLOptionsKey : Any] = [:]
       ) -> Bool {
           if (GIDSignIn.sharedInstance.handle(url)){
               return true
           }
           return ApplicationDelegate.shared.application(
               app,
               open: url,
               sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
               annotation: options[UIApplication.OpenURLOptionsKey.annotation]
           )
       }
    
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        AppEvents.shared.activateApp()
        connectSocket()
    }
    
    func applicationWillResignActive(_ application: UIApplication){
        if Utility.getUserData() != nil{
            SocketHelper.shared.disconnectSocket()
        }
    }
    func connectSocket(){
        if let userData = Utility.getUserData(){
            SocketHelper.shared.connectSocket(completion: { val in
                if(val){
                    print("socket connected")
                    var parameter = [String: Any]()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        // your code here
                        parameter = ["senderId": userData.userId ?? 0,"senderType":"U","isOnline": 1] as [String:Any]
                        SocketHelper.Events.UpdateStatusToOnline.emit(params: parameter)
                    }
                }else{
                    print("socket did't connected")
                }
            })
        }else{
            print("data getting nil")
        }
    }
}

extension AppDelegate: CLLocationManagerDelegate{
    func locationPermistion(){
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        currentLatitude = "0";
        currentLongitude = "0";
        currentLatitude = "\(locValue.latitude)"//(String(format:"%.2f",locValue.latitude) as NSString) as String
        currentLongitude = "\(locValue.longitude)"//(String(format:"%.2f",locValue.longitude) as NSString) as String
        print(currentLatitude)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        print("error::: \(error)")
        locationManager.stopUpdatingLocation()
    }
}

extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        print(userInfo)
        let type = userInfo["type"] as? String
        print(type)
        //        if  Utility.getUserData() != nil{
        //            if let type = userInfo["type"] as? String{
        //                if type == "1"{
        //                    NotificationCenter.default.post(name: Notification.Name("NEW_BID"), object: nil)
        //                }else if type == "5"{
        //                    NotificationCenter.default.post(name: Notification.Name("PAYMENT_DONE"), object: nil)
        //                }
        //            }
        //        }
        if userInfo["gcm.notification.type"] as? String == "2"{
            if let vc = homeVC as? MapWhereAreYouScreen{
                vc.messageCount()
            }
        }
        
        
        // Change this to your preferred presentation option
        completionHandler([.alert,.sound,.badge])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print(userInfo)
        
//        let messagView = MessageView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 120))
//        let banner = NotificationBanner(customView: messagView)
//        banner.bannerHeight = 105 + (topSafeArea > 20 ? 25 : 0)
//        banner.applyStyling()
//        banner.show()
//        guard
//            let aps = userInfo[AnyHashable("aps")] as? NSDictionary,
//            let alert = aps["alert"] as? NSDictionary,
//            let body = alert["body"] as? String,
//            let title = alert["title"] as? String
//            else {
//                // handle any error here
//                return
//            }
//        
//        messagView.messageLabel.text = body
//        messagView.userName.text = title
        
        
        //        if  Utility.getUserData() != nil{
        //            if let type = userInfo["type"] as? String{
        //                guard let rootVC = STORYBOARD.tabbar.instantiateViewController(withIdentifier: "TabbarScreen") as? TabbarScreen else {
        //                    return
        //                }
        //                // rootVC.storIdString = storeId
        //                rootVC.type = Int(type) ?? 0
        //                rootVC.isFromNotification = true
        //                let navigationController = UINavigationController(rootViewController: rootVC)
        //                navigationController.isNavigationBarHidden = true
        //                UIApplication.shared.windows.first?.rootViewController = navigationController
        //                UIApplication.shared.windows.first?.makeKeyAndVisible()
        //            }
        //        }
        if let dictionary = userInfo as? [String: Any],let notification = NotificationResponse(JSON: dictionary){
            Utility.manageNotificationNavigation(data: notification)
        }
        completionHandler()
    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        Messaging.messaging().apnsToken = deviceToken as Data
        let token = tokenParts.joined()
        print("Device Token: \(token)")
        UserDefaults().set(token, forKey: "DEVICE_TOKEN")
        if Utility.getUserData()?.auth != nil{
            self.registerForPush()
        }
    }
}

extension AppDelegate : MessagingDelegate{


    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken ?? "")")
        self.fcmToken = fcmToken ?? ""
        //        let dataDict:[String: String] = ["token": fcmToken]
        //        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)


        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
}
//MARK:- API METHOD
extension AppDelegate {
    func registerForPush(){
        if Utility.isInternetAvailable(){
            var fcmToken = ""
            if let token = Messaging.messaging().fcmToken {
                print("FCM token: \(token)")
                fcmToken = token
            }
            let data = RegisterForPushRequest(device_id: DEVICE_UNIQUE_IDETIFICATION, device_type: "ios", device_token: fcmToken, type: "1")
            print(data.toJSON())
            AuthenticationService.shared.registerForPush(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    print(res.toJSON())
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                //                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
        }
    }
}


//CLLocationManagerDelegate
