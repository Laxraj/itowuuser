//
//  SubscriptionService.swift
//  Tika
//
//  Created by iRoid Dev on 04/12/24.
//

import Foundation

class SubscriptionService{
    
    static let shared = { SubscriptionService() }()
    
    //MARK: - SUBSCRIPTION API
    func subscription(parameter : [String:Any] = [:],success : @escaping (Int, Response?)-> (),failure : @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: subscriptionURL, parameters: parameter, success: {
            (statusCode , response) in
            success(statusCode, response)
        }, failure: { (error) in
            failure(error)
        })
    }
    
    //MARK: - SUBSCRIPTION API
    func checkSubscription(parameter : [String:Any] = [:],success : @escaping (Int, Response?)-> (),failure : @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: checkSubscriptionURL, parameters: parameter, success: {
            (statusCode , response) in
            success(statusCode, response)
        }, failure: { (error) in
            failure(error)
        })
    }
    
}
