//
//  MessageService.swift
//  iTowuDriver
//
//  Created by iroid on 09/11/21.
//

import Foundation

class MessageService {
    static let shared = { MessageService() }()
    
    func getChatUserList(parameters: [String: Any] = [:],url:String, success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: url, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- DELETE CHAT USER
//    func getChatUser(id: Int,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
//        APIClient.shared.requestAPIWithParameters(method: .delete, urlString: deleteUserChatURL+"/\(id)", parameters: [:]) { (statusCode, response) in
//            success(statusCode,response)
//        } failure: { (error) in
//            failure(error)
//        }
//    }
    //MARK:- GET MESASGE DETAIL
    func getChatDetail(url:String, success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString:url) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    
    //MARK:- GET TOTAL MESASGE COUNT
    func getTotalMessageCount( success: @escaping (Int, ChatCount?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString:messageCountUrl) { (statusCode, response) in
            success(statusCode,response.chatCount)
        } failure: { (error) in
            failure(error)
        }
    }
    
}
