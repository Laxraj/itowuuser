//
//  MapService.swift
//  iTowu
//
//  Created by Nikunj on 17/10/21.
//

import Foundation
class MapService {
    static let shared = { MapService() }()

//    //MARK:- REGISTER USER
//    func registerUser(parameters: [String: Any] = [:],success: @escaping (Int, LoginResponse?) -> (), failure: @escaping (String) -> ()){
//        APIClient.shared.requestAPIWithParameters(method: .post, urlString: registerURL, parameters: parameters) { (statusCode, response) in
//            success(statusCode,response.loginResponse)
//        } failure: { (error) in
//            failure(error)
//        }
//    }
    //MARK:- GET SERVICE
    func getService(success: @escaping (Int, [ServiceResponse]?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: serviceURL) { (statusCode, response) in
            success(statusCode,response.serviceRespose)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- PLACE ORDER
    func placeOrder(parameters: [String: Any] = [:],image: Data?,imageParamName: String,success: @escaping (Int, PlaceOrderResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestWithImage(urlString: orderURL, imageParameterName: imageParamName, images: image, parameters: parameters) { (statusCode, response) in
            success(statusCode,response.placeOrderResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- CHECK ORDER
    func checkOrder(success: @escaping (Int, OrderStatusResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: checkOrderURL) { (statusCode, response) in
            success(statusCode,response.orderStatusResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
//    func orderDetail(parameters: [String: Any] = [:],success: @escaping (Int, FirebaseAcceptedServiceResponse?) -> (), failure: @escaping (String) -> ()){
//        APIClient.shared.requestAPIWithParameters(method: .get, urlString: orderDetailURL, parameters: parameters) { (statusCode, response) in
//            success(statusCode,response.firebaseAcceptedServiceResponse)
//        } failure: { (error) in
//            failure(error)
//        }
//    }
    
    
    //MARK:- ORDER DETAIL
    func orderDetail(urlString: String, success: @escaping (Int, FirebaseAcceptedServiceResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: urlString) { (statusCode, response) in
            success(statusCode,response.firebaseAcceptedServiceResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- SERVICE LIST
    func completedserviceList(urlString: String, success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: urlString) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- CANCEL ORDER
    func cancelOrder(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: cancelOrderURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- CANCEL PRICE
    func cancelPrice(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: cancelPriceURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- REVIEW DRIVER
    func reviewDriver(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: rattingURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- DRIVER ORDER REQUEST
    func driverOrderRequest(imageParameterName: String,image: Data?,parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestWithImage(urlString: confirmOrderURL, imageParameterName: imageParameterName, images: image, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- GET FUEL COUNT
    func getFuelPrice(url:String, success: @escaping (Int, FualPriceData?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString:url) { (statusCode, response) in
            success(statusCode,response.fualPriceData)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- ACCEPT/DECLINE EXTRA CHARGES
    func acceptRejectExtraCharges(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: extraChargesURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- CHECK ORDER STATUS
    func checkOrderStatus(success: @escaping (Int, CheckOrderStatusResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: checkOrderStatusURL) { (statusCode, response) in
            success(statusCode,response.checkOrderStatusResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK: CALCULATE ORDER PRICE
    func calculateOrderPrice(parameters: [String: Any],success: @escaping (Int, FirebaseAcceptedServiceResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: orderCalculatePriceURL, parameters: parameters) { statusCode, response in
            success(statusCode,response.firebaseAcceptedServiceResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK: - PLACE ORDER V2
    func placeOrderV2(parameters: [String: Any] = [:],image: Data?,imageParamName: String,success: @escaping (Int, PlaceOrderResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestWithImage(urlString: orderV2URL, imageParameterName: imageParamName, images: image, parameters: parameters) { (statusCode, response) in
            success(statusCode,response.placeOrderResponse)
        } failure: { (error) in
            failure(error)
        }
    }
}
