//
//  NotificationService.swift
//  iTowu
//
//  Created by Nikunj Vaghela on 01/10/23.
//

import Foundation
class NotificationService {
    static let shared = { NotificationService() }()
    
    func getNotification(page: Int,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: notificationURL+"?page=\(page)") { statusCode, response in
            success(statusCode,response)
        } failure: { error in
            failure(error)
        }
    }
    
    func adminJoin(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: adminJoinURL, parameters: parameters) { statusCode, response in
            success(statusCode,response)
        } failure: { error in
            failure(error)
        }
    }
    
    func deleteNotificaiton(notificationId: Int,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .delete, urlString: notificationURL+"/\(notificationId)", parameters: [:]) { statusCode, response in
            success(statusCode,response)
        } failure: { error in
            failure(error)
        }
    }
}
