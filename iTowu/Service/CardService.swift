//
//  CardService.swift
//  iTowu
//
//  Created by iroid on 17/10/21.
//

import Foundation
class CardService {
    static let shared = { CardService() }()
    
    //MARK:- GET CARD
    func getCardList(success: @escaping (Int, [CardResponse]?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: cardListURL) { (statusCode, response) in
            success(statusCode,response.cardResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    //MARK:- ADD DEFAULT CARD
    func getDefaultCard(url:String,success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: url) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- ADD CARD & UPDATE CARD
    func addCardAndUpdate(parameters: [String: Any] = [:],success: @escaping (Int, CardResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: addUpdateCardURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response.addCardResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- DELETE CARD
    func deleteCard(url:String,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .delete, urlString: url) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- SERVICE PAYMENT
    func servicePayment(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: servicePaymentURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
}
