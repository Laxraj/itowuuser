//
//  AuthenticationService.swift
//  iTowu
//
//  Created by Nikunj on 16/10/21.
//

import Foundation
class AuthenticationService {
    static let shared = { AuthenticationService() }()

    //MARK:- REGISTER USER
    func registerUser(parameters: [String: Any] = [:],success: @escaping (Int, LoginResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: registerURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response.loginResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Login
    func login(parameters: [String: Any] = [:],success: @escaping (Int, LoginResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: loginURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response.loginResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- SOCIAL LOGIN
    func socialLogin(parameters: [String: Any] = [:],success: @escaping (Int, LoginResponse?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: socialLoginURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response.loginResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- CHANGE PASSWORD
    func changePassword(parameters: [String: Any] = [:],success: @escaping (Int, LoginResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: changePasswordURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response.loginResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    
    
    //MARK:- REFERRAL CODE
    func deleteUserAccount(success: @escaping (Int, ReferralCodeResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: deleteAccountURL) { (statusCode, response) in
            success(statusCode,response.referralResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    //MARK:- LOGOUT
    func logOut(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: logOutURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- UPDATE PROFILE
    func updateProfile(parameters: [String: Any] = [:],imageData: Data?,success: @escaping (Int, LoginResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestWithImage(urlString: updateProfileURL, imageParameterName: "profile", images: imageData, parameters: parameters) { (statusCode, response) in
            success(statusCode,response.loginResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- Register for push
    func registerForPush(parameters: [String: Any] = [:],success: @escaping (Int, Response?) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: registerFotPushURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- FORGOT PASSWORD USER
    func resetPasswordSendEmail(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: resetPasswordSendEmailURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- VERIFY PASSWORD USER
    func verifyEmail(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: verifyPhoneOrEmailURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- VERIFY EMAIL USER
    func verifyEmailOTP(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: verifyPhoneOrEmailLoginURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    
    //MARK: - CHANGE EMAIL
    func changeEmail(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: changeEmailURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK: - CHANGE Phone number
    func changePhoneNumber(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: changePhoneURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    
    //MARK: - VERIFY EMAIL
    func verifyEmailFromProfile(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: verifyEmailURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK: - VERIFY PHONE
    func verifyPhoneFromProfile(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: verifyPhoneURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK: - VERIFY EMAIL USER
    func resendVerifyOTP(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: resendOtpURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- RESET PASSWORD 
    func resetPassword(parameters: [String: Any] = [:],success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()) {
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: forgetPasswordURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- REFERRAL CODE
    func getReferralCode(success: @escaping (Int, ReferralCodeResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: referralCodeURL) { (statusCode, response) in
            success(statusCode,response.referralResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK: CHECK VERSION
        func checkVersion(parameters: [String: Any],success: @escaping (Int, VersionResponse?) -> (), failure: @escaping (String) -> ()){
            APIClient.shared.requestAPIWithParameters(method: .post, urlString: appVersiobURL, parameters: parameters) { (statusCode, response) in
                success(statusCode,response.versionResponse)
            } failure: { (error) in
                failure(error)
            }
        }
    
    //MARK:- GET US STATE
    func getUSState(success: @escaping (Int, [UsStateResponse]?) -> (), failure: @escaping (String) -> ()){
        if CacheArray.shared.usStateArray.count > 0{
            success(200,CacheArray.shared.usStateArray)
        }else{
            APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: usStateURL) { (statusCode, response) in
                CacheArray.shared.usStateArray = response.usStateResponse ?? []
                success(statusCode,response.usStateResponse)
            } failure: { (error) in
                failure(error)
            }
        }
    }
    
    //MARK: - CHANGE REGISTERD PHONE
    func changeRegisterPhone(parameter: [String: Any],success: @escaping (Int, LoginResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: registerChangePhoneURL, parameters: parameter) { statusCode, response in
            success(statusCode,response.loginResponse)
        } failure: { (error) in
            failure(error)
        }
    }
}
