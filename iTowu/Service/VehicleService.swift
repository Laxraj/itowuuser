//
//  VehicleService.swift
//  iTowu
//
//  Created by Nikunj on 18/10/21.
//

import Foundation
class VehicleService {
    static let shared = { VehicleService() }()

    //MARK:- VEHICLE MAKE
    func getVehicleMake(success: @escaping (Int, [VehicleMakeResponse]?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: vehicleMakeURL) { (statusCode, response) in
            success(statusCode,response.vehicleMakeResose)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- VEHICLE YEAR
    func getVehicleYear(success: @escaping (Int, [VehicleYearRespose]?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: vehicleYearURL) { (statusCode, response) in
            success(statusCode,response.vehicleYearResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- VEHICLE COLOR
    func getVehicleColor(success: @escaping (Int, [VehicleColorResponse]?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: vehicleColorURL) { (statusCode, response) in
            success(statusCode,response.vehicleColorResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- VEHICLE LIST
    func getVehicles(success: @escaping (Int, [VehicleListResponse]?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: garageVehicleListURL) { (statusCode, response) in
            CacheArray.shared.vehicleListResponse = response.vehicleListResponse ?? []
            success(statusCode,response.vehicleListResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- ADD/EDIT VEHICLE
    func addVehicle(parameters: [String: Any],vehicleID: Int?,image: Data?,imageParamName: String,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestWithImage(urlString: vehicleID == nil ? vehicleURL : vehicleURL+"/\(vehicleID ?? 0)", imageParameterName: imageParamName, images: image, parameters: parameters) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- DELETE VEHICLE
    func deleteVehicle(vehicleID: Int,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .delete, urlString: vehicleURL+"/\(vehicleID)", parameters: [:]) { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- EDIT VEHICLE
    func editVehicle(vehicleID: Int,parameters: [String: Any],image: Data?,imageParamName: String,success: @escaping (Int, VehicleListResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestWithImage(urlString: vehicleURL+"/\(vehicleID)", imageParameterName: imageParamName, images: image, parameters: parameters) { (statusCode, response) in
            success(statusCode,response.vehicleEditResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- COUPON LIST
    func getCoupon(success: @escaping (Int, [CouponListResponse]?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: couponListURL) { (statusCode, response) in
            success(statusCode,response.couponListResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK:- CHECK COUPON
    func checkCoupon(parameters: [String: Any] = [:],success: @escaping (Int, FirebaseAcceptedServiceResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: checkCouponURL, parameters: parameters) { (statusCode, response) in
            success(statusCode,response.firebaseAcceptedServiceResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK: - ADD/UPDATE VEHICLE
    func addUpdateVehicle(parameters: [String: Any],isManual: Bool = false,image: Data?,success: @escaping (Int, VehicleListResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestWithImage(urlString: isManual ? addManualVehicleURL : addUpdateVehicleURL, imageParameterName: "image", images: image, parameters: parameters) { (statusCode, response) in
            success(statusCode,response.vehicleEditResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK: - GARAGE VEHICLE LIST
    func getGarageVehicleList(success: @escaping (Int, [VehicleListResponse]?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: garageVehicleListURL) { statusCode, response in
            success(statusCode,response.vehicleListResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK: - DELETE VEHICLE
    func deleteGarageVehicle(vehicleID: Int,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .delete, urlString: deleteVehicleURL+"/\(vehicleID)", parameters: [:]) { statusCode, response in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK: - GET DRIVER VEHICLE INFO
    func getVehicleInfo(parameters: [String: Any],success: @escaping (Int, VehicleListResponse?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithParameters(method: .post, urlString: getGarageVehicleDetailURL, parameters: parameters) { statusCode, response in
            success(statusCode,response.vehicleEditResponse)
        } failure: { (error) in
            failure(error)
        }
    }
}
