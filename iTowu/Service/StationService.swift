//
//  StationService.swift
//  iTowu
//
//  Created by Nikunj Vaghela on 17/06/23.
//

import Foundation
class StationService {
    static let shared = { StationService() }()
    
    //MARK: - GAS STATION LIST
    func getGasStation(lat: String,long: String,success: @escaping (Int, [NearByGasStationList]?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: nearByGasStationURL+"?lat=\(lat)&long=\(long)") { (statusCode, response) in
            success(statusCode,response.nearByStatationResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK: - CHARGE STATION LIST
    func getChargeStation(lat: String,long: String,success: @escaping (Int, [NearByGasStationList]?) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: nearByChargeStationURL+"?lat=\(lat)&long=\(long)") { (statusCode, response) in
            success(statusCode,response.nearByStatationResponse)
        } failure: { (error) in
            failure(error)
        }
    }
    
    //MARK: ADVERTISEMENT LIST
    func getAdvertisementList(page: Int,perPage: Int,success: @escaping (Int, Response) -> (), failure: @escaping (String) -> ()){
        APIClient.shared.requestAPIWithGetMethod(method: .get, urlString: advertisementURL+"?page=\(page)&perPage=\(perPage)") { (statusCode, response) in
            success(statusCode,response)
        } failure: { (error) in
            failure(error)
        }
    }
}
