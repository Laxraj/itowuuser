//
//  ResponseModel.swift
//  iTowu
//
//  Created by Nikunj on 16/10/21.
//

import Foundation
import ObjectMapper

class LoginResponse: Mappable {

    var userId: Int?
    var firstName: String?
    var lastName: String?
    var profile: String?
    var countryCode: String?
    var phone: String?
    var email: String?
    var latitude: String?
    var longitude: String?
    var hearReason: String?
    var notification: Bool?
    var accountType: Int?
    var invite_code: String?
    var verifiedAt: Int?
    var subscription: SubscriptionResponseData?
    var auth: AuthResponse?

    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        userId <- map["userId"]
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        profile <- map["profile"]
        countryCode <- map["countryCode"]
        phone <- map["phone"]
        email <- map["email"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        hearReason <- map["hearReason"]
        notification <- map["notification"]
        accountType <- map["accountType"]
        invite_code <- map["invite_code"]
        verifiedAt <- map["verifiedAt"]
        subscription <- map["subscription_data"]
        auth <- map["auth"]
    }
}
class AuthResponse: Mappable {

    var tokenType: String?
    var expiresIn: Int?
    var accessToken: String?
    var refreshToken: String?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        tokenType <- map["tokenType"]
        expiresIn <- map["expiresIn"]
        accessToken <- map["accessToken"]
        refreshToken <- map["refreshToken"]
    }
}

class VersionResponse: Mappable{
    var status: NSNumber?
    var message: String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        message <- map["message"]
    }
}

class ServiceResponse: Mappable {
    
    var serviceId: Int?
    var name: String?
    var image: String?
    var dark_image: String?
    var type: Int?
    var questions: [Questions]?
    var isNewService: Bool? = false
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        serviceId <- map["serviceId"]
        name <- map["name"]
        image <- map["image"]
        type <- map["type"]
        questions <- map["questions"]
        dark_image <- map["dark_image"]
        isNewService <- map["isNewService"]
    }
}

class Questions: Mappable {
    var serviceQuestionId: Int?
    var question: String?
    var isPercentRange: Bool?
    var answers: [QuestionAnswer]?
    var selectedAnswer: String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        serviceQuestionId <- map["serviceQuestionId"]
        isPercentRange <- map["isPercentRange"]
        question <- map["question"]
        answers <- map["answers"]
        selectedAnswer <- map["selectedAnswer"]
    }
}
class QuestionAnswer: Mappable{
    var serviceQuestionAnswerId: Int?
    var answer: String?
    var description: String?
    var prompt: String?
    var isConvertToTowu: Bool?
    var isUserInputData: Bool?
    var userInputText: String?
    var selectedWindowAnswer: String?
    
    init(){
        
    }
    
    required init?(map: Map) {
    
    }
    
    func mapping(map: Map) {
        serviceQuestionAnswerId <- map["serviceQuestionAnswerId"]
        answer <- map["answer"]
        description <- map["description"]
        prompt <- map["prompt"]
        isConvertToTowu <- map["isConvertToTowu"]
        isUserInputData <- map["isUserInputData"]
        userInputText <- map["userInputText"]
        selectedWindowAnswer <- map["selectedAnswer"]
    }
}
class VehicleMakeResponse: Mappable {
    
    var vehicleMakeId: Int?
    var name: String?
    var models: [Models]?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        vehicleMakeId <- map["vehicle_make_id"]
        name <- map["name"]
        models <- map["models"]
    }
}

class Models: Mappable {
    
    var id: Int?
    var name: String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
    }
}


class CardResponse: Mappable {
    var id: String?
    var brand: String?
    var expiration_date: String?
    var card_number:String?
    var cardHolderName:String?
    var isDefault:Int?
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        brand <- map["brand"]
        expiration_date <- map["expiration_date"]
        card_number <- map["card_number"]
        cardHolderName <- map["card_holder_name"]
        isDefault <- map["default"]
    }
}

class VehicleYearRespose: Mappable{
    var vehicle_year_id: Int?
    var name: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        vehicle_year_id <- map["vehicle_year_id"]
        name <- map["name"]
    }
}

class VehicleColorResponse: Mappable{
    var vehicle_color_id: Int?
    var name: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        vehicle_color_id <- map["vehicle_color_id"]
        name <- map["name"]
    }
}
class PlaceOrderResponse: Mappable {

    var orderId: Int?
    var plateNumber: String?
    var image: String?
    var pickupLatitude: Int?
    var pickupLongitude: Int?
    var pickupAddress: String?
    var destinationLatitude: Int?
    var destinationLongitude: Int?
    var destinationAddress: String?
    var status: Int?
    var vehicleYear: VehicleYear?
    var vehicleMake: VehicleMake?
    var vehicleModel: VehicleModel?
    var vehicleColor: VehicleColor?
    var questions: [Questions]?
    var defaultPayment:DefaultPayment?
    required init?(map: Map){
    }

    func mapping(map: Map) {
        orderId <- map["orderId"]
        plateNumber <- map["plateNumber"]
        image <- map["image"]
        pickupLatitude <- map["pickup_latitude"]
        pickupLongitude <- map["pickup_longitude"]
        pickupAddress <- map["pickup_address"]
        destinationLatitude <- map["destination_latitude"]
        destinationLongitude <- map["destination_longitude"]
        destinationAddress <- map["destination_address"]
        status <- map["status"]
        vehicleYear <- map["vehicleYear"]
        vehicleMake <- map["vehicleMake"]
        vehicleModel <- map["vehicleModel"]
        vehicleColor <- map["vehicleColor"]
        questions <- map["questions"]
        defaultPayment <- map["default_payment"]
    }
}

class DefaultPayment: Mappable {
    var id: String?
    var card_number: String?
    required init?(map: Map){
    }

    func mapping(map: Map) {
        id <- map["id"]
        card_number <- map["card_number"]
    }
}

class OrderStatusResponse: Mappable {

    var orderId: Int?
    var plateNumber: String?
    var image: String?
    var pickupLatitude: Int?
    var pickupLongitude: Int?
    var pickupAddress: String?
    var destinationLatitude: Int?
    var destinationLongitude: Int?
    var destinationAddress: String?
    var status: Int?
    var vehicleYearId: Int?
    var vehicleYearName: String?
    var vehicleMakeId: Int?
    var vehicleMakeName: String?
    var vehicleModelId: Int?
    var vehicleModelName: String?
    var vehicleColorId: Int?
    var vehicleColorName: String?
    var questions: [CheckStatusQuestions]?
    var user: LoginResponse?
    var driver: Driver?
    var service: Service?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        orderId <- map["orderId"]
        plateNumber <- map["plateNumber"]
        image <- map["image"]
        pickupLatitude <- map["pickup_latitude"]
        pickupLongitude <- map["pickup_longitude"]
        pickupAddress <- map["pickup_address"]
        destinationLatitude <- map["destination_latitude"]
        destinationLongitude <- map["destination_longitude"]
        destinationAddress <- map["destination_address"]
        status <- map["status"]
        vehicleYearId <- map["vehicleYearId"]
        vehicleYearName <- map["vehicleYearName"]
        vehicleMakeId <- map["vehicleMakeId"]
        vehicleMakeName <- map["vehicleMakeName"]
        vehicleModelId <- map["vehicleModelId"]
        vehicleModelName <- map["vehicleModelName"]
        vehicleColorId <- map["vehicleColorId"]
        vehicleColorName <- map["vehicleColorName"]
        questions <- map["questions"]
        user <- map["user"]
        driver <- map["driver"]
        service <- map["service"]
    }
}

class CheckStatusQuestions: Mappable {

    var orderQuestionId: NSNumber?
    var answer: String?
    var question: Question?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        orderQuestionId <- map["orderQuestionId"]
        answer <- map["answer"]
        question <- map["question"]
    }
}

class Question: Mappable {

    var serviceQuestionId: NSNumber?
    var question: String?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        serviceQuestionId <- map["serviceQuestionId"]
        question <- map["question"]
    }
}


class OrderHistoryResponse: Mappable {
    var orderId: Int?
    var plateNumber: String?
    var image: String?
    var pickupLatitude: Int?
    var pickupLongitude: Int?
    var pickupAddress: String?
    var destinationLatitude: Int?
    var destinationLongitude: Int?
    var destinationAddress: String?
    var status: Int?
    var vehicleYearId: Int?
    var vehicleYearName: String?
    var vehicleMakeId: Int?
    var vehicleMakeName: String?
    var vehicleModelId: Int?
    var vehicleModelName: String?
    var vehicleColorId: Int?
    var vehicleColorName: String?
//    var pickUpImages: [PickUpImages]?
//    var dropImages: [DropImages]?
    var questions: [Questions]?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        orderId <- map["orderId"]
        plateNumber <- map["plateNumber"]
        image <- map["image"]
        pickupLatitude <- map["pickup_latitude"]
        pickupLongitude <- map["pickup_longitude"]
        pickupAddress <- map["pickup_address"]
        destinationLatitude <- map["destination_latitude"]
        destinationLongitude <- map["destination_longitude"]
        destinationAddress <- map["destination_address"]
        status <- map["status"]
        vehicleYearId <- map["vehicleYearId"]
        vehicleYearName <- map["vehicleYearName"]
        vehicleMakeId <- map["vehicleMakeId"]
        vehicleMakeName <- map["vehicleMakeName"]
        vehicleModelId <- map["vehicleModelId"]
        vehicleModelName <- map["vehicleModelName"]
        vehicleColorId <- map["vehicleColorId"]
        vehicleColorName <- map["vehicleColorName"]
//        pickUpImages <- map["pickUpImages"]
//        dropImages <- map["dropImages"]
        questions <- map["questions"]
    }
}

class Meta: Mappable{
    var current_page: Int?
    var from: Int?
    var last_page: Int?
    var links: [Link]?
    var path: String?
    var per_page: Int?
    var to: Int?
    var total: Int?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        current_page <- map["current_page"]
        from <- map["from"]
        last_page <- map["last_page"]
        links <- map["links"]
        path <- map["path"]
        per_page <- map["per_page"]
        to <- map["to"]
        total <- map["total"]
    }
}
class Link: Mappable{
    var url: String?
    var label: String?
    var active: Bool?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        url <- map["url"]
        label <- map["label"]
        active <- map["active"]
    }
}
class Links: Mappable{
    var first: String?
    var last: String?
    var prev: String?
    var next: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        first <- map["first"]
        last <- map["last"]
        prev <- map["prev"]
        next <- map["next"]
    }
}

class VehicleColor: Mappable {

    var vehicleColorId: Int?
    var name: String?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        vehicleColorId <- map["vehicle_color_id"]
        name <- map["name"]
    }
}

class VehicleModel: Mappable {

    var vehicleModelId: Int?
    var name: String?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        vehicleModelId <- map["vehicle_model_id"]
        name <- map["name"]
    }
}

class VehicleMake: Mappable {

    var vehicleMakeId: Int?
    var name: String?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        vehicleMakeId <- map["vehicle_make_id"]
        name <- map["name"]
    }
}

class VehicleYear: Mappable {

    var vehicleYearId: Int?
    var name: String?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        vehicleYearId <- map["vehicle_year_id"]
        name <- map["name"]
    }
}
//class VehicleListResponse: Mappable {
//
//    var vehicleId: Int?
//    var plateNumber: String?
//    var image: String?
//    var vehicleYearId: Int?
//    var vehicleYearName: String?
//    var vehicleMakeId: Int?
//    var vehicleMakeName: String?
//    var vehicleModelId: Int?
//    var vehicleModelName: String?
//    var vehicleColorId: Int?
//    var vehicleColorName: String?
//    var img: UIImage?
//
//    required init?(map: Map){
//    }
//
//    func mapping(map: Map) {
//        vehicleId <- map["vehicleId"]
//        plateNumber <- map["plateNumber"]
//        image <- map["image"]
//        vehicleYearId <- map["vehicleYearId"]
//        vehicleYearName <- map["vehicleYearName"]
//        vehicleMakeId <- map["vehicleMakeId"]
//        vehicleMakeName <- map["vehicleMakeName"]
//        vehicleModelId <- map["vehicleModelId"]
//        vehicleModelName <- map["vehicleModelName"]
//        vehicleColorId <- map["vehicleColorId"]
//        vehicleColorName <- map["vehicleColorName"]
//        img <- map["img"]
//    }
//}
class VehicleListResponse: Mappable{
    var id: Int?
    var make: String?
    var model: String?
    var year: String?
    var state: String?
    var vehicleClass: String?
    var bodyClass: String?
    var plate: String?
    var color: String?
    var vin: String?
    var image: String?
    var isDefault: Int?
    var status: Int?
    var img: UIImage?
    var is_added_by_feat_admin: Bool?

    
    required init?(map: ObjectMapper.Map) {
        
    }
    
    func mapping(map: ObjectMapper.Map) {
        id <- map["id"]
        make <- map["make"]
        model <- map["model"]
        year <- map["year"]
        state <- map["state"]
        vehicleClass <- map["class"]
        plate <- map["plate"]
        bodyClass <- map["bodyClass"]
        color <- map["color"]
        vin <- map["vin"]
        image <- map["image"]
        isDefault <- map["isDefault"]
        status <- map["status"]
        img <- map["img"]
        is_added_by_feat_admin <- map["is_added_by_feat_admin"]
    }
    
}
class CouponListResponse: Mappable {

    var couponId: Int?
    var percentage: Int?
    var code: String?
    var description: String?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        couponId <- map["couponId"]
        percentage <- map["percentage"]
        code <- map["code"]
        description <- map["description"]
    }
}
class Service: Mappable {

    var serviceId: Int?
    var name: String?
    var image: String?
    var type: Int?
    var price: Int?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        serviceId <- map["serviceId"]
        name <- map["name"]
        image <- map["image"]
        type <- map["type"]
        price <- map["price"]
    }
}

class DriverVehicle: Mappable {

    var driverVehicleId: NSNumber?
    var image: String?
    var classText: String?
    var vehicleId: String?
    var vehicleTagNo: String?
    var status: Int?
    var isDefault: Int?
    var reason: Any?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        driverVehicleId <- map["driverVehicleId"]
        image <- map["image"]
        classText <- map["classText"]
        vehicleId <- map["vehicleId"]
        vehicleTagNo <- map["vehicleTagNo"]
        status <- map["status"]
        isDefault <- map["isDefault"]
        reason <- map["reason"]
    }
}
class FirebaseAcceptedServiceResponse: Mappable {

    var createdAt: Int?
    var destination_address: String?
    var destination_latitude: Double?
    var destination_longitude: Double?
    var driverOnDestinationLocation: Int?
    var driverStartedAt: Int?
    var driverOnPickupLocation: Int?
    var driver: Driver?
    var driverVehicle: DriverVehicle?
    var image: String?
    var orderId: Int?
    var pickupAddress: String?
    var pickupLatitude: Double?
    var pickupLongitude: Double?
    var plateNumber: String?
    var price: Double?
    var oilProduct: Double?
    var breakProduct: Double?
    var filterPrice: Double?
    var stabilizerPrice: Double?
    var serviceCharge:Double?
    var fuleType: String?
    var fulePrice: Double?
    var driverStartAddress: String?
    var questions: [Questions]?
    var service: Service?
    var status: Int?
    var user: User?
    var vehicleColorId: Int?
    var vehicleColorName: String?
    var vehicleMakeId: Int?
    var vehicleMakeName: String?
    var vehicleModelId: Int?
    var vehicleModelName: String?
    var vehicleYearId: Int?
    var vehicleYearName: String?
    var defaultPayment:DefaultPayment?
    var extraCharge: [ExtraChargeResponse]?
    var old_price: Double?
    var stripeTax: Double?
    var subDiscountPrice: Double?
    var serviceChargeDiscount: Double?
    var fuelPriceDiscount: Double?
    var bookingFeesDiscount: Double?
    var couponAmount: Double?
    var scheduleDate: Int?
    var tip: Int?
    var towWinchPrice: Double?
    var bookingFees: Double?
    var driverGarage: DriverGarageResponse?
    var endRefreshServiceTime: Int?
    
    required init?(map: Map){
    }

    func mapping(map: Map) {
        createdAt <- map["created_at"]
        destination_address <- map["destination_address"]
        destination_latitude <- map["destination_latitude"]
        destination_longitude <- map["destination_longitude"]
        driverOnDestinationLocation <- map["driverOnDestinationLocation"]
        driverStartedAt <- map["driverStartedAt"]
        driver <- map["driver"]
        driverVehicle <- map["driverVehicle"]
        image <- map["image"]
        orderId <- map["orderId"]
        pickupAddress <- map["pickup_address"]
        pickupLatitude <- map["pickup_latitude"]
        pickupLongitude <- map["pickup_longitude"]
        plateNumber <- map["plateNumber"]
        price <- map["price"]
        oilProduct <- map["oilProduct"]
        breakProduct <- map["breakProduct"]
        filterPrice <- map["filterPrice"]
        stabilizerPrice <- map["stabilizerPrice"]
        fuleType <- map["fuleType"]
        questions <- map["questions"]
        service <- map["service"]
        status <- map["status"]
        user <- map["user"]
        vehicleColorId <- map["vehicleColorId"]
        vehicleColorName <- map["vehicleColorName"]
        driverOnPickupLocation <- map["driverOnPickupLocation"]
        vehicleMakeId <- map["vehicleMakeId"]
        vehicleMakeName <- map["vehicleMakeName"]
        vehicleModelId <- map["vehicleModelId"]
        vehicleModelName <- map["vehicleModelName"]
        vehicleYearId <- map["vehicleYearId"]
        vehicleYearName <- map["vehicleYearName"]
        defaultPayment <- map["default_payment"]
        serviceCharge <- map["serviceCharge"]
        extraCharge <- map["extraCharge"]
        old_price <- map["old_price"]
        stripeTax <- map["stripeTax"]
        subDiscountPrice <- map["sub_discount_price"]
        serviceChargeDiscount <- map["serviceChargeDiscount"]
        fuelPriceDiscount <- map["fuelPriceDiscount"]
        bookingFeesDiscount <- map["bookingFeesDiscount"]
        couponAmount <- map["couponAmount"]
        scheduleDate <- map["scheduleDate"]
        driverStartAddress <- map["driverStartAddress"]
        tip <- map["tip"]
        towWinchPrice <- map["towWinchPrice"]
        fulePrice <- map["fulePrice"]
        bookingFees <- map["bookingFees"]
        driverGarage <- map["driver_garage"]
        endRefreshServiceTime <- map["endRefreshServiceTime"]
    }
}

class ExtraChargeResponse: Mappable{
    var orderExtraChargeId: Int?
    var extraChargeCategoryId: Int?
    var extraChargeCategoryName: String?
    var price: Int?
    var reason: String?
    var status: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        orderExtraChargeId <- map["OrderExtraChargeId"]
        extraChargeCategoryId <- map["extraChargeCategoryId"]
        extraChargeCategoryName <- map["extraChargeCategoryName"]
        price <- map["price"]
        reason <- map["reason"]
        status <- map["status"]
    }
}

class User: Mappable {

    var accountType: Int?
    var countryCode: String?
    var email: String?
    var firstName: String?
    var hearReason: String?
    var lastName: String?
    var notification: Bool?
    var phone: String?
    var profile: String?
    var userId: Int?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        accountType <- map["accountType"]
        countryCode <- map["countryCode"]
        email <- map["email"]
        firstName <- map["firstName"]
        hearReason <- map["hearReason"]
        lastName <- map["lastName"]
        notification <- map["notification"]
        phone <- map["phone"]
        profile <- map["profile"]
        userId <- map["userId"]
    }
}

class Driver: Mappable {
    var avgRating: CGFloat?
    var backgroundCheckImage: String?
    var countryCode: String?
    var driverId: Int?
    var email: String?
    var firstName: String?
    var hearReason: String?
    var insuranceBackImage: String?
    var insuranceFrontImage: String?
    var isVehicle: Bool?
    var lastName: String?
    var latitude: Double?
    var licenseBackImage: String?
    var licenseFrontImage: String?
    var licenseNumber: String?
    var longitude: Double?
    var notification: Bool?
    var phone: String?
    var profile: String?
    var status: Int?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        avgRating <- map["avgRating"]
        backgroundCheckImage <- map["backgroundCheckImage"]
        countryCode <- map["countryCode"]
        driverId <- map["driverId"]
        email <- map["email"]
        firstName <- map["firstName"]
        hearReason <- map["hearReason"]
        insuranceBackImage <- map["insuranceBackImage"]
        insuranceFrontImage <- map["insuranceFrontImage"]
        isVehicle <- map["isVehicle"]
        lastName <- map["lastName"]
        latitude <- map["latitude"]
        licenseBackImage <- map["licenseBackImage"]
        licenseFrontImage <- map["licenseFrontImage"]
        licenseNumber <- map["licenseNumber"]
        longitude <- map["longitude"]
        notification <- map["notification"]
        phone <- map["phone"]
        profile <- map["profile"]
        status <- map["status"]
    }
}
class UserdetailData: Mappable{
    var id:Int?
    var firstName:String?
    var lastName:String?
    var profile:String?
    var is_online:Bool?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        is_online <- map["is_online"]
        profile <- map["profile"]
    }
}

class FaceIDResponse: Mappable{
    var faceID:Bool = false
    var email: String?
    var password: String?
    var type: Int?
    var countryCode: String?
    var phone: String?
   
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        faceID <- map["faceID"]
        type <- map["type"]
        countryCode <- map["countryCode"]
        phone <- map["phone"]
        email <- map["email"]
        password <- map["password"]
    }
}

class MessageData: Mappable{
    var id:Int?
    var senderId:Int?
    var receiverId:Int?
    var type:Int?
    var message:String?
    var createdAt:Int?
    var seen:Int?
    var senderType:String?
    var receiverType:String?
    var senderName:String?
    var receiverName:String?
    init(id:Int?,senderId:Int?,receiverId:Int?,type:Int?,message:String?,createdAt:Int?,seen:Int?,senderType:String,receiverType:String?,senderName:String?,receiverName:String?) {
        self.id = id
        self.senderId = senderId
        self.receiverId = receiverId
        self.type = type
        self.message = message
        self.createdAt = createdAt
        self.seen = seen
        self.senderType = senderType
        self.receiverType = receiverType
        self.senderName = senderName
        self.receiverName = receiverName
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        senderId <- map["senderId"]
        receiverId <- map["receiverId"]
        type <- map["type"]
        message <- map["message"]
        createdAt <- map["createdAt"]
        seen <- map["seen"]
        senderType <- map["senderType"]
        receiverType <- map["receiverType"]
        senderName <- map["senderName"]
        receiverName <- map["receiverName"]
        
    }
}

class ChatCount: Mappable{
    var count: Int?
    init(count: Int?) {
        self.count = count
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        count <- map["count"]
    }
}
class FualPriceData: Mappable{
    var state:FualPrices?
    init(state: FualPrices?) {
        self.state = state
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        state <- map["state"]
    }
}

class CancelOrderPriceData: Mappable{
    var amount:Double?
    init(amount: Double?) {
        self.amount = amount
    }
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        amount <- map["amount"]
    }
}

class FualPrices: Mappable{
    var currency: String?
    var name:String?
    var lowerName: String?
    var gasoline:String?
    var midGrade: String?
    var premium:String?
    var diesel:String?
    
    init(currency: String?,name:String?,lowerName: String?,gasoline:String?,midGrade: String?,premium:String?,diesel:String?) {
        self.currency = currency
        self.name = name
        self.lowerName = lowerName
        self.gasoline = gasoline
        self.midGrade = midGrade
        self.premium = premium
        self.diesel = diesel
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        currency <- map["currency"]
        name <- map["name"]
        lowerName <- map["lowerName"]
        gasoline <- map["gasoline"]
        midGrade <- map["midGrade"]
        premium <- map["premium"]
        diesel <- map["diesel"]
    }
}

class SectionMessageData: Mappable{
    var headerInterVal: Int?
    var headerDate:String?
    var messageData:[MessageData]?
    init(headerInterVal: Int? = nil,headerDate:String,messageData:[MessageData]) {
        self.headerInterVal = headerInterVal
        self.headerDate = headerDate
        self.messageData = messageData
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        headerInterVal <- map["headerInterVal"]
        headerDate <- map["headerDate"]
        messageData <- map["messageData"]
      
    }
}
class ReferralCodeResponse: Mappable{
    var totalPrice: NSNumber?
    var inviteCode: String?
    var message: String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        totalPrice <- map["totalPrice"]
        inviteCode <- map["invite_code"]
        message <- map["message"]
    }
}
class CheckOrderStatusResponse: Mappable{
    var isCardAdded: Int?
    var isUpCommingJobExist: Int?
    var isNextHoureJobComming: Int?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        isCardAdded <- map["isCardAdded"]
        isUpCommingJobExist <- map["isUpCommingJobExist"]
        isNextHoureJobComming <- map["isNextHoureJobComming"]
    }
}
class NearByGasStationList: Mappable{
    var name: String?
    var lat: Double?
    var lng: Double?
    var image: String?
    var distance: Double?
    
    required init?(map: ObjectMapper.Map) {
        
    }
    
    func mapping(map: ObjectMapper.Map) {
        name <- map["name"]
        lat <- map["lat"]
        lng <- map["lng"]
        image <- map["image"]
        distance <- map["distance"]
    }
}
class AdvertisementList: Mappable{
    var shop_name: String?
    var description: String?
    var address: String?
    var phone: String?
    var latitude: String?
    var longitude: String?
    var image: String?
    var gradientImage: UIImage?
    
    required init?(map: ObjectMapper.Map) {
        
    }
    
    func mapping(map: ObjectMapper.Map) {
        shop_name <- map["shop_name"]
        description <- map["description"]
        address <- map["address"]
        phone <- map["phone"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        image <- map["image"]
        gradientImage <- map["gradientImage"]
    }
}
class NotificationResponse: Mappable{
    var notification_id: Int?
    var title: String?
    var body: String?
    var type: Int?
    var driverId: Int?
    var userId: Int?
    var orderId: Int?
    var createdAt: Int?
    var feat_admin_id: String?
    var feat_admin_int_id: Int?
    
    var notificationType: String?
    
    required init?(map: ObjectMapper.Map) {
        
    }
    
    func mapping(map: ObjectMapper.Map) {
        notification_id <- map["notification_id"]
        title <- map["title"]
        body <- map["body"]
        type <- map["type"]
        driverId <- map["driverId"]
        userId <- map["userId"]
        orderId <- map["orderId"]
        createdAt <- map["createdAt"]
        feat_admin_id <- map["feat_admin_id"]
        feat_admin_int_id <- map["feat_admin_id"]
        
        notificationType <- map["type"]
    }
}
class UsStateResponse: Mappable{
    var id: Int?
    var name: String?
    var iso2: String?
    var created_at: String?
    var updated_at: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        iso2 <- map["iso2"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
    }
}
class SubscriptionResponseData: Mappable {

    var isPremium: Bool?
    var subscription: SubscriptionData?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        isPremium <- map["is_premium"]
        subscription <- map["subscription"]
    }
}

class SubscriptionData: Mappable {

    var productId: String?
    var expiryDate: String?

    required init?(map: Map){
    }

    func mapping(map: Map) {
        productId <- map["product_id"]
        expiryDate <- map["expiry_date"]
    }
}

class DriverGarageResponse: Mappable{
    var vehicleClass: String?
    var image: String?
    var make: String?
    var model: String?
    var plate: String?
    var color: String?
    var id: Int?
    
    required init?(map: ObjectMapper.Map) {
        
    }
    
    func mapping(map: ObjectMapper.Map) {
        vehicleClass <- map["class"]
        image <- map["image"]
        make <- map["make"]
        model <- map["model"]
        plate <- map["plate"]
        color <- map["color"]
        id <- map["id"]
    }
    
}
