//
//  Response.swift
//  iTowu
//
//  Created by Nikunj on 16/10/21.
//

import Foundation
import ObjectMapper

class Response: Mappable{
    var success: Bool?
    var message: String?
    var loginResponse: LoginResponse?
    var serviceRespose: [ServiceResponse]?
    var cardResponse: [CardResponse]?
    var addCardResponse: CardResponse?
    var vehicleMakeResose: [VehicleMakeResponse]?
    var vehicleYearResponse: [VehicleYearRespose]?
    var vehicleColorResponse: [VehicleColorResponse]?
    var placeOrderResponse: PlaceOrderResponse?
    var orderStatusResponse: OrderStatusResponse?
    var vehicleListResponse: [VehicleListResponse]?
    var vehicleEditResponse: VehicleListResponse?
    var couponListResponse: [CouponListResponse]?
    var orderHistoryResponse: [OrderHistoryResponse]?
    var firebaseAcceptedServiceResponse:FirebaseAcceptedServiceResponse?
    var serviceHistoryResponse:[FirebaseAcceptedServiceResponse]?
    var messageData:[MessageData]?
    var fualPriceData:FualPriceData?
    var cancelOrderPriceData:CancelOrderPriceData?
    var userdetailData:UserdetailData?
    var chatCount:ChatCount?
    var referralResponse: ReferralCodeResponse?
    var versionResponse:VersionResponse?
    var checkOrderStatusResponse: CheckOrderStatusResponse?
    var nearByStatationResponse: [NearByGasStationList]?
    var advertisementResponse: [AdvertisementList]?
    var notificationResponse: [NotificationResponse]?
    var usStateResponse: [UsStateResponse]?
    var subscriptionResponseData: SubscriptionResponseData?
    var meta: Meta?
    
    required init?(map: Map) {
    
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        message <- map["message"]
        loginResponse <- map["data"]
        serviceRespose <- map["data"]
        cardResponse <- map["data"]
        vehicleMakeResose <- map["data"]
        vehicleYearResponse <- map["data"]
        vehicleColorResponse <- map["data"]
        placeOrderResponse <- map["data"]
        orderStatusResponse <- map["data"]
        vehicleListResponse <- map["data"]
        addCardResponse <- map["data"]
        vehicleEditResponse <- map["data"]
        couponListResponse <- map["data"]
        orderHistoryResponse <- map["data"]
        firebaseAcceptedServiceResponse <- map["data"]
        serviceHistoryResponse <- map["data"]
        fualPriceData <- map["data"]
        cancelOrderPriceData <- map["data"]
        userdetailData <- map["userdetail"]
        messageData <- map["data"]
        chatCount <- map["data"]
        referralResponse <- map["data"]
        versionResponse <- map["data"]
        checkOrderStatusResponse <- map["data"]
        nearByStatationResponse <- map["data"]
        advertisementResponse <- map["data"]
        notificationResponse <- map["data"]
        usStateResponse <- map["data"]
        subscriptionResponseData <- map["data"]
        meta <- map["meta"]
    }
}
