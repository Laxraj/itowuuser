//
//  RequestModel.swift
//  iTowu
//
//  Created by Nikunj on 14/10/21.
//

import Foundation
import ObjectMapper

class OnBoardRequest: Mappable{
    var image: UIImage?
    var description: String?
    
    init(image: UIImage?,description: String?) {
        self.image = image
        self.description = description
    }
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        image <- map["image"]
        description <- map["description"]
    }
}

class OnMenuOptionRequest: Mappable{
    var image: String?
    var title: String?
    
    init(image: String?,title: String?) {
        self.image = image
        self.title = title
    }
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        image <- map["image"]
        title <- map["title"]
    }
}

class RegisterRequest: Mappable{
    var firstName: String?
    var lastName: String?
    var countryCode: String?
    var phone: String?
    var email: String?
    var password: String?
    var password_confirmation: String?
    var hearReason: String?
    var invite_code: String?
    
    init(firstName: String?,lastName: String?,countryCode: String?,phone: String?,email: String?,password: String?,password_confirmation: String?,hearReason: String?,invite_code: String?) {
        self.firstName = firstName
        self.lastName = lastName
        self.countryCode = countryCode
        self.phone = phone
        self.email = email
        self.password = password
        self.password_confirmation = password_confirmation
        self.hearReason = hearReason
        self.invite_code = invite_code
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        countryCode <- map["countryCode"]
        phone <- map["phone"]
        email <- map["email"]
        password <- map["password"]
        password_confirmation <- map["password_confirmation"]
        hearReason <- map["hearReason"]
        invite_code <- map["invite_code"]
    }
}

class RegisterWithSocialRequest: Mappable{
    var firstName: String?
    var lastName: String?
    var countryCode: String?
    var phone: String?
    var email: String?
    var providerType:String?
    var providerId:String?
    
    init(firstName: String?,lastName: String?,countryCode: String?,phone: String?,email: String?,providerType: String?,providerId: String? ) {
        self.firstName = firstName
        self.lastName = lastName
        self.countryCode = countryCode
        self.phone = phone
        self.email = email
        self.providerType = providerType
        self.providerId = providerId
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        countryCode <- map["countryCode"]
        phone <- map["phone"]
        email <- map["email"]
        providerType <- map["providerType"]
        providerId <- map["providerId"]
    }
}

class LoginRequest: Mappable {

    var type: Int?
    var countryCode: String?
    var phone: String?
    var email: String?
    var password: String?
    
    init(type: Int?,countryCode: String?,phone: String?,email: String?,password: String?) {
        self.type = type
        self.countryCode = countryCode
        self.phone = phone
        self.email = email
        self.password = password
    }

    required init?(map: Map){
    }

    func mapping(map: Map) {
        type <- map["type"]
        countryCode <- map["countryCode"]
        phone <- map["phone"]
        email <- map["email"]
        password <- map["password"]
    }
}
class ChangePasswordRequest: Mappable {
    var current_password: String?
    var password: String?
    var passwordConfirmation: String?
    
    init(current_password: String?,password: String?,passwordConfirmation: String?) {
        self.current_password = current_password
        self.password = password
        self.passwordConfirmation = passwordConfirmation
    }

    required init?(map: Map){
    }

    func mapping(map: Map) {
        current_password <- map["current_password"]
        password <- map["password"]
        passwordConfirmation <- map["password_confirmation"]
    }
}

class RegisterForPushRequest: Mappable {
    var device_id: String?
    var device_type: String?
    var device_token: String?
    var type: String?
    init(device_id: String?,device_type: String?,device_token: String?,type: String?) {
        self.device_id = device_id
        self.device_type = device_type
        self.device_token = device_token
        self.type = type
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        device_id <- map["device_id"]
        device_type <- map["device_type"]
        device_token <- map["device_token"]
        type <- map["type"]
    }
}

class ForgotPasswordEmailRequest: Mappable{
    var email: String?
    var countryCode:String?
    var type:String?
    var phone:String?
    var otp:String?
    var password:String?
    var password_confirmation:String?
    
    
    init(email: String?,countryCode: String?,type:String?,phone:String?,otp:String?,password:String?,password_confirmation:String?) {
        self.email = email
        self.countryCode = countryCode
        self.type = type
        self.phone = phone
        self.otp = otp
        self.password = password
        self.password_confirmation = password_confirmation
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        email <- map["email"]
        countryCode <- map["countryCode"]
        type <- map["type"]
        phone <- map["phone"]
        otp <- map["otp"]
        password <- map["password"]
        password_confirmation <- map["password_confirmation"]
    }
}

class EmailVerificationRequest: Mappable{
    var email: String?
    var otp:String?
    var phone:String?
    var countryCode:String?
    
    init(email: String?,otp:String?,phone:String?,countryCode:String?) {
        self.email = email
        self.otp = otp
        self.phone = phone
        self.countryCode = countryCode
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        email <- map["email"]
        otp <- map["otp"]
        phone <- map["phone"]
        countryCode <- map["countryCode"]
    }
}

class PhoneVerificationRequest: Mappable{
    var countryCode: String?
    var phone: String?
    var otp:String?
    
    init(countryCode: String?,phone: String?,otp:String?) {
        self.countryCode = countryCode
        self.phone = phone
        self.otp = otp
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        countryCode <- map["countryCode"]
        phone <- map["phone"]
        otp <- map["otp"]
    }
}

class ResendVerificationRequest: Mappable{
    var email: String?
    var countryCode: String?
    var phone: String?
    
    init(email: String?,countryCode: String?,phone: String?) {
        self.email = email
        self.countryCode = countryCode
        self.phone = phone
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        email <- map["email"]
        countryCode <- map["countryCode"]
        phone <- map["phone"]
    }
}

class ChangeEmailRequest: Mappable{
    var email: String?
    
    init(email: String?) {
        self.email = email
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        email <- map["email"]
    }
}

class ChangePhoneNumberRequest: Mappable{
    var userId: Int?
    var countryCode: String?
    var phone: String?
    var email: String?
    
    init(userId: Int? = nil,countryCode: String?, phone: String?,email: String? = nil) {
        self.userId = userId
        self.countryCode = countryCode
        self.phone = phone
        self.email = email
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        userId <- map["userId"]
        countryCode <- map["countryCode"]
        phone <- map["phone"]
        email <- map["email"]
    }
}

class LogOutRequest: Mappable{
    var deviceId: String?
    
    init(deviceId: String?) {
        self.deviceId = deviceId
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        deviceId <- map["deviceId"]
    }
}
class CheckVersionRequest: Mappable{
    var currentVersion: String?
    var deviceType: String?
    var appType:String?
    
    init(currentVersion: String?,deviceType: String?,appType:String?){
        self.currentVersion = currentVersion
        self.deviceType = deviceType
        self.appType = appType
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        currentVersion <- map["currentVersion"]
        deviceType <- map["deviceType"]
        appType <- map["appType"]
    }
}

class UpdateProfileRequest: Mappable{
    var firstName: String?
    var lastName: String?
    var countryCode: String?
    var phone: String?
    var email: String?
    var notification: String?
    
    init(firstName: String?,lastName: String?,countryCode: String?,phone: String?,email: String?,notification: String?) {
        self.firstName = firstName
        self.lastName = lastName
        self.countryCode = countryCode
        self.phone = phone
        self.email = email
        self.notification = notification
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        firstName <- map["firstName"]
        lastName <- map["lastName"]
        countryCode <- map["countryCode"]
        phone <- map["phone"]
        email <- map["email"]
        notification <- map["notification"]
    }
}
class PlaceOrderRequest: Mappable{
    var serviceId: String?
    var user_garage_id: String?
//    var plateNumber: String?
//    var vehicleYearId: String?
//    var vehicleMakeId: String?
//    var vehicleModelId: String?
//    var vehicleColorId: String?
    var pickup_latitude: String?
    var pickup_longitude: String?
    var pickup_address: String?
    var destination_latitude: String?
    var destination_longitude: String?
    var destination_address: String?
    var in_garage: String?
    var questions: String?
    var fuleType:String?
    var state:String?
    var scheduleDate: String?
    var autoTransportStartDate: String?
    var deviceType: String?
    
    //MARK: NEW CHANGES
    var code: String?
    var payment_id: String?
    
    init(serviceId: String?,user_garage_id: String?/*,plateNumber: String?,vehicleYearId: String?,vehicleMakeId: String?,vehicleModelId: String?,vehicleColorId: String?*/,pickup_latitude: String?,pickup_longitude: String?,pickup_address: String?,destination_latitude: String?,destination_longitude: String?,destination_address: String?,in_garage: String?,questions: String?,fuleType:String?,state:String?,scheduleDate: String?,autoTransportStartDate: String?,deviceType: String?) {
        self.serviceId = serviceId
//        self.plateNumber = plateNumber
//        self.vehicleYearId = vehicleYearId
//        self.vehicleMakeId = vehicleMakeId
//        self.vehicleModelId = vehicleModelId
//        self.vehicleColorId = vehicleColorId
        self.user_garage_id = user_garage_id
        self.pickup_latitude = pickup_latitude
        self.pickup_longitude = pickup_longitude
        self.pickup_address = pickup_address
        self.destination_address = destination_address
        self.destination_latitude = destination_latitude
        self.destination_longitude = destination_longitude
        self.in_garage = in_garage
        self.questions = questions
        self.fuleType = fuleType
        self.state = state
        self.scheduleDate = scheduleDate
        self.autoTransportStartDate = autoTransportStartDate
        self.deviceType = deviceType
    }
        
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        serviceId <- map["serviceId"]
        user_garage_id <- map["user_garage_id"]
//        plateNumber <- map["plateNumber"]
//        vehicleYearId <- map["vehicleYearId"]
//        vehicleMakeId <- map["vehicleMakeId"]
//        vehicleModelId <- map["vehicleModelId"]
//        vehicleColorId <- map["vehicleColorId"]
        pickup_latitude <- map["pickup_latitude"]
        pickup_longitude <- map["pickup_longitude"]
        pickup_address <- map["pickup_address"]
        destination_latitude <- map["destination_latitude"]
        destination_longitude <- map["destination_longitude"]
        destination_address <- map["destination_address"]
        in_garage <- map["in_garage"]
        questions <- map["questions"]
        fuleType <- map["fuleType"]
        state <- map["state"]
        scheduleDate <- map["scheduleDate"]
        autoTransportStartDate <- map["autoTransportStartDate"]
        deviceType <- map["deviceType"]
        
        code <- map["code"]
        payment_id <- map["payment_id"]
    }
}
class AddQuetionRequest: Mappable{
    var questionId: String?
    var answer: String?
    
    init(questionId: String?,answer: String?) {
        self.questionId = questionId
        self.answer = answer
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        questionId <- map["questionId"]
        answer <- map["answer"]
    }
}
class VehicleEditRequest: Mappable{
    var plateNumber: String?
    var vehicleYearId: String?
    var vehicleMakeId: String?
    var vehicleModelId: String?
    var vehicleColorId: String?
    
    init(plateNumber: String?,vehicleYearId: String?,vehicleMakeId: String?,vehicleModelId: String?,vehicleColorId: String?) {
        self.plateNumber = plateNumber
        self.vehicleYearId = vehicleYearId
        self.vehicleMakeId = vehicleMakeId
        self.vehicleModelId = vehicleModelId
        self.vehicleColorId = vehicleColorId
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        plateNumber <- map["plateNumber"]
        vehicleYearId <- map["vehicleYearId"]
        vehicleMakeId <- map["vehicleMakeId"]
        vehicleModelId <- map["vehicleModelId"]
        vehicleColorId <- map["vehicleColorId"]
    }
}
class CardAddEditRequest: Mappable{
    var payment_id: String?
    var card_holder_name: String?
    var card_number: String?
    var expiration_date: String?
    var cvv: String?
    
    init(payment_id: String?,card_holder_name: String?,card_number: String?,expiration_date: String?,cvv: String?) {
        self.payment_id = payment_id
        self.card_holder_name = card_holder_name
        self.card_number = card_number
        self.expiration_date = expiration_date
        self.cvv = cvv
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        payment_id <- map["payment_id"]
        card_holder_name <- map["card_holder_name"]
        card_number <- map["card_number"]
        expiration_date <- map["expiration_date"]
        cvv <- map["cvv"]
    }
}

class PaymentRequest: Mappable{
    var orderId: String?
    var payment_id: String?
    var price: String?
    
    init(orderId: String?,payment_id: String?,price: String?) {
        self.orderId = orderId
        self.payment_id = payment_id
        self.price = price
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        orderId <- map["orderId"]
        payment_id <- map["payment_id"]
        price <- map["price"]
    }
}

class FirebaseVehicleRequest: Mappable{
    var vehicleUserPinImage: String?
    var pickUpLongtitude: String?
    var pickUpLatitude: String?
    var destinationLongtitude: String?
    var destinationLatitude: String?
    
    init(vehicleUserPinImage: String?,pickUpLatitude: String?,pickUpLongtitude: String?,destinationLatitude: String?,destinationLongtitude: String?) {
        self.vehicleUserPinImage = vehicleUserPinImage
        self.pickUpLongtitude = pickUpLongtitude
        self.pickUpLatitude = pickUpLatitude
        self.destinationLongtitude = destinationLongtitude
        self.destinationLatitude = destinationLatitude
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        vehicleUserPinImage <- map["vehicleUserPinImage"]
        pickUpLongtitude <- map["pickUpLongtitude"]
        pickUpLatitude <- map["pickUpLatitude"]
        destinationLongtitude <- map["destinationLongtitude"]
        destinationLatitude <- map["destinationLatitude"]
    }
}
class FirebaseVehiclePickUpRequest: Mappable{
    var pickUpLongtitude: Double?
    var pickUpLatitude: Double?
    
    init(pickUpLatitude: Double?,pickUpLongtitude: Double?) {
        self.pickUpLongtitude = pickUpLongtitude
        self.pickUpLatitude = pickUpLatitude
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        pickUpLongtitude <- map["pickup_longitude"]
        pickUpLatitude <- map["pickup_latitude"]
    }
}
class CancelOrderRequest: Mappable{
    var orderId: Int?
    var latitude:String?
    var longitude:String?
    var cancelReason: String?
    
    init(orderId: Int?,latitude:String?,longitude:String?,cancelReason: String? = nil) {
        self.orderId = orderId
        self.latitude = latitude
        self.longitude = longitude
        self.cancelReason = cancelReason
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        orderId <- map["orderId"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        cancelReason <- map["cancelReason"]
    }
}

class CancelOrderPriceRequest: Mappable{
    var orderId: Int?
    var latitude:String?
    var longitude:String?
    init(orderId: Int?,latitude:String?,longitude:String?) {
        self.orderId = orderId
        self.latitude = latitude
        self.longitude = longitude
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        orderId <- map["orderId"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }
}

class ReviewRequest: Mappable{
    var orderId: Int?
    var driverId: Int?
    var rating: Double?
    var feedback: String?
    var tip: Double?
    init(orderId: Int?,driverId: Int?,rating: Double?,feedback: String?,tip: Double?) {
        self.orderId = orderId
        self.driverId = driverId
        self.rating = rating
        self.feedback = feedback
        self.tip = tip
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        orderId <- map["orderId"]
        driverId <- map["driverId"]
        rating <- map["rating"]
        feedback <- map["feedback"]
        tip <- map["tip"]
    }
}

class UpdateServiceStatusRequest: Mappable{
    var status: Int?
    
    init(status: Int?) {
        self.status = status
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status <- map["status"]
    }
}

class OrderDriverRequest: Mappable{
    var orderId: String?
    var status: String?
    var reason: String?
    var payment_id:String?
    var code: String?
    var plateNumber: String?
    var vehicleYearId: String?
    var vehicleMakeId: String?
    var vehicleModelId: String?
    var vehicleColorId: String?
    
    init(orderId: String?,status: String?,reason: String?,payment_id:String?,code: String?,plateNumber: String?,vehicleYearId: String?,vehicleMakeId: String?,vehicleModelId: String?,vehicleColorId: String?) {
        self.orderId = orderId
        self.status = status
        self.reason = reason
        self.payment_id = payment_id
        self.code = code
        self.plateNumber = plateNumber
        self.vehicleYearId = vehicleYearId
        self.vehicleMakeId = vehicleMakeId
        self.vehicleModelId = vehicleModelId
        self.vehicleColorId = vehicleColorId
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        orderId <- map["orderId"]
        status <- map["status"]
        reason <- map["reason"]
        payment_id <- map["payment_id"]
        code <- map["code"]
        plateNumber <- map["plateNumber"]
        vehicleYearId <- map["vehicleYearId"]
        vehicleMakeId <- map["vehicleMakeId"]
        vehicleModelId <- map["vehicleModelId"]
        vehicleColorId <- map["vehicleColorId"]
    }
}
class ExtraChargesRequest: Mappable{
    
    var orderExtraChargeId: Int?
    var status: String?
    
    init(orderExtraChargeId: Int?,status: String?) {
        self.orderExtraChargeId = orderExtraChargeId
        self.status = status
    }

    required init?(map: Map){
    }

    func mapping(map: Map) {
        orderExtraChargeId <- map["OrderExtraChargeId"]
        status <- map["status"]
    }
}
class OrderCheckCouponRequest: Mappable{
    var orderId: Int?
    var code: String?
    var referralCodeCheck: Int?
    
    init(orderId: Int?,code: String?,referralCodeCheck: Int?) {
        self.orderId = orderId
        self.code = code
        self.referralCodeCheck = referralCodeCheck
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        orderId <- map["orderId"]
        code <- map["code"]
        referralCodeCheck <- map["referralCodeCheck"]
    }
}
class ImageNameRequest: Mappable{
    var imageName: String?
    var imageData: Data?
    
    init(imageName: String?,imageData: Data?) {
        self.imageName = imageName
        self.imageData = imageData
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        imageName <- map["imageName"]
        imageData <- map["imageData"]
    }
}
class GarageVehicleDetailRequest: Mappable{
    var state: String?
    var plate: String?
    
    init(state: String? = nil, plate: String? = nil) {
        self.state = state
        self.plate = plate
    }
    
    required init?(map: ObjectMapper.Map) {
        
    }
    
    func mapping(map: ObjectMapper.Map) {
        state <- map["state"]
        plate <- map["plate"]
    }
}
class GarageAddVehicleRequest: Mappable{
    var state: String?
    var color: String?
    var make: String?
    var model: String?
    var year: String?
    var vehicleClass: String?
    var vin: String?
    var plate: String?
    var bodyClass: String?
    
    init(){
        
    }

    init(state: String?, color: String?, make: String?,model: String?, year: String?,vehicleClass: String?,vin: String?,plate: String?, bodyClass: String?) {
        self.state = state
        self.color = color
        self.make = make
        self.model = model
        self.year = year
        self.vehicleClass = vehicleClass
        self.vin = vin
        self.plate = plate
        self.bodyClass = bodyClass
    }
    
    required init?(map: ObjectMapper.Map) {
        
    }
    
    func mapping(map: ObjectMapper.Map) {
        state <- map["state"]
        color <- map["color"]
        make <- map["make"]
        model <- map["model"]
        year <- map["year"]
        vehicleClass <- map["class"]
        vin <- map["vin"]
        plate <- map["plate"]
        bodyClass <- map["bodyClass"]
    }
}
class WindowAnswerRequest: Mappable{
    var name: String?
    var percentage: String?
    
    init(name: String?, percentage: String?) {
        self.name = name
        self.percentage = percentage
    }
    
    required init?(map: ObjectMapper.Map) {
        
    }
    
    func mapping(map: ObjectMapper.Map) {
        name <- map["name"]
        percentage <- map["percentage"]
    }
}

class FeatAdminJoinRequest: Mappable{
    var feat_admin_id: String?
    var status: Int?
    var notification_id: Int?
    
    init(feat_admin_id: String?,status: Int?,notification_id: Int?) {
        self.feat_admin_id = feat_admin_id
        self.status = status
        self.notification_id = notification_id
    }
    
    required init?(map: ObjectMapper.Map) {
        
    }
    
    func mapping(map: ObjectMapper.Map) {
        feat_admin_id <- map["feat_admin_id"]
        status <- map["status"]
        notification_id <- map["notification_id"]
    }
}

class PurchaseSubscriptionRequest: Mappable{
    
    var deviceType: String?
    var receipt: String?
    var productId: String?
    var isTestEnvironment: Bool?
    
    
    init(deviceType: String? = nil,receipt: String?,productId: String? = nil,isTestEnvironment:Bool?) {
        
        self.deviceType = deviceType
        self.receipt = receipt
        self.productId = productId
        self.isTestEnvironment = isTestEnvironment
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        deviceType <- map["device_type"]
        receipt <- map["receipt"]
        productId <- map["product_id"]
        isTestEnvironment <- map["isTestEnvironment"]
    }
}

class CheckSubscriptionRequest: Mappable{
    
    var deviceType: String?
    
    init(deviceType: String? = nil) {
        
        self.deviceType = deviceType
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        deviceType <- map["device_type"]
    }
}

class ReasonsRequest: Mappable{
    var reason: String?
    var selected: Bool?
    var isManulReason: Bool?
    var otherReason: String?
    
    init(reason: String?, selected: Bool = false, isManulReason: Bool = false,otherReason: String? = nil) {
        self.reason = reason
        self.selected = selected
        self.isManulReason = isManulReason
        self.otherReason = otherReason
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        reason <- map["reason"]
        selected <- map["selected"]
        isManulReason <- map["isManulReason"]
        otherReason <- map["otherReason"]
    }
}
