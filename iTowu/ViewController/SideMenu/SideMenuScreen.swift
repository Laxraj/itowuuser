//
//  SideMenuScreen.swift
//  iTowu
//
//  Created by iroid on 16/10/21.
//

import UIKit
import LGSideMenuController
import MessageUI
class SideMenuScreen: UIViewController {

    @IBOutlet weak var profileImageView: dateSportImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var emaiLabel: UILabel!
    
    @IBOutlet weak var menuOptionTableView: UITableView!
    var menuOptionData = [OnMenuOptionRequest]()
    var sideMenuIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intialDetail()
    }
    
 
    func intialDetail(){
        let nib = UINib(nibName: "MenuOptionCell", bundle: nil)
        menuOptionTableView.register(nib, forCellReuseIdentifier: "MenuOptionCell")
        menuOptionData = [
            OnMenuOptionRequest(image: "Location_icon", title: "HOME"),
            OnMenuOptionRequest(image: "garage_icon", title: "GARAGE"),
            OnMenuOptionRequest(image: "time_icon", title: "SCHEDULE"),
            OnMenuOptionRequest(image: "service_history_icon", title: "SERVICE HISTORY"),
            OnMenuOptionRequest(image: "payment_icon", title: "PAYMENTS"),
            OnMenuOptionRequest(image: "manage_subscription_icon", title: "SUBSCRIPTION"),
            OnMenuOptionRequest(image: "PromoCode_icon", title: "PROMO CODE"),
            OnMenuOptionRequest(image: "gift_icon", title: "REFERRAL"),
            OnMenuOptionRequest(image: "about_us_icon", title: "HELP"),
//            OnMenuOptionRequest(image: "user_icon", title: "DELETE ACCOUNT"),
//            OnMenuOptionRequest(image: "contact_us_icon", title: "CONTACT US"),
//            OnMenuOptionRequest(image: "about_us_icon", title: "ABOUT US"),
//            OnMenuOptionRequest(image: "privacy_policy_icon", title: "PRIVACY POLICY"),
//            OnMenuOptionRequest(image: "faq_icon", title: "FAQ"),
            OnMenuOptionRequest(image: "Logout_icon", title: "LOG OUT")
        ]
    }

    func displayLogOutDialogue(){
        let alertController = UIAlertController(title: APPLICATION_NAME, message: "Are you sure you want to logout?", preferredStyle: .alert)
        let resendAction = UIAlertAction(title: "Yes", style: .default, handler: { [weak self] (action) in
            self?.logOut()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(cancelAction)
        alertController.addAction(resendAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func deleteAccountAlert() {
        let alertController = UIAlertController(title: APPLICATION_NAME, message: "Are you sure you want to delete your account? All of your information and content will be deleted.", preferredStyle: UIAlertController.Style.alert)
        let resendAction = UIAlertAction(title: "Yes", style: .default, handler: { [weak self] (action) in
            self?.deleteUserApi()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(cancelAction)
        alertController.addAction(resendAction)
        self.present(alertController, animated: true, completion: nil)
    }

    
    @IBAction func onCancel(_ sender: UIButton) {
        self.sideMenuController?.hideLeftView()
        if let vc = homeVC as? MapScreen{
//            vc.observeOrder()
        }
    }
    
    @IBAction func onEdit(_ sender: UIButton) {
        self.sideMenuIndex = 0
        self.sideMenuController?.hideLeftView()
        let vc = STORYBOARD.authentication.instantiateViewController(withIdentifier: "ProfileScreen") as! ProfileScreen
        homeVC?.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func onSetting(_ sender: UIButton) {
//        if #available(iOS 13.0, *) {
//            overrideUserInterfaceStyle = .dark
//        }
        self.sideMenuController?.hideLeftView()
        let vc = STORYBOARD.authentication.instantiateViewController(withIdentifier: "ModeScreen") as! ModeScreen
        homeVC?.navigationController?.pushViewController(vc, animated: true)
//        appDelegate.window?.overrideUserInterfaceStyle = .dark
    }

    
    func logOut(){
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = LogOutRequest(deviceId: DEVICE_UNIQUE_IDETIFICATION)
            AuthenticationService.shared.logOut(parameters: data.toJSON()) { (statusCode, response) in
                Utility.hideIndicator()
                Utility.removeUserData()
                Utility.setLoginRootScreen()
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }
    }
    
    func deleteUserApi(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            AuthenticationService.shared.deleteUserAccount{ [weak self] (statusCode, response) in
                Utility.hideIndicator()
                //                if let res = response{
                SocketHelper.shared.disconnectSocket()
                Utility.removeUserData()
                Utility.setLoginRootScreen()
                //                }
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func openMail(){
        let emailTitle = ""
        let messageBody = ""
        let toRecipents = ["contact@itowu.com"]
        let mc: MFMailComposeViewController = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        mc.setSubject(emailTitle)
        mc.setMessageBody(messageBody, isHTML: false)
        mc.setToRecipients(toRecipents)
        self.present(mc, animated: true, completion: nil)
    }
}
extension SideMenuScreen: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuOptionData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuOptionCell", for: indexPath) as! MenuOptionCell
        cell.item = menuOptionData[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.sideMenuIndex = indexPath.row
        if indexPath.row == 0{
            self.sideMenuController?.hideLeftView()
            if let vc = homeVC as? MapScreen{
                vc.observeOrder()
            }
//            self.sideMenuController?.showLeftViewAnimated(sender: self)
        }else if indexPath.row == 1{
            self.sideMenuController?.hideLeftView()
            let vc = STORYBOARD.garage.instantiateViewController(withIdentifier: "GarageScreen") as! GarageScreen
            homeVC?.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 2{
            self.sideMenuController?.hideLeftView()
            let vc = STORYBOARD.service.instantiateViewController(withIdentifier: "ServiceHistoryScreen") as! ServiceHistoryScreen
            vc.isFromSchedule = true
            homeVC?.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 3{
            self.sideMenuController?.hideLeftView()
            let vc = STORYBOARD.service.instantiateViewController(withIdentifier: "ServiceHistoryScreen") as! ServiceHistoryScreen
            homeVC?.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 4{
            self.sideMenuController?.hideLeftView()
            let vc = STORYBOARD.payment.instantiateViewController(withIdentifier: "CardListScreen") as! CardListScreen
            homeVC?.navigationController?.pushViewController(vc, animated: true)
        } else if indexPath.row == 5{
            self.sideMenuController?.hideLeftView()
            let vc = STORYBOARD.subscription.instantiateViewController(withIdentifier: "SubscriptionScreen") as! SubscriptionScreen
            vc.isFromSideMenu = true
            homeVC?.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 6{
            self.sideMenuController?.hideLeftView()
            let vc = STORYBOARD.promocode.instantiateViewController(withIdentifier: "PromoCodeScreen") as! PromoCodeScreen
            homeVC?.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 7{
            self.sideMenuController?.hideLeftView()
            let vc = STORYBOARD.refral.instantiateViewController(withIdentifier: "RefralFriendScreen") as! RefralFriendScreen
            homeVC?.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 8{
            self.sideMenuController?.hideLeftView()
            let vc = STORYBOARD.help.instantiateViewController(withIdentifier: "HelpScreen") as! HelpScreen
            homeVC?.navigationController?.pushViewController(vc, animated: true)

//            self.openMail()
//        }else if indexPath.row == 8{
//            self.sideMenuController?.hideLeftView()
//            let vc = STORYBOARD.webView.instantiateViewController(withIdentifier: "WebViewScreen") as! WebViewScreen
//            vc.linkUrl =  aboutUsURL
//            vc.titlaString = "ABOUT US"
//            homeVC?.navigationController?.pushViewController(vc, animated: true)
//        }else if indexPath.row == 9{
//            self.sideMenuController?.hideLeftView()
//            let vc = STORYBOARD.webView.instantiateViewController(withIdentifier: "WebViewScreen") as! WebViewScreen
//            vc.linkUrl = privacyPolicyURL
//            vc.titlaString = "PRIVACY POLICY"
//            homeVC?.navigationController?.pushViewController(vc, animated: true)
//        }else if indexPath.row == 10{
//            self.sideMenuController?.hideLeftView()
//            let vc = STORYBOARD.webView.instantiateViewController(withIdentifier: "WebViewScreen") as! WebViewScreen
//            vc.linkUrl =  faqUsURL
//            vc.titlaString = "FAQ"
//            homeVC?.navigationController?.pushViewController(vc, animated: true)
//        }else if indexPath.row == 8{
//            self.deleteAccountAlert()
//        }
        
        } else if indexPath.row == 9{
            self.displayLogOutDialogue()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
extension SideMenuScreen: LGSideMenuDelegate{
    func didTransformRootView(sideMenuController: LGSideMenuController, percentage: CGFloat) {
        
    }
    
    func didTransformLeftView(sideMenuController: LGSideMenuController, percentage: CGFloat) {
        print("didTransformLeftView")
    }
    
    func didTransformRightView(sideMenuController: LGSideMenuController, percentage: CGFloat) {
        print("didTransformRightView")

    }
    
    func didShowLeftView(sideMenuController: LGSideMenuController) {
        print("didShowLeftView")
        if let data = Utility.getUserData(){
            Utility.setImage(data.profile, imageView: self.profileImageView)
            self.userNameLabel.text = (data.firstName ?? "")+" "+(data.lastName ?? "")
            self.emaiLabel.text = data.email
        }

    }
    
    func willShowLeftView(_ leftView: UIView, sideMenuController: LGSideMenuController) {
        self.hidesBottomBarWhenPushed = false
//        let userData = Utility.getUserData()
//        if userData?.phone == nil{
//            exclamationImageView.isHidden = false
//        }else{
//            exclamationImageView.isHidden = true
//        }
//        if let profileImageUrl = userData?.profile{
//            Utility.setImage(profileImageUrl, imageView: profileImageView)
//            profileImageView.contentMode = .scaleAspectFill
//            profileImageView.clipsToBounds = true
//        }
//        getTaskDetail()
//        self.nameLabel.text = (userData?.family_name ?? "")+" "+(userData?.name ?? "")//"\((userData?.family_name) ??) \(userData?.name ?? "")"

    }
    
    func didHideLeftView(sideMenuController: LGSideMenuController) {
        if self.sideMenuIndex == nil{
            if let vc = homeVC as? MapScreen{
                vc.observeOrder()
            }
        }else{
            self.sideMenuIndex = nil
        }
    }
    
    func didHideRightView(sideMenuController: LGSideMenuController) {
        debugPrint("\(#function)")
    }
}
extension SideMenuScreen:MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        //        switch result {
        //        case res:
        //            print("Mail cancelled")
        //        case MFMailComposeResultSaved:
        //            print("Mail saved")
        //        case MFMailComposeResultSent:
        //            print("Mail sent")
        //        case MFMailComposeResultFailed:
        //            print("Mail sent failure: \(error?.localizedDescription)")
        //        default:
        //            break
        //        }
        self.dismiss(animated: true, completion: nil)
    }
}
