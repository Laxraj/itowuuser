//
//  PlanPriceCell.swift
//  Tika
//
//  Created by Lax on 08/11/24.
//

import UIKit
import RevenueCat

class PlanPriceCell: UITableViewCell {

    @IBOutlet weak var planNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var discountPriceLabel: UILabel!
    @IBOutlet weak var planSelectImageView: UIImageView!
    
    var viewController: SubscriptionScreen?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item:StoreProduct?{
        didSet{
            if let subscriptionPeriod = item?.subscriptionPeriod {
                switch subscriptionPeriod.unit {
                case .month:
                    print("This is a monthly subscription.")
                    let planType = self.item?.productIdentifier == IAPM.shared.SIX_MONTHLY_STANDER_PLAN_ID ? "Standard" : "Premium"
                    self.planNameLabel.text = "6-Month \(planType)"//"\(item?.localizedPriceString)" + "\(item?.price)"
                    self.priceLabel.text = "\(item?.localizedPriceString ?? "")"

                case .year:
                    print("This is a yearly subscription.")
                    self.planNameLabel.text = "Yearly Plan" //"\(item?.localizedPriceString)" + "\(item?.price)"
                    self.priceLabel.text = "\(item?.localizedPriceString ?? "")"
                default:
                    print("Subscription period is neither monthly nor yearly.")
                }
            } else {
                print("This product doesn't have a subscription period.")
            }
            
            self.discountPriceLabel.text = self.discountPriceText(for: self.item?.productIdentifier ?? "")
        }
    }
    
    func discountPriceText(for productIdentifier: String) -> String {
        switch productIdentifier {
        case IAPM.shared.YEAR_STANDER_PLAN_ID:
            return "Save $450 on Your Yearly Plan!"
        case IAPM.shared.SIX_MONTHLY_STANDER_PLAN_ID:
            return "Save $225 on Your 6-Month Plan!"
        case IAPM.shared.YEAR_PREMIUM_PLAN_ID:
            return "Save $750 on Your Yearly Plan!"
        case IAPM.shared.SIX_MONTHLY_PREMIUM_PLAN_ID:
            return "Save $374 on Your 6-Month Plan!"
        default:
            return ""
        }
    }
}
