//
//  SubscriptionInfoCell.swift
//  iTowu
//
//  Created by Laxmansinh Rajpurohit on 15/12/24.
//

import UIKit

class SubscriptionInfoCell: UITableViewCell {

    @IBOutlet weak var detailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
