//
//  SubscriptionScreen.swift
//  Tika
//
//  Created by Lax on 08/11/24.
//

import UIKit
import RevenueCat

class SubscriptionScreen: UIViewController {
    
    @IBOutlet weak var planTableView: UITableView!
    @IBOutlet weak var planTableViewHight: NSLayoutConstraint!
    
    @IBOutlet weak var infoTableView: UITableView!
    @IBOutlet weak var infoTableViewHight: NSLayoutConstraint!
    
    @IBOutlet weak var subscriptionSegmentController: UISegmentedControl!
    @IBOutlet weak var restoreButton: dateSportButton!
    
    @IBOutlet weak var tearmAndConditionTextView: UITextView!
    
    @IBOutlet weak var skipButton: dateSportButton!
    @IBOutlet weak var backButton: UIButton!
    
    var products: [StoreProduct]?
   var infoSubscription: [String]?
   var selectedProductId: String? = ""
   var discountPriceText: String? = ""
   var acceptAndCancelPayment: (()->Void)?
   var isFromSideMenu = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize()
    }
    
    func initialize() {
        self.backButton.isHidden = self.acceptAndCancelPayment != nil
        
        if self.isFromSideMenu{
            self.skipButton.setTitle("Cancel", for: .normal)
        }
        
        self.planTableView?.register(UINib(nibName: "PlanPriceCell", bundle: nil), forCellReuseIdentifier: "PlanPriceCell")
        
        self.infoTableView?.register(UINib(nibName: "SubscriptionInfoCell", bundle: nil), forCellReuseIdentifier: "SubscriptionInfoCell")
        
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(named: "app_black_color")]
        let unSelectedtitleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)]
        
        self.subscriptionSegmentController.setTitleTextAttributes(unSelectedtitleTextAttributes, for: .normal)
        self.subscriptionSegmentController.setTitleTextAttributes(titleTextAttributes as [NSAttributedString.Key : Any], for: .selected)
        
        let title = "Restore Purchases"
        
        // Create an attributed string with underline
        let attributes: [NSAttributedString.Key: Any] = [
            .underlineStyle: NSUnderlineStyle.single.rawValue,
            .font: UIFont(name: "LexendDeca-Regular", size: 16) ?? UIFont.systemFont(ofSize: 16)
        ]
        let attributedTitle = NSAttributedString(string: title, attributes: attributes)
        // Set the attributed title to the button
        self.restoreButton.setAttributedTitle(attributedTitle, for: .normal)
        self.subscriptionSegmentController.selectedSegmentIndex = 1
        self.setUpInfoData()
        self.setUpPrivacyPolicy()
    }
    
    func setUpPrivacyPolicy() {
        tearmAndConditionTextView.isEditable = false // Make the text view non-editable
        tearmAndConditionTextView.isSelectable = true // Allow selection
        tearmAndConditionTextView.dataDetectorTypes = .link // Enable links detection
        tearmAndConditionTextView.textAlignment = .left // Set text alignment to center
        
        // Set the text with "Terms of Service" and "Privacy Policy"
        let text = "By clicking Continue your payment will be charged to your iTunes account. Subscriptions will automatically renew for the same package length unless auto-renew is turned off and you cancel at least 24 hours before the end of your current period. You agree to our Terms of Service & Privacy Policy. All purchases are non-refundable."
        
        // Create an attributed string with underlines
        let attributedString = NSMutableAttributedString(string: text)
        
        // Define the range for "Terms of Service" and "Privacy Policy"
        let termsRange = (text as NSString).range(of: "Terms of Service")
        let privacyRange = (text as NSString).range(of: "Privacy Policy")
        
        // Add underline style for "Terms of Service"
        attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: termsRange)
        
        // Add underline style for "Privacy Policy"
        attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: privacyRange)
        
        // Add links to "Terms of Service" and "Privacy Policy"
        attributedString.addAttribute(.link, value: "https://www.yourapp.com/terms", range: termsRange)
        attributedString.addAttribute(.link, value: "https://www.yourapp.com/privacy", range: privacyRange)
        
        // Set custom font and color (white) for the entire text
        let customFont = UIFont(name: "LexendDeca-Regular", size: 12) // Replace with your desired font
        let whiteColor = UIColor.white
        
        attributedString.addAttribute(.font, value: customFont!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(.foregroundColor, value: whiteColor, range: NSRange(location: 0, length: text.count))
        
        // Set the link color to white
        attributedString.addAttribute(.foregroundColor, value: whiteColor, range: termsRange)
        attributedString.addAttribute(.foregroundColor, value: whiteColor, range: privacyRange)
        
        // Set the attributed text to the UITextView
        tearmAndConditionTextView.attributedText = attributedString
        
        // Set the delegate
        tearmAndConditionTextView.delegate = self
        
        // Set the link color specifically using linkTextAttributes
        tearmAndConditionTextView.linkTextAttributes = [
            .foregroundColor: UIColor.white,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
        // Add the UITextView to the view
        tearmAndConditionTextView.isUserInteractionEnabled = true // Enable user interaction
    }
            
    @IBAction func onBack(_ sender: UIButton) {
        if self.acceptAndCancelPayment != nil {
            self.acceptAndCancelPayment?()
            self.dismiss(animated: true)
        }else{
            if self.isFromSideMenu{
                if let vc = self.navigationController?.viewControllers.first as? MapScreen{
                    vc.observeOrder()
                }
            }
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func onSegment(_ sender: UISegmentedControl) {
        self.setUpInfoData()
    }
    
    
    
    @IBAction func onContinue(_ sender: UIButton) {
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            IAPM.shared.purchaseProduct(productId: self.selectedProductId ?? "") { [weak self]  purchaseStatusMessage in
                guard let self = self else {return}
                print(purchaseStatusMessage)
            } success: { [weak self] token in
                guard let self = self else {return}
                self.purchasePlan(receipt: token)
            } failure: {  [weak self] error in
                guard let self = self else {return}
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
       
    }
    
    
    @IBAction func onSkip(_ sender: UIButton) {
        if self.isFromSideMenu == true{
            if let url = URL(string: "itms-apps://apps.apple.com/account/subscriptions") {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:])
                }
            }
        }else if self.acceptAndCancelPayment != nil {
            self.acceptAndCancelPayment?()
            self.dismiss(animated: true)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func onRestorePurchase(_ sender: UIButton) {
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            IAPM.shared.restorePurchase {  [weak self] token, productId  in
                guard let self = self else {return}
                self.selectedProductId = productId
                self.purchasePlan(receipt: token)
            } failure: { [weak self] error in
                guard let self = self else {return}
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    
    @IBAction func onTerms(_ sender: UIButton) {
        let vc = STORYBOARD.webView.instantiateViewController(withIdentifier: "WebViewScreen") as! WebViewScreen
        vc.linkUrl = termConditionURL
        vc.titlaString = "TERM & CONDITIONS"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onPrivacy(_ sender: UIButton) {
        let vc = STORYBOARD.webView.instantiateViewController(withIdentifier: "WebViewScreen") as! WebViewScreen
        vc.linkUrl = privacyPolicyURL
        vc.titlaString = "PRIVACY POLICY"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setUpInfoData(){
        if self.subscriptionSegmentController.selectedSegmentIndex == 0{
            self.infoSubscription = getSubscriptionInfo(for: IAPM.shared.YEAR_STANDER_PLAN_ID)
            IAPM.shared.subscriptionProduct = [IAPM.shared.SIX_MONTHLY_STANDER_PLAN_ID,IAPM.shared.YEAR_STANDER_PLAN_ID]
        }else{
            self.infoSubscription = getSubscriptionInfo(for: IAPM.shared.YEAR_PREMIUM_PLAN_ID)
            IAPM.shared.subscriptionProduct = [IAPM.shared.SIX_MONTHLY_PREMIUM_PLAN_ID,IAPM.shared.YEAR_PREMIUM_PLAN_ID]
        }
        
        self.getProduct(selectedIndex: self.subscriptionSegmentController.selectedSegmentIndex)
        self.infoTableView.reloadData()
    }
    
    
}
extension SubscriptionScreen: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.infoTableView {
            return self.infoSubscription?.count ?? 0
        }else{
            return self.products?.count ?? 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.infoTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubscriptionInfoCell", for: indexPath) as! SubscriptionInfoCell
            cell.detailLabel.text = self.infoSubscription?[indexPath.row]
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlanPriceCell", for: indexPath) as! PlanPriceCell
            cell.item = self.products?[indexPath.row]
            cell.planSelectImageView.image = self.products?[indexPath.row].productIdentifier == self.selectedProductId ? UIImage(named: "selected_tick_circle") : UIImage(named: "un_selected_tick_circle")
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.planTableView {
            self.infoSubscription = getSubscriptionInfo(for: self.products?[indexPath.row].productIdentifier ?? "")
            self.selectedProductId = self.products?[indexPath.row].productIdentifier
            self.planTableView.reloadData()
            self.infoTableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == self.infoTableView {
            DispatchQueue.main.async {
                self.infoTableViewHight.constant = tableView.contentSize.height
            }
        }else{
            DispatchQueue.main.async {
                self.planTableViewHight.constant = tableView.contentSize.height
            }
        }
        
    }
    
    func getSubscriptionInfo(for productIdentifier: String) -> [String] {
        switch productIdentifier {
        case IAPM.shared.YEAR_STANDER_PLAN_ID:
            return ["Priority booking", "No booking fees", "4 free roadside assistance services per year (Battery, Tire, and Lockout)", "10% discount on all other services", "1 tow up to 10 miles", "Ad free experience"]
            
        case IAPM.shared.SIX_MONTHLY_STANDER_PLAN_ID:
            return ["Priority booking", "No booking fees", "2 free roadside assistance services per year (Battery, Tire, and Lockout)", "5% discount on all other services", "1 tow up to 5 miles", "Ad free experience"]
        case IAPM.shared.YEAR_PREMIUM_PLAN_ID:
            return ["Priority Booking", "No Booking Fees", "6 free roadside assistance services per year(Battery, Tire, and Lockout)", "1 Free Gas delivery per year(up to 5 gallons)", "15% discount on all other services", "1 tow up to 20 miles free tows", "Ad free experience"]
        case IAPM.shared.SIX_MONTHLY_PREMIUM_PLAN_ID:
            return ["Priority Booking", "No Booking Fees", "3 free roadside assistance services per year(Battery, Tire, and Lockout)", "10% discount on all other services", "1 tow up to 20 miles", "Ad free experience"]
        default:
            return []
        }
    }
    
}

extension SubscriptionScreen: UITextViewDelegate{
    // UITextView delegate method to handle link taps
     func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
         if url.absoluteString == termConditionURL {
             // Open the Terms of Service URL
             //openTermsOfService()
             return false // Prevent the UITextView from opening the URL by itself
         } else if url.absoluteString == privacyPolicyURL {
             // Open the Privacy Policy URL
             //openPrivacyPolicy()
             return false // Prevent the UITextView from opening the URL by itself
         }
         return true // Let UITextView handle other links
     }
}

//MARK: - API CALLING
extension SubscriptionScreen {
    
    //MARK: - GET PRODUCT API CALLING
    func getProduct(selectedIndex: Int){
        IAPM.shared.getProduct { [weak self] storeProduct in
            guard let self = self else { return }
            self.products = storeProduct
            
            if Utility.getUserData()?.subscription?.subscription?.productId == nil{
                // Sorting based on selectedIndex
                if selectedIndex == 0 {
                    // Sort the products so that YEAR_STANDER_PLAN_ID comes first
                    self.products?.sort {
                        if $0.productIdentifier == IAPM.shared.YEAR_STANDER_PLAN_ID { return true }  // Move YEAR_STANDER_PLAN_ID to the front
                        if $1.productIdentifier == IAPM.shared.YEAR_STANDER_PLAN_ID { return false } // Keep YEAR_STANDER_PLAN_ID at the front
                        return $0.productIdentifier < $1.productIdentifier  // Default sorting for the remaining items
                    }
                } else {
                    // Sort the products so that YEAR_PREMIUM_PLAN_ID comes first
                    self.products?.sort {
                        if $0.productIdentifier == IAPM.shared.YEAR_PREMIUM_PLAN_ID { return true }  // Move YEAR_PREMIUM_PLAN_ID to the front
                        if $1.productIdentifier == IAPM.shared.YEAR_PREMIUM_PLAN_ID { return false } // Keep YEAR_PREMIUM_PLAN_ID at the front
                        return $0.productIdentifier < $1.productIdentifier  // Default sorting for the remaining items
                    }
                }
                
                // Update selectedProductId to reflect the first item after sorting
                self.selectedProductId = self.products?.first?.productIdentifier
            }else{
                self.selectedProductId = Utility.getUserData()?.subscription?.subscription?.productId
            }
            // Reload the table view
            self.planTableView.reloadData()

        }
    }
    
    //MARK: - PURCHASE PLAN API CALLING
    func purchasePlan(receipt:String) {
        var isTestEnvironment:Bool?
#if DEBUG
        isTestEnvironment = true
#else
        isTestEnvironment = false
#endif
        let request =  PurchaseSubscriptionRequest(deviceType: "ios",
                                                   receipt: receipt,
                                                   productId: self.selectedProductId,
                                                   isTestEnvironment: isTestEnvironment)
        SubscriptionService.shared.subscription(parameter: request.toJSON(), success: {
            (statusCode,response) in
            Utility.hideIndicator()
            
            FirebaseanalyticsManager.share.logEvent(event: self.isFromSideMenu == true ? .subscriptionFromSideMenuFlow : .subscriptionFromServiceFlow)
            if let res = response?.subscriptionResponseData {
                let userData = Utility.getUserData()
                userData?.subscription = res
                Utility.saveUserData(data: userData?.toJSON() ?? [:])
                if self.isFromSideMenu{
                    if let vc = self.navigationController?.viewControllers.first as? MapScreen{
                        vc.observeOrder()
                    }
                }
                
                self.navigationController?.popViewController(animated: true)
//                let controller = STORYBOARD.home.instantiateViewController(withIdentifier: "HomeScreen") as!  HomeScreen
//                self.navigationController?.pushViewController(controller, animated: true)
            }
        }, failure: { [weak self] (error) in
            guard let strongSelf = self else {
                return
            }
            Utility.hideIndicator()
            Utility.showAlert(vc:strongSelf , message: error)
        })
    }
}
