//
//  InviteFriendScreen.swift
//  iTowu
//
//  Created by iroid on 24/03/22.
//

import UIKit

class RefralFriendScreen: UIViewController {

    @IBOutlet weak var refralCodeLabel: UILabel!
    @IBOutlet weak var howItWorksLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialDetail()
    }
    
    func initialDetail(){

        let labelAtributes:[NSAttributedString.Key : Any]  = [
            NSAttributedString.Key.foregroundColor: Utility.getUIcolorfromHex(hex: "0175E3"),
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
            NSAttributedString.Key
                .underlineColor: Utility.getUIcolorfromHex(hex: "0175E3")
                ]
        self.howItWorksLabel.attributedText = NSAttributedString(string: "How it works", attributes:                            labelAtributes
        )
        self.refralCodeLabel.text = Utility.getUserData()?.invite_code ?? ""
    }

    @IBAction func onBack(_ sender: UIButton) {
        if let vc = self.navigationController?.viewControllers.first as? MapScreen{
            vc.observeOrder()
        }
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onHowItWorks(_ sender: UIButton) {
        let vc = STORYBOARD.webView.instantiateViewController(withIdentifier: "WebViewScreen") as! WebViewScreen
        vc.linkUrl = refferalURL
        vc.titlaString = "HOW IT REFERRAL WORKS"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onShare(_ sender: UIButton) {
        FirebaseanalyticsManager.share.logEvent(event: .referralShareClick)
        let text = "If you enjoy the app, refer your friends and receive a reward!\nInvite Code:-  \(Utility.getUserData()?.invite_code ?? "")\nhttps://apps.apple.com/us/app/itowu/id1591145630"
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
}
