//
//  TransportServiceTimeScreen.swift
//  iTowu
//
//  Created by Nikunj on 05/03/22.
//

import UIKit

class TransportServiceTimeScreen: UIViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    
    var onBack: (() -> Void)?
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeDetails()
        // Do any additional setup after loading the view.
    }
    
    func initializeDetails(){
        self.datePicker.minimumDate = Calendar.current.date(byAdding: .hour, value: 1, to: Date())
        if #available(iOS 13.4, *) {
            self.datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.onBack?()
    }
    
    @IBAction func onConfirm(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        CacheArray.shared.placeOrderRequest?.scheduleDate = dateFormatter.string(from: self.datePicker.date)
//        Utility.setMapWhereAreYou()
//        self.navigationController?.popViewController(animated: true)
//        self.onBack?()
        self.getBookingCalculation()
    }
    
    func getBookingCalculation(){
        let obj = CacheArray.shared.placeOrderRequest
        let data = PlaceOrderRequest(serviceId: obj?.serviceId, user_garage_id: obj?.user_garage_id/*plateNumber: obj?.plateNumber, vehicleYearId: obj?.vehicleYearId, vehicleMakeId: obj?.vehicleMakeId, vehicleModelId: obj?.vehicleModelId, vehicleColorId: obj?.vehicleColorId*/, pickup_latitude: obj?.pickup_latitude, pickup_longitude: obj?.pickup_longitude, pickup_address: obj?.pickup_address, destination_latitude: obj?.destination_latitude, destination_longitude: obj?.destination_longitude, destination_address: obj?.destination_address, in_garage: obj?.in_garage, questions: obj?.questions, fuleType: obj?.fuleType, state: obj?.state, scheduleDate: obj?.scheduleDate, autoTransportStartDate: obj?.autoTransportStartDate, deviceType: obj?.deviceType)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            MapService.shared.calculateOrderPrice(parameters: data.toJSON()) { [weak self] statusCode, response in
                Utility.hideIndicator()
                self?.navigationController?.popViewController(animated: true)
                self?.onBack?()
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
                CacheArray.shared.placeOrderRequest?.scheduleDate = nil
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
