//
//  ServiceDetailScreen.swift
//  iTowu
//
//  Created by iroid on 17/10/21.
//

import UIKit
import MapKit
import Firebase

class MapServiceDetailScreen: UIViewController {
    
    @IBOutlet weak var pickUpLocationLabel: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var startLocationLabel: UILabel!
    
    @IBOutlet weak var pickUpDateLabel: UILabel!
    @IBOutlet weak var destinationDateLabel: UILabel!
    @IBOutlet weak var startDriverDateLabel: UILabel!
    
    @IBOutlet weak var searviceChargeView: UIView!
    @IBOutlet weak var texesAndChargeView: UIView!
    @IBOutlet weak var fuleView: UIView!
    
    @IBOutlet weak var serviceChargeLabel: UILabel!
    @IBOutlet weak var texesLabel: UILabel!
    @IBOutlet weak var fuleLabel: UILabel!
    @IBOutlet weak var serviceTitleLabel: UILabel!
    @IBOutlet weak var tipLabel: UILabel!
    
    @IBOutlet weak var totalCostLabel: UILabel!
    
    @IBOutlet weak var pickupLocationLabel: UILabel!
    
    @IBOutlet weak var destinationLocationView: UIView!
    @IBOutlet weak var startDriverView: UIView!
    @IBOutlet weak var tipView: UIView!
    
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var filterPriceLabel: UILabel!
    @IBOutlet weak var StabiliserPriceLabel: UILabel!
    
    @IBOutlet weak var serviceChargeDiscountLabel: UILabel!
    @IBOutlet weak var fuelPriceDiscountLabel: UILabel!
    @IBOutlet weak var bookingFeesDiscountLabel: UILabel!
    @IBOutlet weak var totalDiscountLabel: UILabel!
    
    //MARK: Variable
    var orderHistoryData:FirebaseAcceptedServiceResponse?
    var serviceObj: OrderStatusResponse?
    var placeOrderObj: PlaceOrderResponse?
    var observerOrderHandler: DatabaseHandle?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialDetail()
    }
    
    func initialDetail(){
        
        self.pickUpLocationLabel.text = orderHistoryData?.pickupAddress
        self.destinationLabel.text = orderHistoryData?.destination_address
//        self.startLocationLabel.text = orderHistoryData?.driverStartAddress
        self.startLocationLabel.superview?.isHidden = true//orderHistoryData?.driverStartAddress == nil
        
        self.pickUpDateLabel.text = Utility.serviceDate(inerval: orderHistoryData?.driverOnPickupLocation ?? 0)
        self.destinationDateLabel.text = Utility.serviceDate(inerval: orderHistoryData?.driverOnDestinationLocation ?? 0)
        self.startDriverDateLabel.text = Utility.serviceDate(inerval: orderHistoryData?.driverStartedAt ?? 0)
        
        self.pickUpDateLabel.isHidden = orderHistoryData?.driverOnPickupLocation == 0
        self.destinationDateLabel.isHidden = orderHistoryData?.driverOnDestinationLocation == 0
        
        self.destinationLocationView.isHidden = orderHistoryData?.service?.type == 1
        
        self.pickupLocationLabel.text = orderHistoryData?.service?.type == 1 ? "Destination Location":"Pickup Location"
        let serviceCharge = String(format: "$%.2f", orderHistoryData?.serviceCharge ?? 0.0)

        self.serviceChargeLabel.text = serviceCharge
        
        let productPrice = self.orderHistoryData?.oilProduct == 0 ? self.orderHistoryData?.breakProduct : self.orderHistoryData?.oilProduct
        self.productTitleLabel.text = self.orderHistoryData?.oilProduct == 0 ? "Brake product" : "Oil product"
        
        self.productPriceLabel.text = String(format: "$%.2f", productPrice ?? 0.0)
        
        self.filterPriceLabel.text = String(format: "$%.2f", self.orderHistoryData?.filterPrice ?? 0.0)
        
        self.StabiliserPriceLabel.text = String(format: "$%.2f", self.orderHistoryData?.stabilizerPrice ?? 0.0)
        
        self.filterPriceLabel.superview?.isHidden = self.orderHistoryData?.filterPrice == 0 ? true : false
        
        self.StabiliserPriceLabel.superview?.isHidden = self.orderHistoryData?.stabilizerPrice == 0 ? true : false
        
        self.productPriceLabel.superview?.isHidden = self.orderHistoryData?.oilProduct == 0 && self.orderHistoryData?.breakProduct == 0 ? true : false
        
        self.totalCostLabel.text =  "$"+String(format: "%.2f", orderHistoryData?.price ?? 0)
        
//        self.totalCostLabel.text = "$"+"\(orderHistoryData?.price ?? 0.0)"
        let serviceTitle = "\(orderHistoryData?.service?.name ?? "") service"
        self.serviceTitleLabel.text = serviceTitle.uppercased()
        
        
        if orderHistoryData?.tip != 0{
            self.tipView.isHidden = false
            self.tipLabel.text = "$"+"\(orderHistoryData?.tip ?? 0)"
        }else{
            self.tipView.isHidden = true
        }
        
        self.totalCostLabel.text = String(format: "$%.2f", (orderHistoryData?.price ?? 0) - (orderHistoryData?.subDiscountPrice ?? 0))
        
        if orderHistoryData?.subDiscountPrice != 0 || orderHistoryData?.subDiscountPrice == nil{
            self.totalDiscountLabel.superview?.isHidden = false
            self.totalDiscountLabel.text = "$\(String(format:"%.02f",orderHistoryData?.price ?? 0))"
        }
        
        self.serviceChargeLabel.text = String(format: "$%.2f", (orderHistoryData?.serviceCharge ?? 0) - (orderHistoryData?.serviceChargeDiscount ?? 0))

        
        if orderHistoryData?.serviceChargeDiscount != 0 || orderHistoryData?.serviceChargeDiscount == nil{
            self.serviceChargeDiscountLabel.superview?.isHidden = false
            self.serviceChargeDiscountLabel.text = "$\(String(format:"%.02f",orderHistoryData?.serviceCharge ?? 0))"
        }
        
        let totalTaxes = ((orderHistoryData?.stripeTax ?? 0.0) + (orderHistoryData?.towWinchPrice ?? 0.0) + (orderHistoryData?.bookingFees ?? 0.0))
         
         let totalTaxeString = String(format: "$%.2f", (totalTaxes) - (orderHistoryData?.bookingFeesDiscount ?? 0) )

         self.texesLabel.text = totalTaxeString
        
        if orderHistoryData?.bookingFeesDiscount != 0 || orderHistoryData?.bookingFeesDiscount == nil{
            self.bookingFeesDiscountLabel.superview?.isHidden = false
            self.bookingFeesDiscountLabel.text = "$\(String(format:"%.02f",totalTaxes))"
        }
        
        if orderHistoryData?.fuleType != ""{
            self.fuleView.isHidden = false
            self.fuleLabel.text = String(format: "$%.2f", (orderHistoryData?.fulePrice ?? 0) - (orderHistoryData?.fuelPriceDiscount ?? 0))
        }else{
            self.fuleView.isHidden = true
        }
        
        if orderHistoryData?.fuelPriceDiscount != 0 || orderHistoryData?.fuelPriceDiscount == nil{
            self.fuelPriceDiscountLabel.superview?.isHidden = false
            self.fuelPriceDiscountLabel.text = "$\(String(format:"%.02f",orderHistoryData?.fulePrice ?? 0))"
        }
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func onCancel(_ sender: UIButton) {
        var orderIds = 0
        if let orderId = self.serviceObj?.orderId{
            orderIds = orderId
        }else if let orderId = self.orderHistoryData?.orderId{
            orderIds = orderId
        }else if let orderId = self.placeOrderObj?.orderId{
            orderIds = orderId
        }
        
        if orderIds == 0{
            self.cancelServiceAlert { reason in
                FirebaseanalyticsManager.share.logEvent(event: .beforeAcceptPaymentCancelClick)
                Utility.setHomeRoot(false,nil)
            }
        }else{
            if self.orderHistoryData?.driver == nil{
                self.cancelServiceAlert { [weak self] reason in
                    FirebaseanalyticsManager.share.logEvent(event: .afterAcceptPaymentcancelClick)
                    self?.cancelService(reason: reason)
                }
            }else{
                self.cancelServiceAlert { [weak self] reason in
                    self?.cancelServicePrice(reason: reason)
                }
            }
        }
    }
    
    func cancelServiceAlert(completion: @escaping (String?) -> Void){
        let alertController = UIAlertController(title: APPLICATION_NAME, message: "Are you sure you want to cancel your service?", preferredStyle: .alert)
        let resendAction = UIAlertAction(title: "Yes", style: .default, handler: { [weak self] (action) in
            self?.openReasonScreen { reason in
                completion(reason)
            }
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(cancelAction)
        alertController.addAction(resendAction)
        self.present(alertController, animated: true)
    }
    
    func openReasonScreen(completion: @escaping (String?) -> Void){
        let vc = STORYBOARD.map.instantiateViewController(withIdentifier: "CancelServiceReasonScreen") as! CancelServiceReasonScreen
        vc.selectReason = { reason in
            completion(reason)
        }
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: false)
    }
}

//MARK: - API
extension MapServiceDetailScreen{
    
    func cancelService(reason: String?){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            var orderIds = 0
    //            var latitude = ""
    //            var longitude = ""
            if let orderId = self.serviceObj?.orderId{
                orderIds = orderId
            }else if let orderId = self.orderHistoryData?.orderId{
                orderIds = orderId
    //                latitude = "\(self.currentServiceObj?.driver?.latitude ?? 0.0)"
    //                longitude = "\(self.currentServiceObj?.driver?.longitude ?? 0.0)"
            }else if let orderId = self.placeOrderObj?.orderId{
                orderIds = orderId
            }
            let data = CancelOrderRequest(orderId: orderIds, latitude: nil, longitude: nil,cancelReason: reason)
            MapService.shared.cancelOrder(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                self?.orderHistoryData = nil
                Utility.hideIndicator()
                if let handler = self?.observerOrderHandler{
                    FirebaseRealtimeDB.ref.removeObserver(withHandle: handler)
                }
                Utility.setHomeRoot(false,nil)
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func cancelServicePrice(reason: String?){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            var orderIds = 0
            var latitude = ""
            var longitude = ""
            if let orderId = self.serviceObj?.orderId{
                orderIds = orderId
            }else if let orderId = self.orderHistoryData?.orderId{
                orderIds = orderId
                latitude = "\(self.orderHistoryData?.driver?.latitude ?? 0.0)"
                longitude = "\(self.orderHistoryData?.driver?.longitude ?? 0.0)"
            }else if let orderId = self.placeOrderObj?.orderId{
                orderIds = orderId
            }
            let data = CancelOrderPriceRequest(orderId: orderIds, latitude: latitude, longitude: longitude)
            MapService.shared.cancelPrice(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                //FIXME: CANCEL SERVICE IF AMOUNT IS 0(ZERO)
                if let priceData = response.cancelOrderPriceData,let amount = priceData.amount,amount > 0{
                    self?.cancelBookingAlert(price:  "\(String(format: "%.2f", amount))", reason: reason)
                }else{
                    self?.cancelService(reason: reason)
                }
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func cancelBookingAlert(price:String,reason: String?){
        let alertController = UIAlertController(title: "Are you sure?", message: "It’s been 5 min since your request,a service charge of $\(price) will be applied if canceled.", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "No, Go Back", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        let cancelAction = UIAlertAction(title: "Yes, Cancel", style: UIAlertAction.Style.destructive) {
            [weak self] UIAlertAction in
            FirebaseanalyticsManager.share.logEvent(event: .cancelAfterDriverAc)
            self?.cancelService(reason: reason)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
}

