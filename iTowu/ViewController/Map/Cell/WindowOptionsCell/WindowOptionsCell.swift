//
//  WindowOptionsCell.swift
//  iTowu
//
//  Created by Nikunj Vaghela on 12/04/24.
//

import UIKit

class WindowOptionsCell: UITableViewCell {

    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var answerView: dateSportView!
    
    @IBOutlet weak var answerTextField: UITextField!
    
    var onSelectAnswer: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item: QuestionAnswer?{
        didSet{
            self.questionLabel.text = item?.answer
            if let answer = item?.selectedWindowAnswer{
                self.answerTextField.text = answer+"%"
            }else{
                self.answerTextField.text = nil
            }
        }
    }
    
    @IBAction func onAnswer(_ sender: Any) {
        self.onSelectAnswer?()
    }
    
}
