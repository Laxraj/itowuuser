//
//  ServiceTableHeaderView.swift
//  iTowu
//
//  Created by Nikunj Vaghela on 01/08/23.
//

import UIKit

class ServiceTableHeaderView: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var sapratorView: UIView!
    
    
    var seeMore: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func onSeeMore(_ sender: Any) {
        self.seeMore?()
    }
    
}
