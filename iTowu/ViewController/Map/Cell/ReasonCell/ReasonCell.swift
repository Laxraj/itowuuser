//
//  ReasonCell.swift
//  iTowu
//
//  Created by Nikunj Vaghela on 15/02/25.
//

import UIKit
import IQKeyboardManagerSwift

class ReasonCell: UITableViewCell {
    
    @IBOutlet weak var selectedView: UIView!
    
    @IBOutlet weak var reasonLabel: UILabel!
    
    @IBOutlet weak var reasonTextView: IQTextView!
    
    @IBOutlet weak var seperatorView: UIView!
    var onTextChange: ((String?)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.reasonTextView.delegate = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item: ReasonsRequest?{
        didSet{
            self.selectedView.isHidden = !(item?.selected ?? false)
            
            self.reasonLabel.text = item?.reason
            
            self.reasonTextView.superview?.isHidden = !(item?.isManulReason == true && item?.selected == true)
            
            self.reasonTextView.text = item?.otherReason
        }
    }
}

//MARK: - TEXTVIEW DELEGATE
extension ReasonCell: UITextViewDelegate{
    
    func textViewDidChange(_ textView: UITextView) {
        self.onTextChange?(textView.text)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard range.location == 0 else {
            return true
        }
        let newString = (textView.text! as NSString).replacingCharacters(in: range, with: text) as NSString
        return newString.rangeOfCharacter(from: NSCharacterSet.whitespacesAndNewlines).location != 0
    }
}
