//
//  ServiceMainTableViewCell.swift
//  iTowu
//
//  Created by Nikunj Vaghela on 31/07/23.
//

import UIKit

class ServiceMainTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var itemArray: [ServiceResponse] = []

    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "ServiceCell", bundle: Bundle.main), forCellWithReuseIdentifier: "ServiceCell")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if !self.bounds.size.equalTo(self.intrinsicContentSize) {
            self.invalidateIntrinsicContentSize()
        }
    }

    override var intrinsicContentSize: CGSize {
        get {
            let intrinsicContentSize = self.collectionView.contentSize
            return intrinsicContentSize
        }
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
            collectionView.layoutIfNeeded()
            collectionView.frame = CGRect(x: 0, y: 0, width: targetSize.width , height: 1)
            return collectionView.collectionViewLayout.collectionViewContentSize
    }

    
}
//MARK:- COLLECIONVIEW DELEGATE & DATASOURCE
extension ServiceMainTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 4
//    }
//
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        switch kind {
//        case UICollectionView.elementKindSectionHeader:
//            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "StationHeaderView", for: indexPath) as! StationHeaderView
//            if indexPath.section == 1{
//                headerView.titleLabel.text = "Shop Near By"
//            }else if indexPath.section == 2{
//                headerView.titleLabel.text = "Gas Station Near By"
//            }else if indexPath.section == 3{
//                headerView.titleLabel.text = "Charging Station Near By"
//            }
//            headerView.seeMore = { [weak self] in
//                if indexPath.section == 1{
//                    let vc = STORYBOARD.map.instantiateViewController(withIdentifier: "ShopListScreen") as! ShopListScreen
//                    vc.titleString = "Shop Near By"
//                    self?.navigationController?.pushViewController(vc, animated: true)
//                }else{
//                    let vc = STORYBOARD.map.instantiateViewController(withIdentifier: "StationListScreen") as! StationListScreen
//                    if indexPath.section == 2{
//                        vc.titleString = "Gas Station Near By"
//                        vc.itemArray = self?.gasStationListArray ?? []
//                    }else{
//                        vc.titleString = "Charging Station Near By"
//                        vc.itemArray = self?.chargeStationListArray ?? []
//                    }
//                    self?.navigationController?.pushViewController(vc, animated: true)
//                }
//            }
//            if indexPath.section == 1{
//                headerView.sapratorView.isHidden = self.advertisementListArray.isEmpty ? true : false
//            }else if indexPath.section == 2{
//                headerView.sapratorView.isHidden = (self.advertisementListArray.isEmpty && self.gasStationListArray.isEmpty == false) ? false : true
//            }else {
//                headerView.sapratorView.isHidden = (self.gasStationListArray.isEmpty && self.chargeStationListArray.isEmpty == false) ? false : true
//            }
//            return headerView
//        default:
//            assert(false, "Unexpected element kind")
//        }
//        return UICollectionReusableView()
//    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        let leftSpace = (self.serviceCollectionView.collectionViewLayout as! UICollectionViewFlowLayout).sectionInset.left
//        let rightSpace = (self.serviceCollectionView.collectionViewLayout as! UICollectionViewFlowLayout).sectionInset.right
//        var height: CGFloat = 0
//        if section == 1{
//            height = self.advertisementListArray.isEmpty ? 0 : 40
//        }else if section == 2{
//            height = self.gasStationListArray.isEmpty ? 0 : 40
//        }else if section == 3{
//            height = self.chargeStationListArray.isEmpty ? 0 : 40
//        }
//        return CGSize(width: screenWidth - (leftSpace + rightSpace), height: height)
//    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.itemArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCell", for: indexPath) as! ServiceCell
            cell.item = self.itemArray[indexPath.row]
//            if self.selectedService == indexPath.row{
//
//                cell.mainView.borderColor =  Utility.checkLightModeUserDefalt() ? Utility.getUIcolorfromHex(hex: "051626"): Utility.getUIcolorfromHex(hex: "41EAD4")
//
//
//                cell.mainView.borderWidth = 2
//                cell.mainView.shadowColor = Utility.getUIcolorfromHex(hex: "000000").withAlphaComponent(0.1)
//                cell.mainView.shadowOpacity = 1
//                cell.mainView.shadowOffset.width = 1
//                cell.mainView.shadowOffset.height = 1
//                cell.mainView.shadowRadius = 1
//            }else{
//                cell.mainView.borderColor = .clear
//                cell.mainView.borderWidth = 0
//                cell.mainView.shadowColor = .clear
//                cell.mainView.shadowOpacity = 0
//                cell.mainView.shadowOffset.width = 0
//                cell.mainView.shadowOffset.height = 0
//                cell.mainView.shadowRadius = 0
//            }
            return cell
    }
    
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let leftSpace = (self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout).sectionInset.left
            let rightSpace = (self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout).sectionInset.right
            let cellSpacing = (self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout).minimumInteritemSpacing
            let numberOFRows : CGFloat = 4
            let scWidth : CGFloat = screenWidth - (leftSpace + rightSpace)
            let totalSpace : CGFloat = cellSpacing * (numberOFRows - 1)
            let width = (scWidth - totalSpace) / numberOFRows
            return CGSize(width: width, height: width + 40)
        }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        DispatchQueue.main.async { [weak self] in
//            self?.servieCollectionViewHeight.constant = collectionView.contentSize.height > (screenHeight - 320) ? screenHeight - 320 : collectionView.contentSize.height
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//        if indexPath.section == 0{
//            self.selectedService = indexPath.row
//            self.serviceCollectionView.reloadData()
//            self.manageContinueButton()
//            self.questionItemArray = self.serviceArray[indexPath.row].questions ?? []
//            self.answerDescriptionView.isHidden = true
//            self.vehicleAnswerTableView.reloadData()
//        }else if indexPath.section == 1{
//            OpenMapDirections.present(in: self, sourceView: self.view, fromLat: currentLatitude, fromLong: currentLongitude, toLat: "\(self.gasStationListArray[indexPath.item].lat ?? 0)", toLong: "\(self.gasStationListArray[indexPath.item].lng ?? 0)")
//        }else{
//            OpenMapDirections.present(in: self, sourceView: self.view, fromLat: currentLatitude, fromLong: currentLongitude, toLat: "\(self.chargeStationListArray[indexPath.item].lat ?? 0)", toLong: "\(self.chargeStationListArray[indexPath.item].lng ?? 0)")
//        }
    }
}
