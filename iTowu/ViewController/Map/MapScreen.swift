//
//  MapScreen.swift
//  iTowu
//
//  Created by iroid on 16/10/21.
//

import UIKit
import MapKit
import DropDown
import FirebaseDatabase
import GoogleMobileAds
import PanModal
import FirebaseAnalytics

class MapScreen: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var selectVehicleView: UIView!
    @IBOutlet weak var addVehicleView: dateSportView!
    @IBOutlet weak var selectGarageView: dateSportView!
    @IBOutlet weak var vehicleSubView: UIView!
    
    @IBOutlet weak var garageVehicleTableView: UITableView!
    @IBOutlet weak var serviceCollectionView: UICollectionView!
    
    @IBOutlet weak var garageView: UIView!
    @IBOutlet weak var addMotoristView: UIView!
    @IBOutlet weak var selectServiceView: UIView!
    @IBOutlet weak var serviceSubView: UIView!
    @IBOutlet weak var serviceContinueButton: dateSportButton!
    @IBOutlet weak var nowButton: dateSportButton!
    
    
    @IBOutlet weak var selectVehicleTextField: UITextField!
    
    @IBOutlet weak var vehiclePlateNumberTextFiled: UITextField!
    @IBOutlet weak var vehicleModelYearTextField: UITextField!
    @IBOutlet weak var vehicleMakeTextField: UITextField!
    @IBOutlet weak var vehicleModelTextField: UITextField!
    @IBOutlet weak var colorTextField: UITextField!
    @IBOutlet weak var addVehicleCheckBoxView: UIImageView!
    @IBOutlet weak var vehicleImageView: UIImageView!
    
    
    @IBOutlet weak var answerView: UIView!
    @IBOutlet weak var vehicleAnswerTableView: UITableView!
    
    @IBOutlet weak var servieCollectionViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var fuelQuestionTitleLabel: UILabel!
    @IBOutlet weak var fuelStackView: UIStackView!
    
    
    @IBOutlet weak var gasolineLabel: UILabel!
    @IBOutlet weak var midGradeLabel: UILabel!
    @IBOutlet weak var premiumLabel: UILabel!
    @IBOutlet weak var dieselLabel: UILabel!
    
    @IBOutlet weak var gasolineView: UIView!
    @IBOutlet weak var midGradeView: UIView!
    @IBOutlet weak var premiumView: UIView!
    @IBOutlet weak var dieselView: UIView!
    
    @IBOutlet weak var bannerAdShowingView: UIView!
    @IBOutlet weak var bannerViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var answerDescriptionView: dateSportView!
    
    @IBOutlet weak var answerDescriptionLabel: UILabel!
    
    var serviceArray: [ServiceResponse] = []
    var vehicleMakeArray: [VehicleMakeResponse] = []
    var vehicleYearArray: [VehicleYearRespose] = []
    var vehicleColorArray: [VehicleColorResponse] = []
    var vehicleModelArray: [Models] = []
    
    var questionItemArray: [Questions] = []
    var vehicleListArray: [VehicleListResponse] = []
    
    var isGarage: Bool = false
    var selectedService: Int = -1
    var selectedGarageIndex = -1
    //MARK: Variable Declaration
    let locationManager = CLLocationManager()
    
    var selectedVehicle: VehicleListResponse?
    
    var selectedFualTitle = String()

    var isScheduleSerivce: Bool = false
    var orderCheckStatus: CheckOrderStatusResponse?
    
    
    var googleBannerAdView: GADBannerView?
    var gasStationListArray: [NearByGasStationList] = []
    var chargeStationListArray: [NearByGasStationList] = []
    var advertisementListArray: [AdvertisementList] = []
    let gradientArray = [UIImage(named: "gradient_1"),UIImage(named: "gradient_2"),UIImage(named: "gradient_3")]
    
    var serviceScreen: ServicesScreen?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.modeChange), name: NSNotification.Name("MODE_CHANGE"), object: nil)
        
        self.initialDetail()
//        let data = FirebaseVehicleRequest(vehicleUserPinImage: "", pickUpLatitude: "21.2174179", pickUpLongtitude: "72.8233813", destinationLatitude: "21.2091224", destinationLongtitude: "72.8271517")
//        FirebaseRealtimeDBHelper.shared.saveData(childName: "1_2", value: data.toJSON())
        for poll in self.mapView.overlays {
            self.mapView.removeOverlay(poll)
        }
//        self.showRouteOnMap(pickupCoordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees("21.2194995")!, longitude: CLLocationDegrees("72.8229383")!), destinationCoordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees("21.2207915")!, longitude: CLLocationDegrees("72.8190298")!))
        
//        FirebaseRealtimeDB.ref.child("1_2").observe(.value) { (snapShot) in
//            print("update")
//            print(snapShot)
//            if let val = snapShot.value as? [String: Any]{
//                let data = FirebaseVehicleRequest(JSON: val)
//                for poll in self.mapView.overlays {
//                    self.mapView.removeOverlay(poll)
//                }
//                self.showRouteOnMap(pickupCoordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees(data?.pickUpLatitude ?? "0")!, longitude: CLLocationDegrees(data?.pickUpLongtitude ?? "0")!), destinationCoordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees(data?.destinationLatitude ?? "0")!, longitude: CLLocationDegrees(data?.destinationLongtitude ?? "0")!))
//            }
//        }
       
        self.observeOrder()
    }
    
    @objc func modeChange(){
        self.garageVehicleTableView.reloadData()
        self.serviceCollectionView.reloadData()
    }
    
    func observeOrder(){
        Utility.showIndicator()
        FirebaseRealtimeDB.ref.child("user").child("order_user_\(Utility.getUserData()?.userId ?? 0)").observe(.value) { [weak self] (snapShot) in
            Utility.hideIndicator()
            if snapShot.value.debugDescription == "Optional(<null>)"{
//                self?.selectServiceView.animShow()//isHidden = false
                DispatchQueue.main.async {
                    self?.openServicesScreen()
                }
            }else if let firebaseDictionary = snapShot.value as? [String: Any]{
                if let orderObj = FirebaseAcceptedServiceResponse(JSON: firebaseDictionary){
                    if orderObj.orderId == nil && orderObj.driver?.driverId == nil && orderObj.user?.userId == nil{
                        FirebaseRealtimeDB.ref.child("user").child("order_user_\(Utility.getUserData()?.userId ?? 0)").removeValue()
                        snapShot.ref.removeAllObservers()
                    }else{
                        snapShot.ref.removeAllObservers()
                        //FIXME: ADD BECAUSE AGAIN SHOW SAME WHILE ACCEPT TRIP
                        CacheArray.shared.placeOrderRequest = nil
                        Utility.setMapWhereAreYou()
                    }
                }
            }
        }
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.sideMenuController?.isLeftViewSwipeGestureDisabled = true
        homeVC = self
        self.getVehicleList()
//        if self.orderCheckStatus == nil{
            self.checkOrderStatus()
//        }
    }
    
    func initialDetail(){
        CacheArray.shared.currentService = nil
        self.navigationController?.navigationBar.isHidden = true
        self.locationPermistion()
        self.centerMapOnUserLocation()
        let currantLocation = CustomPin(pinTitle: "", Location: CLLocationCoordinate2D(latitude: Double(currentLatitude) ?? 0.0, longitude:  Double(currentLongitude) ?? 0.0), storeImage: "Location_icon", pinId: -1)
            self.mapView.addAnnotation(currantLocation)
        self.vehicleSubView.clipsToBounds = true
        self.vehicleSubView.layer.cornerRadius = 30
        self.vehicleSubView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.addMotoristView.clipsToBounds = true
        self.addMotoristView.layer.cornerRadius = 30
        self.addMotoristView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.serviceSubView.clipsToBounds = true
        self.serviceSubView.layer.cornerRadius = 30
        self.serviceSubView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        self.setView(tag: 0)
        self.garageVehicleTableView.register(UINib(nibName: "GarageVehicleCell", bundle: Bundle.main), forCellReuseIdentifier: "GarageVehicleCell")
        self.serviceCollectionView.register(UINib(nibName: "ServiceCell", bundle: Bundle.main), forCellWithReuseIdentifier: "ServiceCell")
        self.serviceCollectionView.register(UINib(nibName: "NearByStationCell", bundle: Bundle.main), forCellWithReuseIdentifier: "NearByStationCell")
        self.vehicleAnswerTableView.register(UINib(nibName: "ServiceQuestionCell", bundle: Bundle.main), forCellReuseIdentifier: "ServiceQuestionCell")
        self.serviceCollectionView.register(UINib(nibName: "StationHeaderView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "StationHeaderView")
        self.serviceCollectionView.register(UINib(nibName: "AdvertisementMainCell", bundle: Bundle.main), forCellWithReuseIdentifier: "AdvertisementMainCell")

        self.garageVehicleTableView.delegate = self
        self.garageVehicleTableView.dataSource = self
//        self.serviceCollectionView.delegate = self
//        self.serviceCollectionView.dataSource = self
        self.vehicleAnswerTableView.delegate = self
        self.vehicleAnswerTableView.dataSource = self
//        self.getService()
        self.getVehicleMakeAPI()
//        self.getVehicleList()
        self.getVehicleYearListAPI()
        self.getVehicleColorListAPI()
        NotificationCenter.default.addObserver(self, selector:#selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
//        self.requestLoadBannerAd()
        self.getGasStationList()
        self.getChargeStationList()
        self.getAdvertisementList()
//        self.openServicesScreen()
    }
    
    @objc func appMovedToForeground(){
        if self.checkLocationPermission() == false{
            self.openLocationAlert()
        }
    }
    
    func checkLocationPermission() -> Bool{
        if CLLocationManager.locationServicesEnabled() {

            let status = CLLocationManager.authorizationStatus()

            // Handle each case of location permissions
            switch status {
            case .authorizedAlways:
                return true
            case .authorizedWhenInUse:
                return true
            case .denied:
                return false
            case .notDetermined:
                return false
            case .restricted:
                return false
            @unknown default:
                return false
            }
        }else{
            print("service not enabled")
            return false
        }
    }
    
    func locationPermistion(){
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        mapView.showsCompass = false
        if CLLocationManager.locationServicesEnabled() {

            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            self.locationManager.distanceFilter = kCLDistanceFilterNone

            self.locationManager.startUpdatingLocation()
//            Utility.showAlert(vc: self, message: "start map")
        }else{
            self.openLocationAlert()
        }
    }
    
    func manageContinueButton(){
//        self.serviceContinueButton.alpha = (self.selectedService == -1 || self.orderCheckStatus?.isUpCommingJobExist == 1) ? 0.3 : 1
//        self.serviceContinueButton.isUserInteractionEnabled = self.selectedService == -1 ? false : true
//        
//        self.nowButton.alpha = self.selectedService == -1 ? 0.3 : 1
//        self.nowButton.isUserInteractionEnabled = self.selectedService == -1 ? false : true
        if !self.selectServiceView.isHidden{
            self.answerView.isHidden = false
        }else{
            
        }
    }
    
    
    @IBAction func onMenu(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            self.sideMenuController?.showLeftViewAnimated(sender: self)
        }
    }
    
    @IBAction func onAddVehicle(_ sender: Any) {
        self.setView(tag: 1)
    }
    
    @IBAction func onNavigateCurrantLocation(_ sender: UIButton) {

        self.centerMapOnUserLocation()
    }
    
    @IBAction func onSelectFromGarage(_ sender: Any) {
        self.setView(tag: 0)
    }
    
    @IBAction func onAddNewVehicle(_ sender: UIButton) {
        self.addMotoristView.isHidden = false
        self.garageView.isHidden = true
    }
    
    @IBAction func onSelectVehicle(_ sender: Any) {
        var arr = self.vehicleListArray.map({$0.make ?? ""})
        arr.append("Add new vehicle")
        self.displayDropDown(array: arr, tag: 4, view: self.selectVehicleTextField)
    }
    
    
    func setView(tag: Int){
        if tag == 0{
            self.isGarage = true
            self.selectGarageView.borderColor = UIColor(named: "app_black_color")
            self.addVehicleView.borderColor = Utility.getUIcolorfromHex(hex: "000000").withAlphaComponent(0.1)
        }else{
            self.isGarage = false
            self.addVehicleView.borderColor = UIColor(named: "app_black_color")
            self.selectGarageView.borderColor = Utility.getUIcolorfromHex(hex: "000000").withAlphaComponent(0.1)
        }
    }
    
    @IBAction func onSelectVehicleContinue(_ sender: Any) {
        if self.isGarage{
            self.garageView.isHidden = false
        }else{
            self.addMotoristView.isHidden = false
        }
    }
    
    @IBAction func onAddVehicleContinue(_ sender: Any) {
        if let error = self.checkAddVehicleValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            self.selectServiceView.isHidden = false
            self.answerView.isHidden = false
            self.selectVehicleTextField.text = self.vehicleMakeTextField.text
        }
    }
    
    @IBAction func onSelectServiceContinue(_ sender: Any) {
        if self.orderCheckStatus?.isUpCommingJobExist == 1{
            Utility.showAlert(vc: self, message: "You can't schedule right now because another scheduled service is already ongoing.")
        }else{
            self.isScheduleSerivce = true
            if self.selectedService != -1{
    //            if self.serviceArray[self.selectedService].name == "Fuel"{
    //                self.vehicleAnswerTableView.isHidden = true
    //                self.fuelStackView.isHidden = false
    //                self.fuelQuestionTitleLabel.isHidden = false
    //            }else{
                    self.vehicleAnswerTableView.isHidden = false
                    self.fuelStackView.isHidden = true
                    self.fuelQuestionTitleLabel.isHidden = true
    //            }
                self.answerView.isHidden = false
            }
        }
    }
    
    @IBAction func onServiceNow(_ sender: Any) {
        self.isScheduleSerivce = false
        if self.selectedService != -1{
//            if self.serviceArray[self.selectedService].name == "Fuel"{
//                self.vehicleAnswerTableView.isHidden = true
//                self.fuelStackView.isHidden = false
//                self.fuelQuestionTitleLabel.isHidden = false
//            }else{
                self.vehicleAnswerTableView.isHidden = false
                self.fuelStackView.isHidden = true
                self.fuelQuestionTitleLabel.isHidden = true
//            }
            self.answerView.isHidden = false
        }
    }
    
    
    @IBAction func onAnswerContiune(_ sender: Any) {
        if self.selectedService != -1{
            if self.serviceArray[self.selectedService].name == "Fuel"{
                if selectedFualTitle == ""{
                    Utility.showAlert(vc: self, message: "Please select fuel option")
                    return
                }
            }
        }
        if self.questionItemArray.allSatisfy({$0.selectedAnswer != nil}) && self.selectVehicleTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count != 0{
            self.placeOrder()
        }else{
            Utility.showAlert(vc: self, message: "Please select vehicle and answer all questions to continue.")
        }
    }
    
    
    @IBAction func onAddVehicleGarrage(_ sender: Any) {
        self.addVehicleCheckBoxView.isHidden = !self.addVehicleCheckBoxView.isHidden
    }
    
    @IBAction func onSelectVehicleBack(_ sender: Any) {
        self.selectVehicleView.isHidden = true
    }
    
    @IBAction func onGarageBack(_ sender: Any) {
        self.garageView.isHidden = true
        self.selectedVehicle = nil
        self.vehicleImageView.image = nil
        self.vehiclePlateNumberTextFiled.text = nil
    }
    
    @IBAction func onAnswerBack(_ sender: Any) {
        self.answerView.isHidden = true
    }
    
    @IBAction func onStartService(_ sender: Any) {
//        if self.orderCheckStatus?.isCardAdded == 0{
//            self.showCardAlert()
//        }else
        if self.orderCheckStatus?.isNextHoureJobComming == 1{
            Utility.showAlert(vc: self, message: "You can't create a service because your scheduled service will be starting in an hour.")
        }else{
            self.selectVehicleView.isHidden = false
        }
    }
    
    @IBAction func onBackMotorist(_ sender: Any) {
        self.view.endEditing(true)
        self.addMotoristView.isHidden = true
        self.answerView.isHidden = false
    }
    
    @IBAction func onServiceBack(_ sender: Any) {
        self.selectServiceView.isHidden = true
    }
    
    
    @IBAction func onGasoline(_ sender: Any) {
        self.gasolineView.layer.backgroundColor = #colorLiteral(red: 0.2549019608, green: 0.9176470588, blue: 0.831372549, alpha: 1)
        self.midGradeView.layer.backgroundColor =  #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
        self.premiumView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
        self.dieselView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
        self.selectedFualTitle = "gasoline"
    }
    
    @IBAction func onMidGrade(_ sender: Any) {
        self.gasolineView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
        self.midGradeView.layer.backgroundColor =  #colorLiteral(red: 0.2549019608, green: 0.9176470588, blue: 0.831372549, alpha: 1)
        self.premiumView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
        self.dieselView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
        self.selectedFualTitle = "midGrade"
    }
    
    @IBAction func onPremium(_ sender: Any) {
        self.gasolineView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
        self.midGradeView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
        self.premiumView.layer.backgroundColor = #colorLiteral(red: 0.2549019608, green: 0.9176470588, blue: 0.831372549, alpha: 1)
        self.dieselView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
        self.selectedFualTitle = "premium"
    }
    
    @IBAction func onDiesel(_ sender: Any) {
        self.gasolineView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
        self.midGradeView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
        self.premiumView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
        self.dieselView.layer.backgroundColor = #colorLiteral(red: 0.2549019608, green: 0.9176470588, blue: 0.831372549, alpha: 1)
        self.selectedFualTitle = "diesel"
    }
    
    @IBAction func onVehicleModelYear(_ sender: Any) {
        var arr: [String] = []
        for i in self.vehicleYearArray{
            if let item = i.name{
                arr.append(item)
            }
        }
        self.displayDropDown(array: arr, tag: 2, view: self.vehicleModelYearTextField)
    }
    
    @IBAction func onVehicleMake(_ sender: Any) {
        var arr: [String] = []
        for i in self.vehicleMakeArray{
            if let item = i.name{
                arr.append(item)
            }
        }
        self.displayDropDown(array: arr, tag: 0, view: self.vehicleMakeTextField)
    }
    
    @IBAction func onVehicleModel(_ sender: Any) {
        var arr: [String] = []
        for i in self.vehicleModelArray{
            if let item = i.name{
                arr.append(item)
            }
        }
        self.displayDropDown(array: arr, tag: 1, view: self.vehicleModelTextField)
    }
    
    @IBAction func onVehicleColor(_ sender: Any) {
        var arr: [String] = []
        for i in self.vehicleColorArray{
            if let item = i.name{
                arr.append(item)
            }
        }
        self.displayDropDown(array: arr, tag: 3, view: self.colorTextField)
    }
    
    @IBAction func onCapturePhoto(_ sender: Any) {
        self.photoSelectOption()
    }
    
    @IBAction func onNotification(_ sender: Any) {
        self.dismiss(animated: true) {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            let vc = STORYBOARD.notification.instantiateViewController(withIdentifier: "NotificationScreen") as! NotificationScreen
            vc.backClicked = { [weak self] in
                self?.observeOrder()
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    func showCardAlert() {
        let alert = UIAlertController(title: APPLICATION_NAME, message: "Please enter your card details so it will be use for creating a service.",         preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
              //Cancel Action
            let vc = STORYBOARD.payment.instantiateViewController(withIdentifier: "CardListScreen") as! CardListScreen
            vc.isAddNewCard = true
            self.navigationController?.pushViewController(vc, animated: true)
          }))
        
          self.present(alert, animated: true, completion: nil)
      }
    
    func photoSelectOption(){
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // Create the actions
        let takePhoto = UIAlertAction(title: "Take photo", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera()
        }
        let galleryPhoto = UIAlertAction(title: "Photo library", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(takePhoto)
        alertController.addAction(galleryPhoto)
        alertController.addAction(cancelAction)
        
        //   Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func openCamera(){
        let imagePicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openGallery(){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker,animated: true,completion: nil)
    }
    
    //MARK:- DROP DOWN
    func displayDropDown(array: [String],tag: Int,view: UIView){
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.anchorView = view.superview
        dropDown.dataSource = array
        dropDown.direction = .bottom
        dropDown.width = screenWidth - 64
        dropDown.bottomOffset = CGPoint(x: 0, y: 65)
        dropDown.backgroundColor = .white
        dropDown.cornerRadius = 15
        dropDown.show()
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            switch tag {
            case 0:
                self?.vehicleMakeTextField.text = item
                self?.vehicleModelTextField.text = nil
                self?.vehicleModelArray = self?.vehicleMakeArray[index].models ?? []
                dropDown.hide()
            case 1:
                self?.vehicleModelTextField.text = item
                dropDown.hide()
            case 2:
                self?.vehicleModelYearTextField.text = item
                dropDown.hide()
            case 4:
                if array.count - 1 == index{
                    self?.answerView.isHidden = true
                    self?.addMotoristView.isHidden = false
                }else{
                    self?.selectedVehicle = self?.vehicleListArray[index]
                    self?.selectVehicleTextField.text = item
//                    Utility.setImage(self?.vehicleListArray[index].image, imageView: self?.vehicleImageView)
                    self?.vehiclePlateNumberTextFiled.text = self?.vehicleListArray[index].plate
                }
                
                dropDown.hide()
            default:
                self?.colorTextField.text = item
                dropDown.hide()
            }
        }
    }
    
    //MARK:- VALIDATION
    func checkAddVehicleValidation() -> String?{
        if self.vehiclePlateNumberTextFiled.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 && self.selectedVehicle == nil{
            return "Please enter vehicle plate number"
        }else if self.vehicleModelYearTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 && self.selectedVehicle == nil{
            return "Please select vehicle model year"
        }else if self.vehicleMakeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 && self.selectedVehicle == nil{
            return "Please select vehicle make"
        }else if self.vehicleModelTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 && self.selectedVehicle == nil{
            return "Please select vehicle model"
        }else if self.colorTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 && self.selectedVehicle == nil{
            return "Please select vehicle color"
        }
        return nil
    }
    
    //MARK:- MAP LOCATION
    // MARK: - showRouteOnMap

    func showRouteOnMap(pickupCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D) {
        
        let sourcePlacemark = MKPlacemark(coordinate: pickupCoordinate, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinate, addressDictionary: nil)

        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)

        let sourceAnnotation = MKPointAnnotation()

        if let location = sourcePlacemark.location {
            sourceAnnotation.coordinate = location.coordinate
        }

        let destinationAnnotation = MKPointAnnotation()

        if let location = destinationPlacemark.location {
            
            destinationAnnotation.coordinate = location.coordinate
        }
        
        let data = CustomPin(pinTitle: "", Location: CLLocationCoordinate2D(latitude: Double(currentLatitude) ?? 0.0, longitude:  Double(currentLongitude) ?? 0.0), storeImage: "Location_icon", pinId: -1)
        
        let data1 = CustomPin(pinTitle: "", Location:destinationCoordinate, storeImage: "Location_icon", pinId: -1)

        
        //self.mapView.addAnnotations()
        self.mapView.showAnnotations([data,data1], animated: true )

        let directionRequest = MKDirections.Request()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile

        // Calculate the direction
        let directions = MKDirections(request: directionRequest)

        directions.calculate {
            (response, error) -> Void in

            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }

                return
            }

            let route = response.routes[0]
            self.mapView.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)

            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
        }
    }
    
    //MARK:- DROP DOWN
    func displayQuestionAnswerDropDown(array: [String],tag: Int,view: UIView){
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.anchorView = view
        dropDown.dataSource = array
        dropDown.direction = .bottom
        dropDown.width = screenWidth - 64
        dropDown.bottomOffset = CGPoint(x: 0, y: 65)
        dropDown.backgroundColor = .white
        dropDown.cornerRadius = 15
        dropDown.show()
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            
            if self?.selectedService != -1{
                if self?.serviceArray[self?.selectedService ?? 0].name == "Fuel"{
                    let array = item.components(separatedBy: " ")
                    self?.selectedFualTitle = array.first?.lowercased() ?? ""
                }
            }
            
            self?.questionItemArray[tag].selectedAnswer = item
            self?.vehicleAnswerTableView.reloadData()
            dropDown.hide()
            if let description = self?.questionItemArray[tag].answers?[index].description{
                self?.answerDescriptionView.isHidden = false
                self?.answerDescriptionLabel.text = description
            }else{
                self?.answerDescriptionView.isHidden = true
            }
//            switch tag {
//            case 0:
//                self?.vehicleMakeTextField.text = item
//                self?.vehicleModelTextField.text = nil
//                self?.vehicleModelArray = self?.vehicleMakeArray[index].models ?? []
//                dropDown.hide()
//            case 1:
//                self?.vehicleModelTextField.text = item
//                dropDown.hide()
//            case 2:
//                self?.vehicleModelYearTextField.text = item
//                dropDown.hide()
//            default:
//                self?.colorTextField.text = item
//                dropDown.hide()
//            }
        }
    }
}
//MARK: ADMOB BANNER AD
extension MapScreen: GADBannerViewDelegate{
    
    func requestLoadBannerAd(){
        self.googleBannerAdView = GADBannerView(adSize: GADAdSizeBanner)
        self.googleBannerAdView?.adSize = GADCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(screenWidth)
        self.googleBannerAdView?.adUnitID = google_banner_ad_id
        self.googleBannerAdView?.rootViewController = self
        self.googleBannerAdView?.load(GADRequest())
        self.googleBannerAdView?.delegate = self
    }
    
    func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
      print("bannerViewDidReceiveAd")
        if Utility.getUserData()?.subscription?.isPremium == false{
            self.bannerAdShowingView.addSubview(bannerView)
            UIView.animate(withDuration: 0.5) { [weak self] in
                self?.bannerViewHeightConstraint.constant = GADCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(screenWidth).size.height
                self?.view.layoutIfNeeded()
            }
        }
    }

    func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
      print("bannerView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }

    func bannerViewDidRecordImpression(_ bannerView: GADBannerView) {
      print("bannerViewDidRecordImpression")
    }

    func bannerViewWillPresentScreen(_ bannerView: GADBannerView) {
      print("bannerViewWillPresentScreen")
    }

    func bannerViewWillDismissScreen(_ bannerView: GADBannerView) {
      print("bannerViewWillDIsmissScreen")
    }

    func bannerViewDidDismissScreen(_ bannerView: GADBannerView) {
      print("bannerViewDidDismissScreen")
    }
}
extension MapScreen: CLLocationManagerDelegate,MKMapViewDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        currentLatitude = "\(locValue.latitude)"
        currentLongitude = "\(locValue.longitude)"

        print("locations = \(locValue.latitude) \(locValue.longitude)")
//        Utility.showAlert(vc: self, message: "update")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        print("error::: \(error)")
//        locationManager.stopUpdatingLocation()
//        Utility.showAlert(vc: self, message: "error::: \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
           
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            manager.startUpdatingLocation()
            
            break
        case .authorizedAlways:
            // If always authorized
            ()
            manager.startUpdatingLocation()
            break
        case .restricted:
            self.openLocationAlert()
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            self.openLocationAlert()
            // If user denied your app access to Location Services, but can grant access from Settings.app
            break
        default:
            break
        }
    }
    
    func openLocationAlert(){
        let alert = UIAlertController(title: "Settings", message: "Allow location permission from settings", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Open", style: .default, handler: { action in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: center Map On UserLocation
    func centerMapOnUserLocation() {
        guard let coordinate = locationManager.location?.coordinate else { return }
        let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: 5000, longitudinalMeters: 5000)
        mapView.setRegion(region, animated: true)
    }
    
    // MARK: Set Pin On MapView
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        if annotation is MKUserLocation {
            return nil
        }
        let reuseID = "Location"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseID)
            annotationView?.canShowCallout = true
            let pin = MKAnnotationView(annotation: annotation,
                                       reuseIdentifier: reuseID)
            
            let pinData = annotation as? CustomPin
            
            if pinData?.pinId == -1{
                pin.image = UIImage(named: "currant_location_pin")
            }else{
                pin.isEnabled = true
                pin.canShowCallout = true
                let storeImageView = UIImageView()
                storeImageView.frame = CGRect(x: 4, y: 4, width: 44, height: 44)
                storeImageView.layer.cornerRadius = storeImageView.frame.height/2
                storeImageView.layer.masksToBounds = true
//                storeImageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//                storeImageView.layer.borderWidth = 2
                
                let label = UILabel(frame: CGRect(x: pin.center.x, y: pin.center.y, width: 25, height: 25))
                label.textAlignment = .center
                label.textColor = .white
                label.font = label.font.withSize(14)
               
                //            label.text = cpa.pinId
                Utility.setImage(pinData?.storeImage, imageView: storeImageView)
                pin.addSubview(storeImageView)
                
            }
            annotationView = pin
            annotationView?.annotation = annotation
        } else {
            let pin = MKAnnotationView(annotation: annotation,
                                       reuseIdentifier: reuseID)
            let pinData = annotation as? CustomPin
            
            if pinData?.pinId == -1{
                pin.image = UIImage(named: "currant_location_pin")
            }else{
                pin.isEnabled = true
                pin.canShowCallout = true
                let storeImageView = UIImageView()
                storeImageView.frame = CGRect(x: 4, y: 4, width: 44, height: 44)
                storeImageView.layer.cornerRadius = storeImageView.frame.height/2
                storeImageView.layer.masksToBounds = true
//                storeImageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//                storeImageView.layer.borderWidth = 2
                
                let label = UILabel(frame: CGRect(x: pin.center.x, y: pin.center.y, width: 25, height: 25))
                label.textAlignment = .center
                label.textColor = .white
                label.font = label.font.withSize(14)
               
                //            label.text = cpa.pinId
                Utility.setImage(pinData?.storeImage, imageView: storeImageView)
                pin.addSubview(storeImageView)
                
            }
            annotationView = pin
            annotationView?.annotation = annotation
        }
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {

        let renderer = MKPolylineRenderer(overlay: overlay)

        renderer.strokeColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)

        renderer.lineWidth = 5.0

        return renderer
    }
}
//MARK:- TABLEVIEW DELEGATE & DATASOURCE
extension MapScreen: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.vehicleAnswerTableView{
            return self.questionItemArray.count
        }
        return self.vehicleListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.garageVehicleTableView{
            let cell = self.garageVehicleTableView.dequeueReusableCell(withIdentifier: "GarageVehicleCell", for: indexPath) as! GarageVehicleCell
            cell.item = self.vehicleListArray[indexPath.row]
            if selectedGarageIndex == indexPath.row{
                cell.garageView.layer.borderColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
//                cell.garageView.layer.borderWidth = 3
            }else{
                cell.garageView.layer.borderColor = #colorLiteral(red: 0.01960784314, green: 0.0862745098, blue: 0.1490196078, alpha: 0.2)
//                cell.garageView.layer.borderWidth = 3
            }
            return cell
        }
        let cell = self.vehicleAnswerTableView.dequeueReusableCell(withIdentifier: "ServiceQuestionCell", for: indexPath) as! ServiceQuestionCell
        cell.item = self.questionItemArray[indexPath.row]
        
//        if self.questionItemArray[indexPath.row].questionAnswer == 0{
//            cell.yesButton.backgroundColor = Utility.getUIcolorfromHex(hex: "2F80ED").withAlphaComponent(0.2)
//            cell.yesButton.setTitleColor(Utility.getUIcolorfromHex(hex: "2F80ED"), for: .normal)
//
//            cell.noButton.backgroundColor = Utility.getUIcolorfromHex(hex: "EB5757").withAlphaComponent(0.2)
//            cell.noButton.setTitleColor(Utility.getUIcolorfromHex(hex: "EB5757"), for: .normal)
//        }else if self.questionItemArray[indexPath.row].questionAnswer == 1{
//            cell.yesButton.backgroundColor = Utility.getUIcolorfromHex(hex: "2F80ED")
//            cell.yesButton.setTitleColor(UIColor.white, for: .normal)
//
//            cell.noButton.backgroundColor = Utility.getUIcolorfromHex(hex: "EB5757").withAlphaComponent(0.2)
//            cell.noButton.setTitleColor(Utility.getUIcolorfromHex(hex: "EB5757"), for: .normal)
//        }else{
//            cell.yesButton.backgroundColor = Utility.getUIcolorfromHex(hex: "2F80ED").withAlphaComponent(0.2)
//            cell.yesButton.setTitleColor(Utility.getUIcolorfromHex(hex: "2F80ED"), for: .normal)
//
//            cell.noButton.backgroundColor = Utility.getUIcolorfromHex(hex: "EB5757")
//            cell.noButton.setTitleColor(UIColor.white, for: .normal)
//        }
        
        cell.onYesQuestion = {
//            cell.yesButton.backgroundColor = Utility.getUIcolorfromHex(hex: "2F80ED")
//            cell.yesButton.setTitleColor(UIColor.white, for: .normal)
//
//            cell.noButton.backgroundColor = Utility.getUIcolorfromHex(hex: "EB5757").withAlphaComponent(0.2)
//            cell.noButton.setTitleColor(Utility.getUIcolorfromHex(hex: "EB5757"), for: .normal)
//            self.questionItemArray[indexPath.row].questionAnswer = 1
            self.vehicleAnswerTableView.reloadRows(at: [indexPath], with: .none)
        }
        cell.onNoQuestion = {
//            cell.yesButton.backgroundColor = Utility.getUIcolorfromHex(hex: "2F80ED").withAlphaComponent(0.2)
//            cell.yesButton.setTitleColor(Utility.getUIcolorfromHex(hex: "2F80ED"), for: .normal)
//
//            cell.noButton.backgroundColor = Utility.getUIcolorfromHex(hex: "EB5757")
//            cell.noButton.setTitleColor(UIColor.white, for: .normal)
//            self.questionItemArray[indexPath.row].questionAnswer = 2
            self.vehicleAnswerTableView.reloadRows(at: [indexPath], with: .none)
        }
        
        cell.onDropDown = { [weak self] in
            let answers = self?.questionItemArray[indexPath.row].answers?.map({$0.answer ?? ""}) ?? [""]
            self?.displayQuestionAnswerDropDown(array: answers, tag: indexPath.row, view: cell.answerView)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if tableView == self.garageVehicleTableView{
            let deleteAction = UIContextualAction(style: .normal, title: "") { [weak self] (action, view, completion) in
                self?.deleteVehicle(vehicleID: self?.vehicleListArray[indexPath.row].id ?? 0)
                completion(true)
            }
            let editAction =  UIContextualAction(style: .normal, title: "") { [weak self] (action, view, completion) in
                let vc = STORYBOARD.garage.instantiateViewController(withIdentifier: "AddVehicleScreen") as! AddVehicleScreen
                vc.modalPresentationStyle = .overCurrentContext
                vc.vehicleData = self?.vehicleListArray[indexPath.row]
                vc.addNewVehicle = { [weak self] in
                    self?.garageVehicleTableView.reloadRows(at: [indexPath], with: .none)
                }
                self?.present(vc, animated: true, completion: nil)
                completion(true)
            }
            editAction.image = UIImage(named: "swipe_edit_icon")
            editAction.backgroundColor = Utility.getUIcolorfromHex(hex: "2F80ED")
            
            deleteAction.image = UIImage(named: "swipe_delete_icon")
            deleteAction.backgroundColor = Utility.getUIcolorfromHex(hex: "EB5757")
            
            let deleteSwipe = UISwipeActionsConfiguration(actions: [deleteAction,editAction])
            
            deleteSwipe.performsFirstActionWithFullSwipe = false // This is the line which disables full swipe
            return deleteSwipe
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.garageVehicleTableView{
            selectedGarageIndex = indexPath.row
            self.garageVehicleTableView.reloadData()
            self.selectedVehicle = self.vehicleListArray[indexPath.row]
            Utility.setImage(self.vehicleListArray[indexPath.row].image, imageView: self.vehicleImageView)
            self.vehiclePlateNumberTextFiled.text = self.vehicleListArray[indexPath.row].plate
            self.selectServiceView.isHidden = false
        }
    }
}
//MARK:- COLLECIONVIEW DELEGATE & DATASOURCE
extension MapScreen: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "StationHeaderView", for: indexPath) as! StationHeaderView
            if indexPath.section == 1{
                headerView.titleLabel.text = "Shop Near By"
            }else if indexPath.section == 2{
                headerView.titleLabel.text = "Gas Station Near By"
            }else if indexPath.section == 3{
                headerView.titleLabel.text = "Charging Station Near By"
            }
            headerView.seeMore = { [weak self] in
                if indexPath.section == 1{
                    let vc = STORYBOARD.map.instantiateViewController(withIdentifier: "ShopListScreen") as! ShopListScreen
                    vc.titleString = "Shop Near By"
                    self?.navigationController?.pushViewController(vc, animated: true)
                }else{
                    let vc = STORYBOARD.map.instantiateViewController(withIdentifier: "StationListScreen") as! StationListScreen
                    if indexPath.section == 2{
                        vc.titleString = "Gas Station Near By"
                        vc.itemArray = self?.gasStationListArray ?? []
                    }else{
                        vc.titleString = "Charging Station Near By"
                        vc.itemArray = self?.chargeStationListArray ?? []
                    }
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
            }
            if indexPath.section == 1{
                headerView.sapratorView.isHidden = self.advertisementListArray.isEmpty ? true : false
            }else if indexPath.section == 2{
                headerView.sapratorView.isHidden = (self.advertisementListArray.isEmpty && self.gasStationListArray.isEmpty == false) ? false : true
            }else {
                headerView.sapratorView.isHidden = (self.gasStationListArray.isEmpty && self.chargeStationListArray.isEmpty == false) ? false : true
            }
            return headerView
        default:
            assert(false, "Unexpected element kind")
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let leftSpace = (self.serviceCollectionView.collectionViewLayout as! UICollectionViewFlowLayout).sectionInset.left
        let rightSpace = (self.serviceCollectionView.collectionViewLayout as! UICollectionViewFlowLayout).sectionInset.right
        var height: CGFloat = 0
        if section == 1{
            height = self.advertisementListArray.isEmpty ? 0 : 40
        }else if section == 2{
            height = self.gasStationListArray.isEmpty ? 0 : 40
        }else if section == 3{
            height = self.chargeStationListArray.isEmpty ? 0 : 40
        }
        return CGSize(width: screenWidth - (leftSpace + rightSpace), height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            return self.serviceArray.count
        }else if section == 1{
            return 1
        }else if section == 2{
            return self.gasStationListArray.count > 2 ? 2 : self.gasStationListArray.count
        }else {
            return self.chargeStationListArray.count > 2 ? 2 : self.chargeStationListArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0{
            let cell = self.serviceCollectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCell", for: indexPath) as! ServiceCell
            cell.item = self.serviceArray[indexPath.row]
            if self.selectedService == indexPath.row{
                
                cell.mainView.borderColor =  Utility.checkLightModeUserDefalt() ? Utility.getUIcolorfromHex(hex: "051626"): Utility.getUIcolorfromHex(hex: "41EAD4")
                
                
                cell.mainView.borderWidth = 2
                cell.mainView.shadowColor = Utility.getUIcolorfromHex(hex: "000000").withAlphaComponent(0.1)
                cell.mainView.shadowOpacity = 1
                cell.mainView.shadowOffset.width = 1
                cell.mainView.shadowOffset.height = 1
                cell.mainView.shadowRadius = 1
            }else{
                cell.mainView.borderColor = .clear
                cell.mainView.borderWidth = 0
                cell.mainView.shadowColor = .clear
                cell.mainView.shadowOpacity = 0
                cell.mainView.shadowOffset.width = 0
                cell.mainView.shadowOffset.height = 0
                cell.mainView.shadowRadius = 0
            }
            return cell
        }else if indexPath.section == 1{
            let cell = self.serviceCollectionView.dequeueReusableCell(withReuseIdentifier: "AdvertisementMainCell", for: indexPath) as! AdvertisementMainCell
            cell.itemArray = self.advertisementListArray
            cell.collectionView.reloadData()
            return cell
        }
        let cell = self.serviceCollectionView.dequeueReusableCell(withReuseIdentifier: "NearByStationCell", for: indexPath) as! NearByStationCell
        if indexPath.section == 2{
            cell.item = self.gasStationListArray[indexPath.item]
        }else{
            cell.item = self.chargeStationListArray[indexPath.item]
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let leftSpace = (self.serviceCollectionView.collectionViewLayout as! UICollectionViewFlowLayout).sectionInset.left
        let rightSpace = (self.serviceCollectionView.collectionViewLayout as! UICollectionViewFlowLayout).sectionInset.right
        let cellSpacing = (self.serviceCollectionView.collectionViewLayout as! UICollectionViewFlowLayout).minimumInteritemSpacing
        if indexPath.section == 0{
            let numberOFRows : CGFloat = 4
            let scWidth : CGFloat = screenWidth - (leftSpace + rightSpace)
            let totalSpace : CGFloat = cellSpacing * (numberOFRows - 1)
            let width = (scWidth - totalSpace) / numberOFRows
            return CGSize(width: width, height: width + 40)
        }else if indexPath.section == 1{
            let width = (screenWidth * 315) / 375
            return CGSize(width: screenWidth, height: (width * 100) / 375)
        }
        let scWidth : CGFloat = screenWidth - (leftSpace + rightSpace)
        return CGSize(width: scWidth, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        DispatchQueue.main.async { [weak self] in
//            self?.servieCollectionViewHeight.constant = collectionView.contentSize.height > (screenHeight - 320) ? screenHeight - 320 : collectionView.contentSize.height
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        self.openScreen()
//        return
        if indexPath.section == 0{
            self.selectedService = indexPath.row
            self.serviceCollectionView.reloadData()
            self.manageContinueButton()
            self.questionItemArray = self.serviceArray[indexPath.row].questions ?? []
            self.answerDescriptionView.isHidden = true
            self.vehicleAnswerTableView.reloadData()
        }else if indexPath.section == 1{
            OpenMapDirections.present(in: self, sourceView: self.view, fromLat: currentLatitude, fromLong: currentLongitude, toLat: "\(self.gasStationListArray[indexPath.item].lat ?? 0)", toLong: "\(self.gasStationListArray[indexPath.item].lng ?? 0)")
        }else{
            OpenMapDirections.present(in: self, sourceView: self.view, fromLat: currentLatitude, fromLong: currentLongitude, toLat: "\(self.chargeStationListArray[indexPath.item].lat ?? 0)", toLong: "\(self.chargeStationListArray[indexPath.item].lng ?? 0)")
        }
    }
}
//MARK:- UIIMAGEPICKER DELEGATE
extension MapScreen: UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        self.vehicleImageView.image = image
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: false, completion: nil)
    }
}
//MARK:- API{
extension MapScreen{
    
    func getService(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            MapService.shared.getService { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self?.serviceArray = res
                    CacheArray.shared.serviceArray = res
                }
                self?.serviceCollectionView.reloadData()
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }

        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Vehicle List API
    func getVehicleMakeAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            VehicleService.shared.getVehicleMake{ [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self?.vehicleMakeArray = res
//                    self.setData()
                    CacheArray.shared.vehicleMakeArray = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Vehicle Year API
    func getVehicleYearListAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            VehicleService.shared.getVehicleYear { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self?.vehicleYearArray = res
                    CacheArray.shared.vehicleYearArray = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Vehicle Color API
    func getVehicleColorListAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            VehicleService.shared.getVehicleColor{ [weak self]  (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self?.vehicleColorArray = res
                    CacheArray.shared.vehicleColorArray = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Vehicle LIST API
    func getVehicleList(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
//            Utility.showIndicator()
            VehicleService.shared.getVehicles { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self?.vehicleListArray = res
                }
                self?.garageVehicleTableView.reloadData()
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- PLACE ORDER API
    func placeOrder(){
        self.view.endEditing(true)
        var vehicleYearID: String?
        var vehicleMakeID: String?
        var vehicleModelID: String?
        var vehicleColorID: String?
        if let data = self.vehicleYearArray.first(where: {$0.name == self.vehicleModelYearTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)}){
            vehicleYearID = "\(data.vehicle_year_id ?? 0)"
        }else{
            vehicleYearID = self.selectedVehicle?.year//"\(self.selectedVehicle?.vehicleYearId ?? 0)"
        }
        if let data = self.vehicleMakeArray.first(where: {$0.name == self.vehicleMakeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)}){
            vehicleMakeID = "\(data.vehicleMakeId ?? 0)"
        }else{
            vehicleMakeID = self.selectedVehicle?.make//"\(self.selectedVehicle?.vehicleMakeId ?? 0)"
        }
        
        if let data = self.vehicleModelArray.first(where: {$0.name == self.vehicleModelTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)}){
            vehicleModelID = "\(data.id ?? 0)"
        }else{
            vehicleModelID = self.selectedVehicle?.model//"\(self.selectedVehicle?.vehicleModelId ?? 0)"
        }
        
        if let data = self.vehicleColorArray.first(where: {$0.name == self.colorTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)}){
            vehicleColorID = "\(data.vehicle_color_id ?? 0)"
        }else{
            vehicleColorID = self.selectedVehicle?.color//"\(self.selectedVehicle?.vehicleColorId ?? 0)"
        }
        
        var questionArr: [AddQuetionRequest] = []
        for i in self.questionItemArray{
            questionArr.append(AddQuetionRequest(questionId: "\(i.serviceQuestionId ?? 0)", answer: i.selectedAnswer))
        }
        
        let data = PlaceOrderRequest(serviceId: "\(self.serviceArray[self.selectedService].serviceId ?? 0)",user_garage_id: "\(self.selectedVehicle?.id ?? 0)"/*, plateNumber: self.vehiclePlateNumberTextFiled.text?.trimmingCharacters(in: .whitespacesAndNewlines), vehicleYearId: vehicleYearID, vehicleMakeId: vehicleMakeID, vehicleModelId: vehicleModelID, vehicleColorId: vehicleColorID*/, pickup_latitude: nil, pickup_longitude: nil, pickup_address: nil, destination_latitude: nil, destination_longitude: nil, destination_address: nil, in_garage: self.isGarage ? "TRUE" : "FALSE", questions: questionArr.toJSONString(),fuleType: selectedFualTitle,state: nil, scheduleDate: nil,autoTransportStartDate: nil, deviceType: "ios")
        CacheArray.shared.placeOrderRequest = data
        CacheArray.shared.vehicleImage = self.vehicleImageView.image?.jpegData(compressionQuality: 0.3)

        if self.isScheduleSerivce{
            let vc = STORYBOARD.map.instantiateViewController(withIdentifier: "TransportServiceTimeScreen") as! TransportServiceTimeScreen
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            Utility.setMapWhereAreYou()
        }
    }
        
    func deleteVehicle(vehicleID: Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            VehicleService.shared.deleteVehicle(vehicleID: vehicleID) { [weak self] (stausCode, response) in
                Utility.hideIndicator()
                if let index = self?.vehicleListArray.firstIndex(where: {$0.id == vehicleID}){
                    self?.vehicleListArray.remove(at: index)
                    self?.garageVehicleTableView.reloadData()
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func checkOrderStatus(){
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            MapService.shared.checkOrderStatus { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self.orderCheckStatus = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }

        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func getGasStationList(){
        if Utility.isInternetAvailable(){
            StationService.shared.getGasStation(lat: currentLatitude, long: currentLongitude) { [weak self] statusCode, response in
                if let res = response{
                    self?.gasStationListArray = res
                    self?.serviceCollectionView.reloadData()
                }
            } failure: { error in
                print(error)
            }
        }else{
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func getChargeStationList(){
        if Utility.isInternetAvailable(){
            StationService.shared.getChargeStation(lat: currentLatitude, long: currentLongitude) { [weak self] statusCode, response in
                if let res = response{
                    self?.chargeStationListArray = res
                    self?.serviceCollectionView.reloadData()
                }
            } failure: { error in
                print(error)
            }
        }else{
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func getAdvertisementList(){
        if Utility.isInternetAvailable(){
            StationService.shared.getAdvertisementList(page: 1, perPage: 5) { [weak self] statusCode, response in
                if let res = response.advertisementResponse{
                    var counter = 0
                    for i in res{
                        let obj = i
                        obj.gradientImage = self?.gradientArray[counter]
                        self?.advertisementListArray.append(obj)
                        counter += 1
                        if counter == 3{
                            counter = 0
                        }
                    }
                    self?.serviceCollectionView.reloadData()
                }
            } failure: { error in
                print(error)
            }

        }else{
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
//MARK: PANMODEL
extension ServicesScreen: PanModalPresentable{
    var panScrollable: UIScrollView? {
        return self.serviceCollectionView
    }

    var panModalBackgroundColor: UIColor{
        return .clear
    }
    
    var allowsDragToDismiss: Bool{
        return false
    }
    
    var anchorModalToLongForm: Bool{
        return true
    }
    
    var longFormHeight: PanModalHeight{
        return .contentHeight(self.serviceCollectionView.frame.size.height)
    }
    
    var shortFormHeight: PanModalHeight{
        return .contentHeight(250)
    }
    
    var allowsTapToDismiss: Bool{
        return false
    }
    
    var isUserInteractionEnabled: Bool{
        return true
    }
    
    var cornerRadius: CGFloat{
        return 20
    }

    var backgroundInteraction: PanModalBackgroundInteraction {
        return .forward
    }
    
//    var scrollIndicatorInsets: UIEdgeInsets{
//        return UIEdgeInsets(top: 0, left: 0, bottom: bottomSafeArea, right: 0)
//    }
    
    func close(){
        self.dismiss(animated: false)
    }
    
    func panModalWillDismiss() {
        print("panModalWillDismiss")
        self.stopTimer()
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    

}
extension MapScreen{
    
    func openServicesScreen(){
        let vc = STORYBOARD.map.instantiateViewController(withIdentifier: "ServicesScreen") as! ServicesScreen
        vc.superVC = self
        self.serviceScreen = vc
        self.navigationController?.navigationBar.isHidden = true
        presentPanModal(vc,sourceView: self.view)
        self.sideMenuController?.hideLeftView()
    }
    
}
