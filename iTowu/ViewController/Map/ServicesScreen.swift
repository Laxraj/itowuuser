//
//  ServicesScreen.swift
//  iTowu
//
//  Created by Nikunj Vaghela on 28/07/23.
//

import UIKit
import GoogleMobileAds
import PanModal
import CoreLocation
import DropDown

class ServicesScreen: UIViewController {

    @IBOutlet weak var serviceCollectionView: UICollectionView!
        
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var bannerAdShowingView: UIView!
    @IBOutlet weak var bannerViewHeightConstraint: NSLayoutConstraint!
    
    var serviceArray: [ServiceResponse] = []
    var vehicleMakeArray: [VehicleMakeResponse] = []
    var vehicleYearArray: [VehicleYearRespose] = []
    var vehicleColorArray: [VehicleColorResponse] = []
    var vehicleModelArray: [Models] = []
    
    var questionItemArray: [Questions] = []
    var vehicleListArray: [VehicleListResponse] = []
    
    var isGarage: Bool = false
    var selectedService: Int = -1
    var selectedGarageIndex = -1
    //MARK: Variable Declaration
    
    var selectedVehicle: VehicleListResponse?
    
    var selectedFualTitle = String()

    var isScheduleSerivce: Bool = false
    var orderCheckStatus: CheckOrderStatusResponse?
    
    
    var googleBannerAdView: GADBannerView?
    var gasStationListArray: [NearByGasStationList] = []
    var chargeStationListArray: [NearByGasStationList] = []
    var advertisementListArray: [AdvertisementList] = []
    let gradientArray = [UIImage(named: "gradient_1"),UIImage(named: "gradient_2"),UIImage(named: "gradient_3")]
    var superVC: MapScreen?
    
    var timer: Timer?
    var indexPath: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialDetail()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func initialDetail(){
        self.serviceCollectionView.register(UINib(nibName: "ServiceCell", bundle: Bundle.main), forCellWithReuseIdentifier: "ServiceCell")
        self.serviceCollectionView.register(UINib(nibName: "NearByStationCell", bundle: Bundle.main), forCellWithReuseIdentifier: "NearByStationCell")
       
        self.serviceCollectionView.register(UINib(nibName: "StationHeaderView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "StationHeaderView")
        self.serviceCollectionView.register(UINib(nibName: "AdvertisementMainCell", bundle: Bundle.main), forCellWithReuseIdentifier: "AdvertisementMainCell")

        self.serviceCollectionView.delegate = self
        self.serviceCollectionView.dataSource = self
//
//        self.tableView.delegate = self
//        self.tableView.dataSource = self
        
        self.tableView.register(UINib(nibName: "CardTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "CardTableViewCell")
        
        self.tableView.register(UINib(nibName: "NearByStationTableVieCell", bundle: Bundle.main), forCellReuseIdentifier: "NearByStationTableVieCell")

        
        self.tableView.register(UINib(nibName: "ServiceMainTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "ServiceMainTableViewCell")
        
        self.tableView.register(UINib(nibName: "ServiceTableHeaderView", bundle: Bundle.main), forCellReuseIdentifier: "ServiceTableHeaderView")
        
        self.tableView.register(UINib(nibName: "AdvertisementMainTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "AdvertisementMainTableViewCell")
//        self.getService()
        self.getVehicleMakeAPI()
//        self.getVehicleList()
        self.getVehicleYearListAPI()
        self.getVehicleColorListAPI()
        
        self.getService()
        self.requestLoadBannerAd()
        self.getGasStationList()
        self.getChargeStationList()
        self.getAdvertisementList()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.sideMenuController?.isLeftViewSwipeGestureDisabled = true
        self.getVehicleList()
        //        if self.orderCheckStatus == nil{
        self.checkOrderStatus()
        //        }
    }
    
    func startTimer() {
        self.timer?.invalidate()
        self.timer = nil
        self.timer =  Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
     }
    
    func stopTimer(){
        self.timer?.invalidate()
        self.timer = nil
    }
    
    @objc func scrollAutomatically(){
        if let cell = self.serviceCollectionView.cellForItem(at: IndexPath(item: 0, section: 1)) as? AdvertisementMainCell{
            if self.indexPath > self.advertisementListArray.count - 1{
                self.indexPath = 0
                cell.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: false)
            }else{
                cell.collectionView.scrollToItem(at: IndexPath(row: self.indexPath, section: 0), at: .centeredHorizontally, animated: true)
//                self.pageControl.currentPage = self.indexPath
                self.indexPath += 1
            }
        }
        print("New Index:::::\(self.indexPath)")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopTimer()
    }
}
////MARK: TABLEVIEW DELEGATE AND DATASOURCE
//extension ServicesScreen: UITableViewDelegate, UITableViewDataSource{
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 4
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if section == 0{
//            return 1
//        }
//        else if section == 1{
//            return 1
//        }else if section == 2{
//            return self.gasStationListArray.count > 2 ? 2 : self.gasStationListArray.count
//        }else {
//            return self.chargeStationListArray.count > 2 ? 2 : self.chargeStationListArray.count
//        }
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if indexPath.section == 0{
//            let cell = self.tableView.dequeueReusableCell(withIdentifier: "ServiceMainTableViewCell", for: indexPath) as! ServiceMainTableViewCell
//            cell.itemArray = self.serviceArray
//            cell.collectionView.reloadData()
//            return cell
//        }else if indexPath.section == 1{
//            let cell = self.tableView.dequeueReusableCell(withIdentifier: "AdvertisementMainTableViewCell", for: indexPath) as! AdvertisementMainTableViewCell
//            cell.itemArray = self.advertisementListArray
//            cell.collectionView.reloadData()
//            return cell
//        }
//        let cell = self.tableView.dequeueReusableCell(withIdentifier: "NearByStationTableVieCell", for: indexPath) as! NearByStationTableVieCell
//        if indexPath.section == 2{
//            cell.item = self.gasStationListArray[indexPath.row]
//        }else{
//            cell.item = self.chargeStationListArray[indexPath.row]
//        }
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerView = self.tableView.dequeueReusableCell(withIdentifier: "ServiceTableHeaderView") as! ServiceTableHeaderView
//        if section == 1{
//            headerView.titleLabel.text = "Shop Near By"
//        }else if section == 2{
//            headerView.titleLabel.text = "Gas Station Near By"
//        }else if section == 3{
//            headerView.titleLabel.text = "Charging Station Near By"
//        }
//        headerView.seeMore = { [weak self] in
//            if section == 1{
//                let vc = STORYBOARD.map.instantiateViewController(withIdentifier: "ShopListScreen") as! ShopListScreen
//                vc.titleString = "Shop Near By"
//                self?.navigationController?.pushViewController(vc, animated: true)
//            }else{
//                let vc = STORYBOARD.map.instantiateViewController(withIdentifier: "StationListScreen") as! StationListScreen
//                if section == 2{
//                    vc.titleString = "Gas Station Near By"
//                    vc.itemArray = self?.gasStationListArray ?? []
//                }else{
//                    vc.titleString = "Charging Station Near By"
//                    vc.itemArray = self?.chargeStationListArray ?? []
//                }
//                self?.navigationController?.pushViewController(vc, animated: true)
//            }
//        }
//        if section == 1{
//            headerView.sapratorView.isHidden = self.advertisementListArray.isEmpty ? true : false
//        }else if section == 2{
//            headerView.sapratorView.isHidden = (self.advertisementListArray.isEmpty && self.gasStationListArray.isEmpty == false) ? false : true
//        }else {
//            headerView.sapratorView.isHidden = (self.gasStationListArray.isEmpty && self.chargeStationListArray.isEmpty == false) ? false : true
//        }
//        return headerView
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        var height: CGFloat = 0
//        if section == 1{
//            height = self.advertisementListArray.isEmpty ? 0 : 40
//        }else if section == 2{
//            height = self.gasStationListArray.isEmpty ? 0 : 40
//        }else if section == 3{
//            height = self.chargeStationListArray.isEmpty ? 0 : 40
//        }
//        return height
//    }
//
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return .leastNonzeroMagnitude
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.section == 1{
//            let width = (screenWidth * 315) / 375
//            return (width * 100) / 375
//        }
//        return UITableView.automaticDimension
//    }
//}
//MARK:- COLLECIONVIEW DELEGATE & DATASOURCE
extension ServicesScreen: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "StationHeaderView", for: indexPath) as! StationHeaderView
            if indexPath.section == 1{
                headerView.titleLabel.text = "Shop Near By"
            }else if indexPath.section == 2{
                headerView.titleLabel.text = "Gas Station Near By"
            }else if indexPath.section == 3{
                headerView.titleLabel.text = "Charging Station Near By"
            }
            headerView.seeMore = { [weak self] in
//                self?.close()
                if indexPath.section == 1{
                    let vc = STORYBOARD.map.instantiateViewController(withIdentifier: "ShopListScreen") as! ShopListScreen
                    vc.titleString = "Shop Near By"
                    vc.modalPresentationStyle = .fullScreen
                    self?.present(vc, animated: false)
                }else{
                    let vc = STORYBOARD.map.instantiateViewController(withIdentifier: "StationListScreen") as! StationListScreen
                    if indexPath.section == 2{
                        vc.titleString = "Gas Station Near By"
                        vc.itemArray = self?.gasStationListArray ?? []
                    }else{
                        vc.titleString = "Charging Station Near By"
                        vc.itemArray = self?.chargeStationListArray ?? []
                    }
//                    self?.superVC?.navigationController?.pushViewController(vc, animated: true)
                    vc.modalPresentationStyle = .fullScreen
                    self?.present(vc, animated: false)
                }
            }
            if indexPath.section == 1{
                headerView.sapratorView.isHidden = self.advertisementListArray.isEmpty ? true : false
            }else if indexPath.section == 2{
                headerView.sapratorView.isHidden = (self.advertisementListArray.isEmpty && self.gasStationListArray.isEmpty == false) ? false : true
            }else {
                headerView.sapratorView.isHidden = (self.gasStationListArray.isEmpty && self.chargeStationListArray.isEmpty == false) ? false : true
            }
            return headerView
        default:
            assert(false, "Unexpected element kind")
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let leftSpace = (self.serviceCollectionView.collectionViewLayout as! UICollectionViewFlowLayout).sectionInset.left
        let rightSpace = (self.serviceCollectionView.collectionViewLayout as! UICollectionViewFlowLayout).sectionInset.right
        var height: CGFloat = 0
        if section == 1{
            height = self.advertisementListArray.isEmpty ? 0 : 40
        }else if section == 2{
            height = self.gasStationListArray.isEmpty ? 0 : 40
        }else if section == 3{
            height = self.chargeStationListArray.isEmpty ? 0 : 40
        }
        return CGSize(width: screenWidth - (leftSpace + rightSpace), height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            return self.serviceArray.count
        }else if section == 1{
            return self.advertisementListArray.isEmpty ? 0 : 1
        }else if section == 2{
            print("Count gas")
            print(self.gasStationListArray.count)
            return self.gasStationListArray.count > 2 ? 2 : self.gasStationListArray.count
        }else {
            print("Count charge")
            print(self.chargeStationListArray.count)
            return self.chargeStationListArray.count > 2 ? 2 : self.chargeStationListArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0{
            let cell = self.serviceCollectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCell", for: indexPath) as! ServiceCell
            cell.item = self.serviceArray[indexPath.row]
            if self.selectedService == indexPath.row{
                
                cell.mainView.borderColor =  Utility.checkLightModeUserDefalt() ? Utility.getUIcolorfromHex(hex: "051626"): Utility.getUIcolorfromHex(hex: "41EAD4")
                
                
                cell.mainView.borderWidth = 2
                cell.mainView.shadowColor = Utility.getUIcolorfromHex(hex: "000000").withAlphaComponent(0.1)
                cell.mainView.shadowOpacity = 1
                cell.mainView.shadowOffset.width = 1
                cell.mainView.shadowOffset.height = 1
                cell.mainView.shadowRadius = 1
            }else{
                cell.mainView.borderColor = .clear
                cell.mainView.borderWidth = 0
                cell.mainView.shadowColor = .clear
                cell.mainView.shadowOpacity = 0
                cell.mainView.shadowOffset.width = 0
                cell.mainView.shadowOffset.height = 0
                cell.mainView.shadowRadius = 0
            }
            return cell
        }else if indexPath.section == 1{
            let cell = self.serviceCollectionView.dequeueReusableCell(withReuseIdentifier: "AdvertisementMainCell", for: indexPath) as! AdvertisementMainCell
            cell.itemArray = self.advertisementListArray
            cell.collectionView.reloadData()
            cell.changeIndex = { [weak self] index in
                self?.indexPath = index
            }
            return cell
        }
        let cell = self.serviceCollectionView.dequeueReusableCell(withReuseIdentifier: "NearByStationCell", for: indexPath) as! NearByStationCell
        if indexPath.section == 2{
            cell.item = self.gasStationListArray[indexPath.item]
        }else{
            cell.item = self.chargeStationListArray[indexPath.item]
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let leftSpace = (self.serviceCollectionView.collectionViewLayout as! UICollectionViewFlowLayout).sectionInset.left
        let rightSpace = (self.serviceCollectionView.collectionViewLayout as! UICollectionViewFlowLayout).sectionInset.right
        let cellSpacing = (self.serviceCollectionView.collectionViewLayout as! UICollectionViewFlowLayout).minimumInteritemSpacing
        if indexPath.section == 0{
            let numberOFRows : CGFloat = 4
            let scWidth : CGFloat = screenWidth - (leftSpace + rightSpace)
            let totalSpace : CGFloat = cellSpacing * (numberOFRows - 1)
            let width = (scWidth - totalSpace) / numberOFRows
            return CGSize(width: width, height: width + 40)
        }else if indexPath.section == 1{
            let width = (screenWidth * 315) / 375
            return CGSize(width: screenWidth, height: (width * 100) / 375)
        }
        let scWidth : CGFloat = screenWidth - (leftSpace + rightSpace)
        return CGSize(width: scWidth, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        DispatchQueue.main.async { [weak self] in
//            self?.servieCollectionViewHeight.constant = collectionView.contentSize.height > (screenHeight - 320) ? screenHeight - 320 : collectionView.contentSize.height
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0{
            self.selectedService = indexPath.row
            self.serviceCollectionView.reloadData()
//            self.manageContinueButton()
//            self.questionItemArray = self.serviceArray[indexPath.row].questions ?? []
//            self.answerDescriptionView.isHidden = true
//            self.vehicleAnswerTableView.reloadData()
            
            let vc = STORYBOARD.map.instantiateViewController(withIdentifier: "SubServiceScreen") as! SubServiceScreen
            vc.modalPresentationStyle = .fullScreen
            vc.serviceArray = self.serviceArray
            vc.selectedService = indexPath.item
            self.present(vc, animated: false)
        }else if indexPath.section == 1{
                        
            OpenMapDirections.present(in: self, sourceView: self.view, fromLat: currentLatitude, fromLong: currentLongitude, toLat: "\(self.gasStationListArray[indexPath.item].lat ?? 0)", toLong: "\(self.gasStationListArray[indexPath.item].lng ?? 0)")
        }else{
            
            if indexPath.section == 2{
                FirebaseanalyticsManager.share.logEvent(event: .gasStationNearbyClick)
                OpenMapDirections.present(in: self, sourceView: self.view, fromLat: currentLatitude, fromLong: currentLongitude, toLat: "\(self.gasStationListArray[indexPath.item].lat ?? 0)", toLong: "\(self.gasStationListArray[indexPath.item].lng ?? 0)")
            }else{
                FirebaseanalyticsManager.share.logEvent(event: .chargingStationNearbyClick)

                OpenMapDirections.present(in: self, sourceView: self.view, fromLat: currentLatitude, fromLong: currentLongitude, toLat: "\(self.chargeStationListArray[indexPath.item].lat ?? 0)", toLong: "\(self.chargeStationListArray[indexPath.item].lng ?? 0)")
            }
        }
    }
}
//MARK:- API{
extension ServicesScreen{
    
    func getService(){
        self.view.endEditing(true)
        Utility.showIndicator()
        if Utility.isInternetAvailable(){
            MapService.shared.getService { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self?.serviceArray = res
                    CacheArray.shared.serviceArray = res
                }
                self?.serviceCollectionView.reloadData()
                self?.tableView.reloadData()
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func getGasStationList(){
        if Utility.isInternetAvailable(){
            StationService.shared.getGasStation(lat: currentLatitude, long: currentLongitude) { [weak self] statusCode, response in
                if let res = response{
                    self?.gasStationListArray = res
                    self?.serviceCollectionView.reloadData()
//                    self?.tableView.reloadData()
                }
            } failure: { error in
                print(error)
            }
        }else{
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func getChargeStationList(){
        if Utility.isInternetAvailable(){
            StationService.shared.getChargeStation(lat: currentLatitude, long: currentLongitude) { [weak self] statusCode, response in
                if let res = response{
                    self?.chargeStationListArray = res
                    self?.serviceCollectionView.reloadData()
//                    self?.tableView.reloadData()
                }
            } failure: { error in
                print(error)
            }
        }else{
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func getAdvertisementList(){
        if Utility.isInternetAvailable(){
            StationService.shared.getAdvertisementList(page: 1, perPage: 5) { [weak self] statusCode, response in
                if let res = response.advertisementResponse{
                    var counter = 0
                    for i in res{
                        let obj = i
                        obj.gradientImage = self?.gradientArray[counter]
                        self?.advertisementListArray.append(obj)
                        counter += 1
                        if counter == 3{
                            counter = 0
                        }
                    }
                    self?.serviceCollectionView.reloadData()
                    self?.tableView.reloadData()
                    
                    if (self?.advertisementListArray.count ?? 0) > 1{
                        self?.startTimer()
                    }else{
                        self?.stopTimer()
                    }
                }
            } failure: { error in
                print(error)
            }

        }else{
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
//MARK: ADMOB BANNER AD
extension ServicesScreen: GADBannerViewDelegate{
    
    func requestLoadBannerAd(){
        
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = ["A9E8F8BE-B6D1-45E5-9A31-006EB8E0C103"] // or include your actual test device ID

        self.googleBannerAdView = GADBannerView(adSize: GADAdSizeBanner)
        self.googleBannerAdView?.adSize = GADCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(screenWidth)
        self.googleBannerAdView?.adUnitID = google_banner_ad_id
        self.googleBannerAdView?.rootViewController = self
        self.googleBannerAdView?.load(GADRequest())
        self.googleBannerAdView?.delegate = self
    }
    
    func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
      print("bannerViewDidReceiveAd")
        if Utility.getUserData()?.subscription?.isPremium == false{
            self.bannerAdShowingView.addSubview(bannerView)
            UIView.animate(withDuration: 0.5) { [weak self] in
                self?.bannerViewHeightConstraint.constant = GADCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(screenWidth).size.height
                self?.serviceCollectionView.contentInset.bottom = GADCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(screenWidth).size.height
                self?.view.layoutIfNeeded()
                self?.view.setNeedsLayout()
                DispatchQueue.main.async {
                    self?.panModalSetNeedsLayoutUpdate()
                }
            }
        }
    }

    func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
      print("bannerView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }

    func bannerViewDidRecordImpression(_ bannerView: GADBannerView) {
      print("bannerViewDidRecordImpression")
    }

    func bannerViewWillPresentScreen(_ bannerView: GADBannerView) {
      print("bannerViewWillPresentScreen")
    }

    func bannerViewWillDismissScreen(_ bannerView: GADBannerView) {
      print("bannerViewWillDIsmissScreen")
    }

    func bannerViewDidDismissScreen(_ bannerView: GADBannerView) {
      print("bannerViewDidDismissScreen")
    }
}
//MARK: API
extension ServicesScreen{
    
    func checkOrderStatus(){
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            MapService.shared.checkOrderStatus { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self.orderCheckStatus = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }

        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Vehicle List API
    func getVehicleMakeAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            VehicleService.shared.getVehicleMake{ [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self?.vehicleMakeArray = res
//                    self.setData()
                    CacheArray.shared.vehicleMakeArray = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Vehicle Year API
    func getVehicleYearListAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            VehicleService.shared.getVehicleYear { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self?.vehicleYearArray = res
                    CacheArray.shared.vehicleYearArray = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Vehicle Color API
    func getVehicleColorListAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            VehicleService.shared.getVehicleColor{ [weak self]  (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self?.vehicleColorArray = res
                    CacheArray.shared.vehicleColorArray = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Vehicle LIST API
    func getVehicleList(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
//            Utility.showIndicator()
            VehicleService.shared.getVehicles { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self?.vehicleListArray = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
