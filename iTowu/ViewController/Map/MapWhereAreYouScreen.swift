//
//  MapWhereAreYouScreen.swift
//  iTowu
//
//  Created by Nikunj on 19/10/21.
//

import UIKit
import MapKit
import GooglePlaces
import Firebase
import Alamofire
import NVActivityIndicatorView
import DropDown
import PanModal

class MapWhereAreYouScreen: UIViewController {

    @IBOutlet weak var searchPickupTextField: UITextField!
    @IBOutlet weak var searchDestinationTextField: UITextField!
    
    @IBOutlet weak var destinationView: UIView!
    @IBOutlet weak var searchProviderView: UIView!
    @IBOutlet weak var foundServiceProviderView: UIView!
    @IBOutlet weak var serviceRouteView: UIView!
    @IBOutlet weak var serviceLocationView: dateSportView!
    @IBOutlet weak var driverOrderRequetView: dateSportView!
    @IBOutlet weak var inputLocationView: dateSportView!
    @IBOutlet weak var chatWithServiceProviderView: dateSportView!
    @IBOutlet weak var cancelBookingView: dateSportView!
    @IBOutlet weak var extraChargeView: dateSportView!
    
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var searchProviderBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var driverOrderBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var driverImageView: dateSportImageView!
    
    @IBOutlet weak var driverNameLabel: dateSportLabel!
    
    @IBOutlet weak var driverNumberPlateNumberLabel: dateSportLabel!
    
    @IBOutlet weak var driverVehicleImageView: dateSportImageView!
    
    @IBOutlet weak var pickLocationButton: dateSportButton!
    @IBOutlet weak var cancelChatView: UIStackView!
    
    @IBOutlet weak var serviceLocationLabel: UILabel!
    @IBOutlet weak var driverApproxChargeLabel: UILabel!
    @IBOutlet weak var chatWithServiceProviderLabel: UILabel!
    @IBOutlet weak var chargeReasonLabel: UILabel!
    @IBOutlet weak var chargeLabel: UILabel!
    
    @IBOutlet weak var serviceProviderInRouteLabel: UILabel!
    @IBOutlet weak var referralPriceLabel: UILabel!
    @IBOutlet weak var selectcardLabel: UILabel!
    @IBOutlet weak var estimateTimeLabel: UILabel!
    @IBOutlet weak var vehicleNameLabel: UILabel!
    @IBOutlet weak var vehiclePlateLabel: UILabel!
    @IBOutlet weak var oldPriceLabel: UILabel!
    @IBOutlet weak var oldPriceLineLabel: UILabel!
    
    @IBOutlet var serviceTitleLabel: [UILabel]!
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var searchingLoadingView: UIView!
    @IBOutlet weak var checkReferralBGView: dateSportView!
    @IBOutlet weak var inRouteLoadingView: UIView!
    @IBOutlet weak var checkReferralImage: UIImageView!
    
    @IBOutlet weak var homeButton: UIButton!
    
    @IBOutlet weak var scheduleNowLabel: UILabel!
    
    @IBOutlet weak var driverFullImageView: dateSportImageView!
    @IBOutlet weak var driverVehicleFullImageView: dateSportImageView!
    @IBOutlet weak var driverFullNameLabel: dateSportLabel!
    @IBOutlet weak var driverFullNumberPlateNumberLabel: dateSportLabel!
    @IBOutlet weak var driverMakeLabel: UILabel!
    @IBOutlet weak var driverModelLabel: UILabel!
    @IBOutlet weak var driverColorLabel: UILabel!
    
    @IBOutlet weak var connectingServiceProviderTimeLabel: UILabel!
        
    @IBOutlet weak var scheduleView: dateSportView!
    
    @IBOutlet weak var driverFullView: UIView!
    
    @IBOutlet weak var timerProgressBar: UIProgressView!
    
    var serviceObj: OrderStatusResponse?
    var placeOrderObj: PlaceOrderResponse?
    
    //MARK: Variable Declaration
    let locationManager = CLLocationManager()
    
    var isPickupSearch: Bool = true
    
    var driverPin: CustomPin?
    var userPin: CustomPin?
    var currentServiceObj: FirebaseAcceptedServiceResponse?
    var observerOrderHandler: DatabaseHandle?
    var isOpenPaymentScreen = false
    
    var selectVehicle: VehicleListResponse?
    var paymentId = ""
    var selectPromocode: String?
    var isCheckReferral: Int = 0
    var cardRefferralSelectObj: FirebaseAcceptedServiceResponse?
    
    var progressTimer: Timer?
    var progress: Float = 0.0
    var remainMinutes: Float = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        homeVC = self
        self.initializeDetails()
        NotificationCenter.default.addObserver(self, selector: #selector(self.appMoveForeground(notification:)), name: UIApplication.willEnterForegroundNotification, object: nil)
        // Do any additional setup after loading the view.
    }
    
    @objc func appMoveForeground(notification: NSNotification){
        self.messageCount()
        if let currentServiceObj = self.currentServiceObj,currentServiceObj.status == OrderStatus.pending.rawValue {
            self.timerProgressBar.progress = 0.0 // Reset progress bar
            self.manageWaitingOrderProgress(service: currentServiceObj)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.messageCount()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.progressTimer?.invalidate()
    }
    
    func displayCurrentUserLocation(){
        let currantLocation = CustomPin(pinTitle: "", Location: CLLocationCoordinate2D(latitude: Double(currentLatitude) ?? 0.0, longitude:  Double(currentLongitude) ?? 0.0), storeImage: "Location_icon", pinId: -1)
        self.mapView.addAnnotation(currantLocation)
    }
    
    func removeUsersPin(){
        for annotation in self.mapView.annotations {
            if let annotation = annotation as? CustomPin{
                if annotation.pinId == -1{
                    self.mapView.removeAnnotation(annotation)
                }
            }
        }
    }
    
    func initializeDetails(){
        
        self.navigationController?.navigationBar.isHidden = true
//        self.setupTimer()

        self.homeButton.isHidden = false
        self.driverOrderBottomConstraint.constant = bottomSafeArea + 20
        self.sideMenuController?.isLeftViewSwipeGestureDisabled = true
        self.setTopCornerToView(view: [self.searchProviderView,self.foundServiceProviderView,self.serviceRouteView,driverFullView])
        self.locationPermistion()
        self.centerMapOnUserLocation()
        self.displayCurrentUserLocation()
        //self.getRefferalCode()
        if let data = CacheArray.shared.placeOrderRequest{
            self.driverOrderRequetView.isHidden = true
            if let obj = CacheArray.shared.serviceArray.first(where: {"\($0.serviceId ?? 0)" == "\(data.serviceId ?? "0")"}){
                self.inputLocationView.isHidden = false
                if obj.type == 2{
                    self.topViewHeight.constant = 120
                    self.destinationView.isHidden = false
//                    self.pickLocationButton.isHidden = false
                    
                    self.searchProviderView.isHidden = true
                    self.foundServiceProviderView.isHidden = true
                    self.serviceRouteView.isHidden = true
                    self.serviceLocationView.isHidden = true
                    let cityCoords = CLLocation(latitude: Double(currentLatitude) ?? 0.0, longitude: Double(currentLongitude) ?? 0.0)
                    self.getAdressName(coords: cityCoords,isPickup: true,isBoth: true)
                }else{
                    self.topViewHeight.constant = 60
                    self.destinationView.isHidden = true
//                    self.pickLocationButton.isHidden = false
                    
                    self.searchProviderView.isHidden = true
                    self.foundServiceProviderView.isHidden = true
                    self.serviceRouteView.isHidden = true
                    self.serviceLocationView.isHidden = true
                    let cityCoords = CLLocation(latitude: Double(currentLatitude) ?? 0.0, longitude: Double(currentLongitude) ?? 0.0)
                    self.getAdressName(coords: cityCoords,isPickup: true)
                }
            }else{
                self.inputLocationView.isHidden = true
            }
        }else{
            self.inputLocationView.isHidden = true
        }
        self.scheduleView.isHidden = self.inputLocationView.isHidden
        self.searchPickupTextField.addTarget(self, action: #selector(self.openPickupLocationView), for: .allEditingEvents)
        self.searchDestinationTextField.addTarget(self, action: #selector(self.openDestinationLocationView), for: .allEditingEvents)
        
        //TODO:- OBSERVER ORDER
        self.observeOrder()
//        Lat:"26.177840" long:"-80.239800"
       // self.getAddressFromLatLon(pdblLatitude: currentLatitude, withLongitude: currentLongitude)
//        let cityCoords = CLLocation(latitude: Double(currentLatitude) ?? 0.0, longitude: Double(currentLongitude) ?? 0.0)
//        self.getAdressName(coords: cityCoords,isPickup: true)
        self.searchProviderBottomConstraint.constant = bottomSafeArea + 20
        
        if let service = self.serviceObj{
            if service.status == OrderStatus.pending.rawValue{
                self.displaySearchingProviderView(data: service)
            }else {
            }
        }
        self.StartSearchingLoading()
        self.StartinRouteLoadingView()
    }
    
    func StartSearchingLoading(){
        DispatchQueue.main.async {
            let linkLoadView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 30, height: 30),type: .ballScaleMultiple, color: .white)
            linkLoadView.tag = linkTag
            self.searchingLoadingView.addSubview(linkLoadView)
            linkLoadView.startAnimating()
        }
    }
    
    func StartinRouteLoadingView(){
        DispatchQueue.main.async {
            let linkLoadView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 30, height: 30),type: .ballBeat, color: .white)
            linkLoadView.tag = linkTag
            self.inRouteLoadingView.addSubview(linkLoadView)
            linkLoadView.startAnimating()
        }
    }
    
    
    
    func locationPermistion(){
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        self.mapView.showsCompass = false
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
//            Utility.showAlert(vc: self, message: "start map")
        }
    }
    
    
    @IBAction func onHome(_ sender: Any) {
        CacheArray.shared.placeOrderRequest = nil
        CacheArray.shared.currentService = nil
        Utility.setHomeRoot(false, nil)
    }
    
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler: { [weak self] (placemarks, error) in
            if (error != nil)
            {
                print("reverse geodcode fail: \(error!.localizedDescription)")
            }
            let pm = placemarks! as [CLPlacemark]
            var addressString : String = ""
            if pm.count > 0 {
                let pm = placemarks![0]
                
                if pm.thoroughfare != nil {
                    addressString = addressString + pm.thoroughfare! + ", "
                }
                
                if pm.subLocality != nil {
                    addressString = addressString + pm.subLocality! + ", "
                }

                if pm.locality != nil {
                    addressString = addressString + pm.locality! + ", "
                }
                if pm.country != nil {
                    addressString = addressString + pm.country! + ", "
                }
                if pm.postalCode != nil {
                    addressString = addressString + pm.postalCode! + " "
                }
                
                //return addressString
                print(addressString)
                
                                
            }
        })
    }
    
    //MARK:- ACCEPTED ORDER BY DRIVER
    func observeOrder(){
        self.observerOrderHandler =  FirebaseRealtimeDB.ref.child("user").child("order_user_\(Utility.getUserData()?.userId ?? 0)").observe(.value) { [weak self] (snapShot) in
            print(snapShot)
            if snapShot.value.debugDescription == "Optional(<null>)"{ //TODO:- NULL
                if self?.currentServiceObj != nil{
                    self?.currentServiceObj = nil
                    Utility.setHomeRoot(false, nil)
                }
            }else{
                if self?.currentServiceObj != nil{
                    if let firebaseDictionary = snapShot.value as? [String: Any]{
                        if let orderObj = FirebaseAcceptedServiceResponse(JSON: firebaseDictionary){
                            CacheArray.shared.currentService = orderObj
                            if orderObj.orderId == nil && orderObj.driver?.driverId == nil && orderObj.user?.userId == nil{
                                FirebaseRealtimeDB.ref.child("user").child("order_user_\(Utility.getUserData()?.userId ?? 0)").removeValue()
                                snapShot.ref.removeAllObservers()
                                Utility.setHomeRoot(false, nil)
                            }else if orderObj.driver?.driverId == nil && orderObj.user?.userId == nil{
                                self?.removeAllPin()
                                self?.removeAllOverlayers()
                                self?.driverPin = nil
                                self?.userPin = nil
                                self?.currentServiceObj = nil
                                FirebaseRealtimeDB.ref.child("user").child("order_user_\(Utility.getUserData()?.userId ?? 0)").removeValue()
                                snapShot.ref.removeAllObservers()
                                Utility.setHomeRoot(false, nil)
                            }else if orderObj.status == OrderStatus.paymentPending.rawValue{
                                self?.goFurtherPaymentScreen(orderId: orderObj.orderId ?? 0, serviceObj: orderObj,snapShot: snapShot)
                            }else if orderObj.status == OrderStatus.pending.rawValue{
                                self?.serviceTitleLabel.forEach({$0.text = orderObj.service?.name})
                                self?.currentServiceObj = orderObj
                                self?.driverOrderRequetView.isHidden = true
                                self?.searchProviderView.isHidden = false
                                self?.serviceRouteView.isHidden = true
                                self?.cancelBookingView.isHidden = false
                                self?.serviceLocationLabel.text = orderObj.status == OrderStatus.hookedVehicle.rawValue ? orderObj.destination_address : orderObj.pickupAddress
                                self?.serviceLocationView.isHidden = false
                                self?.chatWithServiceProviderView.isHidden = true
                                self?.driverImageView.superview?.superview?.isHidden = true
                                self?.removeAllOverlayers()
                                self?.removeAllPin()
                                self?.displayCurrentUserLocation()
                                self?.manageWaitingOrderProgress(service: orderObj)
                            }else if orderObj.status == OrderStatus.hookedVehicle.rawValue{
                                self?.currentServiceObj = orderObj
                                self?.driverOrderRequetView.isHidden = true
                                self?.searchProviderView.isHidden = true
                                self?.serviceRouteView.isHidden = false
                                self?.cancelBookingView.isHidden = false
                            }else if orderObj.status == OrderStatus.inProgress.rawValue{
                                self?.currentServiceObj = orderObj
                                self?.searchProviderView.isHidden = true
                                self?.manageWaitingOrderProgress(service: orderObj)
                            }else if orderObj.status == OrderStatus.completed.rawValue{
                                self?.currentServiceObj = orderObj
                                self?.goDriverReviewScreen(serviceObj: orderObj, snapShot: snapShot)
                            }
                            if orderObj.status != OrderStatus.userAcceptDriver.rawValue && orderObj.driver != nil{
                                if self?.driverPin == nil && self?.userPin == nil{
                                    self?.managePin(data: orderObj, snapShot: snapShot)
                                }else{
                                    self?.movePin(data: orderObj,snapShot: snapShot)
                                }
                            }
                            if orderObj.extraCharge?.count ?? 0 > 0 && orderObj.service?.type == 2{
                                if let obj = orderObj.extraCharge?.first(where: {$0.status == "pending"}){
                                    self?.extraChargeView.isHidden = false
                                    self?.setExtraObjData(data: obj)
                                }else{
                                    self?.extraChargeView.isHidden = true
                                }
                            }
                        }else{
                            CacheArray.shared.currentService = nil
                        }
                    }
                }else{
                    self?.inputLocationView.isHidden = true
                    self?.searchProviderView.isHidden = true
                    self?.pickLocationButton.isHidden = true
                    self?.foundServiceProviderView.isHidden = true
                    self?.cancelBookingView.isHidden = true
                    
                    if let firebaseDictionary = snapShot.value as? [String: Any]{
                        if let orderObj = FirebaseAcceptedServiceResponse(JSON: firebaseDictionary){
                            self?.homeButton.isHidden = true
                            CacheArray.shared.currentService = orderObj
                            if orderObj.orderId == nil && orderObj.driver?.driverId == nil && orderObj.user?.userId == nil{
                                FirebaseRealtimeDB.ref.child("user").child("order_user_\(Utility.getUserData()?.userId ?? 0)").removeValue()
                                snapShot.ref.removeAllObservers()
                                Utility.setHomeRoot(false, nil)
                            }else if orderObj.status == OrderStatus.userAcceptDriver.rawValue{
                                if self?.driverOrderRequetView.isHidden == true{
                                    self?.driverOrderRequetView.isHidden = false
                                }
                                self?.cancelBookingView.isHidden = true
                                self?.displayServiceProviderView()
                                self?.setDriverRequestData(data: orderObj)
                            }else if orderObj.status == OrderStatus.inProgress.rawValue || orderObj.status == OrderStatus.jobCompleted.rawValue{
                                FirebaseanalyticsManager.share.logEvent(event: .driveAccepted)
                                self?.managePin(data: orderObj,snapShot: snapShot)
                                self?.driverOrderRequetView.isHidden = true
                                self?.cancelBookingView.isHidden = false
                                self?.searchProviderView.isHidden = true
                            }else if orderObj.status == OrderStatus.paymentPending.rawValue{
                                self?.driverOrderRequetView.isHidden = true
                                self?.cancelBookingView.isHidden = true
                                self?.managePin(data: orderObj,snapShot: snapShot)
                            }else if orderObj.status == OrderStatus.pending.rawValue{
                                self?.currentServiceObj = orderObj
                                self?.serviceTitleLabel.forEach({$0.text = orderObj.service?.name})
                                self?.driverOrderRequetView.isHidden = true
                                self?.searchProviderView.isHidden = false
                                self?.serviceRouteView.isHidden = true
                                self?.cancelBookingView.isHidden = false
                                self?.serviceLocationLabel.text = orderObj.status == OrderStatus.hookedVehicle.rawValue ? orderObj.destination_address : orderObj.pickupAddress
                                self?.serviceLocationView.isHidden = false
                                self?.manageWaitingOrderProgress(service: orderObj)
                            }else if orderObj.status == OrderStatus.hookedVehicle.rawValue{
                                self?.currentServiceObj = orderObj
                                self?.driverOrderRequetView.isHidden = true
                                self?.searchProviderView.isHidden = true
                                self?.serviceRouteView.isHidden = false
                                self?.cancelBookingView.isHidden = false
                                self?.managePin(data: orderObj,snapShot: snapShot)

                            }else if orderObj.status == OrderStatus.completed.rawValue{
                                self?.currentServiceObj = orderObj
                                self?.goDriverReviewScreen(serviceObj: orderObj, snapShot: snapShot)
                            }
                            
                            if orderObj.extraCharge?.count ?? 0 > 0 && orderObj.service?.type == 2{
                                if let obj = orderObj.extraCharge?.first(where: {$0.status == "pending"}){
                                    self?.extraChargeView.isHidden = false
                                    self?.setExtraObjData(data: obj)
                                }else{
                                    self?.extraChargeView.isHidden = true
                                }
                            }
                        }else{
                            CacheArray.shared.currentService = nil
                        }
                    }else{
                        self?.cancelChatView.isHidden = true
                    }
                }
            }
            self?.cancelBookingView.isHidden = true //FIXME: HIDDEN DUE TO MOVE INSIDE IN DETAIL SCREEN
        }
    }
    
    func goDriverReviewScreen(serviceObj: FirebaseAcceptedServiceResponse,snapShot: DataSnapshot){
        if serviceObj.status == OrderStatus.completed.rawValue{
            if self.isOpenPaymentScreen == false{
                snapShot.ref.removeAllObservers()
                self.isOpenPaymentScreen = true
                let vc = STORYBOARD.review.instantiateViewController(withIdentifier: "ReviewScreen") as! ReviewScreen
                vc.currentServiceObj = serviceObj
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
//    func setupTimer(){
//        self.timerProgressBar.progress = 0.0
//        self.startProgressTimer()
//    }
    
    func startProgressTimer(progress: Float,completionMinutes: Int) {
        self.progress = progress
        
        // Invalidate any existing timer before starting a new one
        self.progressTimer?.invalidate()
        self.progressTimer = nil
        
        // Create and start the timer
        self.progressTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateProgress), userInfo: nil, repeats: true)
    }
    
    func getRemainMinutesDifference(endDateTime: Int){
        let endDate = Utility.getDateTimeFromTimeInterVel(from: endDateTime)
        let minutes = Calendar.current.dateComponents([.minute], from: Date(), to: endDate).minute ?? 0
        let totalMinutes: Float = 10
        print("Remaining Minutes:::::\(minutes)")
        self.connectingServiceProviderTimeLabel.text = "Connecting you with a service provider by \(Utility.getDateValue(date: endDate, dateFormate: HH_MM_A))"
        let newProgress: Float = 1 - (Float((minutes * 60)) / (totalMinutes * 60))
        self.remainMinutes = Float(minutes)
        self.startProgressTimer(progress: newProgress, completionMinutes: minutes)
    }
    
    // Function to update progress
    @objc func updateProgress() {
        
        self.timerProgressBar.progress += 1.0/Float((self.remainMinutes * 60))
        
        if self.timerProgressBar.progress >= 1.0 {
            print("Completed")
            self.stopWaitingProgress()
        }
    }
    
    func stopWaitingProgress(){
        self.progressTimer?.invalidate()
        self.progressTimer = nil
    }
    
    func manageWaitingOrderProgress(service: FirebaseAcceptedServiceResponse){
        guard let endTime = service.endRefreshServiceTime else { return }
        self.getRemainMinutesDifference(endDateTime: endTime)
    }
    
    func setExtraObjData(data: ExtraChargeResponse){
        self.chargeReasonLabel.text = data.reason
        self.chargeLabel.text = "$\(data.price ?? 0)"
    }
    
    func managePin(data: FirebaseAcceptedServiceResponse,snapShot: DataSnapshot){
        self.currentServiceObj = data
        self.serviceTitleLabel.forEach({$0.text = data.service?.name})
        self.serviceLocationLabel.text = data.status == OrderStatus.hookedVehicle.rawValue ? data.destination_address : data.pickupAddress
        self.serviceRouteView.isHidden = false
        self.serviceLocationView.isHidden = false
        self.cancelChatView.isHidden = false
        self.chatWithServiceProviderView.isHidden = false
        self.driverImageView.superview?.superview?.isHidden = false
        
        if let driver = data.driver{
            Utility.setImage(driver.profile, imageView: self.driverImageView)
            Utility.setImage(driver.profile, imageView: self.driverFullImageView)
            
            self.driverNameLabel.text = "\(driver.firstName ?? "") \(driver.lastName ?? "")"
            self.driverFullNameLabel.text = "\(driver.firstName ?? "") \(driver.lastName ?? "")"
            
            self.driverNumberPlateNumberLabel.text = "Plate No. - \(data.driverGarage?.plate ?? "")"
            self.driverFullNumberPlateNumberLabel.text = "Plate No. - \(data.driverGarage?.plate ?? "")"
            
            Utility.setImage(data.driverGarage?.image, imageView: self.driverVehicleImageView)
            Utility.setImage(data.driverGarage?.image, imageView: self.driverVehicleFullImageView)
            
            self.driverMakeLabel.text = data.driverGarage?.make
            self.driverModelLabel.text = data.driverGarage?.model
            self.driverColorLabel.text = data.driverGarage?.color
        }else{
            self.driverImageView.superview?.superview?.isHidden = true
        }
        
        if data.status == OrderStatus.paymentPending.rawValue{
            self.goFurtherPaymentScreen(orderId: data.orderId ?? 0 ,serviceObj: data,snapShot: snapShot)
        }else{
            self.movePin(data: data,snapShot: snapShot)
        }
        
        if data.status == OrderStatus.hookedVehicle.rawValue{
            let kmDiff = Utility.getKmDifference(fromLatitude:  Double((data.destination_latitude ?? 0.0)), fromLongtitude: Double(data.destination_longitude ?? 0.0), destinationLatitude:Double(data.driver?.latitude ?? 0.0), destinationLongtitude: Double(data.driver?.longitude ?? 0.0))
            
            if kmDiff == 0.00 || kmDiff < 0.03 || data.status == OrderStatus.jobCompleted.rawValue{
                self.serviceProviderInRouteLabel.text = "Service provider arrived"
            }else{
                self.serviceProviderInRouteLabel.text = "Service provider in route"
            }
        }else{
            let kmDiff = Utility.getKmDifference(fromLatitude:  Double((data.pickupLatitude ?? 0.0)), fromLongtitude: Double(data.pickupLongitude ?? 0.0), destinationLatitude:Double(data.driver?.latitude ?? 0.0), destinationLongtitude: Double(data.driver?.longitude ?? 0.0))
            
            if kmDiff == 0.00 || kmDiff < 0.03 || data.status == OrderStatus.jobCompleted.rawValue{
                self.serviceProviderInRouteLabel.text = "Service provider arrived"
            }else{
                self.serviceProviderInRouteLabel.text = "Service provider in route"
            }
        }
        
//        self.serviceProviderInRouteLabel.text = data.status == OrderStatus.jobCompleted.rawValue ? "Service provider arrived" : "Service provider in route"
    }
    
    func goFurtherPaymentScreen(orderId: Int,serviceObj: FirebaseAcceptedServiceResponse,snapShot: DataSnapshot){
        if serviceObj.status == OrderStatus.paymentPending.rawValue{//serviceObj.status == OrderStatus.inProgress.rawValue {
            if self.isOpenPaymentScreen == false{
//                if serviceObj.status != OrderStatus.paymentPending.rawValue{
//                    self.updateStatus(status: OrderStatus.paymentPending.rawValue)
//                }
                snapShot.ref.removeAllObservers()
                self.isOpenPaymentScreen = true
                let vc = STORYBOARD.payment.instantiateViewController(withIdentifier: "ServicePaymentScreen") as! ServicePaymentScreen
                vc.orderId = orderId
                vc.currentServiceObj = serviceObj
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func setDriverRequestData(data: FirebaseAcceptedServiceResponse){
        self.currentServiceObj = data
        Utility.setImage(data.driver?.profile, imageView: self.driverImageView)
        self.driverApproxChargeLabel.text = String(format: "$%.2f", data.price ?? 0)
        if let cardData = data.defaultPayment{
            self.paymentId = cardData.id ?? ""
            self.selectcardLabel.text  = "Card Number:- **** **** **** " + "\(cardData.card_number ?? "")"
        }else{
            self.selectcardLabel.text = "Select Card"
        }
        self.serviceLocationLabel.text = data.status == OrderStatus.hookedVehicle.rawValue ? data.destination_address : data.pickupAddress
        self.searchProviderView.isHidden = true
        self.serviceLocationView.isHidden = false
        self.vehicleNameLabel.text = "\(data.vehicleMakeName ?? "") "+"\(data.vehicleModelName ?? "")" //data.vehicleModelName
        self.vehiclePlateLabel.text = data.plateNumber
    }
    
    func setDriverRequestDataFromLocal(data: FirebaseAcceptedServiceResponse){
        self.driverOrderRequetView.isHidden = false
        self.serviceTitleLabel.forEach({$0.text = data.service?.name})
        
//        self.driverApproxChargeLabel.text = String(format: "$%.2f", (data.price ?? 0) - (data.subDiscountPrice ?? 0))
        self.driverApproxChargeLabel.text = String(format: "$%.2f", (data.price ?? 0) - (data.subDiscountPrice ?? 0))
        
        if data.subDiscountPrice != 0 || data.subDiscountPrice == nil{
            self.oldPriceLabel.isHidden = false
            self.oldPriceLineLabel.isHidden = false
//            self.oldPriceLabel.text = "$\(String(format:"%.02f",data.subDiscountPrice ?? 0))"
            self.oldPriceLabel.text = "$\(String(format:"%.02f",data.price ?? 0))"
        }
        
        if let cardData = data.defaultPayment{
            self.paymentId = cardData.id ?? ""
            self.selectcardLabel.text  = "Card Number:- **** **** **** " + "\(cardData.card_number ?? "")"
        }else{
            self.selectcardLabel.text = "Select Card"
        }
        self.serviceLocationLabel.text = data.status == OrderStatus.hookedVehicle.rawValue ? data.destination_address : data.pickupAddress
        self.searchProviderView.isHidden = true
        self.serviceLocationView.isHidden = false
//        if let obj = CacheArray.shared.vehicleMakeArray.first(where: {"\($0.vehicleMakeId ?? 0)" == CacheArray.shared.placeOrderRequest?.vehicleMakeId}){
//            self.vehicleNameLabel.text = obj.name
//        }
//        self.vehiclePlateLabel.text = CacheArray.shared.placeOrderRequest?.plateNumber
        if let obj = CacheArray.shared.vehicleListResponse.first(where: {"\($0.id ?? 0)" == CacheArray.shared.placeOrderRequest?.user_garage_id}){
            self.vehicleNameLabel.text = "\(obj.make ?? "") "+"\(obj.model ?? "")" //data.vehicleModelName

            self.vehiclePlateLabel.text = obj.plate
        }
    }
    
    func manageVehicleWithLocalData(vehicle: VehicleListResponse){
//        CacheArray.shared.placeOrderRequest?.plateNumber = vehicle.plate
//        CacheArray.shared.placeOrderRequest?.vehicleYearId = vehicle.year//"\(vehicle.vehicleYearId ?? 0)"
//        CacheArray.shared.placeOrderRequest?.vehicleMakeId = vehicle.make//"\(vehicle.vehicleMakeId ?? 0)"
//        CacheArray.shared.placeOrderRequest?.vehicleModelId = vehicle.model//"\(vehicle.vehicleModelId ?? 0)"
//        CacheArray.shared.placeOrderRequest?.vehicleColorId = vehicle.color//"\(vehicle.vehicleColorId ?? 0)"
        CacheArray.shared.placeOrderRequest?.user_garage_id = "\(vehicle.id ?? 0)"
    }
    
    func removeAllOverlayers(){
        let overlays = self.mapView.overlays
        self.mapView.removeOverlays(overlays)
    }
    
    func movePin(data: FirebaseAcceptedServiceResponse,snapShot: DataSnapshot){
        self.serviceProviderInRouteLabel.text = data.status == OrderStatus.jobCompleted.rawValue ? "Service provider arrived" : "Service provider in route"
        if let driverPin = self.driverPin,let userPin = self.userPin{
            
            self.changePath(pickupCoordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees(data.status == OrderStatus.hookedVehicle.rawValue ? data.destination_latitude ?? 0.0 : data.pickupLatitude ?? 0.0), longitude: CLLocationDegrees(data.status == OrderStatus.hookedVehicle.rawValue ? data.destination_longitude ?? 0.0 : data.pickupLongitude ?? 0.0)), destinationCoordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees(data.driver?.latitude ?? 0.0), longitude: CLLocationDegrees(data.driver?.longitude ?? 0.0)))
            
            UIView.animate(withDuration: 1, delay: 0, options: .allowUserInteraction, animations: { [weak self] in
                if data.status == OrderStatus.hookedVehicle.rawValue{
                    driverPin.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(data.driver?.latitude ?? 0.0), longitude: CLLocationDegrees(data.driver?.longitude ?? 0.0))
                    
                    userPin.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(data.destination_latitude ?? 0.0), longitude: CLLocationDegrees(data.destination_longitude ?? 0.0))
                    
                    let diff = Utility.getKilometerDifferenceFromLatLong(fromLatitude: Double(data.pickupLatitude ?? 0.0), fromLongtitude: Double(data.pickupLongitude ?? 0.0), destinationLatitude: Double((data.driver?.latitude ?? 0.0)), destinationLongtitude: Double(data.driver?.longitude ?? 0.0))
                    
                    let kmDiff = Utility.getKmDifference(fromLatitude: Double(data.destination_latitude ?? 0.0), fromLongtitude: Double(data.destination_longitude ?? 0.0), destinationLatitude: Double((data.driver?.latitude ?? 0.0)), destinationLongtitude: Double(data.driver?.longitude ?? 0.0))
                    
                    print("Difference : -----\(diff)")

                    print("Difference Double : -----\(kmDiff)")
                    if kmDiff == 0.00 || kmDiff < 0.03{
                        print("observe cancel")
                        
                        self?.goFurtherPaymentScreen(orderId: data.orderId ?? 0, serviceObj: data,snapShot: snapShot)
                    }
                }else{
                    driverPin.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(data.driver?.latitude ?? 0.0), longitude: CLLocationDegrees(data.driver?.longitude ?? 0.0))
                    
                    userPin.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(data.pickupLatitude ?? 0.0), longitude: CLLocationDegrees(data.pickupLongitude ?? 0.0))
                    
                    let diff = Utility.getKilometerDifferenceFromLatLong(fromLatitude: Double(data.pickupLatitude ?? 0.0), fromLongtitude: Double(data.pickupLongitude ?? 0.0), destinationLatitude: Double((data.driver?.latitude ?? 0.0)), destinationLongtitude: Double(data.driver?.longitude ?? 0.0))
                    
                    let kmDiff = Utility.getKmDifference(fromLatitude: Double(data.pickupLatitude ?? 0.0), fromLongtitude: Double(data.pickupLongitude ?? 0.0), destinationLatitude: Double((data.driver?.latitude ?? 0.0)), destinationLongtitude: Double(data.driver?.longitude ?? 0.0))
                    
                    print("Difference : -----\(diff)")

                    print("Difference Double : -----\(kmDiff)")
                    if kmDiff == 0.00 || kmDiff < 0.03{
                        print("observe cancel")
                        
                        self?.goFurtherPaymentScreen(orderId: data.orderId ?? 0, serviceObj: data,snapShot: snapShot)
                    }
                }
               
            })

        }else{
            self.showPathOnMap(pickupCoordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees(data.status == OrderStatus.hookedVehicle.rawValue ? data.destination_latitude ?? 0.0 : data.pickupLatitude ?? 0.0), longitude: CLLocationDegrees(data.status == OrderStatus.hookedVehicle.rawValue ? data.destination_longitude ?? 0.0 : data.pickupLongitude ?? 0.0)), destinationCoordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees(data.driver?.latitude ?? 0.0), longitude: CLLocationDegrees(data.driver?.longitude ?? 0.0)),driverObj: data.driver,userObj: data.user)
            
        }
    }
    
    func updateUserLocation(location: CLLocationCoordinate2D){
//        if let userId = Utility.getUserData()?.userId{
//            FirebaseRealtimeDB.ref.child("user").child("order_user_\(userId)").updateChildValues(FirebaseVehiclePickUpRequest(pickUpLatitude: Double(location.latitude.clean), pickUpLongtitude: Double(location.longitude.clean)).toJSON())
//        }
    }
    
    func updateStatus(status: Int){
        if let userId = Utility.getUserData()?.userId{
            FirebaseRealtimeDB.ref.child("user").child("order_user_\(userId)").updateChildValues(UpdateServiceStatusRequest(status: status).toJSON())
        }
    }
            
    func removeAllPin(){
        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)
    }

    func changeCurrentPinLocation(coordinate: CLLocationCoordinate2D){
        if let pin = self.mapView.annotations.first(where: {$0 is CustomPin}),let currentPin = pin as? CustomPin{
            if currentPin.pinId == -1{
                currentPin.coordinate = coordinate
                let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: 5000, longitudinalMeters: 5000)
                self.mapView.setRegion(region, animated: true)
            }
        }
    }
    
    // MARK: - showRouteOnMap
    func showPathOnMap(pickupCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D,driverObj: Driver?,userObj: User?) {
        self.removeAllPin()
        let sourcePlacemark = MKPlacemark(coordinate: pickupCoordinate, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinate, addressDictionary: nil)

        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)

        let sourceAnnotation = MKPointAnnotation()

        if let location = sourcePlacemark.location {
            sourceAnnotation.coordinate = location.coordinate
        }

        let destinationAnnotation = MKPointAnnotation()

        if let location = destinationPlacemark.location {
            destinationAnnotation.coordinate = location.coordinate
        }
        
        let data = CustomPin(pinTitle: "", Location: pickupCoordinate, storeImage: userObj?.profile ?? "", pinId: userObj?.userId ?? 0)
        
        let data1 = CustomPin(pinTitle: "", Location:destinationCoordinate, storeImage: driverObj?.profile ?? "", pinId: driverObj?.driverId ?? 0)

        self.driverPin = data1
        self.userPin = data
        
        //self.mapView.addAnnotations()
        self.mapView.showAnnotations([data,data1], animated: true )
        let directionRequest = MKDirections.Request()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile

        // Calculate the direction
        let directions = MKDirections(request: directionRequest)

        directions.calculate { [weak self] (response, error) -> Void in

            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }

                return
            }

            let route = response.routes[0]

            self?.mapView.addOverlay((route.polyline), level: .aboveRoads)

            let rect = route.polyline.boundingMapRect
            self?.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
            
            self?.displayEstimationTime(route: route)
        }
    }
    
    func changePath(pickupCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D){
        self.removeUsersPin()
        let sourcePlacemark = MKPlacemark(coordinate: pickupCoordinate, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinate, addressDictionary: nil)

        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)

        let sourceAnnotation = MKPointAnnotation()

        if let location = sourcePlacemark.location {
            sourceAnnotation.coordinate = location.coordinate
        }

        let destinationAnnotation = MKPointAnnotation()

        if let location = destinationPlacemark.location {
            destinationAnnotation.coordinate = location.coordinate
        }
        
        let directionRequest = MKDirections.Request()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile

        // Calculate the direction
        let directions = MKDirections(request: directionRequest)

        directions.calculate { [weak self] (response, error) -> Void in

            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }

                return
            }

            let route = response.routes[0]
            let overlays = self?.mapView.overlays ?? []
            self?.mapView.removeOverlays(overlays)
            self?.mapView.addOverlay((route.polyline), level: .aboveRoads)
            
            self?.displayEstimationTime(route: route)
        }
    }
    
    func displayEstimationTime(route: MKRoute){
        let (h, m, s) = secondsToHoursMinutesSeconds (seconds: Int(route.expectedTravelTime))
        //          print ("\(h) H, \(m) M, \(s) S")
        print(String(format: "%02d:%02d:%02d", h, m, s))
        let time = String(format: "%02d:%02d", h, m)
        self.estimateTimeLabel.text = "\(time) MIN"
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func setTopCornerToView(view: [UIView]){
        for i in view{
            i.layer.cornerRadius = 20
            i.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]

        }
    }
    
    @objc func openPickupLocationView(){
        self.isPickupSearch = true
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt64(UInt(GMSPlaceField.name.rawValue) |
                                                                   UInt(GMSPlaceField.placeID.rawValue) |  UInt(GMSPlaceField.coordinate.rawValue)))
        autocompleteController.placeFields = fields
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @objc func openDestinationLocationView(){
        self.isPickupSearch = false
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt64(UInt(GMSPlaceField.name.rawValue) |
                                                                   UInt(GMSPlaceField.placeID.rawValue) |  UInt(GMSPlaceField.coordinate.rawValue)))
        autocompleteController.placeFields = fields
        present(autocompleteController, animated: true, completion: nil)
    }
    
    
    func showAddCardAlert(){
        let alert = UIAlertController(title: APPLICATION_NAME, message: "You must have to select a card to make proceed.",preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { [weak self] (action) in
            //Cancel Action
            self?.selectCard()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

    func selectCard(){
        let vc = STORYBOARD.payment.instantiateViewController(withIdentifier: "CardListScreen") as! CardListScreen
        vc.isFromCreateService = true
        vc.onPaymentSelectedButton1 = { [weak self] cardObj in
            self?.paymentId = cardObj.id ?? ""
            self?.selectcardLabel.text  = "Card Number:- **** **** **** " + "\(cardObj.card_number ?? "")"
        }
        vc.service = self.currentServiceObj
        vc.delegate = self
        vc.isCheckReferral = self.isCheckReferral
        vc.promocodeRes = self.cardRefferralSelectObj
        vc.promoCode = self.selectPromocode
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func displayDropDown(array: [String],tag: Int,view: UILabel){
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.anchorView = view.superview
        dropDown.dataSource = array
        dropDown.direction = .bottom
        dropDown.width = view.superview?.frame.size.width ?? 100
        dropDown.bottomOffset = CGPoint(x: 0, y: 40)
        dropDown.backgroundColor = .white
        dropDown.cornerRadius = 8
        dropDown.show()
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            self?.scheduleNowLabel.text = item
            if index == 1{
                let vc = STORYBOARD.map.instantiateViewController(withIdentifier: "TransportServiceTimeScreen") as! TransportServiceTimeScreen
                vc.onBack = { [weak self] in
                    if CacheArray.shared.placeOrderRequest?.scheduleDate == nil{
                        self?.scheduleNowLabel.text = array.first
                    }else{
                        self?.showScheduleDate()
                        self?.getBookingCalculation()
                    }
                }
                self?.navigationController?.pushViewController(vc, animated: true)
            }else{
                CacheArray.shared.placeOrderRequest?.scheduleDate = nil
            }
        }
    }
    
    func showScheduleDate(){
        if let dateString = CacheArray.shared.placeOrderRequest?.scheduleDate{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            if let date = dateFormatter.date(from: dateString){
                dateFormatter.dateFormat = "EEE,MMM dd 'at' hh:mm a'"
                dateFormatter.timeZone = .current
                self.scheduleNowLabel.text = dateFormatter.string(from: date)
            }
        }
    }
   
    @IBAction func onLeftMenu(_ sender: Any) {
        self.sideMenuController?.showLeftViewAnimated(sender: self)
    }
    
//    @IBAction func onGasoline(_ sender: Any) {
//        self.gasolineView.layer.backgroundColor = #colorLiteral(red: 0.2549019608, green: 0.9176470588, blue: 0.831372549, alpha: 1)
//        self.midGradeView.layer.backgroundColor =  #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
//        self.premiumView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
//        self.dieselView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
//    }
//
//    @IBAction func onMidGrade(_ sender: Any) {
//        self.gasolineView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
//        self.midGradeView.layer.backgroundColor =  #colorLiteral(red: 0.2549019608, green: 0.9176470588, blue: 0.831372549, alpha: 1)
//        self.premiumView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
//        self.dieselView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
//
//    }
//
//    @IBAction func onPremium(_ sender: Any) {
//        self.gasolineView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
//        self.midGradeView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
//        self.premiumView.layer.backgroundColor = #colorLiteral(red: 0.2549019608, green: 0.9176470588, blue: 0.831372549, alpha: 1)
//        self.dieselView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
//    }
//
//    @IBAction func onDiesel(_ sender: Any) {
//        self.gasolineView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
//        self.midGradeView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
//        self.premiumView.layer.backgroundColor = #colorLiteral(red: 0.8039215686, green: 0.8156862745, blue: 0.831372549, alpha: 1)
//        self.dieselView.layer.backgroundColor = #colorLiteral(red: 0.2549019608, green: 0.9176470588, blue: 0.831372549, alpha: 1)
//    }
    
    @IBAction func onPickupDestination(_ sender: Any) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            let vc = STORYBOARD.map.instantiateViewController(withIdentifier: "CovidScreen") as! CovidScreen
            vc.modalPresentationStyle = .overFullScreen //or .overFullScreen for transparency
            vc.confirmCovid = { [weak self] in
                self?.submitService()
            }
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func onCacnelBooking(_ sender: Any) {
        var orderIds = 0
        if let orderId = self.serviceObj?.orderId{
            orderIds = orderId
        }else if let orderId = self.currentServiceObj?.orderId{
            orderIds = orderId
        }else if let orderId = self.placeOrderObj?.orderId{
            orderIds = orderId
        }
        
        if orderIds == 0{
            self.cancelServiceAlert { [weak self] in
                FirebaseanalyticsManager.share.logEvent(event: .beforeAcceptPaymentCancelClick)
                Utility.setHomeRoot(false,nil)
            }
        }else{
            if self.currentServiceObj?.driver == nil{
                self.cancelServiceAlert { [weak self] in
                    FirebaseanalyticsManager.share.logEvent(event: .afterAcceptPaymentcancelClick)
                    self?.cancelService()
                }
            }else{
                self.cancelServicePrice()
            }
        }
    }
    
    @IBAction func onNavigateCurrantLocation(_ sender: UIButton) {
        if CacheArray.shared.placeOrderRequest != nil{
            if let pin = self.mapView.annotations.first(where: {$0 is CustomPin}){
                self.changeCurrentPinLocation(coordinate: pin.coordinate)
            }else{
                centerMapOnUserLocation()
            }
        }else{
            centerMapOnUserLocation()
        }
    }
    
    
    @IBAction func onServiceDetail(_ sender: UIButton) {
        let vc = STORYBOARD.map.instantiateViewController(withIdentifier: "MapServiceDetailScreen") as! MapServiceDetailScreen
        vc.orderHistoryData = currentServiceObj
        vc.serviceObj = self.serviceObj
        vc.placeOrderObj = self.placeOrderObj
        vc.observerOrderHandler = self.observerOrderHandler
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onDriverDetail(_ sender: UIButton) {
        self.driverFullView.isHidden = false
    }
    
    @IBAction func onCloseDriver(_ sender: UIButton) {
        self.driverFullView.isHidden = true
    }
    
    @IBAction func onChat(_ sender: UIButton) {
        let vc = STORYBOARD.message.instantiateViewController(withIdentifier: "MessageScreen") as! MessageScreen
        vc.receiverId = currentServiceObj?.driver?.driverId ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onApplyReferral(_ sender: Any) {
        self.checkCouponCode(isReferral: self.isCheckReferral == 0 ? 1 : 0)
    }
    
    
    @IBAction func onSelectCard(_ sender: UIButton) {
        self.selectCard()
    }
    
    @IBAction func onScheduleNow(_ sender: Any) {
        self.displayDropDown(array: ["Now","Schedule"], tag: 0, view: self.scheduleNowLabel)
    }
    
    
    @IBAction func onApplyPromocode(_ sender: Any) {
//        if self.promocodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
//            Utility.showAlert(vc: self, message: "Please enter promocode")
//        }else{
//            self.checkCouponCode()
//        }
    }    
    
    @IBAction func onDeclineDriverOrder(_ sender: Any) {
        self.acceptDeclineDriverRequest(status: 0, paymentId: "")
    }
    
    @IBAction func onAccpetDriverOrder(_ sender: Any) {
        if paymentId == ""{
            self.showAddCardAlert()
        }else{
            FirebaseanalyticsManager.share.logEvent(event: .acceptPaymentClick)

            self.submitService()
//            self.acceptDeclineDriverRequest(status: 1, paymentId: paymentId)
        }
    }
    
    @IBAction func onAcceptCharge(_ sender: Any) {
        self.acceptRejectExtraCharges(accept: true)
    }
    
    @IBAction func onDeclineCharge(_ sender: Any) {
        self.acceptRejectExtraCharges(accept: false)
    }
    
    @IBAction func onChangeVehicle(_ sender: Any) {
        let vc = STORYBOARD.garage.instantiateViewController(withIdentifier: "GarageScreen") as! GarageScreen
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onPickupCurrent(_ sender: Any) {
        let cityCoords = CLLocation(latitude: Double(currentLatitude) ?? 0.0, longitude: Double(currentLongitude) ?? 0.0)
        self.getAdressName(coords: cityCoords,isPickup: true)
    }
    
    @IBAction func onDestinationCurrent(_ sender: Any) {
        let cityCoords = CLLocation(latitude: Double(currentLatitude) ?? 0.0, longitude: Double(currentLongitude) ?? 0.0)
        self.getAdressName(coords: cityCoords,isPickup: false)
    }
    
    @IBAction func onSelectServicePromocode(_ sender: Any) {
        let vc = STORYBOARD.promocode.instantiateViewController(withIdentifier: "PromoCodeScreen") as! PromoCodeScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
//        self.setPriceView(data: self.pro)
    }
    
    func getAdressName(coords: CLLocation,isPickup: Bool,isBoth: Bool = false) {
        
        CLGeocoder().reverseGeocodeLocation(coords) { (placemark, error) in
            if error != nil {
                print("Hay un error")
            } else {
                
                let place = placemark! as [CLPlacemark]
                print(place)
                if place.count > 0 {
                    let place = placemark![0]
                    var adressString : String = ""
                    
                    if place.name != nil{
                        adressString = adressString + place.name! + ", "
                    }
                    
                    if place.locality != nil {
                        adressString = adressString + place.locality! + " - "
                    }
                    
                    if place.subLocality != nil {
                        adressString = adressString + place.subLocality! + ", "
                    }
                    
                    if place.administrativeArea != nil {
                        adressString = adressString + place.administrativeArea! + ", "
                    }
                    
                    if place.postalCode != nil {
                        adressString = adressString + place.postalCode! + ", "
                    }
                    if place.country != nil {
                        adressString = adressString + place.country!
                    }
                    //                        if place.country != nil {
                    //                            adressString = adressString + place.country!
                    //                        }
                    print(adressString)
                    if let data = CacheArray.shared.placeOrderRequest{
                        
                        if let obj = CacheArray.shared.serviceArray.first(where: {"\($0.serviceId ?? 0)" == "\(data.serviceId ?? "0")"}){
                            if obj.serviceId == 5{
                                CacheArray.shared.placeOrderRequest?.state = place.administrativeArea ?? ""
                            }
                        }
                    }
                    if isBoth{
                        CacheArray.shared.placeOrderRequest?.pickup_address = adressString
                        CacheArray.shared.placeOrderRequest?.pickup_latitude = currentLatitude
                        CacheArray.shared.placeOrderRequest?.pickup_longitude = currentLongitude
                        self.searchPickupTextField.text = adressString
                        
//                        CacheArray.shared.placeOrderRequest?.destination_address = adressString
//                        CacheArray.shared.placeOrderRequest?.destination_latitude = currentLatitude
//                        CacheArray.shared.placeOrderRequest?.destination_longitude = currentLongitude
//                        self.searchDestinationTextField.text = adressString
                    }else{
                        if isPickup{
                            CacheArray.shared.placeOrderRequest?.pickup_address = adressString
                            CacheArray.shared.placeOrderRequest?.pickup_latitude = currentLatitude
                            CacheArray.shared.placeOrderRequest?.pickup_longitude = currentLongitude
                            self.searchPickupTextField.text = adressString
                        }else{
                            CacheArray.shared.placeOrderRequest?.destination_address = adressString
                            CacheArray.shared.placeOrderRequest?.destination_latitude = currentLatitude
                            CacheArray.shared.placeOrderRequest?.destination_longitude = currentLongitude
                            self.searchDestinationTextField.text = adressString
                        }
                    }
                    if CacheArray.shared.placeOrderRequest != nil && isBoth == false{
                        self.getBookingCalculation()
                    }
                }
            }
        }
    }
    
    func getBookingCalculation(){
        let obj = CacheArray.shared.placeOrderRequest
        let data = PlaceOrderRequest(serviceId: obj?.serviceId,user_garage_id: obj?.user_garage_id /*plateNumber: obj?.plateNumber, vehicleYearId: obj?.vehicleYearId, vehicleMakeId: obj?.vehicleMakeId, vehicleModelId: obj?.vehicleModelId, vehicleColorId: obj?.vehicleColorId*/, pickup_latitude: obj?.pickup_latitude, pickup_longitude: obj?.pickup_longitude, pickup_address: obj?.pickup_address, destination_latitude: obj?.destination_latitude, destination_longitude: obj?.destination_longitude, destination_address: obj?.destination_address, in_garage: obj?.in_garage, questions: obj?.questions, fuleType: obj?.fuleType, state: obj?.state, scheduleDate: obj?.scheduleDate, autoTransportStartDate: obj?.autoTransportStartDate, deviceType: obj?.deviceType)
        print(data.toJSON())
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            MapService.shared.calculateOrderPrice(parameters: data.toJSON()) { [weak self] statusCode, response in
                Utility.hideIndicator()
                print(response)
                print(response?.subDiscountPrice)
                if let res = response{
                    self?.setDriverRequestDataFromLocal(data: res)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func cancelBookingAlert(price:String){
        let alertController = UIAlertController(title: "Are you sure?", message: "It’s been 5 min since your request,a service charge of $\(price) will be applied if canceled.", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "No, Go Back", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        let cancelAction = UIAlertAction(title: "Yes, Cancel", style: UIAlertAction.Style.destructive) {
            UIAlertAction in
            FirebaseanalyticsManager.share.logEvent(event: .cancelAfterDriverAc)
            self.cancelService()
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func checkValidation() -> String?{
        if self.searchPickupTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter your pickup address"
        }else if self.searchDestinationTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 && self.destinationView.isHidden == false{
            return "Please enter your destination address"
        }
        return nil
    }
    
    func displaySearchingProviderView(data: OrderStatusResponse){
        self.serviceTitleLabel.forEach({$0.text = data.service?.name})
        self.serviceLocationView.isHidden = false
        self.serviceLocationLabel.text = data.status == OrderStatus.hookedVehicle.rawValue ? data.destinationAddress : data.pickupAddress
        self.driverOrderRequetView.isHidden = true
        self.inputLocationView.isHidden = true
        self.pickLocationButton.isHidden = true
        self.searchProviderView.isHidden = false
        self.foundServiceProviderView.isHidden = true
        self.serviceRouteView.isHidden = true
        self.cancelChatView.isHidden = false
        self.chatWithServiceProviderView.isHidden = true
        self.driverImageView.superview?.superview?.isHidden = true
    }
    
    
    func displayServiceProviderView(){
        self.searchProviderView.isHidden = false
        self.foundServiceProviderView.isHidden = true
        self.serviceRouteView.isHidden = true
    }
    
    func setPriceView(data: FirebaseAcceptedServiceResponse){
        let mainPrice = (data.old_price ?? 0.0) + (data.stripeTax ?? 0.0)
        //let string = "\(data.price ?? 0.0) "+mainPrice
//        let attributedString = NSMutableAttributedString(string: string)
//        let discountPriceRange = (string as NSString).range(of: "\(data.price ?? 0.0) ")
        self.oldPriceLabel.isHidden = false
        self.oldPriceLineLabel.isHidden = false
        self.oldPriceLabel.text = "$\(String(format:"%.02f",mainPrice))"
        self.driverApproxChargeLabel.text = "$\(String(format:"%.02f",data.price ?? 0.0))"
        if self.driverApproxChargeLabel.text == self.oldPriceLabel.text{
            self.oldPriceLabel.text = nil
            self.oldPriceLabel.isHidden = true
            self.oldPriceLineLabel.isHidden = true
        }
        //"\( ?? 0.0)"
        
//        let mainPriceRange = (string as NSString).range(of: mainPrice)
        
//        attributedString.addAttributes([NSAttributedString.Key.foregroundColor : Utility.checkLightMode() == true ? UIColor.black:UIColor.white,NSAttributedString.Key.font: UIFont(name: "LexendDeca-Bold", size: 14)!], range: discountPriceRange)
//        attributedString.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.lightGray,NSAttributedString.Key.font: UIFont(name: "LexendDeca-Bold", size: 12)!,NSAttributedString.Key.strikethroughStyle: 2], range: mainPriceRange)
//        self.driverApproxChargeLabel.attributedText = attributedString
        
    }
    
    func setReferralData(data: ReferralCodeResponse){
        let string = "You have $\(data.totalPrice ?? 0) and You can use up to $5 at a time"
        let attributedString = NSMutableAttributedString(string: string)
        attributedString.addAttributes([NSAttributedString.Key.font : UIFont(name: "LexendDeca-Bold", size: 10)!], range: (string as NSString).range(of: "$\(data.totalPrice ?? 0)"))
        self.referralPriceLabel.attributedText = attributedString
    }
}
//MARK:- API
extension MapWhereAreYouScreen{
    
    func submitService(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = CacheArray.shared.placeOrderRequest
            data?.payment_id = self.paymentId
//            data?.pickup_address = self.searchPickupTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
//            data?.destination_address = self.searchDestinationTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            MapService.shared.placeOrderV2(parameters: data?.toJSON() ?? [:],image: CacheArray.shared.vehicleImage, imageParamName: "image") { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                //                print(response?.toJSON())
                if let data = CacheArray.shared.placeOrderRequest,data.scheduleDate != nil{
                    CacheArray.shared.placeOrderRequest = nil
                    CacheArray.shared.vehicleImage = nil
                    Utility.openMapScreen()
                    appDelegate.window?.makeToast("Your service has been scheduled", duration: 3.0, position: .bottom)
                }
                self?.placeOrderObj = response
                
                self?.paymentId = self?.placeOrderObj?.defaultPayment?.id ?? ""
                self?.placeOrderObj?.defaultPayment?.card_number = "Card Number:- **** **** **** " + "\(self?.placeOrderObj?.defaultPayment?.card_number ?? "")"
                self?.serviceLocationView.isHidden = false
                self?.serviceLocationLabel.text = self?.searchPickupTextField.text
                self?.inputLocationView.isHidden = true
                self?.pickLocationButton.isHidden = true
                self?.searchProviderView.isHidden = true
                self?.foundServiceProviderView.isHidden = true
                self?.serviceRouteView.isHidden = true
                self?.cancelChatView.isHidden = true
                self?.chatWithServiceProviderView.isHidden = true
                self?.driverImageView.superview?.superview?.isHidden = true
                self?.driverOrderRequetView.isHidden = true
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func cancelService(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            var orderIds = 0
//            var latitude = ""
//            var longitude = ""
            if let orderId = self.serviceObj?.orderId{
                orderIds = orderId
            }else if let orderId = self.currentServiceObj?.orderId{
                orderIds = orderId
//                latitude = "\(self.currentServiceObj?.driver?.latitude ?? 0.0)"
//                longitude = "\(self.currentServiceObj?.driver?.longitude ?? 0.0)"
            }else if let orderId = self.placeOrderObj?.orderId{
                orderIds = orderId
            }
            let data = CancelOrderRequest(orderId: orderIds, latitude: nil, longitude: nil)
            MapService.shared.cancelOrder(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                self?.currentServiceObj = nil
                Utility.hideIndicator()
                if let handler = self?.observerOrderHandler{
                    FirebaseRealtimeDB.ref.removeObserver(withHandle: handler)
                }
                Utility.setHomeRoot(false,nil)
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func cancelServiceAlert(completion: @escaping () -> Void){
        let alertController = UIAlertController(title: APPLICATION_NAME, message: "Are you sure you want to cancel your service?", preferredStyle: .alert)
        let resendAction = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            completion()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(cancelAction)
        alertController.addAction(resendAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func cancelServicePrice(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            var orderIds = 0
            var latitude = ""
            var longitude = ""
            if let orderId = self.serviceObj?.orderId{
                orderIds = orderId
            }else if let orderId = self.currentServiceObj?.orderId{
                orderIds = orderId
                latitude = "\(self.currentServiceObj?.driver?.latitude ?? 0.0)"
                longitude = "\(self.currentServiceObj?.driver?.longitude ?? 0.0)"
            }else if let orderId = self.placeOrderObj?.orderId{
                orderIds = orderId
            }
            let data = CancelOrderPriceRequest(orderId: orderIds, latitude: latitude, longitude: longitude)
            MapService.shared.cancelPrice(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                //FIXME: CANCEL SERVICE IF AMOUNT IS 0(ZERO)
                if let priceData = response.cancelOrderPriceData,let amount = priceData.amount,amount > 0{
                    self?.cancelBookingAlert(price:  "\(String(format: "%.2f", amount))")
                }else{
                    self?.cancelService()
                }
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func getFuelPrice(stateName:String){
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            MapService.shared.getFuelPrice(url: fuelPriceURL+stateName) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                   // self?.midGradePriceLabel.text = "$" + "\(res.state?.midGrade ?? "")"
                   // self?.gasolinePriceLabel.text = "$"+"\(res.state?.gasoline ?? "")"
                   // self?.premiumPriceLabel.text = "$"+"\(res.state?.premium ?? "")"
                  //  self?.dieselPriceLabel.text = "$"+"\(res.state?.diesel ?? "")"
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func messageCount(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            //Utility.showIndicator()
            MessageService.shared.getTotalMessageCount {[weak self] (statusCode, response) in
                Utility.hideIndicator()
                let messageCount = response?.count ?? 0
                self?.chatWithServiceProviderLabel.text = "CHAT WITH YOUR SERVICE PROVIDER (" + "\(messageCount))"
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)

            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }

    func acceptDeclineDriverRequest(status: Int,paymentId:String){
        self.view.endEditing(true)
//        if Utility.isInternetAvailable(){
//            Utility.showIndicator()
//            let data = OrderDriverRequest(orderId: "\(self.currentServiceObj?.orderId ?? 0)", status: "\(status)", reason: nil, payment_id: paymentId,code: self.selectPromocode,plateNumber: self.selectVehicle == nil ? self.currentServiceObj?.plateNumber : self.selectVehicle?.plateNumber,vehicleYearId: self.selectVehicle == nil ? "\(self.currentServiceObj?.vehicleYearId ?? 0)" : "\(self.selectVehicle?.vehicleYearId ?? 0)",vehicleMakeId: self.selectVehicle == nil ? "\(self.currentServiceObj?.vehicleMakeId ?? 0)" : "\(self.selectVehicle?.vehicleMakeId ?? 0)",vehicleModelId: self.selectVehicle == nil ? "\(self.currentServiceObj?.vehicleModelId ?? 0)" : "\(self.selectVehicle?.vehicleModelId ?? 0)",vehicleColorId: self.selectVehicle == nil ? "\(self.currentServiceObj?.vehicleColorId ?? 0)" : "\(self.selectVehicle?.vehicleColorId ?? 0)")
//            var imgData: Data?
//            if let img = self.selectVehicle?.img{
//                imgData = img.jpegData(compressionQuality: 0.3)
//            }else if let img = self.vehicleDownloadImage{
//                imgData = img.jpegData(compressionQuality: 0.3)
//            }
//            do {
//                
//                if let imageUrl = URL(string: self.selectVehicle == nil ? self.currentServiceObj?.image ?? "" : self.selectVehicle?.image ?? ""){
//                    try imgData = Data(contentsOf: imageUrl)
//                }
//               
//            }catch{
//                
//            }
//           
//            MapService.shared.driverOrderRequest(imageParameterName: "image", image: imgData,parameters: data.toJSON()) { [weak self] (statusCode, response) in
//                Utility.hideIndicator()
//                if let message = response.message{
//                    appDelegate.window?.makeToast(message, duration: 3.0, position: .bottom)
//                }
//                self?.driverOrderRequetView.isHidden = true
//                self?.cancelChatView.isHidden = false
//                self?.chatWithServiceProviderView.isHidden = true
//                self?.searchProviderView.isHidden = false
//                self?.cancelBookingView.isHidden = false
//            } failure: { [weak self] (error) in
//                guard let stronSelf = self else { return }
//                Utility.hideIndicator()
//                Utility.showAlert(vc: stronSelf, message: error)
//            }
//        }else{
//            Utility.hideIndicator()
//            Utility.showNoInternetConnectionAlertDialog(vc: self)
//        }
    }
    
    func acceptRejectExtraCharges(accept: Bool){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            guard let orderExtraChargeId = self.currentServiceObj?.extraCharge?.first(where: {$0.status == "pending"})?.orderExtraChargeId else {
                return
            }
            let data = ExtraChargesRequest(orderExtraChargeId: orderExtraChargeId, status: accept ? "accept" : "reject")
            MapService.shared.acceptRejectExtraCharges(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                self?.extraChargeView.isHidden = true
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func getRefferalCode(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            AuthenticationService.shared.getReferralCode { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self?.setReferralData(data: res)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func checkCouponCode(isReferral: Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = OrderCheckCouponRequest(orderId: self.currentServiceObj?.orderId, code: self.selectPromocode, referralCodeCheck: isReferral)
            VehicleService.shared.checkCoupon(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                self?.isCheckReferral = isReferral
                self?.checkReferralImage.isHidden = !(self?.checkReferralImage.isHidden ?? true)
                self?.checkReferralBGView.backgroundColor = self?.checkReferralImage.isHidden == true ? .clear : Utility.getUIcolorfromHex(hex: "051626")
                if let res = response{
                    self?.setPriceView(data: res)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }

        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
//MARK:- GOOGLE AUTOCOMPLETTION DELEGATE
extension MapWhereAreYouScreen: GMSAutocompleteViewControllerDelegate {

  // Handle the user's selection.
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    print(place)
    print("Place latitude: \(place.coordinate.latitude)")
    print("Place longitude: \(place.coordinate.longitude)")
    print("Place name: \(place.name ?? "")")
    print("Place ID: \(place.placeID ?? "")")
    print("Place attributions: \(String(describing: place.attributions))")
    self.view.endEditing(true)

    if self.isPickupSearch{
        CacheArray.shared.placeOrderRequest?.pickup_address = place.name
        CacheArray.shared.placeOrderRequest?.pickup_latitude = place.coordinate.latitude.clean
        CacheArray.shared.placeOrderRequest?.pickup_longitude = place.coordinate.longitude.clean
        self.searchPickupTextField.text = place.name
        self.changeCurrentPinLocation(coordinate: place.coordinate)
      //  CacheArray.shared.placeOrderRequest?.state = place.administrativeArea ?? ""
    }else{
        CacheArray.shared.placeOrderRequest?.destination_address = place.name
        CacheArray.shared.placeOrderRequest?.destination_latitude = place.coordinate.latitude.clean
        CacheArray.shared.placeOrderRequest?.destination_longitude = place.coordinate.longitude.clean
        self.searchDestinationTextField.text = place.name
    }
//    if selectedLocationTag == 0{
//        self.locationAddressTextField.text = place.name ?? ""
//        self.locationLatitude = "\(place.coordinate.latitude)"
//        self.locationLongitude = "\(place.coordinate.longitude)"
//    }else{
//        self.searchTextField.text = place.name ?? ""
//        self.radiusLocationLatitude = "\(place.coordinate.latitude)"
//        self.radiusLocationLongitude = "\(place.coordinate.longitude)"
//        self.setSearchRadiusButton()
//    }
      viewController.dismiss(animated: true, completion: { [weak self] in
          self?.getBookingCalculation()
      })
  }

  func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // TODO: handle the error.
    print(error.localizedDescription)
    print("Error: ", error.localizedDescription)
  }

  // User canceled the operation.
  func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    dismiss(animated: true, completion: nil)
  }

}
//MARK:- MAP DELEGATE
extension MapWhereAreYouScreen: CLLocationManagerDelegate,MKMapViewDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
//        print("locations = \(locValue.latitude) \(locValue.longitude)")
        if self.currentServiceObj != nil{
            self.updateUserLocation(location: locValue)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        print("error::: \(error)")
    }
    
    // MARK: center Map On UserLocation
    func centerMapOnUserLocation() {
        guard let coordinate = locationManager.location?.coordinate else { return }
        let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: 5000, longitudinalMeters: 5000)
        self.mapView.setRegion(region, animated: true)
    }
    
    // MARK: Set Pin On MapView
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        if annotation is MKUserLocation {
            return nil
        }
        let reuseID = "Location"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseID)
            annotationView?.canShowCallout = true
            let pin = MKAnnotationView(annotation: annotation,
                                       reuseIdentifier: reuseID)
            
            let pinData = annotation as? CustomPin
            
            if pinData?.pinId == -1{
                pin.image = UIImage(named: "currant_location_pin")
            }else{
                pin.isEnabled = true
                pin.canShowCallout = true
                pin.image = UIImage(named: "driver_map_pin")
                let storeImageView = UIImageView()
                storeImageView.frame = CGRect(x: 6.5, y: 7, width: 27, height: 27)
                storeImageView.layer.cornerRadius = storeImageView.frame.height/2
                storeImageView.layer.masksToBounds = true
//                storeImageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//                storeImageView.layer.borderWidth = 2
                
                let label = UILabel(frame: CGRect(x: pin.center.x, y: pin.center.y, width: 25, height: 25))
                label.textAlignment = .center
                label.textColor = .white
                label.font = label.font.withSize(14)
               
                //            label.text = cpa.pinId
                Utility.setImage(pinData?.storeImage, imageView: storeImageView)
                pin.addSubview(storeImageView)
                
            }
            annotationView = pin
            annotationView?.annotation = annotation
        } else {
            let pin = MKAnnotationView(annotation: annotation,
                                       reuseIdentifier: reuseID)
            let pinData = annotation as? CustomPin
            
            if pinData?.pinId == -1{
                pin.image = UIImage(named: "currant_location_pin")
            }else{
                pin.isEnabled = true
                pin.canShowCallout = true
                pin.image = UIImage(named: "driver_map_pin")
                let storeImageView = UIImageView()
                storeImageView.frame = CGRect(x: 6.5, y: 7, width: 27, height: 27)
                storeImageView.layer.cornerRadius = storeImageView.frame.height/2
                storeImageView.layer.masksToBounds = true
//                storeImageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//                storeImageView.layer.borderWidth = 2
                
                let label = UILabel(frame: CGRect(x: pin.center.x, y: pin.center.y, width: 25, height: 25))
                label.textAlignment = .center
                label.textColor = .white
                label.font = label.font.withSize(14)
               
                //            label.text = cpa.pinId
                Utility.setImage(pinData?.storeImage, imageView: storeImageView)
                pin.addSubview(storeImageView)
                
            }
            annotationView = pin
            annotationView?.annotation = annotation
        }
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {

        let renderer = MKPolylineRenderer(overlay: overlay)

        renderer.strokeColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)

        renderer.lineWidth = 5.0

        return renderer
    }
}
//MARK:- VEHICLE DELEGATE
extension MapWhereAreYouScreen: SelectVehicleDelegate{
    func selectVehicle(vehicle: VehicleListResponse) {
        self.selectVehicle = vehicle
        self.vehicleNameLabel.text = "\(vehicle.make ?? "") "+"\(vehicle.model ?? "")"

        self.vehiclePlateLabel.text = vehicle.plate
        
        //MARK: MANAGE LOCAL CACHE VEHICLE
        self.manageVehicleWithLocalData(vehicle: vehicle)
    }
}
//MARK:- PROMOCDE DELEGATE
extension MapWhereAreYouScreen: FirebaseAcceptServiceDelegate{
    func applyCouponCode(data: FirebaseAcceptedServiceResponse, promocode: String?, isReferralCheck: Int) {
        self.setPriceView(data: data)
        self.selectPromocode = promocode
        self.isCheckReferral = isReferralCheck
        self.checkReferralImage.isHidden = true
        self.cardRefferralSelectObj = data
        self.checkReferralBGView.backgroundColor = self.checkReferralImage.isHidden  ? .clear : Utility.getUIcolorfromHex(hex: "051626")
    }
}

