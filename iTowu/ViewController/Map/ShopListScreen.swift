//
//  ShopListScreen.swift
//  iTowu
//
//  Created by Nikunj Vaghela on 08/07/23.
//

import UIKit

class ShopListScreen: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var titleString : String?
    
    var itemArray: [AdvertisementList] = []
    var meta: Meta?
    let gradientArray = [UIImage(named: "gradient_1"),UIImage(named: "gradient_2"),UIImage(named: "gradient_3")]
    var counter = 0
    var hasMorePage: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeDetails()
        // Do any additional setup after loading the view.
    }
    
    func initializeDetails(){
        self.titleLabel.text = self.titleString
        self.collectionView.register(UINib(nibName: "AdvertisementListCell", bundle: Bundle.main), forCellWithReuseIdentifier: "AdvertisementListCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.getAdvertisementList(page: 1)
    }
    
    func getAdvertisementList(page: Int){
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            StationService.shared.getAdvertisementList(page: page, perPage: 20) { [weak self] statusCode, response in
                Utility.hideIndicator()
                if let res = response.advertisementResponse{
                    for i in res{
                        let obj = i
                        obj.gradientImage = self?.gradientArray[self?.counter ?? 0]
                        self?.itemArray.append(obj)
                        self?.counter += 1
                        if self?.counter == 3{
                            self?.counter = 0
                        }
                    }
                    self?.collectionView.reloadData()
                }
            } failure: { [weak self] error in
                Utility.hideIndicator()
                guard let strongSelf = self else {
                    return
                }
                Utility.showAlert(vc: strongSelf, message: error)
            }
        }else{
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onBack(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: false)
    }
    
}
//MARK: COLLECTIONVIEW DELEGATE & DATASOURCE
extension ShopListScreen: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.itemArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "AdvertisementListCell", for: indexPath) as! AdvertisementListCell
        cell.item = self.itemArray[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let leftSpace = (self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout).sectionInset.left
        let rightSpace = (self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout).sectionInset.right
        let scWidth : CGFloat = screenWidth - (leftSpace + rightSpace)
        return CGSize(width: scWidth, height: (scWidth * 100) / 375)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        OpenMapDirections.present(in: self, sourceView: self.view, fromLat: currentLatitude, fromLong: currentLongitude, toLat: "\(self.itemArray[indexPath.item].lat ?? 0)", toLong: "\(self.itemArray[indexPath.item].lng ?? 0)")
    }
}

extension ShopListScreen{
    
    // MARK: scroll method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        let reload_distance:CGFloat = 10.0
        if y > (h + reload_distance) {
            if let meta = self.meta{
                if meta.total != self.itemArray.count{
                    if self.hasMorePage{
                        self.hasMorePage = false
                        let page = (meta.current_page ?? 0) + 1
                        self.getAdvertisementList(page: page)
                    }
                }
            }
        }
    }
}
