//
//  CancelServiceReasonScreen.swift
//  iTowu
//
//  Created by Nikunj Vaghela on 15/02/25.
//

import UIKit

class CancelServiceReasonScreen: UIViewController {
    
    //MARK: - OUTLET
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var reasonTableView: UITableView!
    
    @IBOutlet weak var continueButton: dateSportButton!
    
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    var itemArray: [ReasonsRequest] = []
    var selectReason: ((String) -> Void)?

    //MARK: - FUNCTIONS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeDetails()
        // Do any additional setup after loading the view.
    }
    
    func setReasonArray(){
        self.itemArray.append(ReasonsRequest(reason: "Selected Wrong Pickup Location"))
        self.itemArray.append(ReasonsRequest(reason: "Selected Wrong Drop Location"))
        self.itemArray.append(ReasonsRequest(reason: "Booked by mistake"))
        self.itemArray.append(ReasonsRequest(reason: "Selected different service/vehicle"))
        self.itemArray.append(ReasonsRequest(reason: "Taking too long to confirm the service"))
        self.itemArray.append(ReasonsRequest(reason: "Got a service elsewhere"))
        self.itemArray.append(ReasonsRequest(reason: "Others",isManulReason: true))
    }
    
    func initializeDetails(){
        self.setReasonArray()
        self.reasonTableView.delegate = self
        self.reasonTableView.dataSource = self
        self.reasonTableView.register(UINib(nibName: "ReasonCell", bundle: Bundle.main), forCellReuseIdentifier: "ReasonCell")
        self.manageContinueButton()
        
        self.mainView.clipsToBounds = true
        self.mainView.layer.cornerRadius = 10
        self.mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top right corner, Top left corner respectively
    }
    
    func manageReason(indexPath: IndexPath){
        for (index,obj) in self.itemArray.enumerated(){
            if index == indexPath.row{
                self.itemArray[index].selected = !(obj.selected ?? false)
            }else{
                self.itemArray[index].selected = false
            }
        }
        self.reasonTableView.reloadData()
        self.manageContinueButton()
    }
    
    func manageContinueButton(){
        let reason = self.itemArray.first(where: {$0.selected == true})
        let enable = ((reason?.isManulReason == true && (reason?.otherReason?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0) > 0) || (reason?.selected == true && reason?.isManulReason == false))
        self.continueButton.isUserInteractionEnabled = enable
        self.continueButton.alpha = enable ? 1 : 0.5
    }
    

    //MARK: - ACTIONS
    @IBAction func onClose(_ sender: Any) {
        self.dismiss(animated: false)
    }
    
    @IBAction func onContinue(_ sender: Any) {
        self.dismiss(animated: false) { [weak self] in
            if let reason = self?.itemArray.first(where: {$0.selected == true}){
                if let customReason = reason.otherReason,customReason.trimmingCharacters(in: .whitespacesAndNewlines).count > 0{
                    self?.selectReason?(customReason)
                }else{
                    self?.selectReason?(reason.reason ?? "")
                }
            }
        }
    }
    
}
//MARK: - TABLEVIEW DELEGATE AND DATASOURCE
extension CancelServiceReasonScreen: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReasonCell", for: indexPath) as! ReasonCell
        cell.item = self.itemArray[indexPath.row]
        cell.seperatorView.isHidden = (self.itemArray.count - 1 == indexPath.row)
        cell.onTextChange = { [weak self] reason in
            self?.itemArray[indexPath.row].otherReason = reason
            self?.manageContinueButton()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        DispatchQueue.main.async { [weak self] in
            self?.tableViewHeightConstraint.constant = tableView.contentSize.height > (screenHeight - 200) ? (screenHeight - 200) : tableView.contentSize.height
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.manageReason(indexPath: indexPath)
    }
}
