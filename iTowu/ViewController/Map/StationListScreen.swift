//
//  StationListScreen.swift
//  iTowu
//
//  Created by Nikunj Vaghela on 17/06/23.
//

import UIKit

class StationListScreen: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var titleString : String?
    
    var itemArray: [NearByGasStationList] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeDetails()
        // Do any additional setup after loading the view.
    }
    
    func initializeDetails(){
        self.titleLabel.text = self.titleString
        self.collectionView.register(UINib(nibName: "NearByStationCell", bundle: Bundle.main), forCellWithReuseIdentifier: "NearByStationCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onBack(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: false)
    }
    
}
//MARK: COLLECTIONVIEW DELEGATE & DATASOURCE
extension StationListScreen: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.itemArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "NearByStationCell", for: indexPath) as! NearByStationCell
        cell.item = self.itemArray[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let leftSpace = (self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout).sectionInset.left
        let rightSpace = (self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout).sectionInset.right
        let scWidth : CGFloat = screenWidth - (leftSpace + rightSpace)
        return CGSize(width: scWidth, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            FirebaseanalyticsManager.share.logEvent(event: titleString == "Gas Station Near By" ? .gasStationNearbyClick : .chargingStationNearbyClick)

        OpenMapDirections.present(in: self, sourceView: self.view, fromLat: currentLatitude, fromLong: currentLongitude, toLat: "\(self.itemArray[indexPath.item].lat ?? 0)", toLong: "\(self.itemArray[indexPath.item].lng ?? 0)")
    }
}
