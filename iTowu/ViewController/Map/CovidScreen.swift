//
//  CovidScreen.swift
//  iTowu
//
//  Created by iroid on 02/01/22.
//

import UIKit

class CovidScreen: UIViewController {

    @IBOutlet weak var checkMarkIcon: UIImageView!
    @IBOutlet weak var confirmButton: dateSportButton!

    var confirmCovid:(()->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func onBack(_ sender: UIButton) {
        self.dismiss(animated: true) {
        }
    }
    @IBAction func onIamWearingMask(_ sender: UIButton) {
        self.checkMarkIcon.isHidden = false
        self.confirmButton.alpha = 1
        self.confirmButton.isUserInteractionEnabled = true
    }
    
    
    @IBAction func onConfirm(_ sender: UIButton) {
        self.dismiss(animated: true) {
            print("dismis")
            self.confirmCovid!()
        }
    }
    
}
