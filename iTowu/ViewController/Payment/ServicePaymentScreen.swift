//
//  ServicePaymentScreen.swift
//  iTowu
//
//  Created by Nikunj on 14/12/21.
//

import UIKit

class ServicePaymentScreen: UIViewController {

    
    @IBOutlet weak var pickUpLocatioinNameLabel: UILabel!
    @IBOutlet weak var destinationLocationNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    var orderId = 0
    
    var currentServiceObj: FirebaseAcceptedServiceResponse?
    var cardId = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialDetail()
    }
    
    func initialDetail(){
        self.getService(orderId: orderId)
    }

   
    @IBAction func onExplorePromocode(_ sender: Any) {
        let vc = STORYBOARD.promocode.instantiateViewController(withIdentifier: "PromoCodeScreen") as! PromoCodeScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onPay(_ sender: UIButton) {
        let vc = STORYBOARD.payment.instantiateViewController(withIdentifier: "CardListScreen") as! CardListScreen
        vc.isFromPayment = true
        
        vc.onPaymentSelectedButton = { [weak self] paymentId in
            self?.cardId = paymentId
            self?.doPaymentAPI()
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func setUiData(){
        self.pickUpLocatioinNameLabel.text = currentServiceObj?.pickupAddress
        self.priceLabel.text  = "$\(currentServiceObj?.price ?? 0)"
    }
    
    
}
//MARK:- API
extension ServicePaymentScreen{
    
    func getService(orderId:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            let url = orderDetailURL + "\(orderId)"
            MapService.shared.orderDetail(urlString: url) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                
                if let res = response{
                    print(res.toJSON())
                    self?.currentServiceObj = res
                    self?.setUiData()
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }

        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func doPaymentAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = PaymentRequest(orderId: "\(orderId)", payment_id: cardId, price: "20")
            CardService.shared.servicePayment(parameters: data.toJSON()) { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    print(res)
                    let vc = STORYBOARD.review.instantiateViewController(withIdentifier: "ReviewScreen") as! ReviewScreen
                    vc.currentServiceObj = self.currentServiceObj
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }

        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
}
