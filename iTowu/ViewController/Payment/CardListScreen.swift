//
//  CardListScreen.swift
//  iTowu
//
//  Created by iroid on 17/10/21.
//

import UIKit

class CardListScreen: UIViewController {

    @IBOutlet weak var cardTableView: UITableView!
    @IBOutlet weak var couponCodeButton: UIButton!
    
    @IBOutlet weak var couponCodeHeight: NSLayoutConstraint!
    @IBOutlet weak var promocodeViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var promocodeTextField: UITextField!
    
    @IBOutlet weak var promocodeView: dateSportView!
    @IBOutlet weak var checkReferralBGView: dateSportView!
    
    @IBOutlet weak var checkReferralImage: UIImageView!
    @IBOutlet weak var referralPriceLabel: UILabel!
    
    @IBOutlet weak var promoCardView: UIView!
    
    var isCheckReferral: Int = 0
    
    var cardArray: [CardResponse] = []
    var onPaymentSelectedButton:((_ paymentId:String)->Void)?
    
    var onPaymentSelectedButton1:((_ cardResponse:CardResponse)->Void)?

    
    var isFromPayment = false
    var isFromCreateService = false
    var service: FirebaseAcceptedServiceResponse?
    
    weak var delegate: FirebaseAcceptServiceDelegate?
    var promocodeRes: FirebaseAcceptedServiceResponse?
    var promoCode: String?
    var isAddNewCard: Bool = false
    var refferalObj: ReferralCodeResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intialDetail()
    }

    func intialDetail(){
        let nib = UINib(nibName: "CardTableViewCell", bundle: nil)
        cardTableView.register(nib, forCellReuseIdentifier: "CardTableViewCell")
        self.couponCodeButton.isHidden = !self.isFromCreateService
        self.couponCodeHeight.constant = self.isFromCreateService ? 20 : 0
        self.promocodeViewHeight.constant = self.service == nil ? 0 : 54
        self.promocodeView.isHidden = self.service == nil
        self.checkReferralImage.isHidden = self.isCheckReferral == 1 ? false : true
        self.checkReferralBGView.backgroundColor = self.checkReferralImage.isHidden == true ? .clear : Utility.getUIcolorfromHex(hex: "051626")
        if let promo = self.promoCode{
            self.promocodeTextField.text = promo
        }
        self.promoCardView.isHidden = self.isFromCreateService == false
        self.getCardList()
        self.getRefferalCode()
//        DispatchQueue.main.async { [weak self] in
//            guard let strongSelf = self else {
//                return
//            }
//            if strongSelf.isAddNewCard {
//                Utility.showAlert(vc: strongSelf, message: "Please enter your card details here so it will be use for creating a service.")
//            }
//        }
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        if self.isAddNewCard && self.cardArray.count > 0{
            Utility.setHomeRoot(false, nil)
        }else{
            if isFromPayment{
                self.dismiss(animated: true, completion: nil)
            }else{
                if let res = self.promocodeRes{
                    self.delegate?.applyCouponCode(data: res,promocode: self.promocodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),isReferralCheck: self.isCheckReferral)
                }else{
                    if let vc = self.navigationController?.viewControllers.first as? MapScreen,delegate == nil{
                        vc.observeOrder()
                    }
                }
                self.navigationController?.popViewController(animated: false)
            }
        }
    }
    
    @IBAction func onPromocode(_ sender: Any) {
        let vc = STORYBOARD.promocode.instantiateViewController(withIdentifier: "PromoCodeScreen") as! PromoCodeScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onApply(_ sender: Any) {
        if self.promocodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            Utility.showAlert(vc: self, message: "Please enter promocode")
        }else{
            self.checkCouponCode()
        }
    }
    
    @IBAction func onApplyReferral(_ sender: Any) {
        if self.refferalObj?.totalPrice == 0{
            Utility.showAlert(vc: self, message: "Sorry, you don't have any points for applying for a referral.")
        }else{
            self.checkCouponCode(isReferral: self.isCheckReferral == 0 ? 1 : 0)
        }
    }
    
    func checkCouponCode(isReferral: Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = OrderCheckCouponRequest(orderId: self.service?.orderId, code: nil, referralCodeCheck: isReferral)
            VehicleService.shared.checkCoupon(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                self?.isCheckReferral = isReferral
                self?.checkReferralImage.isHidden = !(self?.checkReferralImage.isHidden ?? true)
                self?.checkReferralBGView.backgroundColor = self?.checkReferralImage.isHidden == true ? .clear : Utility.getUIcolorfromHex(hex: "051626")
                if let res = response{
                    self?.promocodeRes = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }

        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func displayLogOutDialogue(indexPath:Int){
        let alertController = UIAlertController(title: APPLICATION_NAME, message: "Are you sure you want to delete the card?", preferredStyle: .alert)
        let resendAction = UIAlertAction(title: "Yes", style: .default, handler: { [weak self] (action) in
            //self?.logOut()
            FirebaseanalyticsManager.share.logEvent(event: .deleteCard)
            let data = self?.cardArray[indexPath]
            self?.cardArray.remove(at: indexPath)
            self?.cardTableView.reloadData()
            
            self?.deleteCard(cardId: data?.id ?? "")
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(cancelAction)
        alertController.addAction(resendAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func setReferralData(data: ReferralCodeResponse){
        let string = "You have $\(data.totalPrice ?? 0) and You can use up to $5 at a time"
        let attributedString = NSMutableAttributedString(string: string)
        attributedString.addAttributes([NSAttributedString.Key.font : UIFont(name: "LexendDeca-Bold", size: 10)!], range: (string as NSString).range(of: "$\(data.totalPrice ?? 0)"))
        self.referralPriceLabel.attributedText = attributedString
    }
}
extension CardListScreen: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFromPayment{
            return cardArray.count
        }else{
            return cardArray.count + 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardTableViewCell", for: indexPath) as! CardTableViewCell
        
        if isFromCreateService{
            cell.editButton.isHidden = true
            cell.deleteButton.isHidden = true
        }
        cell.onDelete = { [weak self] in
            self?.displayLogOutDialogue(indexPath: indexPath.row)
        }
        cell.onEdit = { [weak self] in
            let vc = STORYBOARD.payment.instantiateViewController(withIdentifier: "AddNewCardScreen") as! AddNewCardScreen
            vc.editCardObj = self?.cardArray[indexPath.row]
            vc.addCard = { [weak self] cardObj in
                self?.cardArray[indexPath.row] = cardObj
                //self?.cardTableView.reloadData()
                let indexPosition = IndexPath(row: indexPath.row, section: 0)
                self?.cardTableView.reloadRows(at: [indexPosition], with: .none)
            }
            self?.navigationController?.pushViewController(vc, animated: true)
        }
        if !isFromPayment{
        if cardArray.count == indexPath.row{
            cell.addCardButtonView.isHidden = false
            cell.cardView.isHidden = true
        }else{
            cell.item =  cardArray[indexPath.row]
            cell.addCardButtonView.isHidden = true
            cell.cardView.isHidden = false
            if CacheArray.shared.currentService?.defaultPayment?.id == self.cardArray[indexPath.row].id{
                cell.editButton.isHidden = true
                cell.deleteButton.isHidden = true
            }else{
                cell.editButton.isHidden = false
                cell.deleteButton.isHidden = false
            }
         }
        }else{
            cell.item =  cardArray[indexPath.row]
            cell.addCardButtonView.isHidden = true
            cell.cardView.isHidden = false
        }
//        if CacheArray.shared.currentService?.defaultPayment?.id == self.cardArray[indexPath.row + 1].id{
//            cell.editButton.isHidden = true
//            cell.deleteButton.isHidden = true
//        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isFromPayment{
            self.onPaymentSelectedButton!(cardArray[indexPath.row].id ?? "")
            self.dismiss(animated: true, completion: nil)
        }else{
            if cardArray.count == indexPath.row{
                let vc = STORYBOARD.payment.instantiateViewController(withIdentifier: "AddNewCardScreen") as! AddNewCardScreen
                vc.addCard = { [weak self] cardObj in
                    self?.cardArray.append(cardObj)
                    self?.cardTableView.reloadData()
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                if isFromCreateService{
                    self.addDefaultCard(id: cardArray[indexPath.row].id ?? "")
                    if let res = self.promocodeRes{
                        self.delegate?.applyCouponCode(data: res,promocode: self.promocodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),isReferralCheck: self.isCheckReferral)
                    }
                    self.navigationController?.popViewController(animated: false)
                }else{
                    self.addDefaultCard(id: cardArray[indexPath.row].id ?? "")
                }
            }
        }
        
    }
}
//MARK:- API
extension CardListScreen{
    
    func getCardList(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            CardService.shared.getCardList { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self.cardArray = res
                }
                self.cardTableView.reloadData()
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- ADD DEFAULT CARD
    
        func addDefaultCard(id:String){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            CardService.shared.getDefaultCard (url: defaultCardURL+id){ (statusCode, response) in
                Utility.hideIndicator()
                self.cardArray.forEach({ (val) in
                    val.isDefault = 0
                })
                if let index = self.cardArray.firstIndex(where: {$0.id == id}){
                    self.cardArray[index].isDefault = 1
                    if self.isFromCreateService{
                        self.onPaymentSelectedButton1!(self.cardArray[index])
                    }
                }
                self.cardTableView.reloadData()
                

            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func deleteCard(cardId:String){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            CardService.shared.deleteCard(url: deleteCardURL+cardId) { statusCode, response in
                Utility.hideIndicator()
            } failure: {[weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func checkCouponCode(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = OrderCheckCouponRequest(orderId: self.service?.orderId,code: self.promocodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),referralCodeCheck: 0)
            VehicleService.shared.checkCoupon(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                guard let strongSelf = self else { return }
                Utility.hideIndicator()
                strongSelf.isCheckReferral = 0
                strongSelf.checkReferralImage.isHidden = strongSelf.isCheckReferral == 1 ? false : true
                strongSelf.checkReferralBGView.backgroundColor = strongSelf.checkReferralImage.isHidden == true ? .clear : Utility.getUIcolorfromHex(hex: "051626")
                Utility.showAlert(vc: strongSelf, message: "Pramocode applied successfully!")
                if let res = response{
                    strongSelf.promocodeRes = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }

        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func getRefferalCode(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            AuthenticationService.shared.getReferralCode { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self?.refferalObj = res
                    self?.setReferralData(data: res)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
