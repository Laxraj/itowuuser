//
//  AddNewCardScreen.swift
//  iTowu
//
//  Created by iroid on 17/10/21.
//

import UIKit

class AddNewCardScreen: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var cardHolderNameTextField: UITextField!
    @IBOutlet weak var cardNumberTextField: UITextField!
    @IBOutlet weak var expirationTextField: UITextField!
    @IBOutlet weak var cvvTextField: UITextField!
    
    @IBOutlet weak var addCardButton: dateSportButton!
    
    var addCard:((CardResponse)->Void)?
    var editCardObj:CardResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDatePicker()
        self.initialDetail()
    }
    func initialDetail(){
        if let data = editCardObj{
            self.cardHolderNameTextField.text = data.cardHolderName
            self.cardNumberTextField.text = data.card_number
            self.expirationTextField.text = data.expiration_date
            self.titleLabel.text = "Edit Card"
            self.addCardButton.setTitle("EDIT CARD", for: .normal)
        }else{
            self.addCardButton.setTitle("ADD CARD", for: .normal)
        }
    }
    
    func setDatePicker(){
        let expiryDatePicker = MonthYearPickerView()
        self.expirationTextField.inputView = expiryDatePicker
        expiryDatePicker.onDateSelected = { (month: Int, year: Int) in
            let string = String(format: "%02d/%d", month, year % 100)
            NSLog(string) // should show something like 05/2015
            self.expirationTextField.text = string
        }

    }

    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func onAddCard(_ sender: UIButton) {
        if let error = self.checkValidation(){
        Utility.showAlert(vc: self, message: error)
    }else{
        self.addAndUpdateCardAPI()
        //self.updateUserData()
    }
    }
    func checkValidation() -> String?{
         if self.cardHolderNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter card holder name"
        }else if editCardObj == nil && self.cardNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 < 14{
            return "Please enter proper card number"
        }else if self.expirationTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter expiration date"
        }else if self.cvvTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter cvv"
        }
        return nil
    }
}
//MARK:- API
extension AddNewCardScreen{
    func addAndUpdateCardAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = CardAddEditRequest(payment_id: editCardObj?.id, card_holder_name: cardHolderNameTextField.text, card_number: cardNumberTextField.text, expiration_date: expirationTextField.text, cvv: cvvTextField.text)
            CardService.shared.addCardAndUpdate(parameters: data.toJSON()) { (statusCode, response) in
                Utility.hideIndicator()
                FirebaseanalyticsManager.share.logEvent(event: self.editCardObj == nil ? .addCard : .editCard)
                if let res = response{
                    print(res)
                    self.addCard!(res)
                }
                self.navigationController?.popViewController(animated: true)
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }

        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
