//
//  NotificationScreen.swift
//  iTowu
//
//  Created by Nikunj Vaghela on 16/08/23.
//

import UIKit

class NotificationScreen: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noNotificationLabel: UILabel!
    
    @IBOutlet weak var deleteAllButton: UIButton!
    
    
    var itemArray: [NotificationResponse] = []
    var meta: Meta?
    var hasMorePage: Bool = true
    var backClicked: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeDetails()
        // Do any additional setup after loading the view.
    }
    
    func initializeDetails(){
        FirebaseanalyticsManager.share.logEvent(event: .notificationScreen)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "NotificationListCell", bundle: Bundle.main), forCellReuseIdentifier: "NotificationListCell")
        self.getNotifications(page: 1)
    }
    
    func manageNavigation(notificationObj: NotificationResponse){
        switch notificationObj.type{
        case NotificationType.order.rawValue,NotificationType.referral.rawValue:
            Utility.setHomeRoot(false, nil)
        case NotificationType.admin.rawValue:
            break
        default:
            break
        }
    }
    
    func manageAccept(action: Int,notification: NotificationResponse,indexPath:Int){
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = FeatAdminJoinRequest(feat_admin_id: "\(notification.feat_admin_int_id ?? 0)",status: action,notification_id: notification.notification_id)
            NotificationService.shared.adminJoin(parameters: data.toJSON()) { [weak self] statusCode, response in
                Utility.hideIndicator()
                debugPrint(response)
                if let message = response.message{
                    Utility.showToastWarning(message: message)
                }
                self?.itemArray.remove(at: indexPath)
                self?.tableView.reloadData()
                self?.manageNoData()
                
            } failure: { error in
                Utility.hideIndicator()
                    Utility.showToastWarning(message: error)
            }
        }else{
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func deleteNotificationAlert(notification: NotificationResponse?){
        
        let alertController = UIAlertController(title: APPLICATION_NAME, message: notification == nil ? "Are you sure your want to delete all notifications?" : "Are you sure your want to delete this notification?", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.destructive) {
            [weak self] UIAlertAction in
            self?.deleteNotification(notification: notification)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func manageNoData(){
        self.noNotificationLabel.isHidden = !(self.itemArray.isEmpty)
        self.deleteAllButton.isHidden = self.itemArray.isEmpty
    }

    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.backClicked?()
    }
    
    @IBAction func onDeleteAllNotifications(_ sender: Any) {
        self.deleteNotificationAlert(notification: nil)
    }

}
//MARK: TABLEVIEW DELEGATE AND DATASOURCE
extension NotificationScreen: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "NotificationListCell", for: indexPath) as! NotificationListCell
        cell.item = self.itemArray[indexPath.row]
        cell.manageAction = { [weak self] accept in
            guard let strongSelf = self else { return }
            strongSelf.manageAccept(action: accept, notification: strongSelf.itemArray[indexPath.row], indexPath: indexPath.row)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.manageNavigation(notificationObj: self.itemArray[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            self.deleteNotificationAlert(notification: self.itemArray[indexPath.row])
        }
    }
}
//MARK: API
extension NotificationScreen{
    func getNotifications(page: Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            NotificationService.shared.getNotification(page: page) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.notificationResponse,res.count > 0{
                    if page == 1{
                        self?.itemArray = res
                        self?.tableView.reloadData()
                    }else{
                        self?.appendDataTableView(data: res)
                    }
                    self?.hasMorePage = true
                }else{
                    self?.hasMorePage = false
                }
                self?.manageNoData()
                
                if let meta = response.meta{
                    self?.meta = meta
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func appendDataTableView(data: [NotificationResponse]){
        var indexPath : [IndexPath] = []
        for i in self.itemArray.count..<self.itemArray.count+data.count{
            indexPath.append(IndexPath(row: i, section: 0))
        }
        self.itemArray.append(contentsOf: data)
        self.tableView.insertRows(at: indexPath, with: .bottom)
    }
    
    func deleteNotification(notification: NotificationResponse?){
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            NotificationService.shared.deleteNotificaiton(notificationId: notification?.notification_id ?? -1) { [weak self] statusCode, response in
                Utility.hideIndicator()
                if let index = self?.itemArray.firstIndex(where: {$0.notification_id == notification?.notification_id}){
                    self?.itemArray.remove(at: index)
                    self?.tableView.reloadData()
                }else if notification == nil{
                    self?.itemArray = []
                    self?.tableView.reloadData()
                }
                self?.manageNoData()
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
extension NotificationScreen{
    
    // MARK: scroll method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        let reload_distance:CGFloat = 10.0
        if y > (h + reload_distance) {
            if let meta = self.meta{
                if meta.total != self.itemArray.count{
                    if self.hasMorePage{
                        self.hasMorePage = false
                        let page = (meta.current_page ?? 0) + 1
                        self.getNotifications(page: page)
                    }
                }
            }
        }
    }
}
