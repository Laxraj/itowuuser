//
//  NotificationListCell.swift
//  iTowu
//
//  Created by Nikunj Vaghela on 16/08/23.
//

import UIKit

class NotificationListCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var actionStackView: UIStackView!
    
    var manageAction: ((Int) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item: NotificationResponse?{
        didSet{
            self.titleLabel.text = item?.title
            self.descriptionLabel.text = item?.body
            self.timeLabel.text = Utility.timeAgoSinceDate(Utility.getDateTimeFromTimeInterVel(from: item?.createdAt ?? 0))
            
            self.actionStackView.isHidden = !(item?.type == NotificationType.featAdminJoin.rawValue)
        }
    }
    
    @IBAction func onAccept(_ sender: Any) {
        self.manageAction?(1)
    }
    
    
    @IBAction func onDecline(_ sender: Any) {
        self.manageAction?(2)
    }
}
