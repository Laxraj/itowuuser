//
//  ReviewScreen.swift
//  iTowu
//
//  Created by iroid on 17/10/21.
//

import UIKit
import Cosmos
class ReviewScreen: UIViewController {

    @IBOutlet weak var rattingView: CosmosView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var reviewAvgLabel: UILabel!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var reviewPlaceholderLabel: UILabel!
    @IBOutlet weak var giveRatingToLabel: UILabel!
    @IBOutlet weak var tipTextField: UITextField!
    @IBOutlet weak var feedbackTextView: UITextView!
    
    @IBOutlet weak var oneDollerButton: UIButton!
    @IBOutlet weak var threeDollerButton: UIButton!
    @IBOutlet weak var fiveDollerButton: UIButton!
    
    var currentServiceObj: FirebaseAcceptedServiceResponse?

    var orderId = 0
    var driverId = 0
    var rating = 0.0
    var tips = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialDetail()
    }
    
    func initialDetail(){
        self.rattingView.didFinishTouchingCosmos = didFinishTouchingCosmos
        self.rattingView.settings.minTouchRating = 0
        self.tipTextField.addTarget(self, action: #selector(ReviewScreen.textFieldDidChange(_:)), for: .editingChanged)
        self.driverNameLabel.text = "\(currentServiceObj?.driver?.firstName ?? "")" + " " +  "\(currentServiceObj?.driver?.lastName ?? "")"
        print()

        self.reviewAvgLabel.text = "\(String(format: "%.2f", currentServiceObj?.driver?.avgRating ?? 0))"
        
        self.giveRatingToLabel.text = "Please leave a rating for " + "\(currentServiceObj?.driver?.firstName ?? "")" + " " +  "\(currentServiceObj?.driver?.lastName ?? "")"
        Utility.setImage(self.currentServiceObj?.driver?.profile, imageView: self.profileImageView)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.tips = Double(textField.text ?? "0.0") ?? 0.0
        self.oneDollerButton.backgroundColor = #colorLiteral(red: 0.7450980392, green: 0.7450980392, blue: 0.7450980392, alpha: 0.5)
        self.threeDollerButton.backgroundColor = #colorLiteral(red: 0.7450980392, green: 0.7450980392, blue: 0.7450980392, alpha: 0.5)
        self.fiveDollerButton.backgroundColor = #colorLiteral(red: 0.7450980392, green: 0.7450980392, blue: 0.7450980392, alpha: 0.5)
    }
    
    private func didFinishTouchingCosmos(_ rating: Double) {
        self.rating = rating
    }
    
    @IBAction func onSubmit(_ sender: UIButton) {
        self.doReview()
//        return
//        let vc = STORYBOARD.payment.instantiateViewController(withIdentifier: "CardListScreen") as! CardListScreen
//        vc.isFromPayment = true
//        vc.onPaymentSelectedButton = { [weak self] paymentId in
//           // self?.cardId = paymentId
//            self?.doReview()
//        }
//        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func onSkip(_ sender: Any) {
        FirebaseRealtimeDB.ref.child("user").child("order_user_\(Utility.getUserData()?.userId ?? 0)").removeValue()
        Utility.setHomeRoot(false, nil)
    }
    
    @IBAction func onTipButton(_ sender: UIButton) {
        self.tipTextField.text = ""
        if sender.tag == 0{
            self.tips = 1
            self.oneDollerButton.backgroundColor = #colorLiteral(red: 0.2549019608, green: 0.9176470588, blue: 0.831372549, alpha: 1)
            self.threeDollerButton.backgroundColor = #colorLiteral(red: 0.01960784314, green: 0.0862745098, blue: 0.1490196078, alpha: 0.2)
            self.fiveDollerButton.backgroundColor = #colorLiteral(red: 0.01960784314, green: 0.0862745098, blue: 0.1490196078, alpha: 0.2)
        }else if sender.tag == 1{
            self.tips = 3
            self.oneDollerButton.backgroundColor = #colorLiteral(red: 0.01960784314, green: 0.0862745098, blue: 0.1490196078, alpha: 0.2)
            self.threeDollerButton.backgroundColor = #colorLiteral(red: 0.2549019608, green: 0.9176470588, blue: 0.831372549, alpha: 1)
            self.fiveDollerButton.backgroundColor = #colorLiteral(red: 0.01960784314, green: 0.0862745098, blue: 0.1490196078, alpha: 0.2)
        }else{
            self.tips = 5
            self.threeDollerButton.backgroundColor = #colorLiteral(red: 0.01960784314, green: 0.0862745098, blue: 0.1490196078, alpha: 0.2)
            self.oneDollerButton.backgroundColor = #colorLiteral(red: 0.01960784314, green: 0.0862745098, blue: 0.1490196078, alpha: 0.2)
            self.fiveDollerButton.backgroundColor = #colorLiteral(red: 0.2549019608, green: 0.9176470588, blue: 0.831372549, alpha: 1)
        }
    }
}
extension ReviewScreen:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView){
        print(textViewDidBeginEditing)
        self.reviewPlaceholderLabel.isHidden = true
    }

    func textViewDidEndEditing(_ textView: UITextView){
        if textView.text == ""{
            self.reviewPlaceholderLabel.isHidden = false
        }
    }

    
    
}
extension ReviewScreen{
    func doReview(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = ReviewRequest(orderId: currentServiceObj?.orderId, driverId: currentServiceObj?.driver?.driverId, rating: rating, feedback: feedbackTextView.text, tip: tips)
            MapService.shared.reviewDriver(parameters: data.toJSON()) { (statusCode, response) in
                Utility.hideIndicator()
                FirebaseanalyticsManager.share.logEvent(event: .addEditVehicle)
                FirebaseRealtimeDB.ref.child("user").child("order_user_\(Utility.getUserData()?.userId ?? 0)").removeValue()
                Utility.setHomeRoot(false,nil)
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
