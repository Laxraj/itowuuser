//
//  ServiceHistoryScreen.swift
//  iTowu
//
//  Created by iroid on 17/10/21.
//

import UIKit
import GooglePlaces
import DropDown

class ServiceHistoryScreen: UIViewController {
    
    @IBOutlet weak var searchHistoryTableView: UITableView!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var inProgressLabel: UILabel!
    @IBOutlet weak var noDataFoundLabel: UILabel!
    @IBOutlet weak var serviceHistoryTitleLabel: UILabel!
    
    
    var orderHistoryData:[FirebaseAcceptedServiceResponse] = []
    
    var page: Int = 1
    var status: Int = 1
    var meta: Meta?
    var hasMorePage:Bool = false
    var isFromSchedule:Bool = false
    var filterOption = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialDetail()
    }
    
    func initialDetail(){
        FirebaseanalyticsManager.share.logEvent(event: .serviceHistoryScreen)
        let nib = UINib(nibName: "ServiceHistoryCell", bundle: nil)
        self.searchHistoryTableView.register(nib, forCellReuseIdentifier: "ServiceHistoryCell")
        if isFromSchedule{
            self.inProgressLabel.text = "Upcoming"
            self.serviceHistoryTitleLabel.text = "SERVICE"
            self.status = historyType.upcoming.rawValue
            self.getService(pageNumber: page, status: status)
        }else{
            self.filterButton.isHidden = false
            self.inProgressLabel.text = "In Progress"
            self.serviceHistoryTitleLabel.text = "SERVICE HISTORY"
            self.status = historyType.inprogress.rawValue
            self.getService(pageNumber: page, status: status)
            self.configFilterButton()
        }
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        if let vc = self.navigationController?.viewControllers.first as? MapScreen{
            vc.observeOrder()
        }
    }
    
    @IBAction func onFilter(_ sender: UIButton) {
        self.filterOption.show()
    }
    
    func configFilterButton() {
        let  filterArray = ["Inprogress","Completed","Canceled"]
     
        filterOption.backgroundColor = UIColor(white: 1, alpha: 1)
        filterOption.selectionBackgroundColor = UIColor(white: 1, alpha: 1)
        filterOption.shadowColor = UIColor(white: 0.6, alpha: 1)
        filterOption.shadowOpacity = 0.9
        filterOption.shadowRadius = 4
        filterOption.cornerRadius = 4
        filterOption.animationduration = 0.25
        filterOption.textColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        filterOption.selectedTextColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        filterOption.anchorView = filterButton
        filterOption.bottomOffset = CGPoint(x: -50, y: 50)
        filterOption.direction = .bottom
        filterOption.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
        filterOption.dataSource = filterArray
        filterOption.dismissMode = .onTap
        filterOption.selectRow(-1)
        filterOption.selectionAction = { [weak self] (index, item) in
            print(item)
            if item == "Inprogress"{
                self?.status = historyType.inprogress.rawValue
                self?.inProgressLabel.text = "Inprogress"
            }else if item == "Upcoming"{
                self?.status = historyType.upcoming.rawValue
                self?.inProgressLabel.text = "Upcoming"
            }else if item == "Completed"{
                self?.status = historyType.completed.rawValue
                self?.inProgressLabel.text = "Completed"
            }else{
                self?.status = historyType.cancel.rawValue
                self?.inProgressLabel.text = "Canceled"
            }
            self?.page = 1
            self?.getService(pageNumber: self?.page ?? 1, status: self?.status ?? 1)
        }

    }
    
    // MARK: scroll method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        let reload_distance:CGFloat = 10.0
        if y > (h + reload_distance) {
            if self.hasMorePage{
                if let metaTotal = self.meta?.total{
                    if self.orderHistoryData.count != metaTotal{
                        print("called")
                        self.hasMorePage = false
                        self.page += 1
                        self.getService(pageNumber: self.page, status: self.status)
                    }
                }
            }
        }
    }   
}
extension ServiceHistoryScreen: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderHistoryData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceHistoryCell", for: indexPath) as! ServiceHistoryCell
        cell.deleteButton.isHidden = status == historyType.upcoming.rawValue ? false:true
        cell.item = orderHistoryData[indexPath.row]
        cell.onDelete = {
            self.cancelServicePrice(orderId: self.orderHistoryData[indexPath.row].orderId ?? 0, latitude:  "\(self.orderHistoryData[indexPath.row].driver?.latitude ?? 0.0)", longitude: "\(self.orderHistoryData[indexPath.row].driver?.longitude ?? 0.0)", indexPath: indexPath.row)
           
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = STORYBOARD.service.instantiateViewController(withIdentifier: "ServiceDetailScreen") as! ServiceDetailScreen
        vc.orderHistoryData = orderHistoryData[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func cancelBookingAlert(orderId:Int,latitude:String,longitude:String,indexPath:Int,price:String){
        let alertController = UIAlertController(title: "Are you sure?", message: isFromSchedule ? "Are you sure you would like to cancel the scheduled service? If the scheduled service is canceled with less than an hour left, a charge of $\(price) will be applied.":"It’s been 5 min since your request,a service charge of $\(price) will be applied if canceled.", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "No, Go Back", style: UIAlertAction.Style.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        let cancelAction = UIAlertAction(title: "Yes, Cancel", style: UIAlertAction.Style.destructive) {
            UIAlertAction in
            self.cancelService(orderId: orderId, latitude: latitude, longitude: longitude)
            self.orderHistoryData.remove(at: indexPath)
            if let vc = homeVC as? MapScreen{
                vc.orderCheckStatus = nil
            }
            self.searchHistoryTableView.reloadData()
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func cancelService(orderId:Int,latitude:String,longitude:String){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
         //   Utility.showIndicator()
            let data = CancelOrderRequest(orderId: orderId, latitude: latitude, longitude: longitude)
            MapService.shared.cancelOrder(parameters: data.toJSON()) { [weak self] (statusCode, response) in
              //  Utility.hideIndicator()
             
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func cancelServicePrice(orderId:Int,latitude:String,longitude:String,indexPath:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            
            let data = CancelOrderPriceRequest(orderId: orderId, latitude: latitude, longitude: longitude)
            MapService.shared.cancelPrice(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let priceData = response.cancelOrderPriceData{
                    self?.cancelBookingAlert(orderId: orderId, latitude: latitude, longitude: longitude, indexPath: indexPath, price: "\(String(format: "%.2f", priceData.amount ?? 0.0))")
                }
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
}

//MARK:- API
extension ServiceHistoryScreen{
    func getService(pageNumber:Int, status:Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
//            let url = serviceListURL + "\(pageNumber)"
            let url = serviceListURL + "\(pageNumber)&status="+"\(status)"
            MapService.shared.completedserviceList(urlString: url) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response.serviceHistoryResponse{
                    self?.orderHistoryData = res
                    if self?.page == 1{
                        self?.orderHistoryData = res
                        if self?.orderHistoryData.count == 0{
                            self?.searchHistoryTableView.reloadData()
                            self?.noDataFoundLabel.isHidden = false
                            return
                        }
                        self?.noDataFoundLabel.isHidden = true
                        self?.searchHistoryTableView.reloadData()
                    }else{
                        self?.appendHistoryDataTableView(data: res)
                    }
                }
               
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
            
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func appendHistoryDataTableView(data: [FirebaseAcceptedServiceResponse]){
        var indexPath : [IndexPath] = []
        for i in self.orderHistoryData.count..<self.orderHistoryData.count+data.count{
            indexPath.append(IndexPath(row: i, section: 0))
        }
        self.orderHistoryData.append(contentsOf: data)
        self.searchHistoryTableView.insertRows(at: indexPath, with: .bottom)
    }
}

