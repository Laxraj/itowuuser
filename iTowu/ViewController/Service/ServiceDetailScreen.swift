//
//  ServiceDetailScreen.swift
//  iTowu
//
//  Created by iroid on 17/10/21.
//

import UIKit
import MapKit
class ServiceDetailScreen: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var pickUpLocationLabel: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var startLocationLabel: UILabel!
    
    @IBOutlet weak var pickUpDateLabel: UILabel!
    @IBOutlet weak var destinationDateLabel: UILabel!
    @IBOutlet weak var startDriverDateLabel: UILabel!
    
    @IBOutlet weak var searviceChargeView: UIView!
    @IBOutlet weak var texesAndChargeView: UIView!
    @IBOutlet weak var fuleView: UIView!
    
    @IBOutlet weak var serviceChargeLabel: UILabel!
    @IBOutlet weak var texesLabel: UILabel!
    @IBOutlet weak var fuleLabel: UILabel!
    @IBOutlet weak var serviceTitleLabel: UILabel!
    @IBOutlet weak var tipLabel: UILabel!
    
    @IBOutlet weak var totalCostLabel: UILabel!
    
    @IBOutlet weak var pickupLocationLabel: UILabel!
    
    @IBOutlet weak var destinationLocationView: UIView!
    @IBOutlet weak var startDriverView: UIView!
    @IBOutlet weak var tipView: UIView!
    
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var filterPriceLabel: UILabel!
    @IBOutlet weak var StabiliserPriceLabel: UILabel!
    
    @IBOutlet weak var serviceChargeDiscountLabel: UILabel!
    @IBOutlet weak var fuelPriceDiscountLabel: UILabel!
    @IBOutlet weak var bookingFeesDiscountLabel: UILabel!
    @IBOutlet weak var totalDiscountLabel: UILabel!
    
    var orderHistoryData:FirebaseAcceptedServiceResponse?
    
    //MARK: Variable
    var locationManager: CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialDetail()
    }
    
    func initialDetail(){
        self.mapView.layer.cornerRadius = 14
        self.mapView.layer.borderColor = #colorLiteral(red: 0.7176470588, green: 0.7176470588, blue: 0.7176470588, alpha: 1)
        self.mapView.layer.borderWidth = 1
        
        
        self.pickUpLocationLabel.text = orderHistoryData?.pickupAddress
        self.destinationLabel.text = orderHistoryData?.destination_address
        self.startLocationLabel.text = orderHistoryData?.driverStartAddress
        self.startLocationLabel.superview?.isHidden = orderHistoryData?.driverStartAddress == nil

        
        self.pickUpDateLabel.text = Utility.serviceDate(inerval: orderHistoryData?.driverOnPickupLocation ?? 0)
        self.destinationDateLabel.text = Utility.serviceDate(inerval: orderHistoryData?.driverOnDestinationLocation ?? 0)
        self.startDriverDateLabel.text = Utility.serviceDate(inerval: orderHistoryData?.driverStartedAt ?? 0)
        
        self.destinationLocationView.isHidden = orderHistoryData?.service?.type == 1
        
        self.pickupLocationLabel.text = orderHistoryData?.service?.type == 1 ? "Destination Location":"Pickup Location"
        let serviceCharge = String(format: "$%.2f", orderHistoryData?.serviceCharge ?? 0.0)

        self.serviceChargeLabel.text = serviceCharge
        
        let productPrice = self.orderHistoryData?.oilProduct == 0 ? self.orderHistoryData?.breakProduct : self.orderHistoryData?.oilProduct
        self.productTitleLabel.text = self.orderHistoryData?.oilProduct == 0 ? "Brake product" : "Oil product"
        
        self.productPriceLabel.text = String(format: "$%.2f", productPrice ?? 0.0)
        
        self.filterPriceLabel.text = String(format: "$%.2f", self.orderHistoryData?.filterPrice ?? 0.0)
        
        self.StabiliserPriceLabel.text = String(format: "$%.2f", self.orderHistoryData?.stabilizerPrice ?? 0.0)
        
        self.filterPriceLabel.superview?.isHidden = self.orderHistoryData?.filterPrice == 0 ? true : false
        
        self.StabiliserPriceLabel.superview?.isHidden = self.orderHistoryData?.stabilizerPrice == 0 ? true : false
        
        self.productPriceLabel.superview?.isHidden = self.orderHistoryData?.oilProduct == 0 && self.orderHistoryData?.breakProduct == 0 ? true : false
        
        self.totalCostLabel.text =  "$"+String(format: "%.2f", orderHistoryData?.price ?? 0)
        
//        self.totalCostLabel.text = "$"+"\(orderHistoryData?.price ?? 0.0)"
        let serviceTitle = "\(orderHistoryData?.service?.name ?? "") service"
        self.serviceTitleLabel.text = serviceTitle.uppercased()
        
        
        if orderHistoryData?.tip != 0{
            self.tipView.isHidden = false
            self.tipLabel.text = String(format: "$%.2f", self.orderHistoryData?.tip ?? 0.0)

        }else{
            self.tipView.isHidden = true
        }
        
        self.totalCostLabel.text = String(format: "$%.2f", (orderHistoryData?.price ?? 0) - (orderHistoryData?.subDiscountPrice ?? 0))
        
        if orderHistoryData?.subDiscountPrice != 0 || orderHistoryData?.subDiscountPrice == nil{
            self.totalDiscountLabel.superview?.isHidden = false
            self.totalDiscountLabel.text = "$\(String(format:"%.02f",orderHistoryData?.price ?? 0))"
        }
        
        self.serviceChargeLabel.text = String(format: "$%.2f", (orderHistoryData?.serviceCharge ?? 0) - (orderHistoryData?.serviceChargeDiscount ?? 0))

        
        if orderHistoryData?.serviceChargeDiscount != 0 || orderHistoryData?.serviceChargeDiscount == nil{
            self.serviceChargeDiscountLabel.superview?.isHidden = false
            self.serviceChargeDiscountLabel.text = "$\(String(format:"%.02f",orderHistoryData?.serviceCharge ?? 0))"
        }
        
        let totalTaxes = ((orderHistoryData?.stripeTax ?? 0.0) + (orderHistoryData?.towWinchPrice ?? 0.0) + (orderHistoryData?.bookingFees ?? 0.0))
         
         let totalTaxeString = String(format: "$%.2f", (totalTaxes) - (orderHistoryData?.bookingFeesDiscount ?? 0) )

         self.texesLabel.text = totalTaxeString
        
        if orderHistoryData?.bookingFeesDiscount != 0 || orderHistoryData?.bookingFeesDiscount == nil{
            self.bookingFeesDiscountLabel.superview?.isHidden = false
            self.bookingFeesDiscountLabel.text = "$\(String(format:"%.02f",totalTaxes))"
        }
        
        if orderHistoryData?.fuleType != ""{
            self.fuleView.isHidden = false
            self.fuleLabel.text = String(format: "$%.2f", (orderHistoryData?.fulePrice ?? 0) - (orderHistoryData?.fuelPriceDiscount ?? 0))
        }else{
            self.fuleView.isHidden = true
        }
        
        if orderHistoryData?.fuelPriceDiscount != 0 || orderHistoryData?.fuelPriceDiscount == nil{
            self.fuelPriceDiscountLabel.superview?.isHidden = false
            self.fuelPriceDiscountLabel.text = "$\(String(format:"%.02f",orderHistoryData?.fulePrice ?? 0))"
        }
        
        
        
        self.mapViewSetup()
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- removeRoute
    func removeRoute(){
        let overlays = mapView.overlays
        self.mapView.removeAnnotations(self.mapView.annotations)
        mapView.removeOverlays(overlays)
    }
    
    // MARK: MapView Setup
    func mapViewSetup(){
        removeRoute()
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.showsUserLocation = false
        
        if let coor = mapView.userLocation.location?.coordinate{
            mapView.setCenter(coor, animated: true)
        }
        
        let fromLat = Double(orderHistoryData?.pickupLatitude ?? 0)
        let fromLong = Double(orderHistoryData?.pickupLongitude ?? 0)
        
        let toLat = Double(orderHistoryData?.driver?.latitude ?? 0)
        let toLong = Double(orderHistoryData?.driver?.longitude ?? 0)
    
        
        if let annotations = self.mapView?.annotations {
            for _annotation in annotations {
                if let annotation = _annotation as? MKAnnotation
                {
                      self.mapView.removeAnnotation(annotation)
                }
            }
        }
        
        let newPin1 = MKPointAnnotation()
        
        let location1 = CLLocationCoordinate2D(latitude: fromLat , longitude: fromLong )
        newPin1.coordinate = location1
        
        let pin1 = CustomPin(pinTitle: orderHistoryData?.user?.firstName ?? "", Location: location1, storeImage: orderHistoryData?.user?.profile ?? "", pinId: orderHistoryData?.user?.userId ?? 0)
        self.mapView.addAnnotation(pin1)
        
        let newPin2 = MKPointAnnotation()
        let location2 = CLLocationCoordinate2D(latitude: toLat , longitude: toLong )
        newPin2.coordinate = location2
        let pin2 = CustomPin(pinTitle: orderHistoryData?.driver?.firstName ?? "", Location: location2, storeImage: orderHistoryData?.driver?.profile ?? "", pinId: orderHistoryData?.driver?.driverId ?? 0)
        self.mapView.addAnnotation(pin2)
       
        self.mapView.showAnnotations(self.mapView.annotations, animated: true)
        let myLocation = CLLocation(latitude: location1.latitude, longitude: location1.longitude)
        self.requestETA(userCLLocation: myLocation, coordinate: location2)
//        mapView.fitAllAnnotations()
    }
}
//MARK: MKMapViewDelegate and CLLocationManagerDelegate Delegate Methods
extension ServiceDetailScreen: CLLocationManagerDelegate, MKMapViewDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //        if let location = locations.last{
        //            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        //            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        //            self.mapView.setRegion(region, animated: true)
        //        }
    }
    
    // MARK: Set Pin On MapView
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        if annotation is MKUserLocation {
            return nil
        }
        let reuseID = "Location"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseID)
            annotationView?.canShowCallout = true
            let pin = MKAnnotationView(annotation: annotation,
                                       reuseIdentifier: reuseID)
            
            let pinData = annotation as? CustomPin
            
            if pinData?.pinId == -1{
                pin.image = UIImage(named: "currant_location_pin")
            }else{
                pin.isEnabled = true
                pin.canShowCallout = true
                pin.image = UIImage(named: "driver_map_pin")
                let storeImageView = UIImageView()
                storeImageView.frame = CGRect(x: 6.5, y: 7, width: 27, height: 27)
                storeImageView.layer.cornerRadius = storeImageView.frame.height/2
                storeImageView.layer.masksToBounds = true
//                storeImageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//                storeImageView.layer.borderWidth = 2
                
                let label = UILabel(frame: CGRect(x: pin.center.x, y: pin.center.y, width: 25, height: 25))
                label.textAlignment = .center
                label.textColor = .white
                label.font = label.font.withSize(14)
               
                //            label.text = cpa.pinId
                Utility.setImage(pinData?.storeImage, imageView: storeImageView)
                pin.addSubview(storeImageView)
                
            }
            annotationView = pin
            annotationView?.annotation = annotation
        } else {
            let pin = MKAnnotationView(annotation: annotation,
                                       reuseIdentifier: reuseID)
            let pinData = annotation as? CustomPin
            
            if pinData?.pinId == -1{
                pin.image = UIImage(named: "currant_location_pin")
            }else{
                pin.isEnabled = true
                pin.canShowCallout = true
                pin.image = UIImage(named: "driver_map_pin")
                let storeImageView = UIImageView()
                storeImageView.frame = CGRect(x: 6.5, y: 7, width: 27, height: 27)
                storeImageView.layer.cornerRadius = storeImageView.frame.height/2
                storeImageView.layer.masksToBounds = true
//                storeImageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//                storeImageView.layer.borderWidth = 2
                
                let label = UILabel(frame: CGRect(x: pin.center.x, y: pin.center.y, width: 25, height: 25))
                label.textAlignment = .center
                label.textColor = .white
                label.font = label.font.withSize(14)
               
                //            label.text = cpa.pinId
                Utility.setImage(pinData?.storeImage, imageView: storeImageView)
                pin.addSubview(storeImageView)
                
            }
            annotationView = pin
            annotationView?.annotation = annotation
        }
        return annotationView
    }
    
    
    func requestETA(userCLLocation: CLLocation, coordinate: CLLocationCoordinate2D) {
        let request = MKDirections.Request()
        
        let mapItem1 = MKMapItem(placemark: MKPlacemark(coordinate: userCLLocation.coordinate))
        let mapItem2 = MKMapItem(placemark: MKPlacemark(coordinate: coordinate))
        
        request.source = mapItem1
        request.destination = mapItem2
        let directions = MKDirections(request: request)
        
        var travelTime: String?
        directions.calculate { response, error in
            if let route = response?.routes.first {
                travelTime = String(route.expectedTravelTime/60)
                self.mapView.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)
            }
        }
    }
    
//    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay)
//        -> MKOverlayRenderer {
//            let renderer = MKPolylineRenderer(overlay: overlay)
//            renderer.strokeColor = UIColor.blue
//            renderer.lineWidth = 5.0
//
//            return renderer
//    }
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        renderer.lineWidth = 5.0
        return renderer
    }
}
