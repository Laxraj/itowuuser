//
//  HelpScreen.swift
//  iTowuDriver
//
//  Created by Nikunj on 06/05/22.
//

import UIKit
import MessageUI

class HelpScreen: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.sideMenuController?.isLeftViewSwipeGestureDisabled = true
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.sideMenuController?.isLeftViewSwipeGestureDisabled = false
    }
    
    
    @IBAction func onBack(_ sender: Any) {
        if let vc = self.navigationController?.viewControllers.first as? MapScreen{
            vc.observeOrder()
        }
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func onFaq(_ sender: Any) {
        let vc = STORYBOARD.webView.instantiateViewController(withIdentifier: "WebViewScreen") as! WebViewScreen
        vc.linkUrl =  faqUsURL
        vc.titlaString = "FAQ"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onPrivacyPolicy(_ sender: Any) {
        self.sideMenuController?.hideLeftView()
        let vc = STORYBOARD.webView.instantiateViewController(withIdentifier: "WebViewScreen") as! WebViewScreen
        vc.linkUrl = privacyPolicyURL
        vc.titlaString = "PRIVACY POLICY"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onAboutUs(_ sender: Any) {
        let vc = STORYBOARD.webView.instantiateViewController(withIdentifier: "WebViewScreen") as! WebViewScreen
        vc.linkUrl =  aboutUsURL
        vc.titlaString = "ABOUT US"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onContactUs(_ sender: Any) {
        self.openMail()
    }
    
    func openMail(){
        let emailTitle = ""
        let messageBody = ""
        let toRecipents = ["contact@itowu.com"]
        let mc: MFMailComposeViewController = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        mc.setSubject(emailTitle)
        mc.setMessageBody(messageBody, isHTML: false)
        mc.setToRecipients(toRecipents)
        self.navigationController?.present(mc, animated: true, completion: nil)
    }
}
extension HelpScreen:MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        //        switch result {
        //        case res:
        //            print("Mail cancelled")
        //        case MFMailComposeResultSaved:
        //            print("Mail saved")
        //        case MFMailComposeResultSent:
        //            print("Mail sent")
        //        case MFMailComposeResultFailed:
        //            print("Mail sent failure: \(error?.localizedDescription)")
        //        default:
        //            break
        //        }
        self.dismiss(animated: true, completion: nil)
    }
}
