//
//  PramocodeScreen.swift
//  iTowu
//
//  Created by iroid on 19/10/21.
//

import UIKit

class PromoCodeScreen: UIViewController {
    
    @IBOutlet weak var promocodeTableView: UITableView!
    
    var itemArray: [CouponListResponse] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialDetail()
    }
    
    func initialDetail(){
        FirebaseanalyticsManager.share.logEvent(event: .promoCodeScreen)

        self.promocodeTableView.register(UINib(nibName: "PramocodeCell", bundle: Bundle.main), forCellReuseIdentifier: "PramocodeCell")
        
        self.getPromoCodeList()
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        if let vc = self.navigationController?.viewControllers.first as? MapScreen{
            vc.observeOrder()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK:- TABLEVIEW DELEGATE & DATASOURCE
extension PromoCodeScreen: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.promocodeTableView.dequeueReusableCell(withIdentifier: "PramocodeCell", for: indexPath) as! PramocodeCell
        cell.item = self.itemArray[indexPath.row]
        cell.onCopyPromocode = { [weak self] in
            guard let strongSelf = self else {
                return
            }
            guard let code = self?.itemArray[indexPath.row].code else {
                return
            }
            UIPasteboard.general.string = code
            Utility.showAlert(vc: strongSelf, message: "Copied Successfully")
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
}
//MARK:- API
extension PromoCodeScreen{
    
    //MARK:- PROMOCODE LIST API
    func getPromoCodeList(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            VehicleService.shared.getCoupon{ (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self.itemArray = res
                }
                self.promocodeTableView.reloadData()
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
