//
//  MainScreen.swift
//  iTowu
//
//  Created by Nikunj on 14/10/21.
//

import UIKit

class MainScreen: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var itemArray: [OnBoardRequest] = []
    var timer: Timer?
    var indexPath: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.initializeDetails()
        // Do any additional setup after loading the view.
    }
    
    func initializeDetails(){
        self.manageArray()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "OnBoardCell", bundle: Bundle.main), forCellWithReuseIdentifier: "OnBoardCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.startTimer()
    }

    func manageArray(){
        self.itemArray.append(OnBoardRequest(image: UIImage(named: "onboard_first_icon"), description: "Help is closer than you think."))
        self.itemArray.append(OnBoardRequest(image: UIImage(named: "onboard_second_icon"), description: "Peace of mind."))
        self.itemArray.append(OnBoardRequest(image: UIImage(named: "onboard_third_icon"), description: "Built in safety."))
        self.itemArray.append(OnBoardRequest(image: UIImage(named: "onboard_fourth_icon"), description: "Relax & leave the work to us."))
        self.pageControl.numberOfPages = self.itemArray.count
    }
    
    @IBAction func onSignIn(_ sender: UIButton) {
        let vc = STORYBOARD.authentication.instantiateViewController(withIdentifier: "LoginScreen") as! LoginScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onCreateAccount(_ sender: UIButton) {
        let vc = STORYBOARD.authentication.instantiateViewController(withIdentifier: "RegisterScreen") as! RegisterScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func startTimer() {
        self.timer =  Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
     }
    
    func stopTimer(){
        self.timer?.invalidate()
        self.timer = nil
    }
    
    @objc func scrollAutomatically(){
        if self.indexPath > self.itemArray.count - 1{
            self.indexPath = 0
            self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: false)
            self.pageControl.currentPage = self.indexPath
        }else{
            self.collectionView.scrollToItem(at: IndexPath(row: self.indexPath, section: 0), at: .centeredHorizontally, animated: true)
            self.pageControl.currentPage = self.indexPath
            self.indexPath += 1
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.stopTimer()
    }
    
}
//MARK:- COLLECTIONVIEW DELEGATE & DATASOURCE
extension MainScreen: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.itemArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "OnBoardCell", for: indexPath) as! OnBoardCell
        cell.item = self.itemArray[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: screenWidth, height: self.collectionView.frame.size.height)
    }
}
//MARK:- SCROLLVIEW DELEGATE
extension MainScreen{
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.indexPath = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        self.pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.indexPath = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        self.pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.indexPath = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        self.pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.indexPath = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        self.pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        self.indexPath = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        self.pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
}
