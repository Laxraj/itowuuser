//
//  ChangeRegisteredPhoneScreen.swift
//  iTowu
//
//  Created by Nikunj Vaghela on 18/04/24.
//

import UIKit
import GCCountryPicker

class ChangeRegisteredPhoneScreen: UIViewController {
    
//MARK: - OUTLET
    @IBOutlet weak var countryCodeTextField: UITextField!
    @IBOutlet weak var phoneTextField: dateSportTextField!
    
    @IBOutlet weak var emailTextField: dateSportTextField!
    

    var phoneNum:String?
    var countryCode:String?
    var email: String?
    var phoneNumberChanged:(() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        // Do any additional setup after loading the view.
    }
    
    func setData(){
        if let phone = self.phoneNum,let code = self.countryCode{
            self.countryCodeTextField.text = code
            self.phoneTextField.text = phone
        }
        if let email = self.email{
            self.emailTextField.text = email
            self.emailTextField.superview?.isHidden = false
        }else{
            self.emailTextField.superview?.isHidden = true
        }
    }

    func checkValidation() -> String?{
        if self.phoneTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter phone number"
        }else if self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter email"
        }else if self.emailTextField.text?.isValidEmail() == false{
            return "Please enter valid email"
        }
        return nil
    }
    
    func changeMobileAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = ChangePhoneNumberRequest(userId: Utility.getUserData()?.userId, countryCode: self.countryCodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), phone: self.phoneTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),email: self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))
            AuthenticationService.shared.changeRegisterPhone(parameter: data.toJSON()) { [weak self] statusCode, response in
                if let res = response{
                    Utility.saveUserData(data: res.toJSON())
                    self?.dismiss(animated: false){
                        self?.phoneNumberChanged?()
                    }
                }
                Utility.hideIndicator()
            } failure: { [weak self] error in
                guard let strongSelf = self else {
                    return
                }
                Utility.hideIndicator()
                Utility.showAlert(vc: strongSelf, message: error)
            }
        }else{
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK: - ACTIONS
    @IBAction func onClose(_ sender: Any) {
        self.dismiss(animated: false)
    }
    
    @IBAction func onChange(_ sender: Any) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            self.changeMobileAPI()
        }
    }
    
    
    @IBAction func onCountryCode(_ sender: UIButton) {
        let countryPickerViewController = GCCountryPickerViewController(displayMode: .withCallingCodes)
        
        countryPickerViewController.delegate = self
        countryPickerViewController.navigationItem.title = "Countries"
        
        let navigationController = UINavigationController(rootViewController: countryPickerViewController)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
}
//MARK: - GCCountryPicker Delegate & DataSource
extension ChangeRegisteredPhoneScreen: GCCountryPickerDelegate{
    
    func countryPickerDidCancel(_ countryPicker: GCCountryPickerViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func countryPicker(_ countryPicker: GCCountryPickerViewController, didSelectCountry country: GCCountry) {
        self.countryCodeTextField.text = country.callingCode
        self.dismiss(animated: true, completion: nil)
    }
}
