//
//  ChangeEmailScreen.swift
//  iTowu
//
//  Created by iMac on 26/01/23.
//

import UIKit
import GCCountryPicker

class ChangeEmailScreen: UIViewController {

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var emailView: UIView!
    
    @IBOutlet weak var phoneNumberTextField: dateSportTextField!
    @IBOutlet weak var countryCodeTextField: UITextField!

    var type = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initialDetail()
    }
    
    func initialDetail(){
        if type == 1{
            self.emailLabel.text = Utility.getUserData()?.email
            self.phoneView.isHidden = true
        }else{
            self.emailLabel.text = "\(Utility.getUserData()?.countryCode ?? "") \(Utility.getUserData()?.phone ?? "")"
            self.titleLabel.text = "CHANGE PHONE"
            self.emailView.isHidden = true
        }
        self.setupCallingCode()
    }
    
    private func setupCallingCode() {
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String, let code = GCCountry(countryCode:countryCode)?.callingCode {
            self.countryCodeTextField.text = code
        }
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onCountryCode(_ sender: UIButton) {
        let countryPickerViewController = GCCountryPickerViewController(displayMode: .withCallingCodes)
        
        countryPickerViewController.delegate = self
        countryPickerViewController.navigationItem.title = "Countries"
        
        let navigationController = UINavigationController(rootViewController: countryPickerViewController)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func onChange(_ sender: UIButton) {
        if let error = checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            if self.type == 1{
                self.changeEmailAPI()
            }else{
                self.changePhoneNumberAPI()
            }
        }
    }
    
    func checkValidation() -> String?{
        if type == 2{
            if self.phoneNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
                return "Please enter phone number"
            }else if self.phoneNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 < 8 {
                return "Please enter valid phone number"
            }
        }else{
            if self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
                return "Please enter email"
            }else if !emailTextField.text.isEmailValid(){
                return "Please enter valid email address"
            }
        }
        return nil
    }
    
}
//MARK:- GCCountryPicker Delegate & DataSource
extension ChangeEmailScreen: GCCountryPickerDelegate{
    
    func countryPickerDidCancel(_ countryPicker: GCCountryPickerViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func countryPicker(_ countryPicker: GCCountryPickerViewController, didSelectCountry country: GCCountry) {
        self.countryCodeTextField.text = country.callingCode
        self.dismiss(animated: true, completion: nil)
    }
}
extension ChangeEmailScreen{
    //MARK:- CHANGE EMAIL  API
    func changeEmailAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data =  ChangeEmailRequest(email: self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))
            AuthenticationService.shared.changeEmail(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
//                if let res = response{
                    print(response.toJSON())
                let control = STORYBOARD.authentication.instantiateViewController(withIdentifier: "EnterVerificationCodeScreen") as! EnterVerificationCodeScreen
                control.type = 1
                control.phoneNum = nil
                control.email = self?.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                control.countryCode = nil
                control.isFromProfileScreen = true
                self?.navigationController?.pushViewController(control, animated: true)
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func changePhoneNumberAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data =  ChangePhoneNumberRequest(countryCode: self.countryCodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), phone: self.phoneNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))
            AuthenticationService.shared.changePhoneNumber(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
//                if let res = response{
                    print(response.toJSON())
                let control = STORYBOARD.authentication.instantiateViewController(withIdentifier: "EnterVerificationCodeScreen") as! EnterVerificationCodeScreen
                control.type = 2
                control.phoneNum = self?.phoneNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                control.email = nil
                control.countryCode = self?.countryCodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                control.isFromProfileScreen = true
                self?.navigationController?.pushViewController(control, animated: true)
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
