//
//  LoginScreen.swift
//  iTowu
//
//  Created by iroid on 15/10/21.
//

import UIKit
import GCCountryPicker
import GoogleSignIn

class LoginScreen: UIViewController {
    
    
    @IBOutlet weak var loginOptionView: UIView!
    @IBOutlet weak var loginOptionTextField: UITextField!
    @IBOutlet weak var loginOptionViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var passwordTextField: dateSportTextField!
    @IBOutlet weak var phoneNumberTextField: dateSportTextField!
    @IBOutlet weak var CountryCodeTextField: UITextField!
    
    @IBOutlet weak var numberView: dateSportView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var countryCodeView: dateSportView!
    @IBOutlet weak var emailTextField: dateSportTextField!
    
    @IBOutlet weak var eyeButton: UIButton!
    
    var type = 2
    var faceIdUser = [String:Any]()
    var email:String?
    var password:String?
    var countryCode:String?
    var phone:String?
       
    var loginResponse:LoginResponse?
    var isFaceAdded:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.eyeButton.setImage(UIImage(named: "eye_close_icon"), for: .normal)
        self.eyeButton.setImage(UIImage(named: "eye_show_icon"), for: .selected)
        self.setupCallingCode()
    }
    
    private func setupCallingCode() {
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String, let code = GCCountry(countryCode:countryCode)?.callingCode {
            CountryCodeTextField.text = code
        }
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onLoginOption(_ sender: UIButton) {
        self.loginOptionView.isHidden = false
    }
    
    @IBAction func onEmailAddress(_ sender: UIButton) {
        self.type = 1
        self.loginOptionView.isHidden = true
        self.setOptionView(isSelectEmail: true)
    }
    
    @IBAction func onPhoneNumber(_ sender: UIButton) {
        self.type = 2
        self.loginOptionView.isHidden = true
        self.setOptionView(isSelectEmail: false)
    }
    
    @IBAction func onForgotPassword(_ sender: UIButton) {
        let vc = STORYBOARD.authentication.instantiateViewController(withIdentifier: "ForgetPassswordScreen") as! ForgetPassswordScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onSignIn(_ sender: UIButton) {
//        Utility.openMapScreen()
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            self.loginAPI(type: type, email: self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "", phoneNum:  self.phoneNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "", password:  self.passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "", countryCode: self.CountryCodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
        }
    }
    
    @IBAction func onEyeHide(_ sender: Any) {
        if self.passwordTextField.isSecureTextEntry == true{
            self.passwordTextField.isSecureTextEntry = false
            self.eyeButton.isSelected = true
        }else{
            self.passwordTextField.isSecureTextEntry = true
            self.eyeButton.isSelected = false
        }
    }
    
    @IBAction func onGoogleLogin(_ sender: UIButton) {
        let loginManager = GoogleLoginManager()
        loginManager.handleGoogleLoginButtonTap(viewController: self)
        loginManager.delegate = self
    }
    
    @IBAction func onAppleLogin(_ sender: UIButton) {
        if #available(iOS 13.0, *) {
            let loginManager = AppleLoginManager()
            loginManager.mainView = self.view
            loginManager.handleAuthorizationAppleIDButtonPress()
            loginManager.delegate = self
        } else {
            Utility.showAlert(vc: self, message: "Apple login not supported below iOS 13 OS.")
        }
    }
    
    @IBAction func onFaceLock(_ sender: UIButton) {
        let authType = LocalAuthManager.shared.biometricType
        switch authType {
        case .none:
            self.isFaceAdded = false
            Utility.showAlert(vc: self, message: "Seems like you haven't added FaceID in your OS setting. Please goto your OS settings and add your FaceID to use this feature")
            //self.faceIDPopupLabel.text = "Seems like you haven't added FaceID in your OS setting. Please goto your OS settings and add your FaceID to use this feature"
            return
        case .touchID:
            print("Device support TouchID")
        case .faceID:
            if biometricType() == BiometricType.face{
                if Utility.getFaceID()?.faceID == true{
                    self.faceID()
                }else{
                    self.isFaceAdded = false
                    Utility.showAlert(vc: self, message: "You haven't setup FaceID in the app. Please login in the app to set it up.")
                }
            }
        }
    }
    
    func faceID(){
        Utility.BiometricsLock { (success) in
            //if FaceID Match
            if success == true{
              //  self.faceIDView.isHidden = true
                if Utility.getFaceID()?.faceID != true{
                    self.faceIdUser["faceID"] = true
                    self.faceIdUser["email"] = self.email
                    self.faceIdUser["password"] = self.password
                    self.faceIdUser["type"] = self.type
                    self.faceIdUser["countryCode"] = self.countryCode
                    self.faceIdUser["phone"] = self.phone
                   
                    Utility.saveFaceID(data: self.faceIdUser)
                    Utility.saveUserData(data: self.loginResponse?.toJSON() ?? [:])
                    Utility.setHomeRoot(false,nil)
                }else{
                    let getFaceLockData = Utility.getFaceID()
                    self.loginAPI(type: getFaceLockData?.type ?? 0, email: getFaceLockData?.email ?? "", phoneNum: getFaceLockData?.phone ?? "", password: getFaceLockData?.password ?? "", countryCode: getFaceLockData?.countryCode ?? "")
                    Utility.saveUserData(data: self.loginResponse?.toJSON() ?? [:])
                }
            }else{
            }
        }
    }
    
    func checkValidation() -> String?{
        if self.CountryCodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 && self.type == 2{
            return "Please select country code"
        }else if self.phoneNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 && self.type == 2{
            return "Please enter phone number"
        }else if self.phoneNumberTextField.text?.isValidPhoneNumber() == false && self.type == 2{
            return "Please enter valid phone"
        }else if self.passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 && self.type == 2{
            return "Please enter password"
        }else if self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 && self.type == 1{
            return "Please enter email"
        }else if self.emailTextField.text?.isValidEmail() == false && self.type == 1{
            return "Please enter valid email"
        }
        return nil
    }
    
    @IBAction func onCountryCode(_ sender: UIButton) {
        let countryPickerViewController = GCCountryPickerViewController(displayMode: .withCallingCodes)
        countryPickerViewController.delegate = self
        countryPickerViewController.navigationItem.title = "Countries"
        let navigationController = UINavigationController(rootViewController: countryPickerViewController)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func setOptionView(isSelectEmail:Bool){
        self.loginOptionTextField.text = isSelectEmail ? "EMAIL":"PHONE NUMBER"
        self.emailView.isHidden = !isSelectEmail
        self.countryCodeView.isHidden = isSelectEmail
        self.numberView.isHidden = isSelectEmail
    }
    
    
    func showFaceLockAlert(message:String) {
        let alert = UIAlertController(title: APPLICATION_NAME, message: "You can always access your content by signing back in",         preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
            if self.isFaceAdded{
                self.faceID()
            }
           
        }))
        alert.addAction(UIAlertAction(title: "No",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        //Sign out action
                                        Utility.saveUserData(data: self.loginResponse?.toJSON() ?? [:])
                                        Utility.setHomeRoot(false,nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}
//MARK:- Google Login Delegate
extension LoginScreen: googleLoginManagerDelegate{
    func onGoogleLoginSuccess(user: GIDGoogleUser) {
        let  data = RegisterWithSocialRequest(firstName: user.profile?.givenName ?? "", lastName: user.profile?.familyName ?? "", countryCode: nil, phone: nil, email: user.profile?.email, providerType: GOOGLE, providerId: user.userID ?? "")
        self.socialLogin(data: data.toJSON())
    }
    
    func onGoogleLoginFailure(error: NSError) {
        Utility.showAlert(vc: self, message: error.localizedDescription)
    }
}
//MARK:- Apple login Delegate
extension LoginScreen: AppleLoginManagerDelegate{
    func onSuccess(result: NSDictionary) {
        print(result)
        let  data =  RegisterWithSocialRequest(firstName: (result.value(forKey: USERNAME) as? String) ?? "", lastName: (result.value(forKey: LAST_NAME) as? String) ?? "", countryCode: nil, phone: nil, email: (result.value(forKey: "email") as? String) ?? "", providerType: APPLE, providerId: (result.value(forKey: "userid") as? String) ?? "")
        self.socialLogin(data: data.toJSON())
    }
    
    func onFailure(error: NSError) {
        Utility.showAlert(vc: self, message: error.localizedDescription)
    }
}
//MARK:- GCCountryPicker Delegate & DataSource
extension LoginScreen: GCCountryPickerDelegate{
    
    func countryPickerDidCancel(_ countryPicker: GCCountryPickerViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func countryPicker(_ countryPicker: GCCountryPickerViewController, didSelectCountry country: GCCountry) {
        self.CountryCodeTextField.text = country.callingCode
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- API
extension LoginScreen{
    
    func loginAPI(type:Int,email:String,phoneNum:String,password:String,countryCode:String){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = LoginRequest(type: type, countryCode: type == 1 ? nil : countryCode, phone: type == 1 ? nil :phoneNum, email: type == 2 ? nil : email, password:password)
            AuthenticationService.shared.login(parameters: data.toJSON()) { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    FirebaseanalyticsManager.share.logEvent(event: .loginSuccessfully)
                    self.loginResponse = res
                    Utility.saveUserData(data: res.toJSON())
                    
                    if res.auth != nil{
                        appDelegate.registerForPush()
                        appDelegate.connectSocket()
                    }
                   
                    
                    
                    if biometricType() == BiometricType.face{
                        let authType = LocalAuthManager.shared.biometricType
                        switch authType {
                        case .none:
                            Utility.saveUserData(data: self.loginResponse?.toJSON() ?? [:])
//                            Utility.setHomeRoot(false,nil)
                            
                            if self.loginResponse?.verifiedAt != nil{
                                self.checkOrderStatus()
                            }else{
                                let control = STORYBOARD.authentication.instantiateViewController(withIdentifier: "EnterVerificationCodeScreen") as! EnterVerificationCodeScreen
                                control.type = type
                                control.phoneNum = res.phone //type == 1 ? nil :phoneNum
                                control.email = Utility.getUserData()?.email
                                control.countryCode = res.countryCode //type == 1 ? nil :countryCode
                                self.navigationController?.pushViewController(control, animated: true)
                            }
                            
                        case .touchID:
                            print("Device support TouchID")
                        case .faceID:
                            if Utility.getFaceID()?.faceID != true{
                                self.showFaceLockAlert(message: "Your device supports the FaceID. Would you like to use your FaceID to make the all next login process smoother and faster?")
                                self.isFaceAdded = true
                                self.email = email
                                self.password = password
                                self.type = type
                                self.countryCode = countryCode
                                self.phone = phoneNum
                            }else{
                                self.faceIdUser["faceID"] = true
                                self.faceIdUser["email"] = self.email
                                self.faceIdUser["password"] = self.password
                                self.faceIdUser["type"] = self.type
                                self.faceIdUser["countryCode"] = self.countryCode
                                self.faceIdUser["phone"] = self.phone
                                Utility.saveFaceID(data: self.faceIdUser)
                                Utility.saveUserData(data: self.loginResponse?.toJSON() ?? [:])
//                                Utility.setHomeRoot(false,nil)
//                                self.checkOrderStatus()
                                if self.loginResponse?.verifiedAt != nil{
                                    self.checkOrderStatus()
                                }else{
                                    let control = STORYBOARD.authentication.instantiateViewController(withIdentifier: "EnterVerificationCodeScreen") as! EnterVerificationCodeScreen
//                                    control.type = type
//                                    control.phoneNum = type == 1 ? nil :phoneNum
//                                    control.email = type == 2 ? nil :email
//                                    control.countryCode = type == 1 ? nil :countryCode
                                    control.type = type
                                    control.phoneNum = res.phone //type == 1 ? nil :phoneNum
                                    control.email = Utility.getUserData()?.email
                                    control.countryCode = res.countryCode //type == 1 ? nil :countryCode
                                    self.navigationController?.pushViewController(control, animated: true)
                                }

                               // self.faceIDView.isHidden = true
                            }
 
                        }
                    }else{
                        Utility.saveUserData(data: self.loginResponse?.toJSON() ?? [:])
//                        Utility.setHomeRoot(false,nil)
                        self.checkOrderStatus()
                        //self.faceIDView.isHidden = true
                    }
                    
                }
               
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }

        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- SOCIAL LOGIN
    func socialLogin(data: [String:Any]){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            print(data)
            AuthenticationService.shared.socialLogin(parameters: data) { [weak self] (statusCode, response) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                
                FirebaseanalyticsManager.share.logEvent(event: .loginSuccessfully)
                
                if let res = response{
                    Utility.saveUserData(data: res.toJSON())
                }
//                Utility.setHomeRoot(false,nil)
                stronSelf.checkOrderStatus()
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
//                Utility.showAlert(vc: stronSelf, message: "User login Successfully")
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func checkOrderStatus(){
        if Utility.isInternetAvailable(){
            MapService.shared.checkOrderStatus { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                   /* if res.isCardAdded == 0{
                        let vc = STORYBOARD.payment.instantiateViewController(withIdentifier: "CardListScreen") as! CardListScreen
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else*/
                    if res.isUpCommingJobExist == 1 || res.isNextHoureJobComming == 1{
                        Utility.setHomeRoot(false,nil,res)
                    }else{
                        Utility.setHomeRoot(false,nil)
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }

        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
