//
//  ProfileScreen.swift
//  iTowu
//
//  Created by iroid on 16/10/21.
//

import UIKit
import GCCountryPicker
class ProfileScreen: UIViewController {

    
    @IBOutlet weak var firstNameTextField: dateSportTextField!
    @IBOutlet weak var lastNameTextField: dateSportTextField!
    @IBOutlet weak var countryCodeTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: dateSportTextField!
    @IBOutlet weak var emailTextField: dateSportTextField!
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var faceLockDescriptionLabel: UILabel!
    @IBOutlet weak var selectedModetitleLabel: UILabel!
    @IBOutlet weak var notificationSwitch: UISwitch!
    @IBOutlet weak var faceLockSwitch: UISwitch!
    
    @IBOutlet weak var editProfileButton: dateSportButton!
    
    @IBOutlet weak var profilePickButton: UIButton!
    @IBOutlet weak var changePasswordButton: dateSportButton!
    
    @IBOutlet weak var changePasswordButtonHeight: NSLayoutConstraint!
    
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var phoneNumberButton: UIButton!
    @IBOutlet weak var darkModeButton: UIButton!
    
    var changesNotificationSwich = false
    private let biometricIDAuth = BiometricIDAuth()

    override func viewDidLoad() {
        super.viewDidLoad()
        FirebaseanalyticsManager.share.logEvent(event: .profileScreen)
        self.setData()
        self.setUpTextFieldChangeEvent()
        self.setUpUI(isEdit: false)
        self.changePasswordButton.isHidden = Utility.getUserData()?.accountType == 1

        self.changePasswordButtonHeight.constant = Utility.getUserData()?.accountType == 1 ? 0 : 61
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let mode = UserDefaults.standard.string(forKey: MODE){
            if mode == "Use device setting"{
                self.selectedModetitleLabel.text = "Use device settings"
            }else if mode == "Always on"{
                self.selectedModetitleLabel.text = "Always on"
            }else{
                self.selectedModetitleLabel.text = "Always off"
            }
        }else{
            self.selectedModetitleLabel.text = "Use device settings"
        }
        
        self.emailTextField.text = Utility.getUserData()?.email
        self.phoneNumberTextField.text = Utility.getUserData()?.phone
        self.countryCodeTextField.text = Utility.getUserData()?.countryCode

    }
    
    
    func setUpUI(isEdit:Bool){
        
        self.firstNameTextField.isUserInteractionEnabled = isEdit
        self.lastNameTextField.isUserInteractionEnabled = isEdit
        self.countryCodeTextField.isUserInteractionEnabled = isEdit
        self.phoneNumberTextField.isUserInteractionEnabled = isEdit
        self.emailTextField.isUserInteractionEnabled = isEdit
        self.notificationSwitch.isUserInteractionEnabled = isEdit
        self.profileImageView.isUserInteractionEnabled = isEdit
        self.profilePickButton.isUserInteractionEnabled = isEdit
        self.emailButton.isUserInteractionEnabled = isEdit
        self.phoneNumberButton.isUserInteractionEnabled = isEdit
        self.darkModeButton.isUserInteractionEnabled = isEdit
    }
    
    func setUpTextFieldChangeEvent(){
        self.firstNameTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.lastNameTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.emailTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.phoneNumberTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        self.countryCodeTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if let data = Utility.getUserData(){
            if data.firstName == firstNameTextField.text && data.lastName == lastNameTextField.text && data.email == emailTextField.text && data.phone == phoneNumberTextField.text && data.countryCode == countryCodeTextField.text && changesNotificationSwich == false{
                //self.editProfileButton.setTitle("EDIT PROFILE", for: .normal)
            }else{
               // self.editProfileButton.setTitle("SAVE CHANGES", for: .normal)
            }
        }
    }

    
    
    func setData(){
        if let data = Utility.getUserData(){
            self.userNameLabel.text = (data.firstName ?? "")+" "+(data.lastName ?? "")
            self.firstNameTextField.text = data.firstName
            self.lastNameTextField.text = data.lastName
            self.countryCodeTextField.text = data.countryCode
            self.phoneNumberTextField.text = data.phone
            self.emailTextField.text = data.email
            self.notificationSwitch.isOn = data.notification == true ? true : false
            Utility.setImage(data.profile, imageView: self.profileImageView)
            
            let faceLock : Bool = UserDefaults.standard.bool(forKey: FACELOCK)
            if faceLock {
                self.faceLockSwitch.isOn = true
                self.faceLockSwitch.setOn(true, animated: false)
                self.faceLockDescriptionLabel.text = ""
            }else {
                self.faceLockSwitch.setOn(false, animated: false)
                self.faceLockSwitch.isOn = false
                biometricIDAuth.canEvaluate { (canEvaluate, _, canEvaluateError) in
                    guard canEvaluate else {
                        self.faceLockSwitch.isUserInteractionEnabled = false
                        self.faceLockDescriptionLabel.text = "To use screen lock, you need to set up Face ID on your iPhone. Go to iPhone Settings and tap Face ID & passcode to get started.To use screen lock, you need to set up Face ID on your iPhone. Go to iPhone Settings and tap Face ID & passcode to get started."
                        return
                    }
                    self.faceLockSwitch.isUserInteractionEnabled = true
                    self.faceLockDescriptionLabel.text = ""
                }
            }
        }
        
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        if let vc = homeVC as? MapScreen{
            vc.observeOrder()
        }
    }
    
    @IBAction func onScreenLock(_ sender: UISwitch) {
        if !self.faceLockSwitch.isOn{
            self.setFaceLock(value: false)
        }else{
            biometricIDAuth.canEvaluate { (canEvaluate, _, canEvaluateError) in
                guard canEvaluate else {
                    self.setFaceLock(value: false)
                    alert(title: "Error",
                          message: canEvaluateError?.localizedDescription ?? "Face ID/Touch ID may not be configured",
                          okActionTitle: "Darn!")
                    return
                }
                
                biometricIDAuth.evaluate { [weak self] (success, error) in
                    guard success else {
                        self?.setFaceLock(value: false)
                        self?.alert(title: "Error",
                                    message: error?.localizedDescription ?? "Face ID/Touch ID may not be configured",
                                    okActionTitle: "Darn!")
                        return
                    }
                    self?.setFaceLock(value: true)
                }
            }
        }
    }
    
    
    func setFaceLock(value:Bool){
        self.faceLockSwitch.setOn(value, animated: false)
        self.faceLockSwitch.isOn = value
        UserDefaults.standard.setValue(value, forKey: FACELOCK)
    }
    
    @IBAction func onProfile(_ sender: UIButton) {
        self.photoSelectOption()
    }
    
    @IBAction func onDarkMode(_ sender: UIButton) {
        let vc = STORYBOARD.authentication.instantiateViewController(withIdentifier: "ModeScreen") as! ModeScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onDeleteAccount(_ sender: UIButton) {
        self.deleteAccountAlert()
    }
    
    @IBAction func onCountryCode(_ sender: UIButton) {
        let countryPickerViewController = GCCountryPickerViewController(displayMode: .withCallingCodes)
        countryPickerViewController.delegate = self
        countryPickerViewController.navigationItem.title = "Countries"
        let navigationController = UINavigationController(rootViewController: countryPickerViewController)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    @IBAction func onNotificationSwitch(_ sender: UISwitch) {
//        self.editProfileButton.setTitle("SAVE CHANGES", for: .normal)
        self.changesNotificationSwich = true
    }
    
    @IBAction func onChangePassword(_ sender: UIButton) {
        let vc = STORYBOARD.authentication.instantiateViewController(withIdentifier: "ChangePasswordScreen") as! ChangePasswordScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func onChangeEmail(_ sender: UIButton) {
        let vc = STORYBOARD.authentication.instantiateViewController(withIdentifier: "ChangeEmailScreen") as! ChangeEmailScreen
        vc.type = sender.tag
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onEditProfile(_ sender: UIButton) {
        
        if editProfileButton.titleLabel?.text == "EDIT PROFILE"{
            self.editProfileButton.setTitle("SAVE CHANGES", for: .normal)
            self.setUpUI(isEdit: true)
            return
        }else if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            self.updateProfileAPI()
        }
    }
    
    func deleteAccountAlert() {
        let alertController = UIAlertController(title: APPLICATION_NAME, message: "Are you sure you want to delete your account? All of your information and content will be deleted.", preferredStyle: UIAlertController.Style.alert)
        let resendAction = UIAlertAction(title: "Yes", style: .default, handler: { [weak self] (action) in
            self?.deleteUserApi()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            alertController.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(cancelAction)
        alertController.addAction(resendAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func deleteUserApi(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            AuthenticationService.shared.deleteUserAccount{ [weak self] (statusCode, response) in
                Utility.hideIndicator()
                //                if let res = response{
                SocketHelper.shared.disconnectSocket()
                Utility.removeUserData()
                Utility.setLoginRootScreen()
                //                }
            } failure: { (error) in
                Utility.hideIndicator()
                Utility.showAlert(vc: self, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func photoSelectOption(){
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // Create the actions
        let takePhoto = UIAlertAction(title: "Take photo", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera()
        }
        let galleryPhoto = UIAlertAction(title: "Photo library", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(takePhoto)
        alertController.addAction(galleryPhoto)
        alertController.addAction(cancelAction)
        
        //   Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func checkValidation() -> String?{
        if self.profileImageView.image == nil{
            return "Please select profile image"
        }else if self.firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter first name"
        }else if self.lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter last name"
        }else if self.countryCodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter country code"
        }else if self.phoneNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter phone number"
        }else if self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter email"
        }else if self.emailTextField.text?.isValidEmail() == false{
            return "Please enter valid email"
        }
        return nil
    }
    
    func alert(title: String, message: String, okActionTitle: String) {
        let alertView = UIAlertController(title: title,
                                          message: message,
                                          preferredStyle: .alert)
        let okAction = UIAlertAction(title: okActionTitle, style: .default)
        alertView.addAction(okAction)
        present(alertView, animated: true)
    }
}
extension ProfileScreen :UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
        self.profileImageView.image = image
        // = (image.resized()?.pngData())!
//        self.profileData = image.jpegData(compressionQuality:0.3)!
        picker.dismiss(animated: true, completion: nil)
    }
    
    func openCamera()
    {
        let imagePicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true//false
            self.present(imagePicker, animated: true, completion: nil)
            
        }
    }
    
    
    func openGallery(){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker,animated: true,completion: nil)
    }
}
//MARK:- GCCountryPicker Delegate & DataSource
extension ProfileScreen: GCCountryPickerDelegate{
    
    func countryPickerDidCancel(_ countryPicker: GCCountryPickerViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func countryPicker(_ countryPicker: GCCountryPickerViewController, didSelectCountry country: GCCountry) {
        //self.editProfileButton.setTitle("SAVE CHANGES", for: .normal)
        self.countryCodeTextField.text = country.callingCode
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- API
extension ProfileScreen{
    
    func updateProfileAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = UpdateProfileRequest(firstName: self.firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), lastName: self.lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), countryCode: self.countryCodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), phone: self.phoneNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), email: self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), notification: self.notificationSwitch.isOn ? "1" : "2")
            AuthenticationService.shared.updateProfile(parameters: data.toJSON(), imageData: self.profileImageView.image?.jpegData(compressionQuality: 0.3)) { (statusCode, response) in
                Utility.hideIndicator()
                let auth = Utility.getUserData()?.auth
                if let res = response{
                    res.auth = auth
                    self.userNameLabel.text = (res.firstName ?? "")+" "+(res.lastName ?? "")
                    Utility.saveUserData(data: res.toJSON())
                }
                
                self.editProfileButton.setTitle("EDIT PROFILE", for: .normal)
                self.setUpUI(isEdit: false)
//                self.navigationController?.popViewController(animated: true)
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
