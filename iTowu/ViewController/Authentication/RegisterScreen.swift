//
//  RegisterScreen.swift
//  iTowu
//
//  Created by iroid on 16/10/21.
//

import UIKit
import DropDown
import GCCountryPicker

class RegisterScreen: UIViewController {
    
    @IBOutlet weak var fromATextField: UITextField!
    @IBOutlet weak var fromAView: dateSportView!
    @IBOutlet weak var firstNameTextField: dateSportTextField!
    @IBOutlet weak var lasTextField: dateSportTextField!
    @IBOutlet weak var countryCodeTextField: UITextField!
    @IBOutlet weak var phoneTextField: dateSportTextField!
    @IBOutlet weak var emailTextField: dateSportTextField!
    @IBOutlet weak var passwordTextField: dateSportTextField!
    @IBOutlet weak var confirmPasswordTextField: dateSportTextField!
    @IBOutlet weak var refferalCodeTextField: dateSportTextField!
    
    @IBOutlet weak var checkBoxPrivacyImageView: UIImageView!
    @IBOutlet weak var checkBoxEndUserImageView: UIImageView!
    @IBOutlet weak var serviceAggrementTextView: UITextView!
    @IBOutlet weak var privacyPolicyTextView: UITextView!
    
    @IBOutlet weak var passwordEyeButton: UIButton!
    @IBOutlet weak var confirmPasswordEyeButton: UIButton!
    
    //    var vehicleMakeArray: [VehicleMakeResponse] = []
    var hearAboutUs = ["Search Engine","Google Ads","Facebook","Youtube","Twitter post","Instagram post/story","Other social media","Email","Radio","TV","Newspaper","Word of mouth","Tiktok","Referral","Other"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setPrivacyPolicyTxt()
        self.setServiceAggrementTextView()
        self.setupCallingCode()
    }
    
    func setPrivacyPolicyTxt(){
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        let string = "By signing up you agree to our Privacy Policy & Our Terms of Service"
        let attributedString = NSMutableAttributedString(string: string,attributes: [NSAttributedString.Key.paragraphStyle : paragraph])
        
        let termConditionRange = (string as NSString).range(of: "Terms of Service")
        self.privacyPolicyTextView.textAlignment = NSTextAlignment.left
        
        attributedString.addAttributes([NSAttributedString.Key.foregroundColor : Utility.getUIcolorfromHex(hex: "41EAD4"),NSAttributedString.Key.font : UIFont(name: "LexendDeca-Regular", size: 14)!], range: (string as NSString).range(of: string))
        
        attributedString.addAttributes([NSAttributedString.Key.link : termConditionURL,NSAttributedString.Key.foregroundColor :Utility.getUIcolorfromHex(hex: "41EAD4"),NSAttributedString.Key.font : UIFont(name: "LexendDeca-Bold", size: 14)!,NSAttributedString.Key.underlineStyle : 1,NSAttributedString.Key.underlineColor: Utility.getUIcolorfromHex(hex: "41EAD4")], range: termConditionRange)
        
        let privacyPoilicyRange = (string as NSString).range(of: "Privacy Policy")
        
        attributedString.addAttributes([NSAttributedString.Key.link : privacyPolicyURL,NSAttributedString.Key.foregroundColor :Utility.getUIcolorfromHex(hex: "41EAD4"),NSAttributedString.Key.font : UIFont(name: "LexendDeca-Bold", size: 14)!,NSAttributedString.Key.underlineStyle : 1,NSAttributedString.Key.underlineColor: Utility.getUIcolorfromHex(hex: "41EAD4")], range: privacyPoilicyRange)

        self.privacyPolicyTextView.linkTextAttributes = [:]
        self.privacyPolicyTextView.delegate = self
        self.privacyPolicyTextView.attributedText = attributedString
    }
    
    func setServiceAggrementTextView(){
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        let string = "By signing up you agree to our End user licence agreement"
        let attributedString = NSMutableAttributedString(string: string,attributes: [NSAttributedString.Key.paragraphStyle : paragraph])
        
        let termConditionRange = (string as NSString).range(of: "End user licence agreement")
        self.serviceAggrementTextView.textAlignment = NSTextAlignment.left
        
        attributedString.addAttributes([NSAttributedString.Key.foregroundColor : Utility.getUIcolorfromHex(hex: "41EAD4"),NSAttributedString.Key.font : UIFont(name: "LexendDeca-Regular", size: 14)!], range: (string as NSString).range(of: string))
        
        attributedString.addAttributes([NSAttributedString.Key.link : serviceAggrementURL,NSAttributedString.Key.foregroundColor :Utility.getUIcolorfromHex(hex: "41EAD4"),NSAttributedString.Key.font : UIFont(name: "LexendDeca-Bold", size: 14)!,NSAttributedString.Key.underlineStyle : 1,NSAttributedString.Key.underlineColor: Utility.getUIcolorfromHex(hex: "41EAD4")], range: termConditionRange)
        
        self.serviceAggrementTextView.linkTextAttributes = [:]
        self.serviceAggrementTextView.delegate = self
        self.serviceAggrementTextView.attributedText = attributedString
    }
    
    
    private func setupCallingCode() {
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String, let code = GCCountry(countryCode:countryCode)?.callingCode {
            self.countryCodeTextField.text = code
        }
    }
    
    func displayDropDown(array: [String],tag: Int,view: UIView){
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.anchorView = view
        dropDown.dataSource = array
        //        dropDown.direction = .bottom
        dropDown.show()
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            switch tag {
            case 0:
                self?.fromATextField.text = item
                //                self?.vehicleModelTextField.text = nil
                //                self?.vehicleModelArray = self?.vehicleMakeArray[index].models ?? []
                dropDown.hide()
            default:
                self?.fromATextField.text = item
                dropDown.hide()
            }
        }
    }
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onCurrentPasswordEyeHide(_ sender: Any) {
        if self.passwordTextField.isSecureTextEntry == true{
            self.passwordTextField.isSecureTextEntry = false
            self.passwordEyeButton.isSelected = true
        }else{
            self.passwordTextField.isSecureTextEntry = true
            self.passwordEyeButton.isSelected = false
        }
    }
    
    @IBAction func onConfirmPasswordEyeHide(_ sender: Any) {
        if self.confirmPasswordTextField.isSecureTextEntry == true{
            self.confirmPasswordTextField.isSecureTextEntry = false
            self.confirmPasswordEyeButton.isSelected = true
        }else{
            self.confirmPasswordTextField.isSecureTextEntry = true
            self.confirmPasswordEyeButton.isSelected = false
        }
    }
    
    @IBAction func onHear(_ sender: UIButton) {
        self.view.endEditing(true)
        self.displayDropDown(array: hearAboutUs, tag: 0, view: self.fromAView)
    }
    
    @IBAction func onCountryCode(_ sender: UIButton) {
        let countryPickerViewController = GCCountryPickerViewController(displayMode: .withCallingCodes)
        
        countryPickerViewController.delegate = self
        countryPickerViewController.navigationItem.title = "Countries"
        
        let navigationController = UINavigationController(rootViewController: countryPickerViewController)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func onSignIn(_ sender: UIButton) {
//        for controller in self.navigationController!.viewControllers as Array {
//            if controller is LoginScreen {
//                self.navigationController!.popToViewController(controller, animated: false)
//                break
//            }
//        }
//        self.navigationController?.popViewController(animated: true)
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            self.regisetrAPI()
        }
    }
    
    @IBAction func onPrivacyPolicyCheckMark(_ sender: UIButton) {
        self.view.endEditing(true)
        if sender.isSelected{
            self.checkBoxPrivacyImageView.isHidden = true
            sender.isSelected = false
        }else{
            sender.isSelected = true
            self.checkBoxPrivacyImageView.isHidden = false
        }
    }
    
    @IBAction func onEndUserCheckMark(_ sender: UIButton) {
        self.view.endEditing(true)
        if sender.isSelected{
            sender.isSelected = false
            self.checkBoxEndUserImageView.isHidden = true
        }else{
            sender.isSelected = true
            self.checkBoxEndUserImageView.isHidden = false
        }
    }
    
    func checkValidation() -> String?{
        if self.firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter first name"
        }else if self.lasTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter last name"
        }else if self.countryCodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter country code"
        }else if self.phoneTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter phone number"
        }else if self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter email"
        }else if self.emailTextField.text?.isValidEmail() == false{
            return "Please enter valid email"
        }else if self.passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter password"
        }else if self.confirmPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter confirm password"
        }else if self.passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) != self.confirmPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines){
            return "Password & confirm password does not match"
        }else if self.fromATextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please select how did you hear about us?"
        }else if self.checkBoxPrivacyImageView.isHidden == true{
            return "Please agree Privacy Policy & Term and Condition"
        }
        else if self.checkBoxEndUserImageView.isHidden == true{
            return "Please agree End user licence agreement"
        }
        return nil
    }
}
//MARK:- GCCountryPicker Delegate & DataSource
extension RegisterScreen: GCCountryPickerDelegate{
    
    func countryPickerDidCancel(_ countryPicker: GCCountryPickerViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func countryPicker(_ countryPicker: GCCountryPickerViewController, didSelectCountry country: GCCountry) {
        self.countryCodeTextField.text = country.callingCode
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- API
extension RegisterScreen{
    
    
    func regisetrAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = RegisterRequest(firstName: self.firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), lastName: self.lasTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), countryCode: self.countryCodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), phone: self.phoneTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), email: self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), password: self.passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), password_confirmation: self.confirmPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), hearReason: self.fromATextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),invite_code: self.refferalCodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))
            AuthenticationService.shared.registerUser(parameters: data.toJSON()) { (statusCode, response) in
                Utility.hideIndicator()
                
                
                if let res = response{
                    Utility.saveUserData(data: res.toJSON())
                }
                
                let parameters: [String: Any] = [
                    "email": response?.email ?? "",
                    "phoneNumber": response?.phone ?? "",
                    "referralCode": self.refferalCodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
                    "hearAbout": self.fromATextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
                ]
                
                FirebaseanalyticsManager.share.logEvent(event: .registerSuccessfully,parameters: parameters)
                
                let control = STORYBOARD.authentication.instantiateViewController(withIdentifier: "EnterVerificationCodeScreen") as! EnterVerificationCodeScreen
                control.type = 1
                control.phoneNum = self.phoneTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                control.email = self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                control.countryCode = self.countryCodeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                self.navigationController?.pushViewController(control, animated: true)
                
//                self.checkOrderStatus()
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }

        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func checkOrderStatus(){
        if Utility.isInternetAvailable(){
            MapService.shared.checkOrderStatus { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                   /* if res.isCardAdded == 0{
                        let vc = STORYBOARD.payment.instantiateViewController(withIdentifier: "CardListScreen") as! CardListScreen
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else*/
                    if res.isUpCommingJobExist == 1 || res.isNextHoureJobComming == 1{
                        Utility.setHomeRoot(false,nil,res)
                    }else{
                        Utility.setHomeRoot(false,nil)
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }

        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
//MARK:- TEXTVIEW DELEGATE
extension RegisterScreen: UITextViewDelegate{
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if (URL.absoluteString == termConditionURL) {
            let vc = STORYBOARD.webView.instantiateViewController(withIdentifier: "WebViewScreen") as! WebViewScreen
            vc.linkUrl = termConditionURL
            vc.titlaString = "TERM & CONDITIONS"
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if (URL.absoluteString == privacyPolicyURL){
            let vc = STORYBOARD.webView.instantiateViewController(withIdentifier: "WebViewScreen") as! WebViewScreen
            vc.linkUrl = privacyPolicyURL
            vc.titlaString = "PRIVACY POLICY"
            self.navigationController?.pushViewController(vc, animated: true)
        }else if (URL.absoluteString == serviceAggrementURL){
            let vc = STORYBOARD.webView.instantiateViewController(withIdentifier: "WebViewScreen") as! WebViewScreen
            vc.linkUrl = endLicenseURL
            vc.titlaString = "END USER LICENCE AGREEMENT"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        return false
    }
}
