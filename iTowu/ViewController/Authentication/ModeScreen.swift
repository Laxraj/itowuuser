//
//  ModeScreen.swift
//  iTowu
//
//  Created by iroid on 09/03/22.
//

import UIKit

class `ModeScreen`: UIViewController {

    @IBOutlet weak var modeTableView: UITableView!
    
    var modeOptionArray = ["Use device setting", "Always on", "Always off"]
    var selectedIndex = -1
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialDetail()
    }
    
    func initialDetail(){
        let nib = UINib(nibName: "ModeTableViewCell", bundle: nil)
        self.modeTableView.register(nib, forCellReuseIdentifier: "ModeTableViewCell")
        self.modeTableView.tableFooterView = UIView()
        
        if let mode = UserDefaults.standard.string(forKey: MODE){
            if mode == "Use device setting"{
                selectedIndex = 0
            }else if mode == "Always on"{
                selectedIndex = 1
            }else{
                selectedIndex = 2
            }
        }else{
            selectedIndex = 0
        }

    }
    
    @IBAction func onBack(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name("MODE_CHANGE"), object: nil)

        self.navigationController?.popViewController(animated: true)
    }


}
extension ModeScreen:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.modeOptionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ModeTableViewCell", for: indexPath) as! ModeTableViewCell
        cell.modeTitleLabel.text = modeOptionArray[indexPath.row]
        if selectedIndex == indexPath.row{
            cell.selectImageView.image = UIImage(named: "selected_icon")
        }else{
            cell.selectImageView.image = UIImage(named: "un_selected_icon")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UserDefaults.standard.set(modeOptionArray[indexPath.row], forKey: MODE) //setObject
        self.selectedIndex = indexPath.row
        if indexPath.row == 0{
            if Utility.checkLightModeUserDefalt(){
                appDelegate.window?.overrideUserInterfaceStyle = .light
            }else{
                appDelegate.window?.overrideUserInterfaceStyle = .dark
            }
            
        }else if indexPath.row == 1{
            appDelegate.window?.overrideUserInterfaceStyle = .dark
        }else{
            appDelegate.window?.overrideUserInterfaceStyle = .light
        }
        self.modeTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
