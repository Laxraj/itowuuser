//
//  NewPasswordScreen.swift
//  Source-App
//
//  Created by Nikunj on 25/03/21.
//

import UIKit

class NewPasswordScreen: UIViewController {
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    
    @IBOutlet weak var confirmPasswordEyeButton: UIButton!
    
    @IBOutlet weak var passwordEyeButton: UIButton!
    
    var forgotEmail = ""
    var phone: String?
    var countryCode: String?
    
    var requestdata:ForgotPasswordEmailRequest?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func onReset(_ sender: Any) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            self.resetPasswordAPI()
        }
    }
    
    @IBAction func onCurrentPasswordEyeHide(_ sender: Any) {
        if self.newPasswordTextField.isSecureTextEntry == true{
            self.newPasswordTextField.isSecureTextEntry = false
            self.passwordEyeButton.isSelected = true
        }else{
            self.newPasswordTextField.isSecureTextEntry = true
            self.passwordEyeButton.isSelected = false
        }
    }
    
    @IBAction func onConfirmPasswordEyeHide(_ sender: Any) {
        if self.confirmPasswordTextField.isSecureTextEntry == true{
            self.confirmPasswordTextField.isSecureTextEntry = false
            self.confirmPasswordEyeButton.isSelected = true
        }else{
            self.confirmPasswordTextField.isSecureTextEntry = true
            self.confirmPasswordEyeButton.isSelected = false
        }
    }
    
    func checkValidation() -> String?{
         if newPasswordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter password"
         }else if self.newPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 < 8{//Utility.isValidPassword(Input: newPasswordTextField.text ?? ""){
            return "Password must be alteast 8 characters"
        }else if confirmPasswordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter confirm password"
        }else if newPasswordTextField.text != confirmPasswordTextField.text{
            return "Password and confirm password don’t match"
        }
        return nil
    }
    
    func showSuccessAlert(message:String) {
        let alert = UIAlertController(title: APPLICATION_NAME  , message: message,preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
            //OK Action
            for controller in self.navigationController!.viewControllers as Array {
                  if controller is LoginScreen {
                      self.navigationController!.popToViewController(controller, animated: false)
                      break
                  }
        }
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
extension NewPasswordScreen{
    //MARK:- New Password API
    func resetPasswordAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data =  ForgotPasswordEmailRequest(email: requestdata?.email, countryCode: requestdata?.countryCode, type: requestdata?.type, phone: requestdata?.phone, otp: requestdata?.otp, password: newPasswordTextField.text, password_confirmation: confirmPasswordTextField.text)
            AuthenticationService.shared.resetPassword(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                print(response.toJSON())
                self?.showSuccessAlert(message: response.message ?? "")
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
