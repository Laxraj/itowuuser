//
//  ForgetPassswordScreen.swift
//  iTowu
//
//  Created by iroid on 16/10/21.
//

import UIKit
import GCCountryPicker

class ForgetPassswordScreen: UIViewController {
    
    @IBOutlet weak var loginOptionView: UIView!
    @IBOutlet weak var loginOptionTextField: UITextField!
    @IBOutlet weak var loginOptionViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var phoneNumberTextField: dateSportTextField!
    @IBOutlet weak var countryCodeTextField: UITextField!
    @IBOutlet weak var emailTextField: dateSportTextField!
    
    @IBOutlet weak var numberView: dateSportView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var countryCodeView: dateSportView!
    
    var type = 2
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupCallingCode()
    }
    
    private func setupCallingCode() {
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String, let code = GCCountry(countryCode:countryCode)?.callingCode {
            self.countryCodeTextField.text = code
        }
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onLoginOption(_ sender: UIButton) {
        self.loginOptionView.isHidden = false
    }
    
    @IBAction func onEmailAddress(_ sender: UIButton) {
        self.loginOptionView.isHidden = true
        self.setOptionView(isSelectEmail: true)
    }
    
    @IBAction func onPhoneNumber(_ sender: UIButton) {
        self.loginOptionView.isHidden = true
        self.setOptionView(isSelectEmail: false)
    }
    
    @IBAction func onCountryCode(_ sender: UIButton) {
        let countryPickerViewController = GCCountryPickerViewController(displayMode: .withCallingCodes)
        
        countryPickerViewController.delegate = self
        countryPickerViewController.navigationItem.title = "Countries"
        
        let navigationController = UINavigationController(rootViewController: countryPickerViewController)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
   
    
    @IBAction func onNext(_ sender: UIButton) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            self.resetPasswordAPI()
        }
    }
    func setOptionView(isSelectEmail:Bool){
        self.loginOptionTextField.text = isSelectEmail ? "EMAIL":"PHONE NUMBER"
        self.emailView.isHidden = !isSelectEmail
        self.countryCodeView.isHidden = isSelectEmail
        self.numberView.isHidden = isSelectEmail
        self.type = isSelectEmail ? 1:2
    }
    
    func checkValidation() -> String?{
        if type == 2{
            if self.phoneNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
                return "Please enter phone number"
            }else if self.phoneNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 < 8 {
                return "Please enter valid phone number"
            }
        }else{
            if self.emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
                return "Please enter email"
            }else if !emailTextField.text.isEmailValid(){
                return "Please enter valid email address"
            }
        }
        return nil
    }

    func goFurtherScreen(requestdata:ForgotPasswordEmailRequest){
            let vc = STORYBOARD.authentication.instantiateViewController(withIdentifier: "EnterCodeScreen") as! EnterCodeScreen
            vc.requestdata = requestdata
            self.navigationController?.pushViewController(vc, animated: false)
    }
}

//MARK:- GCCountryPicker Delegate & DataSource
extension ForgetPassswordScreen: GCCountryPickerDelegate{
    
    func countryPickerDidCancel(_ countryPicker: GCCountryPickerViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func countryPicker(_ countryPicker: GCCountryPickerViewController, didSelectCountry country: GCCountry) {
        self.countryCodeTextField.text = country.callingCode
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- API CALLING
extension ForgetPassswordScreen{
    //MARK:- Reset Password  API
    func resetPasswordAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = ForgotPasswordEmailRequest(email: self.emailTextField.text, countryCode: self.countryCodeTextField.text, type: type == 1 ? "email":"phone", phone: self.phoneNumberTextField.text, otp: nil,password: nil,password_confirmation: nil)
        
            AuthenticationService.shared.resetPasswordSendEmail(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                self?.goFurtherScreen(requestdata: data)
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
