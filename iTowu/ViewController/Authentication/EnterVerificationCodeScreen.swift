//
//  EnterViricationCodeScreen.swift
//  iTowu
//
//  Created by iMac on 18/01/23.
//

import UIKit

class EnterVerificationCodeScreen: UIViewController {
    
    @IBOutlet weak var confirmationCodeTextField: UITextField!

    @IBOutlet weak var verificationEmailOrPhoneLabel: UILabel!
    
    @IBOutlet weak var verificationDescriptionLabel: UILabel!
    
    @IBOutlet weak var newOTPDescriptionLabel: UILabel!
    
    @IBOutlet weak var resendCodeButton: dateSportButton!
    
    @IBOutlet weak var backButton: UIButton!
    
    var timer = Timer()
    var secondsRemaining = 60
    var type:Int? = 0
    var phoneNum:String?
    var email:String?
    var countryCode:String?
    var isFromProfileScreen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.initialDetail()
    }
    
    func initialDetail(){
        let emailAndPhoneString = email == nil ? "\(countryCode ?? "")" + "\(phoneNum ?? "")" : email
        self.verificationEmailOrPhoneLabel.text = "Verify\n" + "\(emailAndPhoneString ?? "")"
//
        self.verificationDescriptionLabel.text = isFromProfileScreen == false ? "Enter the verification code sent to your phone or email by SMS" : email == nil ? "Enter verification code sent \nto your phone by SMS": "Enter verification code sent \nto your email by SMS"
        self.timerStart()
        
        if self.isFromProfileScreen{
            self.backButton.isHidden = false
        }else{
            let emailAndPhoneString = "\(countryCode ?? "")" + "\(phoneNum ?? "")\n or \n\(email ?? "")"

            let fullString = NSMutableAttributedString(string: "Verify\n\n" + "\(emailAndPhoneString) ")

            // create our NSTextAttachment
            let image1Attachment = NSTextAttachment()
            image1Attachment.image = UIImage(named: "swipe_edit_icon")
//            image1Attachment.ori = CGPoint(x: 0, y: 2)

            // wrap the attachment in its own attributed string so we can append it
            let image1String = NSAttributedString(attachment: image1Attachment)

            // add the NSTextAttachment wrapper to our full string, then add some more text.
            fullString.append(image1String)

            // draw the result in a label
            self.verificationEmailOrPhoneLabel.attributedText = fullString
            
            self.setupLabelTap()
        }
        
    }
    
    func setupLabelTap() {
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.labelTapped(_:)))
        self.verificationEmailOrPhoneLabel.isUserInteractionEnabled = true
        self.verificationEmailOrPhoneLabel.addGestureRecognizer(labelTap)
    }
    
    @objc func labelTapped(_ gesture: UITapGestureRecognizer){
        let vc = STORYBOARD.authentication.instantiateViewController(withIdentifier: "ChangeRegisteredPhoneScreen") as! ChangeRegisteredPhoneScreen
        vc.countryCode = self.countryCode
        vc.phoneNum = self.phoneNum
        vc.email = self.email
        vc.phoneNumberChanged = { [weak self] in
            self?.phoneNum = Utility.getUserData()?.phone
            self?.countryCode = Utility.getUserData()?.countryCode
            self?.email = Utility.getUserData()?.email
            self?.initialDetail()
        }
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: false)
    }
    
    
    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onEnter(_ sender: Any) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            if self.isFromProfileScreen{
                if self.type == 1{
                    self.verifyEmailFromProfileAPI()
                }else{
                    self.verifyPhoneFromProfileAPI()
                }
            }else{
                self.checkVerificationAPI()
            }
            
        }
    }
    
    
    @IBAction func onResend(_ sender: UIButton) {
        self.resendVerificationCodeAPI()
    }
    
    func timerStart(){
        self.resendCodeButton.isHidden = true
        self.newOTPDescriptionLabel.isHidden = false
        self.timer.invalidate()
        self.secondsRemaining = 60
        self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (Timer) in
            if self.secondsRemaining > 0 {
                self.setOTPSecondText(second: self.secondsRemaining)
                self.secondsRemaining -= 1
            } else {
                self.newOTPDescriptionLabel.isHidden = true
                self.timer.invalidate()
                self.setResendOTPText()
            }
        }
    }
    
    func setResendOTPText(){
        self.secondsRemaining = 60
        self.resendCodeButton.isHidden = false
    }
    func setOTPSecondText(second: Int){
        var seconds: String = "\(second)"
        if (1...9).contains(self.secondsRemaining){
            seconds = "0"+seconds
        }
        let string = "Having trouble? Request a new OTP in 00:\(seconds)"
        self.newOTPDescriptionLabel.text = string
       
    }
    
    func checkValidation() -> String?{
         if confirmationCodeTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter code"
        }
        return nil
    }
    
    func showAlertMessage(message:String) {
        let alert = UIAlertController(title: APPLICATION_NAME, message: message,         preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                //OK Action
            for controller in self.navigationController!.viewControllers as Array{
                if controller is ProfileScreen {
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
}
extension EnterVerificationCodeScreen{
    //MARK: - Check verification API
    func checkVerificationAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data =  EmailVerificationRequest(email: self.email, otp: self.confirmationCodeTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines), phone: self.phoneNum, countryCode: self.countryCode)
            AuthenticationService.shared.verifyEmailOTP(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                print(response.toJSON())
                if let res = response.loginResponse{
                    Utility.saveUserData(data: res.toJSON())
                    if res.auth != nil{
                        appDelegate.registerForPush()
                        appDelegate.connectSocket()
                    }
                }
                self?.checkOrderStatus()
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Resend verification code API
    func resendVerificationCodeAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data =  ResendVerificationRequest(email: self.email, countryCode: self.countryCode, phone: self.phoneNum)
            AuthenticationService.shared.resendVerifyOTP(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                print(response.toJSON())
                guard let stronSelf = self else { return }
                
                Utility.showAlert(vc: stronSelf, message: response.message ?? "")
//                Utility.setHomeRoot(false,nil)
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK: - Check verification API
    func verifyEmailFromProfileAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
           
            let data = EmailVerificationRequest(email: self.email, otp: self.confirmationCodeTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines), phone: nil, countryCode: nil)
            AuthenticationService.shared.verifyEmailFromProfile(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                print(response.toJSON())
                let data = Utility.getUserData()
                data?.email = self?.email
                Utility.saveUserData(data: data?.toJSON() ?? [:])
                self?.showAlertMessage(message: response.message ?? "")
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    
    //MARK: - Check phone verification API
    func verifyPhoneFromProfileAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data =  PhoneVerificationRequest(countryCode: self.countryCode, phone: self.phoneNum, otp: self.confirmationCodeTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines))
            AuthenticationService.shared.verifyPhoneFromProfile(parameters: data.toJSON()) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                print(response.toJSON())
                let data = Utility.getUserData()
                data?.phone = self?.phoneNum
                data?.countryCode = self?.countryCode
                Utility.saveUserData(data: data?.toJSON() ?? [:])
                
                self?.showAlertMessage(message: response.message ?? "")
               
                
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func checkOrderStatus(){
        if Utility.isInternetAvailable(){
            MapService.shared.checkOrderStatus { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    if res.isCardAdded == 0{
                        Utility.setHomeRoot(false,nil,res)
                    }else if res.isUpCommingJobExist == 1 || res.isNextHoureJobComming == 1{
                        Utility.setHomeRoot(false,nil,res)
                    }else{
                        Utility.setHomeRoot(false,nil)
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }

        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    
}
