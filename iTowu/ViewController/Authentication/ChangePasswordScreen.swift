//
//  ChangePasswordScreen.swift
//  iTowu
//
//  Created by iroid on 16/10/21.
//

import UIKit

class ChangePasswordScreen: UIViewController {

    @IBOutlet weak var currantPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var reEnterPasswordTextField: UITextField!
    
    @IBOutlet weak var currentPasswordEyeButton: UIButton!
    @IBOutlet weak var newPasswordEyeButton: UIButton!
    @IBOutlet weak var confirmPasswordEyeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onCurrentPasswordEyeHide(_ sender: Any) {
        if self.currantPasswordTextField.isSecureTextEntry == true{
            self.currantPasswordTextField.isSecureTextEntry = false
            self.currentPasswordEyeButton.isSelected = true
        }else{
            self.currantPasswordTextField.isSecureTextEntry = true
            self.currentPasswordEyeButton.isSelected = false
        }
    }
    
    @IBAction func onNewPasswordEyeHide(_ sender: Any) {
        if self.newPasswordTextField.isSecureTextEntry == true{
            self.newPasswordTextField.isSecureTextEntry = false
            self.newPasswordEyeButton.isSelected = true
        }else{
            self.newPasswordTextField.isSecureTextEntry = true
            self.newPasswordEyeButton.isSelected = false
        }
    }
    
    @IBAction func onConfirmPasswordEyeHide(_ sender: Any) {
        if self.reEnterPasswordTextField.isSecureTextEntry == true{
            self.reEnterPasswordTextField.isSecureTextEntry = false
            self.confirmPasswordEyeButton.isSelected = true
        }else{
            self.reEnterPasswordTextField.isSecureTextEntry = true
            self.confirmPasswordEyeButton.isSelected = false
        }
    }
    
    
    @IBAction func onSave(_ sender: UIButton) {
        if let error = self.checkValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
            self.changePassword()
        }
    }
    
    func checkValidation() -> String?{
        if self.currantPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter current password"
        }else if self.newPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
            return "Please enter new password"
        }else if self.reEnterPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) != self.newPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines){
            return "New password & reenter password does not match"
        }
        return nil
    }
}
//MARK:- API
extension ChangePasswordScreen{
    
    func changePassword(){
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = ChangePasswordRequest(current_password: self.currantPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),password: self.newPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), passwordConfirmation: self.reEnterPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))
            AuthenticationService.shared.changePassword(parameters: data.toJSON()) { (statusCode, response) in
                Utility.hideIndicator()
                self.navigationController?.popViewController(animated: true)
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }

        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
}
