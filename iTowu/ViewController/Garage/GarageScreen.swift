//
//  GarageScreen.swift
//  iTowu
//
//  Created by iroid on 19/10/21.
//

import UIKit

class GarageScreen: UIViewController {

    @IBOutlet weak var garageVehicleTableView: UITableView!
    
    @IBOutlet weak var noDataLabel: UILabel!
    
    
    var vehicleListArray: [VehicleListResponse] = []
    weak var delegate: SelectVehicleDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialDetail()
    }
    
    func initialDetail(){
        self.garageVehicleTableView.register(UINib(nibName: "GarageVehicleCell", bundle: Bundle.main), forCellReuseIdentifier: "GarageVehicleCell")
        self.garageVehicleTableView.delegate = self
        self.getVehicleListAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.garageVehicleTableView.reloadData()
    }

    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        if let vc = self.navigationController?.viewControllers.first as? MapScreen,delegate == nil{
            vc.observeOrder()
        }
    }
    
    @IBAction func onAddNewVehicle(_ sender: Any) {
        let vc = STORYBOARD.garage.instantiateViewController(withIdentifier: "AddVehicleScreen") as! AddVehicleScreen
        vc.modalPresentationStyle = .overCurrentContext
        vc.addNewVehicle = { [weak self] in
            self?.getVehicleListAPI()
        }
        self.present(vc, animated: true, completion: nil)
    }
}
//MARK:- TABLEVIEW DELEGATE & DATASOURCE
extension GarageScreen: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.vehicleListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.garageVehicleTableView.dequeueReusableCell(withIdentifier: "GarageVehicleCell", for: indexPath) as! GarageVehicleCell
        cell.item = self.vehicleListArray[indexPath.row]
        
        return cell
    }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let del = self.delegate{
            let data = self.vehicleListArray[indexPath.row]
            let cell = self.garageVehicleTableView.cellForRow(at: indexPath) as! GarageVehicleCell
            data.img = cell.vehicleImageView.image
            del.selectVehicle(vehicle: self.vehicleListArray[indexPath.row])
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .normal, title: "") { [weak self] (action, view, completion) in
            self?.deleteVehicle(vehicleID: self?.vehicleListArray[indexPath.row].id ?? 0)
            completion(true)
        }
        let editAction =  UIContextualAction(style: .normal, title: "") { [weak self] (action, view, completion) in
            let vc = STORYBOARD.garage.instantiateViewController(withIdentifier: "AddVehicleScreen") as! AddVehicleScreen
            vc.modalPresentationStyle = .overCurrentContext
            vc.vehicleData = self?.vehicleListArray[indexPath.row]
            vc.addedVehicle = { [weak self] vehicleResponse in
                guard let self = self, let vehicleResponse = vehicleResponse else {return}
                self.vehicleListArray[indexPath.row] = vehicleResponse
                self.garageVehicleTableView.reloadRows(at: [indexPath], with: .none)
            }
            self?.present(vc, animated: true, completion: nil)
            completion(true)
        }
        editAction.image = UIImage(named: "swipe_edit_icon")
        editAction.backgroundColor = Utility.getUIcolorfromHex(hex: "2F80ED")

        deleteAction.image = UIImage(named: "swipe_delete_icon")
        deleteAction.backgroundColor = Utility.getUIcolorfromHex(hex: "EB5757")

        let deleteSwipe = UISwipeActionsConfiguration(actions: [deleteAction,editAction])

        deleteSwipe.performsFirstActionWithFullSwipe = false // This is the line which disables full swipe
        return self.vehicleListArray[indexPath.row].is_added_by_feat_admin == true ? nil : deleteSwipe
    }
}
//MARK:- API
extension GarageScreen{
    //MARK:- Vehicle List API
    func getVehicleListAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            VehicleService.shared.getGarageVehicleList{ [weak self]  (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self?.vehicleListArray = res
                }
                self?.noDataLabel.isHidden = !(self?.vehicleListArray.isEmpty ?? false)
                self?.garageVehicleTableView.reloadData()
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func deleteVehicle(vehicleID: Int){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            VehicleService.shared.deleteGarageVehicle(vehicleID: vehicleID) { [weak self] (stausCode, response) in
                Utility.hideIndicator()
                FirebaseanalyticsManager.share.logEvent(event: .deleteVehicle)
                if let index = self?.vehicleListArray.firstIndex(where: {$0.id == vehicleID}){
                    self?.vehicleListArray.remove(at: index)
                    self?.garageVehicleTableView.reloadData()
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
