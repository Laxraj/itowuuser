//
//  AddVehicleScreen.swift
//  iTowu
//
//  Created by iroid on 19/10/21.
//

import UIKit
import DropDown
import AVKit

class AddVehicleScreen: UIViewController {

    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var vehiclePlateNumberTextFiled: UITextField!
    @IBOutlet weak var vehicleStateTextField: UITextField!
    @IBOutlet weak var vehicleModelYearTextField: UITextField!
    @IBOutlet weak var vehicleMakeTextField: UITextField!
    @IBOutlet weak var vehicleModelTextField: UITextField!
    @IBOutlet weak var colorTextField: UITextField!

    @IBOutlet weak var bodyClassTextField: UITextField!
    
    @IBOutlet weak var vehicleImageView: UIImageView!
    
    @IBOutlet weak var saveButton: dateSportButton!
    
    
    var vehicleMakeArray: [VehicleMakeResponse] = []
    var vehicleYearArray: [VehicleYearRespose] = []
    var vehicleColorArray: [VehicleColorResponse] = []
    var vehicleModelArray: [Models] = []
    var bodyClassArray: [String] = ["Standard","SUV","Truck"]
    var addNewVehicle: (() -> Void)?
    
    var vehicleData: VehicleListResponse?
    var isAddManualVehicle: Bool = false
    var addedVehicle: ((VehicleListResponse?) -> Void)?
    var imageData: Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeDetails()
        // Do any additional setup after loading the view.
    }
    
    func initializeDetails(){
        self.mainView.clipsToBounds = true
        self.mainView.layer.cornerRadius = 30
        self.mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        if CacheArray.shared.vehicleMakeArray.count > 0{
            self.vehicleMakeArray = CacheArray.shared.vehicleMakeArray
        }else{
            self.getVehicleMakeAPI()
        }
        if CacheArray.shared.vehicleYearArray.count > 0{
            self.vehicleYearArray = CacheArray.shared.vehicleYearArray
        }else{
            self.getVehicleYearListAPI()
        }
        if CacheArray.shared.vehicleColorArray.count > 0{
            self.vehicleColorArray = CacheArray.shared.vehicleColorArray
        }else{
            self.getVehicleColorListAPI()
        }
        self.enableTextFields(show: false)
        self.setData()
        self.vehiclePlateNumberTextFiled.addTarget(self, action: #selector(self.changePlaterNumber), for: .editingChanged)
        
        self.mainView.setNeedsDisplay()
    }
    
    @objc func changePlaterNumber(){
        self.enableTextFields(show: false)
    }
    
    func setData(){
        if let data = self.vehicleData{
            self.vehiclePlateNumberTextFiled.text = data.plate
            self.vehicleStateTextField.text = data.state
            self.vehicleModelTextField.text = data.model
            self.vehicleMakeTextField.text = data.make
            self.vehicleModelYearTextField.text = data.year
            self.bodyClassTextField.text = data.bodyClass
            self.colorTextField.text = data.color
            Utility.setImage(data.image, imageView: self.vehicleImageView)
            self.enableTextFields(show: true,isEdit: true)
        }
    }
    
    func enableTextFields(show: Bool,isEdit: Bool = false){
//        self.vehicleTagIdTextField.superview?.isHidden = true
        [self.vehicleModelYearTextField,self.vehicleMakeTextField,self.vehicleModelTextField,self.colorTextField,bodyClassTextField].forEach({$0?.superview?.isHidden = !show})
        
        var textFieldArray = [self.vehicleModelYearTextField,self.vehicleMakeTextField,self.vehicleModelTextField,self.bodyClassTextField]
        
        if isEdit{
            textFieldArray.append(contentsOf: [self.vehiclePlateNumberTextFiled,self.vehicleStateTextField])
        }
        
        textFieldArray.forEach({$0?.superview?.alpha = 0.5})
        
        textFieldArray.forEach({$0?.superview?.isUserInteractionEnabled = false})

        self.vehicleImageView.superview?.isHidden = !show
        self.saveButton.setTitle(show ? "SAVE" : "SEARCH VEHICLE", for: .normal)
    }
    
    func enableTextFieldForAddVehicles(){
        let textFieldArray = [self.vehicleModelYearTextField,self.vehicleMakeTextField,self.vehicleModelTextField,self.colorTextField,self.bodyClassTextField]
                
        textFieldArray.forEach({$0?.superview?.isHidden = false})
        textFieldArray.forEach({$0?.superview?.isUserInteractionEnabled = true})
        textFieldArray.forEach({$0?.superview?.alpha = 1})
        self.vehicleImageView.superview?.isHidden = false
        self.saveButton.setTitle("SAVE", for: .normal)
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onVehicleModelYear(_ sender: Any) {
        var arr: [String] = []
        for i in self.vehicleYearArray{
            if let item = i.name{
                arr.append(item)
            }
        }
        self.displayDropDown(array: arr, tag: 2, view: self.vehicleModelYearTextField)
    }
    
    @IBAction func onVehicleMake(_ sender: Any) {
        var arr: [String] = []
        for i in self.vehicleMakeArray{
            if let item = i.name{
                arr.append(item)
            }
        }
        self.displayDropDown(array: arr, tag: 0, view: self.vehicleMakeTextField)
    }
    
    @IBAction func onVehicleState(_ sender: Any) {
        self.getUSStates()
    }
    
    @IBAction func onVehicleModel(_ sender: Any) {
        var arr: [String] = []
        for i in self.vehicleModelArray{
            if let item = i.name{
                arr.append(item)
            }
        }
        self.displayDropDown(array: arr, tag: 1, view: self.vehicleModelTextField)
    }
    
    
    @IBAction func onBodyClass(_ sender: UIButton) {
        self.displayDropDown(array: self.bodyClassArray, tag: 5, view: self.bodyClassTextField)

    }
    
    @IBAction func onVehicleColor(_ sender: Any) {
        var arr: [String] = []
        for i in self.vehicleColorArray{
            if let item = i.name{
                arr.append(item)
            }
        }
        self.displayDropDown(array: arr, tag: 3, view: self.colorTextField)
    }
    
    @IBAction func onSave(_ sender: Any) {
        if let error = self.checkAddVehicleValidation(){
            Utility.showAlert(vc: self, message: error)
        }else{
//            if let vehicleID = self.vehicleData?.id{
//                self.editVehicle(vehicleID: vehicleID)
//            }else{
//                self.addVehicleAPI()
//            }
            if self.saveButton.titleLabel?.text == "SEARCH VEHICLE"{
                self.isAddManualVehicle = false
                self.getGarageVehicleDetails()
            }else{
                self.garageAddVechicle()
            }
        }
    }
    
    @IBAction func onCapturePhoto(_ sender: Any) {
        self.view.endEditing(true)
        self.photoSelectOption()
    }
    
    func photoSelectOption(){
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // Create the actions
        let takePhoto = UIAlertAction(title: "Take photo", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.checkCameraPermission()
        }
        let galleryPhoto = UIAlertAction(title: "Photo library", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(takePhoto)
        alertController.addAction(galleryPhoto)
        alertController.addAction(cancelAction)
        
        //   Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func checkCameraPermission(){
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            // Already Authorized
            DispatchQueue.main.async { [weak self] in
                self?.openCamera()
            }
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { [weak self] (granted: Bool) -> Void in
                DispatchQueue.main.async { [weak self] in
                    if granted == true {
                        // User granted
                        self?.openCamera()
                    } else {
                        // User rejected
                        self?.openCameraPermissionAlert()
                    }
                }
            })
        }
    }
    
    //MARK:- CAMERA PERMISSION ALERT
    func openCameraPermissionAlert(){
        let alertController = UIAlertController(title: "\u{22}iTowu\u{22} Needs Access to Your Camera", message: "Please go to Settings and turn on the permissions", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Open Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in })
            }
        }
        let cancelAction = UIAlertAction(title: "Not Now", style: .cancel, handler: nil)
        
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func openCamera(){
        let imagePicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openGallery(){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker,animated: true,completion: nil)
    }
    
    //MARK: - NOT FOUND VEHICLE DETAILS ALERT
    func notFoundVehilceDetailsAlert(){
        let alertController = UIAlertController(title: "\u{22}iTowu\u{22}", message: "Sorry, We are unable to find your vehicle details.", preferredStyle: .alert)
        
        let addVehicleAction = UIAlertAction(title: "Add Manually", style: .default) { [weak self] (_) -> Void in
            self?.isAddManualVehicle = true
            self?.enableTextFieldForAddVehicles()
        }
        let cancelAction = UIAlertAction(title: "Not Now", style: .cancel, handler: { _ in
            self.dismiss(animated: false)
        })
        
        alertController.addAction(cancelAction)
        alertController.addAction(addVehicleAction)
        DispatchQueue.main.async { [weak self] in
            self?.present(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK:- DROP DOWN
    func displayDropDown(array: [String],tag: Int,view: UIView){
        self.view.endEditing(true)
        let dropDown = DropDown()
        dropDown.anchorView = view.superview
        dropDown.dataSource = array
        dropDown.direction = .bottom
        dropDown.width = screenWidth - 64
        dropDown.bottomOffset = CGPoint(x: 0, y: 65)
        dropDown.backgroundColor = .white
        dropDown.cornerRadius = 15
        dropDown.show()
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            switch tag {
            case 0:
                self?.vehicleMakeTextField.text = item
                self?.vehicleModelTextField.text = nil
                self?.vehicleModelArray = self?.vehicleMakeArray[index].models ?? []
                dropDown.hide()
            case 1:
                self?.vehicleModelTextField.text = item
                dropDown.hide()
            case 2:
                self?.vehicleModelYearTextField.text = item
                dropDown.hide()
            case 4:
                self?.vehicleStateTextField.text = item
                self?.changePlaterNumber()
                dropDown.hide() 
            case 5:
                self?.bodyClassTextField.text = item
                dropDown.hide()
            default:
                self?.colorTextField.text = item
                dropDown.hide()
            }
        }
    }
    
    //MARK:- VALIDATION
    func checkAddVehicleValidation() -> String?{
        if self.saveButton.titleLabel?.text == "SEARCH VEHICLE"{
            if self.vehiclePlateNumberTextFiled.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
                return "Please enter vehicle plate number"
            }else if self.vehicleStateTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
                return "Please select vehicle state"
            }
        }else{
            if self.vehiclePlateNumberTextFiled.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
                return "Please enter vehicle plate number"
            }else if self.vehicleModelYearTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
                return "Please select vehicle model year"
            }else if self.vehicleMakeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
                return "Please select vehicle make"
            }else if self.vehicleModelTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
                return "Please select vehicle model"
            }else if self.colorTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0{
                return "Please select vehicle color"
            }
//            else if self.vehicleImageView.image == nil{
//                return "Please select vehicle image"
//            }
        }
        return nil
    }
}
//MARK:- UIIMAGEPICKER DELEGATE
extension AddVehicleScreen: UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        self.vehicleImageView.image = image
        self.imageData = image.jpegData(compressionQuality: 0.3)
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: false, completion: nil)
    }
}
//MARK:- API
extension AddVehicleScreen{
    
    //MARK:- Vehicle List API
    func getVehicleMakeAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            VehicleService.shared.getVehicleMake{ [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self?.vehicleMakeArray = res
//                    self.setData()
                    CacheArray.shared.vehicleMakeArray = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Vehicle Year API
    func getVehicleYearListAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            VehicleService.shared.getVehicleYear { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self?.vehicleYearArray = res
                    CacheArray.shared.vehicleYearArray = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    //MARK:- Vehicle Color API
    func getVehicleColorListAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            VehicleService.shared.getVehicleColor{ [weak self]  (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    self?.vehicleColorArray = res
                    CacheArray.shared.vehicleColorArray = res
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func addVehicleAPI(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            self.view.endEditing(true)
            var vehicleYearID: String?
            var vehicleMakeID: String?
            var vehicleModelID: String?
            var vehicleColorID: String?
            if let data = self.vehicleYearArray.first(where: {$0.name == self.vehicleModelYearTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)}){
                vehicleYearID = "\(data.vehicle_year_id ?? 0)"
            }
            if let data = self.vehicleMakeArray.first(where: {$0.name == self.vehicleMakeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)}){
                vehicleMakeID = "\(data.vehicleMakeId ?? 0)"
            }
            
            if let data = self.vehicleModelArray.first(where: {$0.name == self.vehicleModelTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)}){
                vehicleModelID = "\(data.id ?? 0)"
            }
            
            if let data = self.vehicleColorArray.first(where: {$0.name == self.colorTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)}){
                vehicleColorID = "\(data.vehicle_color_id ?? 0)"
            }
            let data = VehicleEditRequest(plateNumber: self.vehiclePlateNumberTextFiled.text?.trimmingCharacters(in: .whitespacesAndNewlines), vehicleYearId: vehicleYearID, vehicleMakeId: vehicleMakeID, vehicleModelId: vehicleModelID, vehicleColorId: vehicleColorID)
            
            VehicleService.shared.addVehicle(parameters: data.toJSON() , vehicleID: nil, image: self.imageData, imageParamName: "image") { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                self?.dismiss(animated: false, completion: {
                    self?.addNewVehicle?()
                })
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func editVehicle(vehicleID: Int){
//        self.view.endEditing(true)
//        if Utility.isInternetAvailable(){
//            Utility.showIndicator()
//            var vehicleYearID: String?
//            var vehicleMakeID: String?
//            var vehicleModelID: String?
//            var vehicleColorID: String?
//            
//            if let data = self.vehicleYearArray.first(where: {$0.name == self.vehicleModelYearTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)}){
//                vehicleYearID = "\(data.vehicle_year_id ?? 0)"
//            }else{
//                vehicleYearID = "\(self.vehicleData?.vehicleYearId ?? 0)"
//            }
//            if let data = self.vehicleMakeArray.first(where: {$0.name == self.vehicleMakeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)}){
//                vehicleMakeID = "\(data.vehicleMakeId ?? 0)"
//            }else{
//                vehicleMakeID = "\(self.vehicleData?.vehicleMakeId ?? 0)"
//            }
//            
//            if let data = self.vehicleModelArray.first(where: {$0.name == self.vehicleModelTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)}){
//                vehicleModelID = "\(data.id ?? 0)"
//            }else{
//                vehicleModelID = "\(self.vehicleData?.vehicleModelId ?? 0)"
//            }
//            
//            if let data = self.vehicleColorArray.first(where: {$0.name == self.colorTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)}){
//                vehicleColorID = "\(data.vehicle_color_id ?? 0)"
//            }else{
//                vehicleColorID = "\(self.vehicleData?.vehicleColorId ?? 0)"
//            }
//            
//            let data = VehicleEditRequest(plateNumber: self.vehiclePlateNumberTextFiled.text?.trimmingCharacters(in: .whitespacesAndNewlines), vehicleYearId: vehicleYearID, vehicleMakeId: vehicleMakeID, vehicleModelId: vehicleModelID, vehicleColorId: vehicleColorID)
//            VehicleService.shared.editVehicle(vehicleID: vehicleID, parameters: data.toJSON(),image: self.vehicleImageView.image?.jpegData(compressionQuality: 0.3), imageParamName: "image") { [weak self] (statusCode, response) in
//                Utility.hideIndicator()
//                
//                self?.vehicleData?.vehicleId = response?.vehicleId
//                self?.vehicleData?.plateNumber = response?.plateNumber
//                self?.vehicleData?.image = response?.image
//                self?.vehicleData?.vehicleYearId = response?.vehicleYearId
//                self?.vehicleData?.vehicleYearName = response?.vehicleYearName
//                self?.vehicleData?.vehicleMakeId = response?.vehicleMakeId
//                self?.vehicleData?.vehicleMakeName = response?.vehicleMakeName
//                self?.vehicleData?.vehicleModelId = response?.vehicleModelId
//                self?.vehicleData?.vehicleModelName = response?.vehicleModelName
//                self?.vehicleData?.vehicleColorId = response?.vehicleColorId
//                self?.vehicleData?.vehicleColorName = response?.vehicleColorName
//             
//                self?.dismiss(animated: false, completion: {
//                    self?.addNewVehicle?()
//                })
//            } failure: {  [weak self] (error) in
//                guard let stronSelf = self else { return }
//                Utility.hideIndicator()
//                Utility.showAlert(vc: stronSelf, message: error)
//            }
//        }else{
//            Utility.hideIndicator()
//            Utility.showNoInternetConnectionAlertDialog(vc: self)
//        }
    }
    
    func getUSStates(){
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            AuthenticationService.shared.getUSState { [weak self] statusCode, response in
                Utility.hideIndicator()
                guard let strongSelf = self else { return }
                if let res = response{
                    strongSelf.displayDropDown(array: res.map({$0.name ?? ""}), tag: 4, view: strongSelf.vehicleStateTextField)
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func garageAddVechicle(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            
            var data = GarageAddVehicleRequest()
            
            if self.isAddManualVehicle{
                data = GarageAddVehicleRequest(state: self.vehicleStateTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), color: self.colorTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), make: self.vehicleMakeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), model: self.vehicleModelTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), year: self.vehicleModelYearTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), vehicleClass: nil, vin: nil, plate: self.vehiclePlateNumberTextFiled.text?.trimmingCharacters(in: .whitespacesAndNewlines), bodyClass: self.bodyClassTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))
            }else{
                data = GarageAddVehicleRequest(state: self.vehicleStateTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), color: self.colorTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), make: nil, model: nil, year: nil, vehicleClass: nil, vin: nil, plate: self.vehiclePlateNumberTextFiled.text?.trimmingCharacters(in: .whitespacesAndNewlines), bodyClass: nil)
            }
            
            VehicleService.shared.addUpdateVehicle(parameters: data.toJSON(),isManual: self.isAddManualVehicle, image: self.imageData) { [weak self] (statusCode, response) in
                Utility.hideIndicator()
                FirebaseanalyticsManager.share.logEvent(event: .addEditVehicle)
                self?.dismiss(animated: false, completion: {
                    self?.addNewVehicle?()
                    self?.addedVehicle?(response)
                })
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
    
    func getGarageVehicleDetails(){
        self.view.endEditing(true)
        if Utility.isInternetAvailable(){
            Utility.showIndicator()
            let data = GarageVehicleDetailRequest(state: self.vehicleStateTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),plate: self.vehiclePlateNumberTextFiled.text?.trimmingCharacters(in: .whitespacesAndNewlines))
            VehicleService.shared.getVehicleInfo(parameters: data.toJSON()) { [weak self] statusCode, response in
                Utility.hideIndicator()
                if statusCode == 203{
                    self?.notFoundVehilceDetailsAlert()
                }else{
                    if let res = response{
                        self?.enableTextFields(show: false)
                        self?.vehicleModelYearTextField.text = res.year
                        self?.vehicleMakeTextField.text = res.make
                        self?.vehicleModelTextField.text = res.model
                        self?.bodyClassTextField.text = res.bodyClass
                        self?.enableTextFields(show: true)
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }
        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
