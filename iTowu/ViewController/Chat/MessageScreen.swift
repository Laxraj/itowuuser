//
//  MessageScreen.swift
//  iTowuDriver
//
//  Created by iroid on 07/11/21.
//

import UIKit
import IQKeyboardManagerSwift

class MessageScreen: UIViewController {

    @IBOutlet weak var messageTableView: UITableView!
    @IBOutlet weak var messagePlaceHolder: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bottomViewBottomConstraint: NSLayoutConstraint!

    @IBOutlet weak var messageView: dateSportView!
    @IBOutlet weak var noDataFoundLabel: UILabel!

    
    var receiverId = 0
    var page = 1
    var metaData:Meta?
    var hasMorePages = false
    var chatDetailList:[MessageData] = []
    var filterDictionary = [String:[MessageData]]()
    var finalDictionary = [SectionMessageData]()
    var tap: UITapGestureRecognizer?
    var totalMessageArray:[MessageData] = []
    
    var refreshControl = UIRefreshControl()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialDetail()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        offSocket()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
//        isCheckCurrantNotificationScreen = false
    }
    
    func offSocket() {
        SocketHelper.Events.newMessage.off()
        SocketHelper.Events.addUser.off()
    }
    
    func initialDetail(){
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        let nib = UINib(nibName: "MessageSenderTableViewCell", bundle: nil)
        messageTableView.register(nib, forCellReuseIdentifier: "MessageSenderTableViewCell")
        
        let nibReceiver = UINib(nibName: "MessageReceiveTableViewCell", bundle: nil)
        messageTableView.register(nibReceiver, forCellReuseIdentifier: "MessageReceiveTableViewCell")
        messageTableView.register(UINib(nibName: "ChatTimeTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatTimeTableViewCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        var parameter = [String: Any]()

        parameter = ["senderId": Utility.getUserData()?.userId ?? 0,
                     "senderType":"U",
                     "isOnline":"1"
        ] as [String:Any]
      
        SocketHelper.Events.addUser.emit(params: parameter)
        socketListenMethods(parameter: parameter)
        self.getChatDetail(pageNumber: page)
    }
    
    
    //MARK:Keyboard Methods
    @objc func keyboardWillShow(notification: NSNotification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height) != nil {
            self.messageTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        self.messageTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let _:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let _:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.bottomViewBottomConstraint?.constant = 16.0
            } else {
                var bottomPadding = CGFloat()
                bottomPadding = 0
                if #available(iOS 11.0, *) {
                    let window = UIApplication.shared.keyWindow
                    _ = window?.safeAreaInsets.top
                    bottomPadding = (window?.safeAreaInsets.bottom)!
                }
                self.bottomViewBottomConstraint?.constant = 0 + ((endFrame?.size.height)! - bottomPadding)
            }
        }
    }
    
    func socketListenMethods(parameter: [String:Any]) {
        SocketHelper.Events.userAdded.listen { [weak self] (result) in
            SocketHelper.Events.getOnlineStatus.emit(params: parameter)
        }    
        SocketHelper.Events.chatDeactivated.listen { [weak self] (result) in
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: result, options: .prettyPrinted)
                let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
                if let dictFromJSON = decoded as? [String:Any] {
                    if((dictFromJSON["isDeactivate"] as! Int) == 0){
                        self!.messageTextView.resignFirstResponder()
                    }else{
                        self!.messageTextView.resignFirstResponder()
                    }
                }
            }catch{}
        }
        
       
        SocketHelper.Events.newMessage.listen { [weak self] (result) in
            print(result)
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: result, options: .prettyPrinted)
                let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
                if let dictFromJSON = decoded as? [String:Any] {
                    let  param = ["senderId":self?.receiverId ?? 0 ,
                                  "receiverId":Utility.getUserData()?.userId ?? 0,
                                  "messageId":dictFromJSON["messageId"] as? Int ?? 0] as [String:Any]
                    SocketHelper.Events.ReadMessage.emit(params: param)
                    let newMessage = MessageData(id: 0, senderId:  dictFromJSON["senderId"] as? Int, receiverId: dictFromJSON["receiverId"] as? Int, type: dictFromJSON["type"] as? Int, message: dictFromJSON["message"] as? String, createdAt: dictFromJSON["createdAt"] as? Int, seen: 1, senderType: "U", receiverType: "D", senderName: "", receiverName: "")
                    
                    self?.addNewElementIntoDictionary(model: MessageData(id: 0, senderId:  Utility.getUserData()?.userId ?? 0, receiverId: dictFromJSON["receiverId"] as? Int, type: dictFromJSON["type"] as? Int, message: dictFromJSON["message"] as? String, createdAt: dictFromJSON["createdAt"] as? Int, seen: 1, senderType: "U", receiverType: "D", senderName: "", receiverName: ""), currentKey: dictFromJSON["createdAt"] as? Int ?? 0)

                    self?.chatDetailList.append(newMessage)
                    self?.messageTableView.reloadData()
                    if self?.chatDetailList.count ?? 0 > 0{
                    self?.messageTableView.scrollToBottom(with: true)
                    }

                }
            } catch {
                print(error.localizedDescription)
            }
        }
        

    }

    @IBAction func onBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSentMessage(_ sender: UIButton) {
        if(messageTextView.text.trimmingCharacters(in: .whitespaces).isEmpty){
            Utility.showAlert(vc: self, message: "Please enter valid message.")
            return
        }
        else{
            self.sendMessage()
        }
    }
    
    // MARK: scroll method
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView == self.messageTableView && !decelerate {
            self.scrollingFinished()
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.scrollingFinished()
    }
    
    func scrollingFinished(){
        var visibleRect = CGRect()
        visibleRect.origin = self.messageTableView.contentOffset
        visibleRect.size = self.messageTableView.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = self.messageTableView.indexPathForRow(at: visiblePoint) else { return }
        if messageTableView.contentOffset.y < 50 {
            if self.hasMorePages{
                if let metaTotal = self.metaData?.total{
                    if self.totalMessageArray.count != metaTotal{
                        print("called")
                        self.hasMorePages = false
                        self.page += 1
                        self.getChatDetail(pageNumber: page)
                    }
                }
            }
        }
    }
    
    func sendMessage(){
        // uesrRole = UserDefaults.standard.object(forKey: "UserType") as? Int ?? 0
        var parameter = [String:Any]()
        parameter = ["message":messageTextView.text!.trimmingCharacters(in: .whitespacesAndNewlines),
                     "senderId":Utility.getUserData()?.userId ?? 0,
                     "senderType":"U",
                     "receiverType":"D",
                     "receiverId":receiverId,
                     "type":1] as [String:Any]
        if SocketHelper.shared.checkConnection() {
            SocketHelper.Events.sendMessage.emit(params: parameter)
            let currentTimeStamp = Date().toMillis()!
            addNewElementIntoDictionary(model: MessageData(id: 0, senderId:  Utility.getUserData()?.userId ?? 0, receiverId: receiverId, type: 1, message: messageTextView.text!.trimmingCharacters(in: .whitespacesAndNewlines), createdAt: Int(currentTimeStamp), seen: 1, senderType: "U", receiverType: "D", senderName: "", receiverName: ""), currentKey: Int(currentTimeStamp))
            self.messageTextView.text = ""
            self.messageTableView.reloadData()
            self.messageTableView.scrollToBottom(with: true)
            view.endEditing(true)
        }else{
            Utility.showAlert(vc: self, message: "Chat server is down")
        }
    }
    
    //MARK:- GET MESSAGE LIST
    func getChatDetail(pageNumber:Int){
        let url = "\(chatDetailUrl)\(receiverId)?perPage=100&page=\(pageNumber)"
        Utility.showIndicator()
//        let data = userChatDetailRequest(id: "\(recieverId)")
        MessageService.shared.getChatDetail( url: url) { (statusCode, response) in
            Utility.hideIndicator()
            self.hasMorePages = true
            self.refreshControl.endRefreshing()
            if let data = response?.userdetailData{
                self.nameLabel.text = "\(data.firstName ?? "") \(data.lastName ?? "")"
                self.receiverId = data.id ?? 0
               
            }
            
            self.totalMessageArray.append(contentsOf: (response?.messageData)!)
            
            if(self.metaData == nil){
                Utility.hideIndicator()
                self.chatDetailList = []
                self.chatDetailList = response?.messageData ?? []
                self.metaData = response?.meta
                if response?.messageData?.count ?? 0 > 0{
                self.formatttedArray(dataArray: response?.messageData?.reversed() ?? [])
                self.messageTableView.reloadData()
                    if self.chatDetailList.count > 0{
                        self.messageTableView.scrollToBottom(with: false)
                    }
                    self.noDataFoundLabel.isHidden = true
                }else{
                    self.noDataFoundLabel.isHidden = false
                }
            }else{
                
                self.chatDetailList.append(contentsOf: response?.messageData?.reversed() ?? [])
                self.formatttedArray(dataArray: self.chatDetailList)
                self.messageTableView.reloadData()
                
                
//                let reverseDataArray = response?.messageData?.reversed()
//                let datesArray = (reverseDataArray)?.compactMap { $0.createdAt } // return array of date
//                var dic = [String:[MessageData]]() // Your required result
//                datesArray?.forEach {
//                    let dateKey = Utility.UTCToLocaltimeInterval(date: $0, fromFormat: YYYY_MM_DDHHMMSS, toFormat: MM_DD_YYYY)
//                    let filterArray = (reverseDataArray)?.filter { Utility.UTCToLocaltimeInterval(date: $0.createdAt!, fromFormat: YYYY_MM_DDHHMMSS, toFormat: MM_DD_YYYY)  == dateKey }
//                    dic[Utility.UTCToLocaltimeInterval(date: $0, fromFormat: YYYY_MM_DDHHMMSS, toFormat: MM_DD_YYYY)] = filterArray
//                }
//                print(dic)
//                let componentArray = Array(dic.keys)
//                for key in componentArray{
//                    if self.filterDictionary.index(forKey: key) != nil {
//                        var lastDateDictionaryArray = dic[key]!
//                        lastDateDictionaryArray.append(contentsOf: self.filterDictionary[key]!)
//                        self.filterDictionary[key] = lastDateDictionaryArray
//                    }else{
//                        var newArray = [MessageData]()
//                        newArray.append(contentsOf: dic[key]!)
//                        self.filterDictionary[key] = newArray
//                    }
//                }
//                self.finalDictionary = []
//                let fruitsTupleArray = self.filterDictionary.sorted { $0.key.localizedCompare($1.key) == .orderedAscending  }
//                for (key, value) in fruitsTupleArray {
//
//                    var keyName = key
//                    let date = Date()
//                    let df = DateFormatter()
//                    df.dateFormat = MM_DD_YYYY
//                    let dateString = df.string(from: date)
//                    let calendar = Calendar.current
//                    let yesterday = calendar.date(byAdding: .day, value: -1, to: Date())!
//                    let yesterdayDate = df.string(from: yesterday)
//                    if dateString == keyName{
//                        keyName = "Today"
//                    }else if yesterdayDate == keyName{
//                        keyName = "Yesterday"
//                    }
////                    finalDictionary.append( SectionMessageData(headerDate: keyName, messageData: value))
//                    self.finalDictionary.append( SectionMessageData(headerDate: keyName, messageData: value))
//                }

            self.metaData = response?.meta
            self.messageTableView.reloadData()
            }
        } failure: { (error) in
            self.refreshControl.endRefreshing()
            Utility.hideIndicator()
            Utility.showAlert(vc: self, message: error)
        }
        
    }
    
//    func formatttedArray(dataArray:[MessageData]){
//        let datesArray = dataArray.compactMap { $0.createdAt }
//        if(filterDictionary.count == 0){
//            datesArray.forEach {
//                let dateKey = Utility.UTCToLocaltimeInterval(date: $0, fromFormat: YYYY_MM_DDHHMMSS, toFormat: MM_DD_YYYY)
//                print(dateKey)
//                let filterArray = dataArray.filter { Utility.UTCToLocaltimeInterval(date: $0.createdAt!, fromFormat: YYYY_MM_DDHHMMSS, toFormat: MM_DD_YYYY)  == dateKey }
//                print(filterArray)
//                filterDictionary[Utility.UTCToLocaltimeInterval(date: $0, fromFormat: YYYY_MM_DDHHMMSS, toFormat: MM_DD_YYYY)] = filterArray
//                print(filterDictionary)
//
//            }
//        }
//        let fruitsTupleArray = filterDictionary.sorted { $0.key.localizedCompare($1.key) == .orderedAscending  }
//        for (key, value) in fruitsTupleArray {
//            var keyName = key
//            let date = Date()
//            let df = DateFormatter()
//            df.dateFormat = MM_DD_YYYY
//            let dateString = df.string(from: date)
//            let calendar = Calendar.current
//            let yesterday = calendar.date(byAdding: .day, value: -1, to: Date())!
//            let yesterdayDate = df.string(from: yesterday)
//            if dateString == keyName{
//                keyName = "Today"
//            }else if yesterdayDate == keyName{
//                keyName = "Yesterday"
//            }
//            finalDictionary.append( SectionMessageData(headerDate: keyName, messageData: value))
//        }
//    }
    
    func formatttedArray(dataArray:[MessageData]){
        var sectionData: [SectionMessageData] = []
        for i in dataArray{
            let temp = dataArray.filter({Utility.compareDate(fromDate: $0.createdAt ?? 0, toDate: i.createdAt ?? 0) == true})
            for i in temp{
                if let index = sectionData.firstIndex(where: {Utility.compareDate(fromDate: $0.headerInterVal ?? 0, toDate: i.createdAt ?? 0) == true}){
                    if sectionData[index].messageData?.contains(where: {$0.id != i.id}) == true{
                        sectionData[index].messageData?.append(i)
                    }
                }else{
                    let dateKey = Utility.UTCToLocaltimeInterval(date: i.createdAt ?? 0, fromFormat: YYYY_MM_DDHHMMSS, toFormat: MM_DD_YYYY)
                    sectionData.append(SectionMessageData(headerInterVal: i.createdAt, headerDate: dateKey, messageData: temp))
                }
            }
        }
        sectionData.sort(by: {Utility.getDateTimeFromTimeInterVel(from: $0.headerInterVal ?? 0) < Utility.getDateTimeFromTimeInterVel(from: $1.headerInterVal ?? 0)})
        self.finalDictionary = sectionData
        print(sectionData)
    }
    
    func addNewElementIntoDictionary(model:MessageData,currentKey: Int){
//        var index = finalDictionary.count
//        if(index == 0){
//            index = 0
//        }else{
//            index = index - 1
//        }
//        if(finalDictionary.count == 0){
//            var newArray = [MessageData]()
//            newArray.append(model)
//            finalDictionary.append(SectionMessageData(headerDate: currentKey, messageData: newArray))
//        }else{
//            let object = finalDictionary[index]
//            if (object.headerDate == currentKey){
//                object.messageData?.append(model)
//            finalDictionary[index] = object
//        }else{
//            var newArray = [MessageData]()
//            newArray.append(model)
//            finalDictionary.append(SectionMessageData(headerDate: currentKey, messageData: newArray))
//        }
//        }
        
        if let index = self.finalDictionary.firstIndex(where: {Utility.compareDate(fromDate: $0.headerInterVal ?? 0, toDate: currentKey) == true}){
            self.finalDictionary[index].messageData?.append(model)
        }else{
            let dateKey = Utility.UTCToLocaltimeInterval(date: currentKey, fromFormat: YYYY_MM_DDHHMMSS, toFormat: MM_DD_YYYY)
            finalDictionary.append(SectionMessageData(headerInterVal: currentKey, headerDate: dateKey, messageData: [model]))
        }
    }
}



//MARK:- TEXTVIEW DELEGATE
extension MessageScreen: UITextViewDelegate{
       
    func textViewDidChange(_ textView: UITextView) {
        self.messagePlaceHolder.isHidden = self.messageTextView.text.trimmingCharacters(in: .whitespacesAndNewlines).count > 0
    }
}
extension MessageScreen:UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return finalDictionary.count
    }
    
//
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "ChatTimeTableViewCell") as! ChatTimeTableViewCell
        header.timeLabel.text = Utility.getLocalDateFromUTCForMessageHeader(timeInterval: finalDictionary[section].headerInterVal ?? 0)

//        header.timeLabel.text = self.tableView(tableView, titleForHeaderInSection: section)
        return header.contentView
    }
    
    func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String? {
       let dateString = finalDictionary[section]
        return (dateString.headerDate)
   }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let dateString = finalDictionary[section]
        return dateString.messageData?.count ?? 0
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let mainArray = finalDictionary[indexPath.section]
        if let model = mainArray.messageData?[indexPath.row]{
            if model.receiverId == receiverId{
                if model.type == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessageSenderTableViewCell", for: indexPath) as! MessageSenderTableViewCell
                cell.setData(data: model)
                return cell
                }
            }else{
                if model.type == 1{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "MessageReceiveTableViewCell", for: indexPath) as! MessageReceiveTableViewCell
                    cell.setData(data: model)
                    return cell
                }
                
            }
        }
        return UITableViewCell()
    }
}
extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970)
    }
}
