//
//  ViewController.swift
//  iTowu
//
//  Created by Nikunj on 14/10/21.
//

import UIKit
import FBSDKCoreKit

import FirebaseAnalytics

class ViewController: UIViewController {
    
    var timer = Timer()
    private let biometricIDAuth = BiometricIDAuth()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
//        let vc = STORYBOARD.subscription.instantiateViewController(withIdentifier: "SubscriptionScreen") as! SubscriptionScreen
//        self.navigationController?.pushViewController(vc, animated: true)
//        return
        
        self.setUpScreenMode()
        self.checkVersion()
    }

    func setUpScreenMode(){
        if let mode = UserDefaults.standard.string(forKey: MODE){
            if mode == "Use device setting"{
                if Utility.checkLightModeUserDefalt(){
                    appDelegate.window?.overrideUserInterfaceStyle = .light
                }else{
                    appDelegate.window?.overrideUserInterfaceStyle = .dark
                }
                
            }else if mode == "Always on"{
                appDelegate.window?.overrideUserInterfaceStyle = .dark
            }else{
                appDelegate.window?.overrideUserInterfaceStyle = .light
            }
        }else{
            if Utility.checkLightModeUserDefalt(){
                appDelegate.window?.overrideUserInterfaceStyle = .light
            }else{
                appDelegate.window?.overrideUserInterfaceStyle = .dark
            }
        }
    }
    func setUpFaceLock(){
        let faceLock : Bool = UserDefaults.standard.bool(forKey: FACELOCK)
        if faceLock {
            biometricIDAuth.canEvaluate { (canEvaluate, _, canEvaluateError) in
                guard canEvaluate else {
                    alert(title: "Error",
                          message: canEvaluateError?.localizedDescription ?? "Face ID/Touch ID may not be configured",
                          okActionTitle: "Darn!")
                    return
                }
                
                biometricIDAuth.evaluate { [weak self] (success, error) in
                    guard success else {
                        self?.alert(title: "Error",
                                    message: error?.localizedDescription ?? "Face ID/Touch ID may not be configured",
                                    okActionTitle: "Darn!")
                        return
                    }
                    self?.setOpenScreen()
                }
            }
        }else {
            self.setOpenScreen()
        }
    }
    
    func setOpenScreen(){
        FirebaseRealtimeDBHelper.shared.authUser {
            if Utility.getUserData() != nil{
                //            self.getService()
                if Utility.getUserData()?.verifiedAt != nil{
                    self.checkOrderStatus()
                }else{
                    self.openOTPScreen()
                }
            }else{
                self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.initializedDetails), userInfo: nil, repeats: false)
            }
        }
    }
    
    func openOTPScreen(){
        let control = STORYBOARD.authentication.instantiateViewController(withIdentifier: "EnterVerificationCodeScreen") as! EnterVerificationCodeScreen
        control.type = 2
        control.phoneNum = Utility.getUserData()?.phone
        control.email = Utility.getUserData()?.email
        control.countryCode = Utility.getUserData()?.countryCode
        self.navigationController?.pushViewController(control, animated: true)
    }
    
    @objc func initializedDetails(){
        timer.invalidate()
        if Utility.getUserData() != nil{
            if Utility.getUserData()?.verifiedAt != nil{
                Utility.setHomeRoot(false,nil,nil)
            }else{
                self.openOTPScreen()
            }
            //            Utility.setHomeRoot(false,nil,nil)
        }else{
            let vc = STORYBOARD.authentication.instantiateViewController(withIdentifier: "MainScreen") as! MainScreen
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func alert(title: String, message: String, okActionTitle: String) {
        let alertView = UIAlertController(title: title,
                                          message: message,
                                          preferredStyle: .alert)
        let okAction = UIAlertAction(title: okActionTitle, style: .default)
        alertView.addAction(okAction)
        present(alertView, animated: true)
    }
    
    func forceAppUpdateVersionAlert(message:String){
        let alert = UIAlertController(title: "Update Available", message: message, preferredStyle: UIAlertController.Style.alert)
        
        
        alert.addAction(UIAlertAction(title: "Update",style: UIAlertAction.Style.destructive,handler: { [weak self] (action) in
            //Sign out action

            if let url = URL(string: appStoreURL), !url.absoluteString.isEmpty {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            
            // or outside scope use this
            guard let url = URL(string: appStoreURL), !url.absoluteString.isEmpty else {
                return
            }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func updateAlertController(message: String) {
        let alert = UIAlertController(title: "Update Available", message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { [weak self] (action) in
//            self?.setUpFaceLock()
            self?.setOpenScreen()
        }))
        
        alert.addAction(UIAlertAction(title: "Update",style: UIAlertAction.Style.destructive,handler: { [weak self] (action) in
            //Sign out action
            
            
            if let url = URL(string: appStoreURL), !url.absoluteString.isEmpty {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            
            // or outside scope use this
            guard let url = URL(string: appStoreURL), !url.absoluteString.isEmpty else {
                return
            }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}

//MARK:- API
extension ViewController{
    func checkOrderStatus(){
        if Utility.isInternetAvailable(){
            MapService.shared.checkOrderStatus { (statusCode, response) in
                Utility.hideIndicator()
                if let res = response{
                    if res.isCardAdded == 0{
                        Utility.setHomeRoot(false,nil,res)
                    }else if res.isUpCommingJobExist == 1 || res.isNextHoureJobComming == 1{
                        Utility.setHomeRoot(false,nil,res)
                    }else{
                        Utility.setHomeRoot(false,nil)
                    }
                }
            } failure: { [weak self] (error) in
                guard let stronSelf = self else { return }
                Utility.hideIndicator()
                Utility.showAlert(vc: stronSelf, message: error)
            }

        }else{
            Utility.hideIndicator()
            Utility.showNoInternetConnectionAlertDialog(vc: self)
        }
    }
}
//MARK: CHECK VERSION API
extension ViewController {
    
    func checkVersion(){
        let data = CheckVersionRequest(currentVersion: Bundle.main.buildVersion, deviceType: "iOS", appType: "user")
        AuthenticationService.shared.checkVersion(parameters: data.toJSON()) { [weak self] (statusCode, response) in
            if let res = response?.status{
                switch res {
                case 0:
//                    self?.setUpFaceLock()
                    if Utility.getUserData() != nil{
                        self?.checkSubscription()
                    }else{
                        self?.setOpenScreen()
                    }
                case 1:
                    self?.forceAppUpdateVersionAlert(message: response?.message ?? "")
                    break
                default:
                    self?.updateAlertController(message: response?.message ?? "")
                    break
                }
            }
        } failure: { [weak self] (error) in
//            self?.setUpFaceLock()
            self?.setOpenScreen()
            print(error)
        }
    }
    
    func checkSubscription(){
        let data = CheckSubscriptionRequest(deviceType: "iOS")
        SubscriptionService.shared.checkSubscription(parameter: data.toJSON()) { [weak self] (statusCode, response) in     if let res = response?.subscriptionResponseData {
                let userData = Utility.getUserData()
                userData?.subscription = res
                Utility.saveUserData(data: userData?.toJSON() ?? [:])
                self?.setOpenScreen()
            }
        } failure: { [weak self] (error) in
//            self?.setUpFaceLock()
            self?.setOpenScreen()
            print(error)
        }
    }
    
}
